<?php 

class TFBase {
        
    public function __construct()
    {
        $key = json_decode(env('API_KEY'));
        $this->apiKey = $key->api_key;
        $this->apiSecret = $key->api_secret;
        $this->url = env('API_URL');
    }
    
    protected function post($resource, $args)
    {
        $args['api_key'] = $this->apiKey;
        $args['api_secret'] = $this->apiSecret; 
        $args['client_token'] = $this->getClientToken();
        $args['server_token'] = $this->getServerToken();
        $args['user_id'] = $this->getUserId();
        
        $res = wp_remote_post( $this->url . $resource, array( 'method' => 'POST', 'body' => $args));
        //dd($res); 
        $response = json_decode($res['body'], true);
        
        if($response && $response['status'] == 'error' || $res['response']['code'] != '200') {
            
            throw new Exception( $response['msg']);
        }

        else if( ! $res ) {
            throw new Exception('Error posting request to TF api.');
        }
        
        return $response;
    }
    
    protected function get($resource, $args)
    {
        $args['api_key'] = $this->apiKey;
        $args['api_secret'] = $this->apiSecret;
        $args['client_token'] = $this->getClientToken();
        $args['server_token'] = $this->getServerToken();
        $args['user_id'] = $this->getUserId();
        
        $res = wp_remote_get( $this->url . $resource, array( 'method' => 'GET', 'body' => $args));
        //dd($res);
        $response = json_decode($res['body'], true);        
        
        if($response && $response['status'] == 'error') {
            throw new Exception($response['msg']);
        }

        else if( ! $res || $res['response']['code'] != '200') {
            throw new Exception('Error getting response from TF api.');
        }
        
        return $response;        
    }
    
    protected function put()
    {
        
    }
    
    protected function destroy()
    {
        
    }
    
    protected function getClientToken()
    {
        return isset($_SESSION['client_token']) ? $_SESSION['client_token'] : session_id();
    }
    
    protected function getServerToken()
    {
        return isset($_SESSION['server_token']) ? $_SESSION['server_token'] : null;
    }
    
    public function getUserId()
    {
        return isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
    }
}