<?php 

class TFAuth extends TFBase {
    
    public function login($username = null, $password = null) 
    {        
        try {
            $res = $this->post('auth/login', ['email' => $username, 'password' => $password, 'client_token' => session_id()]);
            
            if( ! $res || ! isset($res['status']) || ! $res['status']) {
                throw new Exception('Invalid email or password.');
            }
            
            $_SESSION['client_token'] = $this->getClientToken();
            $_SESSION['server_token'] = $res['data']['server_token'];
            $_SESSION['user_id'] = $res['data']['user_id'];
            $_SESSION['user_name'] = $res['data']['user_name'];
            $_SESSION['user_email'] = $res['data']['user_email'];
            $_SESSION['user_roles'] = $res['data']['user_roles'];
        }
        
        catch(Exception $e) {
            throw $e;
        }
    }
    
    public function initResetPassword($username = null)
    {
        try {
            $res = $this->post('auth/init-reset-password', ['email' => $username]);
    
            if( ! $res || ! isset($res['status']) || ! $res['status']) {
                throw new Exception('Invalid email.');
            }
            
            return $res;
        }
    
        catch(Exception $e) {
            throw $e;
        }
    }
    
    public function resetPassword($token = null, $username = null, $password = null)
    {
        try {
            $res = $this->post('auth/reset-password', ['email' => $username, 'token' => $token, 'password' => $password]);
    
            if( ! $res || ! isset($res['status']) || ! $res['status']) {
                throw new Exception('Invalid token.');
            }
        }
    
        catch(Exception $e) {
            throw $e;
        }
    }
    
    public static function logout()
    {
        wp_logout();
        wp_set_current_user(0);
        
        if( $_SESSION) {
            foreach($_SESSION as $k => $v) {
                unset($_SESSION[$k]);
            }
        }
        
        session_destroy();
    }
    
    public function getLoggedInUser()
    {
        if( ! $this->getUserId()) {
            return false;
        }
        
        return [
            'id' => $_SESSION['user_id'],
            'name' => $_SESSION['user_name'],
            'email' => $_SESSION['user_email'],
            'roles' => $_SESSION['user_roles'],
        ];
    }
    
    public function userHasRole($role) 
    {
        $user = $this->getLoggedInUser();

        if( $user && isset($user['roles']) && in_array($role, $user['roles'])) return true;
        
        return false;
    }
}