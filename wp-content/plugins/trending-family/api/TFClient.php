<?php 

class TFClient extends TFBase {
    
    public function find($id) {
        
        try {
            $res = $this->get('clients/' . $id);            
            return $res;
        }
        
        catch(Exception $e) {
            throw $e;
        }
    }
    
    public function save($data)
    {  
        try {
            return $this->post('clients', $data);
        }
        
        catch(Exception $e) {
            throw $e;
        }
    }
}