<?php 

class TFInfluencer extends TFBase {
    
    public function find($id) {
        
        try {
            $res = $this->get('influencers/' . $id);            
            return $res;
        }
        
        catch(Exception $e) {
            throw $e;
        }
    }
    
    public function save($data)
    {  
        try {
            return $this->post('influencers', $data);
        }
        
        catch(Exception $e) {
            throw $e;
        }
    }
}