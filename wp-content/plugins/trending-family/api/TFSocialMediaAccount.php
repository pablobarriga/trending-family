<?php 

class TFSocialMediaAccount extends TFBase {
    
    /**
     * Connect accounts
     * 
     * @param array $data
     * @throws Exception
     */
    public function connectAccounts($data)
    {  
        try {
            return $this->post('social-media-accounts/connect', ['accounts' => $data]);
        }
        
        catch(Exception $e) {
            throw $e;
        }
    }
    
    /**
     * Connect accounts
     * 
     * @param array $data
     * @throws Exception
     */
    public function disconnectAccounts($data)
    {  
        try {
            return $this->post('social-media-accounts/disconnect', ['accounts' => $data]);
        }
        
        catch(Exception $e) {
            throw $e;
        }
    }
}