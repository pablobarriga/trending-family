<?php
session_start();
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              elegrit.com
 * @since             1.0.0
 * @package           Trending_Family
 *
 * @wordpress-plugin
 * Plugin Name:       Trending Family
 * Plugin URI:        n/a
 * Description:       Trending Family API
 * Version:           1.0.0
 * Author:            Zbigniew Jasek
 * Author URI:        elegrit.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       trending-family
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-trending-family-activator.php
 */
function activate_trending_family() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-trending-family-activator.php';
	Trending_Family_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-trending-family-deactivator.php
 */
function deactivate_trending_family() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-trending-family-deactivator.php';
	Trending_Family_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_trending_family' );
register_deactivation_hook( __FILE__, 'deactivate_trending_family' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-trending-family.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_trending_family() {

	$plugin = new Trending_Family();
	$plugin->run();

}
run_trending_family();

function tfRoundNumber($n = 0)
{
    // We're using floor() instead of round() for consistency.  Since 'total reach' is calculated 
    // seperately from the individual accounts, the numbers don't always add up with round(), causing
    // confusion.
    if ($n < 10000) {
        // Anything less than 10000
        $n_format = number_format($n);
    }
    else if ($n < 1000000) {
        // Anything less than a million
        $rounded = floor(number_format($n/1000, 1));
        $n_format = ($rounded ? $rounded : number_format($n/1000, 1)) . 'K';
    } else if ($n < 1000000000) {
        // Anything less than a billion
        $rounded = floor(number_format($n/1000, 1));
        $n_format = ($rounded ? $rounded : number_format($n/1000000, 1)) . 'M';
    } else {
        // At least a billion
        $rounded = floor(number_format($n/1000, 1));
        $n_format = ($rounded ? $rounded : number_format($n/1000000000, 1)) . 'B';
    }
    return $n_format;
}

function env($searchKey) {
    
    $found = '';
    $contents = file_get_contents(__DIR__ . '/.env');
    $lines = explode("\n", $contents);
    foreach($lines as $line) {
        list($key, $value) = explode('=', $line);
        
        $key = trim($key);
        $value = trim($value);
        
        if( $key == $searchKey) {
            $found = $value;
            break;
        }
    }
    
    return $found;
}
