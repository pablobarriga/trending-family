<?php

require_once __DIR__ . '/../api/init.php';
require_once __DIR__ . '/../vendor/autoload.php';

/**
 * The public-facing functionality of the plugin.
 *
 * @link       elegrit.com
 * @since      1.0.0
 *
 * @package    Trending_Family
 * @subpackage Trending_Family/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Trending_Family
 * @subpackage Trending_Family/public
 * @author     Zbigniew Jasek <zig@elegrit.com>
 */
class Trending_Family_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	
	private $influencerPage = '/new-influencer-page';
	
	private $clientPage = '/new-client-page';
	
	private $forgotPasswordPage = '/forgot-password';
	
	private $resetPasswordPage = '/reset-password';

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		add_action('wp_head', array($this, 'tf_inline_js'));
		add_action('wp_ajax_nopriv_tf_api', array($this, 'tf_api'));
        add_shortcode( 'tf_page_forgot_password', array($this, 'tf_page_forgot_password') );
        add_shortcode( 'tf_page_reset_password', array($this, 'tf_page_reset_password') );
        add_shortcode( 'tf_page_influencer', array($this, 'tf_page_influencer') );
        add_shortcode( 'tf_page_client', array($this, 'tf_page_client') );
        add_shortcode( 'tf_oauth_callback_handler', array($this, 'tf_oauth_callback_handler') );
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Trending_Family_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Trending_Family_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/trending-family-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Trending_Family_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Trending_Family_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
	    wp_enqueue_script( $this->plugin_name . '-trending-family-public-js', plugin_dir_url( __FILE__ ) . 'js/trending-family-public.js', array( 'jquery' ), $this->version, false );
	    wp_enqueue_script( $this->plugin_name . '-trending-family-html-2-canvas-js', plugin_dir_url( __FILE__ ) . 'js/html-2-canvas/html-2-canvas.min.js', array( 'jquery' ), $this->version, false );
	    wp_enqueue_script( $this->plugin_name . '-trending-family-crs-js', plugin_dir_url( __FILE__ ) . 'js/country-region-selector-master/dist/crs.min.js', null, $this->version, false );
	    

	}
	
	/**
	 * Main api entry point
	 */
	public function tf_api()
	{	    
	    $method = 'tf_' . ( isset($_REQUEST['api_action'])  && $_REQUEST['api_action'] != 'api' ? $_REQUEST['api_action'] : 'login' );
	  
	    if( ! method_exists($this, $method)) {
	        die('Invalid method call.');
	    }
	    
	    $this->{$method}();
	}
	
	public function tf_inline_js()
	{
	?>
        <script>
        var TFclientPage = '<?php echo $this->clientPage; ?>';
        var TFinfluencerPage = '<?php echo $this->influencerPage; ?>';
        </script>
	<?php 
	}
	
	/****************************************
	 *        DISPLAY                       *
	 ****************************************/
	/**
	 * Display 'forgot password' page
	 * 
	 * @param array $args
	 */
	public function tf_page_forgot_password($args)
	{
	    try {
	        if($_SERVER['REQUEST_URI'] == '/wp-admin/post.php') return true;

	        if( $_POST) {
	            $auth = new TFAuth();
	            $res = $auth->initResetPassword($_POST['email']);
	            $token = $res['data']['token'];
	            
	            ob_start();
	            include __DIR__ . '/themes/trending-family/templates/email/password-reset.php';
	            $tpl = ob_get_clean();
	            wp_mail($_POST['email'], 'Trending Family password reset request.', $tpl);
	            wp_redirect( $this->forgotPasswordPage . '?reset=1' );
	        }
    
    	    include __DIR__ . '/themes/trending-family/templates/auth/forgot-password.php';
	    }
	    
	    catch (Exception $e) {
	        wp_redirect( $this->forgotPasswordPage . '?reset=2' );
	    }
	}
	/**
	 * Display 'reset password' page and handle reset request
	 * 
	 * @param array $args
	 */
	public function tf_page_reset_password($args)
	{	    
	    try {
	        if($_SERVER['REQUEST_URI'] == '/wp-admin/post.php') return true;
	        
	        if( ! $_REQUEST['t']) {
	            wp_redirect( $this->forgotPasswordPage );
	        }
	        
	        if( $_POST['pass'] != $_POST['pass2']) {
	            throw new Exception('Passwords do not match.');
	        }

	        if( $_POST) {
	            $auth = new TFAuth();
	            $auth->resetPassword($_POST['token'], $_POST['email'], $_POST['pass']);
	            wp_redirect( $this->resetPasswordPage . '?reset=1&t=' . $_REQUEST['t'] );
	        }
    
    	    include __DIR__ . '/themes/trending-family/templates/auth/reset-password.php';
	    }
	    
	    catch (Exception $e) {
	        wp_redirect( $this->resetPasswordPage . '?reset=2&t=' . $_REQUEST['t'] );
	    }
	}
	
	/**
	 * Display influencer social media listing
	 * 
	 * @param array $args
	 */
	public function tf_page_influencer($args)
	{
	    try {
	        if($_SERVER['REQUEST_URI'] == '/wp-admin/post.php') return true;
	        $auth = new TFAuth();
	        if( ! $auth->userHasRole('influencer')) {
	           wp_redirect( '/?err=accessDenied' );
	        }
	        
            $account = new TFSocialMediaAccount();
    	    $influencer = new TFInfluencer();
    	    $data = $influencer->find($influencer->getUserId());
    	    $influencer =  $data['data']['influencer'];
    	    $accounts =  $data['data']['socialData'];
    	    $platforms = $data['data']['platforms'];
            $influencer['name'] = $influencer['first_name'] . ' ' . $influencer['last_name'];
    	    
    	    foreach($platforms as $key => $platform) {
    	        if(array_key_exists($platform['name'], $accounts)) {
    	            $platforms[$key]['accounts'] = $accounts[$platform['name']];
    	            $platforms[$key]['authUrl'] = $accounts[$platform['name']]['account']['authUrl'];
    	        }
    	        
    	        else {
    	            $platforms[$key]['accounts'] = null;
    	        }
    	    }
    
    	    include __DIR__ . '/themes/trending-family/templates/influencer/main.php';
	    }
	    
	    catch (Exception $e) {
	        TFAuth::logout();
	        wp_redirect( '/?err=' . $e->getMessage() );
	    }
	}
	

	/**
	 * Display client page
	 *
	 * @param array $args
	 */
	public function tf_page_client($args)
	{
	    try {
	      
	        if($_SERVER['REQUEST_URI'] == '/wp-admin/post.php') return true;
	        $auth = new TFAuth();
	        if( ! $auth->userHasRole('client')) {
	           wp_redirect( '/?err=accessDenied' );
	        }
	        
	        // defines which laravel_admin_db.social_media_data fields should be displayed 
	        // and maps the field name to a user friendly display name
	        // i.e - $dataMap['facebook']['followers'] = 'Followers'
	        // will display laravel_admin_db.social_media_data.field `followers` for the Facebook platform
	        // as 'Followers: 123'
	        $dataMap = [
	           'facebook' => [
	               'followers' => 'Followers',
	           ],
	           'twitter' => [
	               'followers' => 'Followers',
	           ],
	            'youtube' => [
	               'subscribers_total' => 'Subscribers',
	               'videos_total' => 'Total Videos',
	           ],
	            'pinterest',
	            'instagram',
	            'snapchat' => [
	               'avg_snaps' => 'Average Snaps',
	             ],
	            'blog' => [
	               'monthly_pageviews' => 'Page Views/Mo', 
	           ],
	        ];


	        $client = new TFClient();
	        $data = $client->find($client->getUserId());
	         
	        // we're only supporting one campaign per client at this time
	        // so we're fetching the first one available
	        //$campaign = $data['data']['client']['campaigns'] ? $data['data']['client']['campaigns'][0] : [];
	        //dd($data);
	        
	        //include __DIR__ . '/themes/trending-family/templates/client/main.php';
	        
	        
	        $client = new TFClient();
	        $data = $client->find($client->getUserId());  
	        $campaigns = $data['data']['client']['campaigns'];    

	        if( ! isset($_GET['cpgn']) || ! $_GET['cpgn']) {
	            include __DIR__ . '/themes/trending-family/templates/client/list.php';
	        }
	        
	        else {
	            $campaign = null;
	            foreach($campaigns as $cpgn) {
	                if( $cpgn['id'] == $_GET['cpgn']) {
	                    $campaign = $cpgn;
	                    break;
	                }
	            }
	            
	            if( ! $campaign) {
	               wp_redirect( $this->clientPage . '?err=campaign+not+found' );
	            }
	            include __DIR__ . '/themes/trending-family/templates/client/main.php';
	        }
	    }
	     
	    catch (Exception $e) { 
	        TFAuth::logout();
	        wp_redirect( '/?err=' . $e->getMessage() );
	    }
	}

    /**
     * Send data back to browser
     * 
     * @param string $msg
     * @param array $data
     */
	public function successResponse($msg = 'Request successfully processed.', $data = [])
	{
	    $res = [
	        'status' => 1,
	        'msg' => $msg,
	        'data' => $data,
	    ];
	
	    die(json_encode($res));
	}

    /**
     * Send data back to browser
     * 
     * @param string $msg
     * @param array $data
     */
	public function errorResponse($msg = 'Request could not be processed at this time.', $data = [])
	{
	    $res = [
	        'status' => 0,
	        'msg' => $msg,
	        'data' => $data,
	    ];
	
	    die(json_encode($res));
	}
	
	/****************************************
	 *        POST/GET ACTIONS              *
	 ****************************************/


	/**
	 * Submit new campaign request
	 */
	public function tf_start_new_campaign()
	{
	    try {
	         
	        $auth = new TFAuth();
	        $user = $auth->getLoggedInUser();
	
	        $to = get_option('admin_email');
            $subject = 'New campaign request from trendingfamily.com';
            $body = "The following user has requested a new campaign:<br><br>Name:{$user['name']}<br>Email:{$user['email']}<br>Requested campaign name:{$_POST['campaign-name']}\r\n";
            $headers = array('Content-Type: text/html; charset=UTF-8');
             
            //wp_mail( $to, $subject, $body, $headers );
            
	        $this->successResponse();
	    }
	     
	    catch (Exception $e) {
	        $this->errorResponse($e->getMessage());
	    }
	}

	/**
	 * Export pdf/csv
	 */
	public function tf_export_charts()
	{
	    $auth = new TFAuth();
	       
	    $user = $auth->getLoggedInUser();
	    

	    foreach(json_decode(str_replace('\\', '', $_POST['charts'])) as $key => $chart) {
	        $chartStr = str_replace(' ', '+', $chart->img);
	        $fileName = $user['id'] . '~' . $chart->platform . '~' . $chart->title . '~' . $chart->account;
	        $fileName = base64_encode($fileName);
	        $h = fopen(__DIR__ . '/tmp/_' . $fileName, 'w');
	        fwrite($h, $chartStr);
	        fclose($h);
	    }
	}

	/**
	 * Export pdf/csv
	 */
	public function tf_export()
	{
	    try {
// 	        $im = new Imagick();
// 	        dd($im);
	        $mpdf = new \Mpdf\Mpdf();
	        
	        $client = new TFClient();
	        $data = $client->find($client->getUserId());
	        $campaigns = $data['data']['client']['campaigns'];
	        $campaign = null;
	        
	        foreach($campaigns as $campaign) {
	           if($campaign['id'] == $_GET['campaign']) {
	               break;
	           }   
	        }
	        //dd($campaign['influencers'][0]);
	        
            // vertical `[influcencer name] posts` image (green bg/white txt):
            // since the pdf generator doesn't respect css in this case, we need to create an image instead
            putenv('GDFONTPATH=' . __DIR__ . '/themes/trending-family/templates/pdf/fonts');
	        $text = 'ABSOLUTE WEB SOLUTIONS POSTS';
	        $font = 'Brandon_blk.otf';
	        $dimensions = imagettfbbox(30, 0, $font, $text);
	        $im = imagecreatetruecolor(100,$dimensions[4]);
	        $black = imagecolorallocate($im, 0,0,0);
	        $textcolor = imagecolorallocate($im, 255,255,255);
	        $bgColor = imagecolorallocate($im, 78,182,155);
	        imagefilledrectangle($im, 0, 0, 100, $dimensions[4], $bgColor);
	        imagettftext($im, 30, 90, 65, $dimensions[4], $textcolor, $font, $text);
	        $img;
	        ob_start();
	        imagepng($im);
	        $img = ob_get_clean();
	        imagedestroy($im);
	        $verticalPostsText = 'data:image/png;base64,' . base64_encode($img);
	        
	        //$feed[] = file_get_contents('https://www.instagram.com/p/BcXzMbWlvSm/embed/captioned/?cr=1&amp;v=8&amp;wp=480#%7B%22ci%22%3A0%2C%22os%22%3A4123.370000000001%7D');
	        //$feed[0] = str_replace( '<img', '<img style="width:300px;" >',$feed[0]);
	        	         
	        ob_start();
	        include __DIR__ . '/themes/trending-family/templates/pdf/campaign.php';
	        $contents = ob_get_clean();
	        //die($contents);
	        die(htmlentities($contents));
	         
	        $mpdf->WriteHTML($contents);
	        $mpdf->Output();
	        die();

	        $auth = new TFAuth();
	        if( ! $auth->userHasRole('client')) {
	           wp_redirect( '/?err=accessDenied' );
	        }
	        


	        // defines which laravel_admin_db.social_media_data fields should be displayed
	        // and maps the field name to a user friendly display name
	        // i.e - $dataMap['facebook']['followers'] = 'Followers'
	        // will display laravel_admin_db.social_media_data.field `followers` for the Facebook platform
	        // as 'Followers: 123'
	        $dataMap = [
	            'facebook' => [
	                'followers' => 'Followers',
	            ],
	            'twitter' => [
	                'followers' => 'Followers',
	            ],
	            'youtube' => [
	                'subscribers_total' => 'Subscribers',
	                'videos_total' => 'Total Videos',
	            ],
	            'pinterest',
	            'instagram',
	            'snapchat' => [
	                'avg_snaps' => 'Average Snaps',
	            ],
	            'blog' => [
	                'monthly_pageviews' => 'Page Views/Mo',
	            ],
	        ];
	         
	        $client = new TFClient();
	        $data = $client->find($client->getUserId());
	         
	        // we're only supporting one campaign per client at this time
	        // so we're fetching the first one available
	        $campaign = $data['data']['client']['campaigns'] ? $data['data']['client']['campaigns'][0] : [];
	        //include __DIR__ . '/themes/trending-family/templates/pdf/campaign.php';die();
	        $mpdf = new \Mpdf\Mpdf();
	        
	        ob_start();
	        include __DIR__ . '/themes/trending-family/templates/pdf/campaign.php';
	        $contents = ob_get_clean();
	        
	        $mpdf->WriteHTML($contents);
	        $mpdf->Output();
	    }
	     
	    catch (Exception $e) {
	        $this->errorResponse($e->getMessage());
	    }
	}
	
	/**
	 * Login via api
	 */
	public function tf_login()
	{
	    try {
	       $auth = new TFAuth();
	       $auth->login($_POST['username'], $_POST['password']);
	       
	       $user = $auth->getLoggedInUser();
	       	       
	       if( in_array('influencer', $user['roles'])) {
	           $user['page'] = $this->influencerPage;
	       }
	       
	       else if( in_array('client', $user['roles'])) {
	           $user['page'] = $this->clientPage;
	       }
	       
	       else {
	           $user['page'] = '/';
	       }
	       
	       $this->successResponse('Welcome', ['url' => $user['page']]);
	    }
	    
	    catch (Exception $e) {
	        $this->errorResponse($e->getMessage());
	    }	    
	}

	/**
	 * Logout via api
	 */
	public function tf_logout()
	{
	    try {
	        TFAuth::logout();	
	        $this->successResponse();
	    }
	     
	    catch (Exception $e) {
	        $this->errorResponse($e->getMessage());
	    }
	}
	
	/**
	 * Return login satus to browser
	 */
	public function tf_get_login_status()
	{
	    try {
	       $auth = new TFAuth();
	       
	       $user = $auth->getLoggedInUser();
	       	       
	       if( in_array('influencer', $user['roles'])) {
	           $user['page'] = $this->influencerPage;
	       }
	       
	       else if( in_array('client', $user['roles'])) {
	           $user['page'] = $this->clientPage;
	       }
	       
	       else {
	           $user['page'] = '/';
	       }
	       	       
	       $this->successResponse(null, ['user' => $user]);
	    }
	    
	    catch (Exception $e) {
	        $this->errorResponse($e->getMessage());
	    }	    
	}
	
	/**
	 * Login via api
	 */
	public function tf_save_influencer()
	{
	    try {
	       $influencer = new TFInfluencer();	       
	       $influencer->save($_POST);
	       
	       $this->successResponse();
	    }
	    
	    catch (Exception $e) {
	        $this->errorResponse($e->getMessage());
	    }	    
	}
	
	/**
	 * Sync social media accounts
	 */
	public function tf_sync_accounts()
	{
	    try {
	       $accounts = [];	       
	       $account= new TFSocialMediaAccount();
	       $auth = new TFAuth();
	       $user = $auth->getLoggedInUser();
	       	       
	       if( isset($_POST['account_ids']) && $_POST['account_ids']) {
	           foreach($_POST['account_ids'] as $accountId) {

	               $accounts[] = [
	                   'user_id' => $user['id'],
	                   'account_id' =>  $accountId,
	                   'platform' => $_POST['platform'],
	               ];
	           }
	       }	       
	       if( ! $accounts) {
	           throw new Exception('Please ensure at least one account is selected.');
	       }
	       
	       $account->connectAccounts($accounts);
	       
	       $accounts = [];	       
           $influencer = new TFInfluencer();
           $data = $influencer->find($user['id']);  
                
           if($data['data']['socialData']) {
               foreach($data['data']['socialData'] as $platform => $platformAccounts) {
       
                   if(strtolower($platform) != strtolower($_POST['platform'])) continue;
       
                   foreach($platformAccounts as $acct) {
                       
       	               if( ! in_array($acct['account']['account_id'], $_POST['account_ids']) ) {
       	                   $accounts[] = [
        	                   'id' => $acct['account']['id']
        	               ];
       	               }
                   }
               }
           }
           if( $accounts) {
               $account->disconnectAccounts($accounts);
           }
           
	       $this->successResponse();
	    }
	    
	    catch (Exception $e) {
	        $this->errorResponse($e->getMessage());
	    }	    
	}
	
	/**
	 * Save manual account data for Snapchat/Blog
	 */
	public function tf_connect_manual_account()
	{
	    try {
	       $account= new TFSocialMediaAccount();
	       $auth = new TFAuth();
	       $user = $auth->getLoggedInUser();

	       $accounts[0] = [
	           'user_id' => $user['id'],
	           'account_id' =>  md5(time()),
	           'platform' => $_POST['platform'],
	       ];
	       
	       $accounts[0] += $_POST['data'];
	       
	       $account->connectAccounts($accounts);
	       $this->successResponse();
	    }
	    
	    catch (Exception $e) {
	        $this->errorResponse($e->getMessage());
	    }	    
	}
	
	/**
	 * Connect social media accounts
	 */
	public function tf_connect_accounts()
	{
	    try {
	      
	       $accounts = [];	       
	       $account= new TFSocialMediaAccount();
	       $auth = new TFAuth();
	       $user = $auth->getLoggedInUser();
	       
	       if( isset($_POST['account_ids']) && $_POST['account_ids']) {
	           foreach($_POST['account_ids'] as $accountId) {

	               $accounts[] = [
	                   'user_id' => $user['id'],
	                   'account_id' =>  $accountId,
	                   'platform' => $_POST['platform'],
	               ];
	           }
	       }
	       
	       $account->connectAccounts($accounts);
	       $this->successResponse();
	    }
	    
	    catch (Exception $e) {
	        $this->errorResponse($e->getMessage());
	    }	    
	}
	
	/**
	 * Disconnect social media account
	 */
	public function tf_disconnect_account()
	{
	    try {
	       $auth = new TFAuth();	       
	       $user = $auth->getLoggedInUser();
	       	       
	       if( ! in_array('influencer', $user['roles']) || ! $_POST['platform']) {
	           throw new Exception('Access denied.');
	       }
	       
	       
	       $influencer = new TFInfluencer();
	       $data = $influencer->find($user['id']);    	       
	       
	       $connectedCount = 0;
	       if($data['data']['socialData']) {
	           foreach($data['data']['socialData'] as $platform => $platformAccounts) {
	               
	               if(strtolower($platform) != strtolower($_POST['platform'])) continue;
	               
	               foreach($platformAccounts as $account) {
	                   if( $account['account']['connected']) {
	                       $connectedCount++;
	                   }
	               }
	           }
	       }   
	           
	       if( $connectedCount < 2) {
	           //throw new Exception('You must have at least one account connected.');
	       }
	        
	       $account= new TFSocialMediaAccount();
	       $accounts[] = [
	           'id' => $_POST['accountId'],
	       ];	
	       
	       $account->disconnectAccounts($accounts);
	       $this->successResponse();
	    }
	    
	    catch (Exception $e) {
	        $this->errorResponse($e->getMessage());
	    }	    
	}
	
	/**
	 * Fetch logged in user accounts (if user is an influencer)
	 */
	public function tf_get_influencer_accounts()
	{
	    try {
	       $auth = new TFAuth();	       
	       $user = $auth->getLoggedInUser();
	       	       
	       if( ! in_array('influencer', $user['roles'])) {
	           throw new Exception('Access denied.');
	       }
	       
	       $accounts = [];
	       
	       if($_POST['platform']) {
    	       $influencer = new TFInfluencer();
    	       $data = $influencer->find($user['id']);    	       
    	       
    	       if($data['data']['socialData']) {
    	           foreach($data['data']['socialData'] as $platform => $platformAccounts) {
    	               
    	               if(strtolower($platform) != strtolower($_POST['platform'])) continue;
    	               
    	               foreach($platformAccounts as $account) {
    	                   $accounts[] = $account;
    	               }
    	           }
    	       }
	       }

	       $this->successResponse('data', $accounts);
	    }
	    
	    catch (Exception $e) {
	        $this->errorResponse($e->getMessage());
	    }	    
	}
	
	/**
	 * Handle social media oAuht callbacks
	 */
	public function tf_oauth_callback_handler()
	{
	    try {
	        if($_SERVER['REQUEST_URI'] == '/wp-admin/post.php') return true;
    	    $accessToken = null;
    	    $accounts = [];
            $account = new TFSocialMediaAccount();         

            /***************************************************
             *                TWITTER                          *
             *                                                 *
             ***************************************************/
            if (isset($_GET['service']) && $_GET['service'] == 'twitter' && isset($_REQUEST['oauth_token']) && isset($_REQUEST['oauth_verifier'])) {         
                $service = new Includes\SocialProviders\Twitter();
                $accessData = $service->getAccessData($_REQUEST['oauth_token'], $_REQUEST['oauth_verifier']);
                
                if($accessData) {
                    $accounts[] = [
                        'user_id' => $account->getUserId(),
                        'account_id' =>   $accessData['user_id'],
                        'access_token' => json_encode($accessData),
                        'platform' => 'twitter',
                    ];
                }
            }
            
            /***************************************************
             *                INSTAGRAM                        *
             *                                                 *
             ***************************************************/
            else if (isset($_GET['code']) && isset($_GET['service']) && $_GET['service'] == 'instagram') {
                $service = new Includes\SocialProviders\Instagram();
                $accessData = $service->getAccessData($_GET['code']);
            }
    	    
    	    /***************************************************
             *                FACEBOOK                         *
             *                                                 *
             ***************************************************/
            else if (isset($_GET['code']) && isset($_GET['service']) && $_GET['service'] == 'facebook') {            
                $service = new Includes\SocialProviders\Facebook();
                $accessData = $service->getAccessData();
                $pageIds = $service->getPageIds($accessData['access_token'], $accessData['account_id']);
                $accounts = [];
                if($pageIds) {
                    foreach($pageIds as $id) {
                        $accounts[] = [
                            'user_id' => $account->getUserId(),
                            'account_id' =>   $id,
                            'access_token' => $accessData['access_token'],
                            'platform' => 'facebook',
                            'connected' => 0,
                        ];
                    }
                }
            }
            
    	    /***************************************************
             *                YOUTUBE                          *
             *                                                 *
             ***************************************************/
            else if (isset($_GET['code']) && !isset($_GET['service'])) {
            
                if (strval($_SESSION['google_auth_state']) !== strval($_GET['state'])) {
                   throw new Exception('The session state did not match.');
                }
            
                $service = new Includes\SocialProviders\Google();
                $accessData = $service->getAccessData($_GET['code']);
                $accessToken = json_decode($accessData['access_token'], true);
                
                $service = new Includes\SocialProviders\Youtube();
                $channels = $service->getChannels($accessToken['access_token']);

                $accounts = [];
                if($channels) {
                    foreach($channels as $id) {
                        $accounts[] = [
                            'user_id' => $account->getUserId(),
                            'account_id' =>   $id,
                            'access_token' => $accessData['access_token'],
                            'platform' => 'youtube',
                        ];
                    }
                }
            }

            // api call to connect accounts and save access tokens
            if($accounts) {
                $account->connectAccounts($accounts);
            }
            
            if( $_GET['service'] == 'facebook') {
                $_SESSION['initaccounts'] = true;
            }
            
            wp_redirect( $this->influencerPage  );
	    }
	    
	    catch (Exception $e) {
            
            if( $_GET['service'] == 'facebook') {
                $_SESSION['initaccounts'] = true;
            }
            
	        wp_redirect( $this->influencerPage . '?err=' . $e->getMessage() );
	    }
	}
}
