
(function( $ ) {
	'use strict';
	$(document).ready(function(){
		
		$('#login_form_api_submit').click(function(e){
			e.preventDefault();
			TF.login();			
			return false;
		});
		
		$('#influencer_form_api_submit').click(function(e){
			e.preventDefault();
			TF.save_influencer($(this));
			return false;
		});
		
		$('#trending_family_api_login_form input').keydown(function(e) {
		    if (e.keyCode == 13) {
				TF.login();			
				return false;
		    }
		});
		
		$('.influencer_social_media_form_api_submit').click(function(e){
			e.preventDefault();
			TF.save_social_media($(this));
			return false;
		});
		
		$('.start_campaign_form_api_submit').click(function(e){
			e.preventDefault();
			TF.submit_start_campaign_form($(this));
			return false;
		});
		
	    $('a.disconnect').on('click' , function( event ) {
	        event.preventDefault();
	        TF.disconnect_account($(this));
	    });
	    
	    $('.has-connection-opts').click(function(e){
	    	e.preventDefault();
	    	TF.connect_account($(this));
	    	return false;
	    });
	    $('.has-connection-opts').removeClass('hidden');
	    
		$('#export-to-pdf').click(function(e){
			e.preventDefault();
		    TF.export_to_pdf();
		    return false;
		});

		TF.init_login_buttons();	 	
	});
})( jQuery );



/**
 * Trending Family class
 */
var TF = {		
    connect_account_checked: [],
	init_login_buttons: function(){ // initialize login buttons
		jQuery.ajax({
			  type: "POST",
			  dataType: 'JSON',
			  url: '/wp-admin/admin-ajax.php',
			  data: {action:"tf_api", api_action:"get_login_status"},
			  success: function(response){
				  if(response.data.user.id) {
				    var html = '';
				    
				    // campaign button
				    console.log(window.location.href);
				    if(window.location.href.indexOf(TFclientPage) >= 0) {
				    	html += '<a href="'+TFclientPage+'" class="btn" style="margin-right:5px;" >Campaigns</a>';
				    }else{
				    	html += '<a href="'+TFinfluencerPage+'" class="btn" style="margin-right:5px;" >Account</a>';
				    }
				    
				    // logout button
				    html += '<a href="'+response.data.user.page+'" class="btn profile-link hidden"><i class="fa fa-tachometer"></i><span>'+response.data.user.name+'</span></a>'
			    	 		  +'<a href="#" class="btn" onclick="TF.logout();return false;"> <i class="fa fa-user"></i><span>Logout</span></a>';
				    
				    
				    jQuery('.login-widget').html(html);
				  }
				  jQuery('.login-widget').show();
			  }
		});
	},			
	login: function(){ // post login request
		jQuery('.login-error').text('Authenticating ...').show();		
		jQuery.ajax({
			  type: "POST",
			  dataType: 'JSON',
			  url: '/wp-admin/admin-ajax.php',
			  data: jQuery('#trending_family_api_login_form').serialize()+'&action=tf_api&api_action=login',
			  success: function(response){
				  if(!response.status) {
					  jQuery('.login-error').text(response.msg).show();
					  return false;
				  } else {
					  window.location = response.data.url;
				  }
			  }
		});
	},		
	logout: function(){ // post logout request
		jQuery.ajax({
			  type: "POST",
			  dataType: 'JSON',
			  url: '/wp-admin/admin-ajax.php',
			  data: 'action=tf_api&api_action=logout',
			  success: function(response){
				  window.location = '/';
			  }
		});
	},		
	submit_start_campaign_form: function(obj){ // submit influencer form
		var frm = obj.closest('form');
		var currentText = obj.text();
		obj.text('Submitting data ... ');
		obj.attr('disabled', true);
		jQuery.ajax({
			  type: "POST",
			  dataType: 'JSON',
			  url: '/wp-admin/admin-ajax.php',
			  data: frm.serialize()+'&action=tf_api',
			  success: function(response){
				  
				  if(!response.status) {
					  jQuery('.login-error').text(response.msg).show();
					  TF.displayMsg(response.msg);
				  } else {
					  swal("Success", "Request successfully sent.", "success");
				  }
					  
				  obj.text(currentText);
				  frm.find('input').val('');
					obj.attr('disabled', false);
				  jQuery('#start-new-campaign-form').hide();
			  }
		});
	},
	save_influencer: function(obj){ // submit influencer form
		var currentText = obj.text();
		obj.text('Submitting data ... ');
		jQuery.ajax({
			  type: "POST",
			  dataType: 'JSON',
			  url: '/wp-admin/admin-ajax.php',
			  data: jQuery('#influencer_form').serialize()+'&action=tf_api&api_action=save_influencer',
			  success: function(response){
				  if(!response.status) {
					  jQuery('.login-error').text(response.msg).show();
					  TF.displayMsg(response.msg);
				  } else {
					  swal("Success", "Data successfully saved.", "success");
				  }
					  
				  jQuery('#influencer_form_api_submit').text(currentText);

			  }
		});
	},
	save_social_media: function(obj){ // submit influencer form
		var frm = obj.closest('form');
		var currentText = obj.text();
		obj.text('Submitting data ... ');
		jQuery.ajax({
			  type: "POST",
			  dataType: 'JSON',
			  url: '/wp-admin/admin-ajax.php',
			  data: frm.serialize()+'&action=tf_api',
			  success: function(response){
				  if(!response.status) {
					  jQuery('.login-error').text(response.msg).show();
					  TF.displayMsg(response.msg);
				  } else {
					  swal("Success", "Data successfully saved.", "success");
					  location.reload(); 
				  }
					  
				  jQuery('#influencer_form_api_submit').text(currentText);

			  }
		});
	},
	connect_account_toggle: function(obj) {
		var id = jQuery(obj).val();
		if(jQuery(obj).attr('checked')) {
			TF.connect_account_checked.push(id);
		} else {
			var index = TF.connect_account_checked.indexOf(id);
			if (index > -1) {
				TF.connect_account_checked.splice(index, 1);
			}
		}
		console.log(TF.connect_account_checked);
	},
	connect_account: function(obj) { // connect social media account

		swal({
			  type: 'info',
			  title: 'Please wait...',
			  text: 'Fetching existing account information.',
			  showConfirmButton: false
			})

	    TF.connect_account_checked = [];
	    
		// get available, disconnected accounts		
        jQuery.ajax({
			  type: "POST",
			  dataType: 'JSON',
			  url: '/wp-admin/admin-ajax.php',
			  data: {action:"tf_api", api_action:"get_influencer_accounts", "platform":obj.data('platform'), "status":"disconnected"},
			  success: function(response){
				  var html = '<div>';
				  
				  if(response.data.length) {
					  html += '<h4 style="font-size:26px;">Connect Authorized Pages</h4><p style="font-size:14px;" >Select from the authorized pages below or <a href="'+obj.attr('href')+'">Connect Pages From Different Account</a>.</p><hr>';
					  // add accounts
					  var existingAccountsHtml = '<div class="panel panel-default"><ul class="list-group">';;
					  for(i=0;i<response.data.length;i++) {
						  existingAccountsHtml += '<li class="list-group-item">'
							  + response.data[i].account.title +
							  '<div class="material-switch pull-right">'
	                            +'<input '+( response.data[i].account.connected ? 'checked="checked"' : '' )+' id="'+response.data[i].account.id+'" onclick="TF.connect_account_toggle(this);" value="'+response.data[i].account.account_id+'" type="checkbox"/>'
	                            +'<label for="'+response.data[i].account.id+'" style="width:0px;right:40px;" class="label-info"></label></div></li>';
						  if(response.data[i].account.connected) {
							  TF.connect_account_checked.push(response.data[i].account.account_id);
						  }
					  }
					  
					  html += existingAccountsHtml + '</ul></div>';
				  }				  

				  html += '<a href="'+obj.attr('href')+'">Connect Pages From Different Account</a><p style="font-size:14px;" >Please log out of any FaceBook accounts prior to taking this action.</p>';
				  html += '</div><hr>';
				  
				  // display in modal	    
			      swal({
			    	  html: html,
			    	  animation: false,
			    	  customClass: 'animated tada',
				      showCancelButton: true,
				      showConfirmButton: true,
				      width: 'auto',
				      confirmButtonText: 'Connect Selected Authorized Accounts'
			      }).then(function() {
			  			swal({
						  type: 'info',
						  title: 'Please wait...',
						  text: 'Connecting selected accounts.',
						  showConfirmButton: false
						});
			    	    
			            jQuery.ajax({
			    			  type: "POST",
			    			  dataType: 'JSON',
			    			  url: '/wp-admin/admin-ajax.php',
			    			  data: {action:"tf_api", api_action:"sync_accounts", "platform":obj.data('platform'), account_ids: TF.connect_account_checked},
			    			  success: function(response){
			    				  if(response.status) {
									  swal("Success", "Account(s) successfully connected.", "success");
									  location.reload(); 
			    				  } else {

									  swal("Error", response.msg, "error");
			    				  }
			    				  
			    			  }
			            });
				 });
			  }
		});	
        
        return false;
		
    	var acctObj = obj.closest('.account'),
    		platform = acctObj.data('platform');	    
	    swal({
	    	  title: 'jQuery HTML example',
	    	  html: $('<div>')
	    	    .addClass('some-class')
	    	    .text('jQuery is everywhere.'),
	    	  animation: false,
	    	  customClass: 'animated tada'
	    }).then(function() {
	        var a =  obj.closest("tr");
	        var security = homeUrl.ajaxNonce;
	        var data = {
	            nonce: security
	        };
			
	        jQuery.ajax({
				  type: "POST",
				  dataType: 'JSON',
				  url: '/wp-admin/admin-ajax.php',
				  data: {action:"tf_api", api_action:"disconnect_account", accountId: acctObj.data('account-id')},
				  success: function(response){
					  a.fadeOut();
				  }
			});				
	        swal("Disconnected!", "The account has been disconnected.", "success");
	    });
	},
	disconnect_account: function(obj) { // disconnect social media account
    	var acctObj = obj.closest('.account');	    
	    swal({
	        title: "Are you sure?",
	        text: "This account will be disconnected.",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#f86071",
	        confirmButtonText: "Yes, disconnect!",
	        cancelButtonText: "No, don't!",
	    }).then(function() {
	        var a =  obj.closest("tr");
	        var security = homeUrl.ajaxNonce;
	        var data = {
	            nonce: security
	        };
	        
  			swal({
				  type: 'info',
				  title: 'Please wait...',
				  text: 'Disconnecting account.',
				  showConfirmButton: false
				});
			
	        jQuery.ajax({
				  type: "POST",
				  dataType: 'JSON',
				  url: '/wp-admin/admin-ajax.php',
				  data: {action:"tf_api", api_action:"disconnect_account", accountId: acctObj.data('account-id'), platform: acctObj.data('platform')},
				  success: function(response){
					  if(response.status){
						  a.fadeOut();
					      swal("Disconnected!", "The account has been disconnected.", "success");
					  } else {
						  swal("Error", response.msg, "error");
					  }
				  }
			});				
	    });
	},
	displayMsg: function(msg){	    
	    swal({
	        title: "Error",
	        html: msg,
	        type: "warning",
	        showCancelButton: true,
	        showConfirmButton: false,
	        cancelButtonText: "OK",
	    })
	},
	goToLink: function(link, target){
		if(target == '_blank') {
			window.open(link,'_blank');
		} else {
			window.location = link;
		}
	},
	export_to_pdf: function(){ // submit influencer form
		html2canvas(document.querySelector("#post-1426")).then(canvas => {
		    jQuery('body').after(canvas);
		});
		//return false;

		var data = [];
		jQuery('.exportable-chart').each(function(){
    		var url_base64jp = this.toDataURL("image/jpg");
    		data.push({
    			'img': url_base64jp,
    			'platform': jQuery(this).data('platform'),
    			'title': jQuery(this).data('title'),
    			'account': jQuery(this).data('account')
    		});
		});
		
		//console.log(data);return false;
		TF.post_to_url('/wp-admin/admin-ajax.php', {
			'action':'tf_api', 
			'api_action':'export', 
			'format': 'pdf',
			'charts':JSON.stringify(data)}, 
		'post');
		
//		jQuery.ajax({
//			  type: "POST",
//			  dataType: 'JSON',
//			  url: '/wp-admin/admin-ajax.php',
//			  data: {'action':'tf_api', 'api_action':'export_charts', 'charts':JSON.stringify(data)},
//			  success: function(response){
//				  console.log(response);
//
//			  }
//		});
	},
	post_to_url: function(path, params, method) {
	    method = method || "post";

	    var form = document.createElement("form");
	    form.setAttribute("method", method);
	    form.setAttribute("action", path);

	    for(var key in params) {
	        if(params.hasOwnProperty(key)) {
	            var hiddenField = document.createElement("input");
	            hiddenField.setAttribute("type", "hidden");
	            hiddenField.setAttribute("name", key);
	            hiddenField.setAttribute("value", params[key]);

	            form.appendChild(hiddenField);
	         }
	    }

	    document.body.appendChild(form);
	    form.submit();
	}
}


