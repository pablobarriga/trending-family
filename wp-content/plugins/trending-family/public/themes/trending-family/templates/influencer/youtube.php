<div class="social-option-box offset-bottom-sm-7 offset-bottom-xs-2">
    <table class="table table-social <?php echo $platform['accounts'] ? esc_attr('table-with-hover') : ''; ?>">
        <thead>
        <tr>
            <th class="col-logo without-hp">
                <img src="<?php echo get_template_directory_uri() . '/assets/images/blogo-1.png'; ?>" alt="<?php echo esc_attr( $influencer['name']); ?>">
            </th>
            <th><span class="hidden-xs"><?php echo esc_html__('YouTube Channel(s)', 'trending-family'); ?></span></th>
            <th class="text-right">
                <a class="hidden-xs" href="<?php echo ( new Includes\SocialProviders\Youtube() )->getAuthUrl() ; ?>"><?php echo esc_html__('Connect Account', 'trending-family'); ?>
                    <i class="fa fa-plus-circle"></i>
                </a>
            </th>
        </tr>
        </thead>
        <tbody>
		<?php  
		if($platform['accounts']):
			foreach ($platform['accounts'] as $account): if( ! $account['account']['connected']) continue; ?>
                        <tr class="account" data-platform="youtube" data-account-id="<?php echo $account['account']['id']; ?>"  data-id="<?php echo $account['account']['account_id'];  ?>" data-user="<?php echo $influencer['id']; ?>">
                            <td>
                                <span class="thumb-circle">
                                    <img src="<?php echo esc_url($account['account']['thumbnail']); ?>" alt="<?php echo $account['account']['title']; ?>">
                                </span>
                            </td>
                            <td>
                                <b><?php echo $account['account']['title']; ?></b> 
                                <em>-</em>
                                <span class="social-date"><?php echo $account['data']['subscribers_total']; ?> Subscriber</span>
                                <em>,</em> 
                                <span class="social-date"><?php echo $account['data']['videos_total']; ?> Videos</span>
                                <em>,</em>
                                <span class="social-date"><?php echo $account['data']['views_total']; ?> Views</span> 
                            </td>
                            <td class="text-right">
                                <a class="btn-disconect disconnect">
                                    <span class="hidden-xs"><?php echo esc_html__('Disconnect', 'trending-family'); ?></span> 
                                    <i class="fa fa-minus-circle"></i>
                                </a>
                            </td>
                        </tr>
				    
				<?php endforeach; ?>
		<?php else: ?>
            <tr>
				<td class="without-hp" colspan="3"><i><?php echo esc_html__('No account has been connected yet.', 'trending-family'); ?></i></td>
            </tr>
		<?php endif; ?>
        </tbody>
    </table>
    <a class="btn btn-success btn-add btn-block btn-outline visible-xs" href="<?php echo esc_url($platform['authUrl']); ?>"><?php echo esc_html__('Add New', 'trending-family'); ?></a>
</div>