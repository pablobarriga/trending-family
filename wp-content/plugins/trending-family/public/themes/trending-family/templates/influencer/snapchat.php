<?php //dd($platform); ?>
<div class="social-option-box offset-bottom-sm-7 offset-bottom-xs-2">
    <table class="table table-social <?php echo $platform['accounts'] ? esc_attr('table-with-hover') : ''; ?>">
        <thead>
        <tr>
            <th class="col-logo without-hp"><img src="<?php echo get_template_directory_uri() . '/assets/images/blogo-6.png'; ?>" alt="<?php echo esc_attr( $influencer['name'] ); ?>"></th>
            <th><span class="hidden-xs"><?php echo esc_html__('Snapchat Account(s)', 'trending-family'); ?></span></th>
            <th class="text-right"><a class="hidden-xs btn-add-social" href="#"><?php echo esc_html__('Connect Account', 'trending-family'); ?> <i class="fa fa-plus-circle"></i></a></th>
        </tr>
        </thead>
        <tbody>
		<?php if ( $platform['accounts'] ):
			foreach ( $platform['accounts'] as $account ):  if( ! $account['account']['connected']) continue; ?>
                <tr class="account " data-platform="snapchat" data-account-id="<?php echo $account['account']['id']; ?>" data-page="<?php echo $account['account']['account_id']; ?>" data-user="<?php echo $influencer['id']; ?>">
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <b><?php echo $account['account']['title']; ?></b> 
                        <em>-</em> 
                        <span class="social-date"><?php echo $account['data']['avg_snaps']; ?> 
                            <?php echo esc_html__('Average Snaps', 'trending-family'); ?>
                        </span>
                    </td>
                    
                    <td class="text-right">
                        <a class="btn-disconect disconnect"  id="<?php echo $account['account']['account_id'] ;?>" href="javascript:void(0);">
                            <span class="hidden-xs">Disconnect</span> 
                            <i class="fa fa-minus-circle"></i>
                        </a>
                    </td>
                 </tr>
			<?php endforeach; else: ?>
            <tr>
                <td class="without-hp" colspan="3"><i><?php echo esc_html__('No account has been connected yet.', 'trending-family'); ?></i></td>
            </tr>
		<?php endif; ?>
        </tbody>
    </table>
    <div class="login-popup-box-wrap">
        <div class="login-popup-box">
            <form class="validateForm" id="connect_snapchat" method="POST" autocomplete="off">
                <input type="hidden" name="api_action" value="connect_manual_account">
                <input type="hidden" name="platform" value="snapchat">
                <p class="connect-error" style="display: none;"></p>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="data[handle]" placeholder="<?php echo esc_attr__('Snapchat Handle *', 'trending-family'); ?>" required>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="data[avg_snaps]" placeholder="<?php echo esc_attr__('Average Snaps *', 'trending-family'); ?>" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <button class="btn btn-default influencer_social_media_form_api_submit" ><?php echo esc_html__( 'Submit', 'trending-family'); ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>