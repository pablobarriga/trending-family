<?php //dd($platform); ?>
<div class="social-option-box offset-bottom-sm-7 offset-bottom-xs-2">
    <table class="table table-social <?php echo $platform['accounts'] ? esc_attr('table-with-hover') : ''; ?>">
        <thead>
        <tr>
            <th class="col-logo without-hp"><img src="<?php echo get_template_directory_uri() . '/assets/images/blogo-4.png'; ?>" alt="<?php echo esc_attr( $influencer['name'] ); ?>"></th>
            <th><span class="hidden-xs"><?php echo esc_html__('Twitter Account(s)', 'trending-family'); ?></span></th>
            <th class="text-right"><a class="hidden-xs" href="<?php echo ( new Includes\SocialProviders\Twitter() )->getAuthUrl(); ?>"><?php echo esc_html__('Connect Account', 'trending-family'); ?> <i class="fa fa-plus-circle"></i></a></th>
        </tr>
        </thead>
        <tbody>
		<?php if ( $platform['accounts'] ):
			foreach ( $platform['accounts'] as $account ): 
		       $segments= explode('/', $account['account']['url']);
		       $username = array_pop($segments);
		?>
                <tr class="account " data-platform="twitter" data-account-id="<?php echo $account['account']['id']; ?>" data-page="<?php echo $account['account']['account_id']; ?>" data-user="<?php echo $influencer['id']; ?>">
                    <td>
                        <span class="thumb-circle">
                            <img src="<?php echo esc_url($account['account']['thumbnail']); ?>" alt="<?php echo esc_attr( $account['account']['title'] ); ?>">
                        </span>
                    </td>
                    <td>
                        <b>@<?php echo $username; ?></b> 
                        <em>-</em> 
                        <span class="social-date"><?php echo $account['data']['followers']; ?> 
                            <?php echo esc_html__('followers', 'trending-family'); ?>
                        </span>
                    </td>
                    <td class="text-right"><a class="btn-disconect twitt-disconect"><span class="hidden-xs"><?php echo esc_html__('Disconnect', 'trending-family'); ?></span> <i class="fa fa-minus-circle"></i></a></td>
                </tr>
			<?php endforeach;
		else: ?>
            <tr>
                <td class="without-hp" colspan="3"><i><?php echo esc_html__('No account has been connected yet.', 'trending-family'); ?></i></td>
            </tr>
		<?php endif; ?>
        </tbody>
    </table>
</div>