
    <section class="section pad-top-0 bg-grey bg-white-xs">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="s-title">
                        <h3><?php echo esc_html__('Your profile', 'trending-family'); ?><span class="">:</span></h3>
                    </div>
                </div>
            </div>
            <form id="influencer_form" method="POST" autocomplete="off">
                <input type="hidden" name="action" value="update_influencer">
                <div class="row profile-options-box">
                    <div class="col-xs-12">
                        <fieldset>
                            <div class="legend">
                                <span><?php echo esc_html__( 'Contact Info', 'trending-family' ); ?></span>
                            </div>
                            <div class="row row-sm form-row">
                                <a class="btn-add btnInfluencerDel" href="javascript:void(0);"><i class="fa fa-times-circle"></i></a>
                                <div class="col-sm-6 col-xs-12">
                                    <label class="label_influencer_first_name" for="label_influencer_first_name"><?php echo esc_html__('First Name', 'trending-family'); ?></label>
                                    <div class="form-group">
                                        <input type="text" name="first_name" id="influencer_first_name" class="form-control input-lg influencer_first_name" value="<?php echo isset($influencer['influencer_override']['first_name']) ? esc_attr($influencer['influencer_override']['first_name']) :  esc_attr($influencer['first_name']); ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <label class="label_influencer_last_name" for="influencer_last_name"><?php echo esc_html__('Last Name', 'trending-family'); ?></label>
                                    <div class="form-group">
                                        <input type="text" name="last_name" id="influencer_last_name" class="form-control input-lg influencer_last_name" value="<?php echo isset($influencer['influencer_override']['last_name']) ? esc_attr($influencer['influencer_override']['last_name']) : esc_attr($influencer['last_name']); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm form-row-sm">
                                <div class="col-sm-4 col-xs-12">
                                    <label for="influencer-email"><?php echo esc_html__('E-Mail', 'trending-family'); ?></label>
                                    <div class="form-group">
                                        <input type="text" name="email" class="form-control input-lg" id="influencer-email" value="<?php echo isset($influencer['influencer_override']['email']) ? esc_attr($influencer['influencer_override']['email']) : esc_attr($influencer['email']); ?>">
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <label for="influencer-phone-number"><?php echo esc_html__('Phone Number', 'trending-family'); ?></label>
                                    <div class="form-group">
                                        <input type="text" name="phone" class="form-control input-lg phone" id="influencer-phone-number" value="<?php echo isset($influencer['influencer_override']['phone']) ? esc_attr($influencer['influencer_override']['phone']) : esc_attr($influencer['phone']); ?>">
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <label for="influencer-dob"><?php echo esc_html__('Date of Birth', 'trending-family'); ?></label>
                                    <div class="form-group">
                                        <input type="text" name="dob" class="form-control input-lg dob datepicker-input" id="influencer-dob" value="<?php echo isset($influencer['influencer_override']['dob']) ? esc_attr($influencer['influencer_override']['dob']) : esc_attr($influencer['dob']); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm form-row-sm">
                                <div class="col-xs-12">
                                    <label for="influencer-address"><?php echo esc_html__('Address', 'trending-family'); ?></label>
                                    <div class="form-group">
                                        <input type="text" name="address" class="form-control input-lg" id="influencer-address" value="<?php echo isset($influencer['influencer_override']['address']) ? esc_attr($influencer['influencer_override']['address']) : esc_attr($influencer['address']); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm form-row-sm">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <label for="influencer-city"><?php echo esc_html__('City', 'trending-family'); ?></label>
                                    <div class="form-group">
                                        <input type="text" name="city" class="form-control input-lg" id="influencer-city" value="<?php echo isset($influencer['influencer_override']['city']) ? esc_attr($influencer['influencer_override']['city']) : esc_attr($influencer['city']); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <label for="state"><?php echo esc_html__('State', 'trending-family'); ?></label>
                                    <div class="form-group">
                                        <select class="form-control input-lg " name="state" id="state" data-default-value="<?php echo isset($influencer['influencer_override']['state']) ? esc_attr($influencer['influencer_override']['state']) : esc_attr($influencer['state']); ?>" ></select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <label for="zip"><?php echo esc_html('Zip', 'trending-family'); ?></label>
                                    <div class="form-group">
                                        <input type="text" class="form-control input-lg" name="zip" id="zip" value="<?php echo isset($influencer['influencer_override']['zip']) ? esc_attr($influencer['influencer_override']['zip']) : esc_attr($influencer['zip']); ?>">
                                    </div>
                                </div>
                                <?php 
                                $country = 'United States';
                                if( isset($influencer['influencer_override']['country']) && $influencer['influencer_override']['country']) {
                                    $country = $influencer['influencer_override']['country'];
                                }
                                
                                else if (isset($influencer['influencer']['country']) && $influencer['influencer']['country']) {
                                    $country = $influencer['influencer']['country'];
                                }
                                ?>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <label for="country"><?php echo esc_html__('Country', 'trending-family'); ?></label>
                                    <div class="form-group">
                                        <select class="form-control input-lg crs-country" data-default-value="<?php echo $country; ?>" data-region-id="state" id="country" name="country"></select>
										
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="legend">
								<?php echo esc_html__( 'Kids', 'trending-family' ) ; ?>
                                <em><?php echo esc_html__( '(Optional)', 'trending-family' ); ?></em>
                                <a class="btn-text  pull-right addKids"
                                   href="javascript:void(0);"><?php echo esc_html__( 'Add', 'trending-family' ); ?>
                                    <i class="fa fa-plus-circle"></i></a>
                            </div>
							<?php
							if( $influencer['children'] ) {
								foreach($influencer['children'] as $key => $child) { ?>
                                    <div id="entryKids<?php echo $key; ?>" class="clonedKids">
                                        <div class="row row-sm form-row">
                                            <a class="btn-add btnKidsDel" href="javascript:void(0);"><i
                                                        class="fa fa-times-circle"></i></a>
                                            <div class="col-sm-8 col-xs-12">
                                                <label for="kids_name"
                                                       class="label_kids_name"><?php echo esc_html__( 'Name', 'trending-family' ); ?></label>
                                                <div class="form-group">
                                                    <input name="kids_name[]" type="text"
                                                           class="form-control input-lg kids_name" id="kids_name" value="<?php echo esc_attr( $child['name'] ); ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-xs-12">
                                                <label for="kids_dob"
                                                       class="label_kids_dob"><?php echo esc_html__( 'Date of Birth', 'trending-family' ); ?></label>
                                                <div class="form-group">
                                                    <input name="kids_dob[]" type="text"
                                                           class="form-control input-lg input-date dob datepicker-input kids_dob"
                                                           id="kids_dob" value="<?php echo esc_attr( $child['age'] ); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								<?php } ?>
							<?php } ?>
                                <div id="kidsTpl" class="clonedKids" style="display:none;">
                                    <div class="row row-sm form-row">
                                        <a class="btn-add btnKidsDel" href="javascript:void(0);"><i
                                                    class="fa fa-times-circle"></i></a>
                                        <div class="col-sm-8 col-xs-12">
                                            <label for="kids_name"
                                                   class="label_kids_name"><?php echo esc_html__( 'Name', 'trending-family' ); ?></label>
                                            <div class="form-group">
                                                <input name="kids_name[]" type="text"
                                                       class="form-control input-lg kids_name" id="kids_name">
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-12">
                                            <label for="kids_dob"
                                                   class="label_kids_dob"><?php echo esc_html__( 'Date of birth', 'trending-family' ); ?></label>
                                            <div class="form-group">
                                                <input name="kids_dob[]" type="text"
                                                       class="form-control input-lg input-date dob datepicker-input kids_dob"
                                                       id="kids_dob">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </fieldset>
                        <fieldset>
                            <div class="legend"><?php echo esc_html__( 'Pets', 'trending-family' ); ?>
                                <em><?php echo esc_html__( '(Optional)', 'trending-family' ); ?></em>
                                <a class="btn-text  pull-right addPets" href="javascript:void(0);">
									<?php echo esc_html__( 'Add', 'trending-family' ); ?>
                                    <i class="fa fa-plus-circle"></i></a>
                            </div>
							<?php
							if( $influencer['pets'] ) {
								foreach($influencer['pets'] as $key => $pet) { ?>
                                    <div  class="clonedPets">
                                        <div class="row row-sm form-row">
                                            <a class="btn-add btnPetsDel" href="javascript:void(0);"><i
                                                        class="fa fa-times-circle"></i></a>
                                            <div class="col-sm-6 col-xs-12">
                                                <label for="pets_name"
                                                       class="label_pets_name"><?php echo esc_html__( 'Name', 'trending-family' ); ?></label>
                                                <div class="form-group">
                                                    <input type="text" id="pets_name"
                                                           class="form-control input-lg pets_name" name="pets_name[]" value="<?php echo esc_attr( $pet['name'] ); ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-xs-12">
                                                <label for="pets_description"
                                                       class="label_pets_description"><?php echo esc_html__( 'Description', 'trending-family' ); ?></label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-lg pets_description"
                                                           name="pets_description[]" id="pets_description" value="<?php echo esc_attr( $pet['name'] ); ?>" placeholder="<?php echo esc_attr__('Animal, Age, etc.', 'trending-family'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								<?php } ?>
							<?php } ?>
                                <div id="petsTpl" class="clonedPets" style="display:none;">
                                    <div class="row row-sm form-row">
                                        <a class="btn-add btnPetsDel" href="javascript:void(0);"><i
                                                    class="fa fa-times-circle"></i></a>
                                        <div class="col-sm-6 col-xs-12">
                                            <label for="pets_name"
                                                   class="label_pets_name"><?php echo esc_html__( 'Name', 'trending-family' ); ?></label>
                                            <div class="form-group">
                                                <input type="text" id="pets_name[]"
                                                       class="form-control input-lg pets_name" name="pets_name[]">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <label for="pets_description"
                                                   class="label_pets_description"><?php echo esc_html__( 'Description', 'trending-family' ); ?></label>
                                            <div class="form-group">
                                                <input type="text" class="form-control input-lg pets_description"
                                                       name="pets_description[]" id="pets_description" placeholder="<?php echo esc_attr__('Animal, Age, etc.', 'trending-family'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </fieldset>
                        <fieldset>
                            <div class="legend"><?php echo esc_html__( 'Languages Spoken', 'trending-family' ); ?>
                                <em><?php echo esc_html__( '(Optional)', 'trending-family' ); ?></em>
                                <a class="btn-text  pull-right addLanguages"
                                   href="javascript:void(0);">
                                    <span><?php echo esc_html__( 'Add', 'trending-family' ); ?></span><i
                                            class="fa fa-plus-circle"></i>
                                </a>
                            </div>
							<?php
							if( $influencer['languages_spoken'] ) {
								foreach($influencer['languages_spoken'] as $key => $lang) { ?>
                                    <div class="clonedLanguages">
                                        <div class="row row-sm form-row form-row-wl">
                                            <a class="btn-add btnLanguagesDel" href="javascript:void(0);"><i
                                                        class="fa fa-times-circle"></i></a>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-lg languages"
                                                           name="languages[]" id="languages" value="<?php echo esc_attr( $lang['language'] ); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								<?php } ?>
							<?php } ?>
                                <div id="languagesTpl" class="clonedLanguages" style="display:none;">
                                    <div class="row row-sm form-row form-row-wl">
                                        <a class="btn-add btnLanguagesDel" href="javascript:void(0);"><i
                                                    class="fa fa-times-circle"></i></a>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-lg languages"
                                                       name="languages[]" id="languages" value="<?php echo esc_attr( 'English' ); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </fieldset>
                        <fieldset>
                            <div class="legend"><?php echo  esc_html__( 'Brand Preferences', 'trending-family' ); ?>
                                <em><?php echo esc_html__( '(Optional)', 'trending-family' ); ?></em>
                                <a class="btn-text  pull-right addBrands"
                                   href="javascript:void(0);"><?php echo esc_html__( 'Add', 'trending-family' ); ?>
                                    <i class="fa fa-plus-circle"></i>
                                </a>
                            </div>
							<?php
							if( $influencer['brand_preferences'] ) {
								foreach($influencer['brand_preferences'] as $key => $brand) { ?>
                                    <div class="clonedBrands">
                                        <div class="row row-sm form-row form-row-wl">
                                            <a class="btn-add btnBrandsDel" href="javascript:void(0);"><i
                                                        class="fa fa-times-circle"></i></a>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-lg brands"
                                                           name="brands[]" id="brands" value="<?php echo esc_attr( $brand['brand'] ); ?>" placeholder="<?php echo esc_attr__("WalMart, Macy's, etc.", "trending-family"); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<?php
								} ?>
							<?php } ?>
                                <div id="brandsTpl" class="clonedBrands" style="display: none;">
                                    <div class="row row-sm form-row form-row-wl">
                                        <a class="btn-add btnBrandsDel" href="javascript:void(0);"><i
                                                    class="fa fa-times-circle"></i></a>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-lg brands"
                                                       name="brands[]" id="brands" placeholder="<?php echo esc_attr__("WalMart, Macy's, etc.", "trending-family"); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </fieldset>
                        <fieldset>
                            <div class="legend">
								<?php echo esc_html__('Change Password', 'trending-family'); ?><em><?php echo esc_html__(' (Optional)', 'trending-family'); ?></em>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input type="password" class="form-control input-lg" name="password" id="password" placeholder="<?php echo esc_attr__('New Password', 'trending-family'); ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input type="password" class="form-control input-lg" name="password_confirmation" id="password_confirmation" placeholder="<?php echo esc_attr__('Retype New Password', 'trending-family'); ?>">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <div class="text-center offset-top-sm-7">
                            <a href="#" class="btn btn-success btn-lg btn-save-influencer" id="influencer_form_api_submit">Save Changes</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>