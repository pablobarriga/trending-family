<?php // TODO ?>
<div class="social-option-box offset-bottom-sm-7 offset-bottom-xs-2">
    <table class="table table-social <?php echo $platform['accounts'] ? esc_attr('table-with-hover') : ''; ?>">
        <thead>
        <tr>
            <th class="col-logo without-hp"><img src="<?php echo get_template_directory_uri() . '/assets/images/blogo-5.png'; ?>" alt="<?php echo esc_attr( $influencer['name'] ); ?>"></th>
            <th><span class="hidden-xs"><?php echo esc_html__('Pinterest Account(s)', 'trending-family'); ?></span></th>
            <th class="text-right"><a class="hidden-xs" href="<?php echo $platform['authUrl']; ?>"><?php echo esc_html__('Connect Account', 'trending-family'); ?>
                    <i class="fa fa-plus-circle"></i></a></th>
        </tr>
        </thead>
        <tbody>
		<?php if ( $platform['accounts'] ): 
		?>
			<?php foreach ($platform['accounts'] as $account): ?>
                <tr data-page="<?php echo $account['account']['account_id']; ?>" data-user="<?php echo $influencer['id']; ?>">
                    <td>
                        <span class="thumb-circle">
                            <img src="<?php echo esc_url($account['account']['thumbnail']); ?>" alt="<?php echo esc_attr( $pin_account['username'] ); ?>">
                        </span>
                    </td>
                    <td>
                        <b><?php echo $account['account']['title']; ?></b> 
                        <em>-</em> 
                        <span class="social-date"><?php echo $pin_account['counts']['followers']; ?> <?php echo esc_html__('followers', 'trending-family'); ?></span>
                        <em>,</em> 
                        <span class="social-date"> <?php echo $pin_account['counts']['following']; ?> <?php echo esc_html__('following', 'trending-family'); ?></span>
                        <em>,</em> 
                        <span> <?php echo $pin_account['counts']['boards']; ?> <?php echo esc_html__('boards', 'trending-family'); ?></span>
                        <em>,</em> 
                        <span> <?php echo $pin_account['counts']['pins']; ?> <?php echo esc_html__('pins', 'trending-family'); ?></span>
                    </td>
                    <td class="text-right"><a class="btn-disconect pin-disconect"><span class="hidden-xs"><?php echo esc_html__('Disconnect', 'trending-family'); ?></span> <i class="fa fa-minus-circle"></i></a></td>
                </tr>
			<?php endforeach; ?>
		<?php else: ?>
            <tr>
                <td class="without-hp" colspan="3"><i><?php echo esc_html__('No account has been connected yet.', 'trending-family'); ?></i></td>
            </tr>
		<?php endif; ?>
        </tbody>
    </table>
    <a class="btn btn-success btn-add btn-block btn-outline visible-xs btn-add-social" href="javascript:void(0);"><?php echo esc_html__('Connect Account', 'trending-family'); ?></a>
    <div class="login-popup-box-wrap right-175">
        <div class="login-popup-box">
            <form class="validateForm" method="POST" autocomplete="off">
                <p class="connect-error" style="display: none;"></p>
				<?php wp_nonce_field( 'trending_family_connect_social_nonce', 'security' ); ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="pinterest_account" placeholder="<?php echo esc_attr__('Username *', 'trending-family'); ?>" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <a class="btn btn-default add_page" href="javascript:void(0);"><?php echo esc_html__('Add Account', 'trending-family'); ?></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>