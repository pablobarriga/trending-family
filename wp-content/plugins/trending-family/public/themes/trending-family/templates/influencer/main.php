<?php if ( ! defined( 'ABSPATH' ) ) exit;
/*
 * Template Name: New Influencer Landing Page
 */
get_header();

?>
<style>
.entry-header, .edit-link {
    display:none;
}
</style>
<section class="section bg-green pad-md text-dark text-center-xs">
    <div class="container">
        <div class="row row-lg">
            <div class="flex-center">
                <div class="col-xs-12 p-title">
					<h1>
					  <?php echo esc_html__('Welcome', 'trending-family'); ?> 
					  <span class="txt-wlight"><?php echo esc_html( $influencer['name']); ?></span></h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section pad-top-xxs-0 bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="s-title">
                    <h3><?php echo esc_html__('Your social media', 'trending-family'); ?><span class="hidden-xs">:</span></h3>
                </div>
            </div>
        </div>
        <?php foreach($platforms as $platform): ?>
        <div class="row">
            <div class="col-xs-12 mob-social-option-box">
                <?php 
                if(file_exists(__DIR__ . '/' . strtolower($platform['name']) . '.php')) {
                    include(__DIR__ . '/' . strtolower($platform['name']) . '.php'); 
                }
                ?>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</section>
<?php include(__DIR__ . '/personal-info.php'); ?>
<?php get_footer();?>