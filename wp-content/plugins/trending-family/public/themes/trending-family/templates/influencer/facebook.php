<?php //dd($platform['accounts']);?>
<div class="social-option-box offset-bottom-sm-7 offset-bottom-xs-2">
    <table class="table table-social <?php echo $platform['accounts'] ? esc_attr('table-with-hover') : ''; ?>">
        <thead>
            <tr>
                <th class="col-logo without-hp">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/blogo-2.png'; ?>" alt="<?php echo esc_attr( $influencer['name']  ); ?>">
                </th>
                <th>
                    <span class="hidden-xs"><?php echo esc_html__('Facebook Fan Page(s)', 'trending-family'); ?></span>
                </th>
                <th class="text-right">
                    <a href="<?php echo ( new Includes\SocialProviders\Facebook() )->getAuthUrl(); ?>" id="fbaddpage" data-platform="facebook" class="hidden-xs <?php echo $platform['accounts'] ? 'hidden has-connection-opts' : ''; ?>">
                        <?php echo esc_html__('Connect Account', 'trending_family'); ?> 
                        <i class="fa fa-plus-circle"></i> 
                    </a>
                </th>
            </tr>
        </thead>
        <tbody >
		<?php
		if($platform['accounts']):
			foreach ($platform['accounts'] as $account) { if( ! $account['account']['connected']) continue; ?>
                <tr class="account " data-platform="facebook" data-account-id="<?php echo $account['account']['id']; ?>" data-page="<?php echo $account['account']['account_id'] ;?>" data-user="<?php echo $influencer['id']; ?>">
                    <td>
                        <span class="thumb-circle">
                            <img src="<?php echo $account['account']['thumbnail'] ;?>" alt="<?php echo esc_attr($account['account']['title']); ?>">
                        </span>
                    </td>
                    <td>
                        <b><?php echo $account['account']['title']; ?></b> 
                        <em>-</em> 
                        <span class="social-date"><?php echo $account['data']['followers']; ?> <?php echo esc_html__('Followers', 'trending-family'); ?></span> 
                    </td>
                    <td class="text-right">
                        <a class="btn-disconect disconnect"  id="<?php echo $account['account']['account_id'] ;?>" href="javascript:void(0);">
                            <span class="hidden-xs">Disconnect</span> 
                            <i class="fa fa-minus-circle"></i>
                        </a>
                    </td>
               </tr>
			<?php }
		   else: ?>
            <tr>
				<td class="without-hp" colspan="3"><i><?php echo esc_html__('No account has been connected yet.', 'trending-family'); ?></i></td>
            </tr>
		<?php endif; ?>
        </tbody>
    </table>
    <a class="btn btn-success btn-add btn-block btn-outline visible-xs <?php echo $platform['accounts'] ? 'hidden has-connection-opts' : ''; ?>" data-platform="facebook" href="<?php echo ( new Includes\SocialProviders\Facebook() )->getAuthUrl(); ?>"><?php echo esc_html__('Connect Account', 'trending-family'); ?></a>
</div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap2/bootstrap-switch.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.min.js"></script>
<script type="text/javascript">
    if (window.location.hash && window.location.hash == '#_=_') {
        window.location.hash = '';
    }
</script>
<?php 
// if a facebook account has been previously connected, we all the option to connect those 'fan pages' by displaying a modal w/ choices
if( isset($_SESSION['initaccounts']) && $_SESSION['initaccounts']): unset($_SESSION['initaccounts']); ?>
<script>
jQuery(document).ready(function(){
	jQuery('.has-connection-opts').click();
});
</script>
<?php endif; ?>
