<?php
$datesTWtweetCount = [];
$tweets = [];
$accountNames = [];
$enabled = false;

$twitterAccounts = [];
if($influencer['social_media_accounts']) {
    foreach($influencer['social_media_accounts'] as $key => $val) {
        if(strtolower($val['platformName']) == 'twitter') {
            $twitterAccounts[] = $val;
        }
    }
}

if($twitterAccounts) {
    foreach($twitterAccounts as $account) {
        
        if( ! $account['social_media_data']) continue;

        foreach($account['social_media_data'] as $data) {
            
            if($data['field'] != 'user_timeline') continue;
            
            $timeline = json_decode($data['data'], true);
            
            foreach($timeline as $date => $tweetCount) {
                $datesTWtweetCount[$account['title']][] = date('m.d.Y', strtotime($date));
                $tweets[$account['title']][] = $tweetCount;
                $enabled = true;
            }
        }
        
        $accountNames[$account['title']] = $account['title'];
    }
}
?>

<h3>Twitter Tweet Count</h3>

<?php if($enabled): ?>
<canvas id="canvas-tw-tweet-count-<?php echo $influencer['id']; ?>"></canvas> 

<script>

var configTwTweetCount<?php echo $influencer['id']; ?> = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "Tweet Count",
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            data: []
        }]
    },
    options: {
        responsive: true,
        title:{
            display:false,
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'datesTWtweetCount'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Tweet Count'
                }
            }]
        }
    }
};

var datesTWtweetCount<?php echo $influencer['id']; ?> = <?php echo json_encode($datesTWtweetCount); ?>;
var tweets<?php echo $influencer['id']; ?> = <?php echo json_encode($tweets); ?>;

function TwTweetCountChange<?php echo $influencer['id']; ?>(obj) {
	var selected = obj.val();
	jQuery.each(tweets<?php echo $influencer['id']; ?>, function(k,v){
		if(k == selected) {
			configTwTweetCount<?php echo $influencer['id']; ?>.data.datasets[0].data = v;
		}
	});
	jQuery.each(datesTWtweetCount<?php echo $influencer['id']; ?>, function(k,v){
		if(k == selected) {
			configTwTweetCount<?php echo $influencer['id']; ?>.data.labels = v;
		}
	});

	canvasTwTweetCount<?php echo $influencer['id']; ?>.update();
}

jQuery(document).ready(function() {

	jQuery('#canvas-tw-tweet-count-<?php echo $influencer['id']; ?>').before('<select id="accounts-tw-tweet-count-<?php echo $influencer['id']; ?>" class="form-control" onChange="TwTweetCountChange<?php echo $influencer['id']; ?>(jQuery(this));"><?php foreach($accountNames as $name) echo '<option value="' . $name . '">' . $name . '</option>'; ?></select>');
	
    var canvasTwTweetCount<?php echo $influencer['id']; ?> = document.getElementById("canvas-tw-tweet-count-<?php echo $influencer['id']; ?>").getContext("2d");
    window.canvasTwTweetCount<?php echo $influencer['id']; ?> = new Chart(canvasTwTweetCount<?php echo $influencer['id']; ?>, configTwTweetCount<?php echo $influencer['id']; ?>);

    TwTweetCountChange<?php echo $influencer['id']; ?>(jQuery('#accounts-tw-tweet-count-<?php echo $influencer['id']; ?>'));
});
    
</script>
<?php else: ?>
<p><?php echo esc_html('Not available'); ?></p>
<?php endif; ?>