<?php
$dates = [];
$views = [];
$watchTime = [];
$accountNames = [];
$enabled = false;

$youtubeChannels = [];
if($influencer['social_media_accounts']) {
    foreach($influencer['social_media_accounts'] as $key => $val) {
        if(strtolower($val['platformName']) == 'youtube') {
            $youtubeChannels[] = $val;
        }
    }
}

if($youtubeChannels) {
    foreach($youtubeChannels as $channel) {   
        
        if( ! $channel['social_media_data'] || ! $channel['connected']) continue;
        
        foreach($channel['social_media_data'] as $socialMediaData) {
            
            if($socialMediaData['field'] != 'channel_audience_retention_dates') continue;
            
            $retentionDates = json_decode($socialMediaData['data'], true);
            
            if( ! $retentionDates) continue;
            
            foreach($retentionDates as $date => $stats) {
                $dates[$channel['title']][] = date('m.d.Y', strtotime($date));
                $watchTime[$channel['title']][] = $stats['watchTime'];
                $views[$channel['title']][] = $stats['views'];
                $enabled = true;
            }
          
        } 
        
        $accountNames[$channel['title']] = $channel['title'];
    }
}
?>

<h3>YouTube Audience Retention</h3>

<?php if($enabled): ?>
<canvas id="canvas-yt-audience-retention-<?php echo $influencer['id']; ?>"></canvas> 

<script>

var configYtAudienceRetention<?php echo $influencer['id']; ?> = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "Time Watched (in seconds)",
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            data: []
        }, {
            label: "Views",
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            data: [],
        }]
    },
    options: {
        responsive: true,
        title:{
            display:false,
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Date'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Time Watched/Views'
                }
            }]
        }
    }
};

var datesYTAudienceRet<?php echo $influencer['id']; ?> = <?php echo json_encode($dates); ?>;
var watchTime<?php echo $influencer['id']; ?> = <?php echo json_encode($watchTime); ?>;
var views<?php echo $influencer['id']; ?> = <?php echo json_encode($views); ?>;

function YtAudienceRetentionChange<?php echo $influencer['id']; ?>(obj) {
	var selected = obj.val();
	jQuery.each(watchTime<?php echo $influencer['id']; ?>, function(k,v){
		if(k == selected) {
			configYtAudienceRetention<?php echo $influencer['id']; ?>.data.datasets[0].data = v;
		}
	});
	jQuery.each(views<?php echo $influencer['id']; ?>, function(k,v){
		if(k == selected) {
			configYtAudienceRetention<?php echo $influencer['id']; ?>.data.datasets[1].data = v;
		}
	});
	jQuery.each(datesYTAudienceRet<?php echo $influencer['id']; ?>, function(k,v){
		if(k == selected) {
			configYtAudienceRetention<?php echo $influencer['id']; ?>.data.labels = v;
		}
	});

	canvasYtAudienceRetention<?php echo $influencer['id']; ?>.update();
}

jQuery(document).ready(function() {

	jQuery('#canvas-yt-audience-retention-<?php echo $influencer['id']; ?>').before('<select id="canvas-yt-audience-retention-page-<?php echo $influencer['id']; ?>" class="form-control" onChange="YtAudienceRetentionChange<?php echo $influencer['id']; ?>(jQuery(this));"><?php foreach($accountNames as $name) echo '<option value="' . $name . '">' . $name . '</option>'; ?></select>');
	
    var canvasYtAudienceRetention<?php echo $influencer['id']; ?> = document.getElementById("canvas-yt-audience-retention-<?php echo $influencer['id']; ?>").getContext("2d");
    window.canvasYtAudienceRetention<?php echo $influencer['id']; ?> = new Chart(canvasYtAudienceRetention<?php echo $influencer['id']; ?>, configYtAudienceRetention<?php echo $influencer['id']; ?>);

    YtAudienceRetentionChange<?php echo $influencer['id']; ?>(jQuery('#canvas-yt-audience-retention-page-<?php echo $influencer['id']; ?>'));
});
    
</script>
<?php else: ?>
<p><?php echo esc_html('Not available'); ?></p>
<?php endif; ?>