<html>
<head>
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  <script type='text/javascript' src='http://dev.trendingfamily.com/wp-content/themes/trending-family/assets/js/chart.min.js?ver=4.8.2'></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css" />
<script type='text/javascript' src="<?php echo plugins_url('trending-family/public/js/trending-family-public.js'); ?>" ></script>
<style>
@page {
	size: 8.5in 11in;
	margin: 0;
}
h1 {
    font-size:50px;
}
.section {
    padding:35px;
    height:11in;
    page-break-after: always;
}
.bg-green-diagonal {
    background:url(<?php echo plugins_url('trending-family/public/img/diagonal-green-white-bg-2.png'); ?>) no-repeat;
}
.text-white {
    color:#fff !important;
}
.bg-circle {
    background:url(<?php echo plugins_url('trending-family/public/img/circle.png'); ?>) no-repeat;
    background-position: center center;
    height:174px;
    width:174px;
}
table {
    width:100% !important;
}
table.center tr td {
    text-align:center;
    font-size: 34px;
}
hr {
    color: #49bda8;
}   

#client-logo {
    width: 420px;
    margin-top:500px;
    margin-right:0;
    float:right;
}
#tf-logo {
    width:430px;
    margin-top:120px;
    margin-right:0;
    float:right;
}
#report-generated-text {
    font-size:26px; 
    margin-left:280px;
    margin-top:-20px;
}

#campaign-img {
    width: 100%;
}
#campaign-overview, #campaign-results {
    margin-top: 50px;
}
.influencer-logo {
    width:200px;
    border: 3px solid #49bda8;
    padding:2px;
}

.chart {
    margin-top:70px;
    height:400px;
}

.chart .title {
    color:#49bda8;
    font-size:16px;
}
    
.chart .border {
    background:url('<?php echo plugins_url('trending-family/public/img/pdf-chart-bg-square.png?t='.time()); ?>;') no-repeat;
    width:691px;
    height:356px;
    padding:12px 19px;
}

.progress .dot {
    height:60px;
    width:20%;
    background: #47b397 url(<?php echo plugins_url('trending-family/public/img/green-bar.png'); ?>) no-repeat center right;
}
.progress .complete {
    height:60px;
    width:20%;
    background: #47b397 url(<?php echo plugins_url('trending-family/public/img/green-progress-dot-white.png?t=' . time()); ?>) no-repeat center right !important;
}
.progress .in-progress {
    background: #47b397 url(<?php echo plugins_url('trending-family/public/img/green-progress-dot-light-green.png?t=' . time()); ?>) no-repeat center right;
}
.progress .next-steps {
    background: #47b397 url(<?php echo plugins_url('trending-family/public/img/green-progress-dot-dark-green.png?t=' . time()); ?>) no-repeat center right;
}
.progress .content {
    background: #47b397;
    width:80%;
    color:#fff;
    text-align:left;
    font-size:22px;
    margin-left:10px;
}
.progress .header {
    background: #47b397;
    color:#fff;
    text-align:left;
    margin-left:10px;
    font-size:38px;
}
</style>
<script>
 window.chartColors = {
         red: 'rgba(101, 200, 181, 0.2)',
         orange: 'rgba(255, 159, 64, 0.2)',
         green: 'rgba(101, 200, 181, 0.2)',
         blue: 'rgba(48, 107, 151, 0.2)',
         grey: 'rgba(201, 203, 207, 0.2)'
 };
 </script>
</head>
<body style="margin:0 !important;padding:0 !important;">
<div class="container" >

            
    <?php 
    foreach($campaign['influencers'] as $influencer):
    foreach($influencer['contents'] as $content): ?>


	   <?php 
	   
	   $cmd = __DIR__ . '/../../../../../wkhtmltox/bin/wkhtmltoimage '
            . '--enable-javascript '
            . '--javascript-delay 5000 '
            . '--height 500 '
            . '--custom-header "User-Agent" "Mozilla/5.0 (iPad; CPU OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1" '
            . '--custom-header-propagation '
            . 'https://www.instagram.com/p/BcXzMbWlvSm/?taken-by=jasekz1  '
            . __DIR__ . '/../../../../tmp/test.png';
	   die($cmd);
	   exec($cmd, $out);dd($out);
	   $info = Embed\Embed::create($content['content']);
	   echo $info->code;
	   break;
	   ?>



	<?php endforeach;break; endforeach; ?>
</div>
</body>
</html>