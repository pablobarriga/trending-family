<?php 
$dates = [];
$impressions = [];
$engagement = [];
$accountNames = [];
$enabled = false;

$facebookPages = [];
if($influencer['social_media_accounts']) {
    foreach($influencer['social_media_accounts'] as $key => $val) {
        if(strtolower($val['platformName']) == 'facebook') {
            $facebookPages[] = $val;
        }
    }
}

if($facebookPages) {
    foreach($facebookPages as $page) {

        if( ! $page['social_media_data']) continue;

        foreach($page['social_media_data'] as $socialMediaData) {
            
            // impressions
            if($socialMediaData['field'] == 'impressions') {
            
                $impressionsArr = json_decode($socialMediaData['data']);        
                   
                foreach($impressionsArr as $date => $impression) {
                    $dates[$page['title']][] = date('m.d.Y', strtotime($date));
                    $impressions[$page['title']][] = $impression;
                }        
            }     
            
            // engagement
            if($socialMediaData['field'] == 'engagement') {
            
                $engagementArr = json_decode($socialMediaData['data']);        
                   
                foreach($engagementArr as $date => $eng) {                    
                    $engagement[$page['title']][] = $eng;
                }        
            }     
        }
        
        $enabled = true;
    }
}
?>


<?php if($enabled): ?>
<?php foreach($facebookPages as $facebookPage): ?>
<canvas class="exportable-chart" data-title="FaceBook Viewership by Impressions & Engagement" data-platform="facebook" data-account="<?php echo $facebookPage['title']; ?>" id="canvas-fb-impressions-engagement-<?php echo $facebookPage['id']; ?>"></canvas>


<script>

var configFbImpressionsEngagement<?php echo $facebookPage['id']; ?> = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "Impressions",
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            data: []
        }, {
            label: "Engagement",
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            data: [],
        }]
    },
    options: {
        responsive: true,
        title:{
            display:false,
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Dates'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Views'
                }
            }]
        }
    }
};

var dates<?php echo $facebookPage['id']; ?> = <?php echo json_encode($dates); ?>;
var impressions<?php echo $facebookPage['id']; ?> = <?php echo json_encode($impressions); ?>;
var engagement<?php echo $facebookPage['id']; ?> = <?php echo json_encode($engagement); ?>;

function fbImpressionsEngagementChange<?php echo $facebookPage['id']; ?>(obj) {
	var selected = obj.val();
	jQuery.each(impressions<?php echo $facebookPage['id']; ?>, function(k,v){
		
		if(k == '<?php echo $facebookPage['title']; ?>') {
			configFbImpressionsEngagement<?php echo $facebookPage['id']; ?>.data.datasets[0].data = v;
		}
	});
	jQuery.each(engagement<?php echo $facebookPage['id']; ?>, function(k,v){
		if(k == '<?php echo $facebookPage['title']; ?>') {
			configFbImpressionsEngagement<?php echo $facebookPage['id']; ?>.data.datasets[1].data = v;
		}
	});
	jQuery.each(dates<?php echo $facebookPage['id']; ?>, function(k,v){
		if(k == '<?php echo $facebookPage['title']; ?>') {
			configFbImpressionsEngagement<?php echo $facebookPage['id']; ?>.data.labels = v;
		}
	});

	canvasFbImpressionsEngagement<?php echo $facebookPage['id']; ?>.update();
}

jQuery(document).ready(function() {

	var canvasFbImpressionsEngagement<?php echo $facebookPage['id']; ?> = document.getElementById("canvas-fb-impressions-engagement-<?php echo $facebookPage['id']; ?>").getContext("2d");
    window.canvasFbImpressionsEngagement<?php echo $facebookPage['id']; ?> = new Chart(canvasFbImpressionsEngagement<?php echo $facebookPage['id']; ?>, configFbImpressionsEngagement<?php echo $facebookPage['id']; ?>);

    fbImpressionsEngagementChange<?php echo $facebookPage['id']; ?>(jQuery('#pages-fb-impressions-engagement-<?php echo $facebookPage['id']; ?>'));

});
    
</script> 
<?php endforeach; ?>
<?php endif; ?>

