<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
</head>
<body>
<div >
    <div class="header alert alert-info">
        <img src="http://dev.trendingfamily.com/wp-content/uploads/2017/03/logo.png" alt="Trending Family">
        <h3 >Campaign: <?php echo $campaign['title']; ?></h3>
        <h3 >Client: <?php echo $data['data']['client']['first_name'] . ' ' . $data['data']['client']['last_name']; ?></h3>
        <p>Report generated: <?php echo date('m/d/Y G:i:s'); ?></p>
    </div>
<?php if($campaign['influencers']):?>
	<?php foreach($campaign['influencers'] as $influencer): if( ! $influencer['pivot']['enable_influencer']) continue; ?>
		<h3><u>Influencer: <?php echo $influencer['display_name']; ?></u></h3>
		
		<?php if($influencer['social_media_accounts']):

            $consolidated = [];
            foreach($influencer['social_media_accounts'] as $account) {
                if($account['social_media_data']) {
                    foreach($account['social_media_data'] as $accountData){
                        
                        if( ! array_key_exists($accountData['field'], $dataMap[strtolower($account['platformName'])])) continue;
                        
                        if( isset($consolidated[$account['platformName']][$dataMap[strtolower($account['platformName'])][$accountData['field']]])) {
                            $consolidated[$account['platformName']][$dataMap[strtolower($account['platformName'])][$accountData['field']]] += ( $accountData['data'] ?: 0 );
                        }
                        
                        else {
                            $consolidated[$account['platformName']][$dataMap[strtolower($account['platformName'])][$accountData['field']]] = ( $accountData['data'] ?: 0 );
                        }
                        
                    }
                }
            }
        ?>
        
        
		    <?php if($consolidated): ?>
		        <div>Social Media Total Reach: <?php echo $influencer['total_reach']; ?></div>
	            <?php foreach($consolidated as $platformName => $platformData): ?>
	               <?php foreach($platformData as $field => $count): ?>
	                   <div><?php echo $platformName; ?> <?php echo $field; ?>: <?php echo $count; ?></div>
					<?php endforeach; ?>
				 <?php endforeach; ?>
			   <?php endif; ?>
        <?php else: ?>
        No social media accounts available.
        <?php endif; ?>
	<?php endforeach; ?>
<?php endif;?>

<?php if($campaign['project_timeline'] && $campaign['enable_timeline_section']): ?>
    <?php foreach($campaign['project_timeline'] as $timeline): if( ! $timeline['progress_entries']) continue; ?>
		<h3><u><?php echo $timeline['influencer'] ? $timeline['influencer']['display_name'] : 'Overall'; ?> Progress</u></h3>
		<table class="table">
		  <thead>
		      <th>#</th>
		      <th>Status</th>
		      <th>Comments</th>
		      <th>Date</th>
		  </thead>
		  <tbody>
		    <?php foreach($timeline['progress_entries'] as $key => $entry): ?>
			     <tr>
			         <td><?php echo $key+1; ?></td>
			         <td><?php echo ucwords($entry['status']); ?></td>
			         <td><?php echo $entry['title'] ?: 'Title Unavailable; '; ?></td>
				     <td><?php echo $entry['date'] ?: 'Date Unavailable'; ?></td>
                 </tr>
			<?php endforeach; ?>
		  </tbody>
	    </table>
	<?php endforeach; ?>
<?php endif; ?>
</div>
</body>
</html>