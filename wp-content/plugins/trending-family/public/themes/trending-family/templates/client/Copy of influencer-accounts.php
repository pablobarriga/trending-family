<?php if($influencer['social_media_accounts']):?>
<h3 class="offset-top-sm-3">Total Reach: <?php echo $influencer['total_reach']; ?></h3>
<div class="folowers-data-grid">
    <div class="flex-box">
	    <?php foreach($influencer['social_media_accounts'] as $account): ?>
		    <?php if($account['social_media_data']): ?>
	            <?php foreach($account['social_media_data'] as $accountData): 
	                  if( ! array_key_exists($accountData['field'], $dataMap[strtolower($account['platformName'])])) continue;?>
				    <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
						<div class="folower-item-box">
							<div class="num"><?php echo $accountData['data'] ?: 0; ?></div>
							<hr>
							<div class="desc">
							     <?php 
							     $icon = strtolower($account['platformName']); 
							     if( $icon == 'blog') {
							         $icon = 'commenting';
							     }
							     ?>
							     <i class="fa fa-<?php echo $icon; ?>"></i> 
							     <?php echo $dataMap[strtolower($account['platformName'])][$accountData['field']]; ?>
							</div>
						</div>
					</div>
				 <?php endforeach; ?>
			   <?php endif; ?>
		<?php endforeach; ?>
	</div>
</div>
<?php else: ?>
<p>No social media accounts available.</p>
<?php endif;?>