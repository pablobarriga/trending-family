<?php

if (! defined('ABSPATH')) exit();

get_header();
?>
<style>
.entry-header, .edit-link {
	display: none;
}
.feed-item {
    padding:0 5px 0 5px;
}
.owl-dot {
    margin-right:50px !important;
}
</style>
<script>
    var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var randomScalingFactor = function() {
        return Math.round(Math.random() * 30);
    };

    var ColorRed = "rgba(255, 100, 76, 0.9)";
    var ColorBlue = "rgba(48, 107, 151, 0.9)";
    var ColorGreen = "rgba(101, 200, 181, 0.9)";
</script>
<?php if( ! $campaign): ?>
<section class="section bg-green pad-md text-dark text-center-xs">
    <div class="container">
        <div class="row row-lg">
            <div class="flex-center">
                <div class="col-xs-12 p-title">
                    <h1>Campaign not found.</h1>
                    <a href="<?php echo $this->clientPage; ?>">View All Campaigns</a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<section class="section bg-green pad-md text-dark text-center-xs">
    <div class="container">
        <div class="row row-lg">
            <div class="flex-center">
                <div class="col-xs-12 p-title">
                    <h1><?php echo $campaign['title']; ?>                     
                    <?php if(isset($campaign['img']) && $campaign['img']): ?>
                    &nbsp;&nbsp;&nbsp;<img src="<?php echo $campaign['img']; ?>">
                    <?php endif; ?>
                    </h1>                    
                    <a href="<?php echo $this->clientPage; ?>">View All Campaigns</a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php 
include dirname(__FILE__) . '/influencers.php'; 
include dirname(__FILE__) . '/results.php'; 
include dirname(__FILE__) . '/timeline.php';
include dirname(__FILE__) . '/links.php';
?>


<?php if( $campaign && $campaign['enable_influencers_section'] && $campaign['influencers']): ?>
<section class="section bg-green text-center">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="btn-group">
					<a href="/wp-admin/admin-ajax.php?action=tf_api&api_action=export&format=pdf&campaign=<?php echo $campaign['id']; ?>" id="export-to-pdf" class="btn btn-default btn-lg st2">Download PDF</a>
					<a href="/wp-admin/admin-ajax.php?action=tf_api&api_action=export&format=csv&campaign=<?php echo $campaign['id']; ?>" class="btn btn-default btn-lg st2">Export to Excel</a>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<?php get_footer();