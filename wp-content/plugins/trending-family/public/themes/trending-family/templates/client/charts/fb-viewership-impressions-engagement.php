<?php 
$dates = [];
$impressions = [];
$engagement = [];
$accountNames = [];
$enabled = false;

$facebookPages = [];
if($influencer['social_media_accounts']) {
    foreach($influencer['social_media_accounts'] as $key => $val) {
        if(strtolower($val['platformName']) == 'facebook') {
            $facebookPages[] = $val;
        }
    }
}

if($facebookPages) {
    foreach($facebookPages as $page) {

        if( ! $page['social_media_data']) continue;

        foreach($page['social_media_data'] as $socialMediaData) {
            
            // impressions
            if($socialMediaData['field'] == 'impressions') {
            
                $impressionsArr = json_decode($socialMediaData['data']);        
                   
                foreach($impressionsArr as $date => $impression) {
                    $dates[$page['title']][] = date('m.d.Y', strtotime($date));
                    $impressions[$page['title']][] = $impression;
                }        
            }     
            
            // engagement
            if($socialMediaData['field'] == 'engagement') {
            
                $engagementArr = json_decode($socialMediaData['data']);        
                   
                foreach($engagementArr as $date => $eng) {                    
                    $engagement[$page['title']][] = $eng;
                }        
            }     
        }
        
        $accountNames[$page['title']] = $page['title'];
        $enabled = true;
    }
}
?>

<h3>FaceBook Viewership by Impressions & Engagement</h3>

<?php if($enabled): ?>
<canvas id="canvas-fb-impressions-engagement-<?php echo $influencer['id']; ?>"></canvas>

<script>

var configFbImpressionsEngagement<?php echo $influencer['id']; ?> = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "Impressions",
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            data: []
        }, {
            label: "Engagement",
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            data: [],
        }]
    },
    options: {
        responsive: true,
        title:{
            display:false,
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Dates'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Views'
                }
            }]
        }
    }
};

var dates<?php echo $influencer['id']; ?> = <?php echo json_encode($dates); ?>;
var impressions<?php echo $influencer['id']; ?> = <?php echo json_encode($impressions); ?>;
var engagement<?php echo $influencer['id']; ?> = <?php echo json_encode($engagement); ?>;

function fbImpressionsEngagementChange<?php echo $influencer['id']; ?>(obj) {
	var selected = obj.val();
	jQuery.each(impressions<?php echo $influencer['id']; ?>, function(k,v){
		if(k == selected) {
			configFbImpressionsEngagement<?php echo $influencer['id']; ?>.data.datasets[0].data = v;
		}
	});
	jQuery.each(engagement<?php echo $influencer['id']; ?>, function(k,v){
		if(k == selected) {
			configFbImpressionsEngagement<?php echo $influencer['id']; ?>.data.datasets[1].data = v;
		}
	});
	jQuery.each(dates<?php echo $influencer['id']; ?>, function(k,v){
		if(k == selected) {
			configFbImpressionsEngagement<?php echo $influencer['id']; ?>.data.labels = v;
		}
	});

	canvasFbImpressionsEngagement<?php echo $influencer['id']; ?>.update();
}

jQuery(document).ready(function() {

	jQuery('#canvas-fb-impressions-engagement-<?php echo $influencer['id']; ?>').before('<select id="pages-fb-impressions-engagement-<?php echo $influencer['id']; ?>" class="form-control" onChange="fbImpressionsEngagementChange<?php echo $influencer['id']; ?>(jQuery(this));"><?php foreach($accountNames as $name) echo '<option value="' . addslashes($name) . '">' . addslashes($name) . '</option>'; ?></select>');
	
    var canvasFbImpressionsEngagement<?php echo $influencer['id']; ?> = document.getElementById("canvas-fb-impressions-engagement-<?php echo $influencer['id']; ?>").getContext("2d");
    window.canvasFbImpressionsEngagement<?php echo $influencer['id']; ?> = new Chart(canvasFbImpressionsEngagement<?php echo $influencer['id']; ?>, configFbImpressionsEngagement<?php echo $influencer['id']; ?>);

    fbImpressionsEngagementChange<?php echo $influencer['id']; ?>(jQuery('#pages-fb-impressions-engagement-<?php echo $influencer['id']; ?>'));
});
    
</script> 

<?php else: ?>
<p><?php echo esc_html('Not available'); ?></p>
<?php endif; ?>