<?php if($campaign['enable_influencers_section']): ?>
<section class="section bg-grey bg-white-xs collapse-animation <?php echo $campaign['collapse_influencers_section_by_default'] ? 'collapse-section' : ''; ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 offset-bottom-sm-6 offset-bottom-xs-0 title-box">
				<h2>Influencers <a class="btn-collapse-section collapse-influencers" href="#">&nbsp;</a></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
			<?php if($campaign['influencers']):?>
				<div class="feature-controls-dots stl-2 bordered feature-controls-dots-1 flex-box"></div>
				<div class="feature-carousel feature-carousel-1 mobile-cont-active owl-carousel">
				<?php foreach($campaign['influencers'] as $influencer): if( ! $influencer['pivot']['enable_influencer']) continue; ?>
					<div class="feature-slide stl-2" data-dot='<i class="icon" style="background-image: url(<?php echo $influencer['img'] ? $influencer['img'] : plugins_url() . '/trending-family/public/img/user.png'; ?>);"><?php echo $influencer['id']; ?></i> <?php echo $influencer['display_name']; ?>'>
						<div class="top-subheader-img visible-xs">
							<img class="img-circle v2" src="<?php echo $influencer['img']; ?>">
							<div class="subhead-btn"><?php echo $influencer['display_name']; ?></div>
						</div>
						<div class="feature-slide-cont">
							<div class="feed-wrap">
								<div class="row">
									<div class="col-xs-12">										
                                        <?php 
                                        include dirname(__FILE__) . '/influencer-accounts.php'; 
                                        if($influencer['contents']) {
                                            include dirname(__FILE__) . '/influencer-contents.php'; 
                                            include dirname(__FILE__) . '/influencer-graphs.php';
                                        }
                                        
                                        else {
                                            echo '<p>No social media contents available.</p>';
                                        }
                                        ?>	
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				</div>
		    <?php else: ?>
		    <h1>There are no influencers assigned to this campaign.</h1>
		    <?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>