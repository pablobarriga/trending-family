<?php if($campaign['project_timeline'] && $campaign['enable_timeline_section']): ?>
<section class="section bg-blue collapse-animation <?php echo $campaign['collapse_timeline_section_by_default'] ? 'collapse-section' : ''; ?>">
	<div class="container">
		<div class="row offset-bottom-xs-7">
			<div class="col-xs-12 title-box">
				<h2>Project timeline <a class="btn-collapse-section" href="#">&nbsp;</a></h2>
				<hr>
			</div>
			<div class="col-md-4 col-md-offset-8 col-xs-12 data-markers-descript-wrap">
				<div class="row data-markers-descript">
					<div class="flex-center">
						<div class="col-xs-4">
							<div class="circle-mark mark-complete"></div>
							<span>Complete</span>
						</div>
						<div class="col-xs-4">
							<div class="circle-mark mark-progress"></div>
							<span>In Progress</span>
						</div>
						<div class="col-xs-4">
							<div class="circle-mark"></div>
							<span>Next Steps</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row data-carousels-box">
		    <?php foreach($campaign['project_timeline'] as $timeline): if( ! $timeline['progress_entries']) continue; ?>
			<div class="col-xs-12 offset-bottom-xs-5">
				<h4><?php echo $timeline['influencer'] ? $timeline['influencer']['display_name'] : 'Overall Progress'; ?></h4>
				<div class="data-carousel owl-carousel">
				    <?php foreach($timeline['progress_entries'] as $entry): ?>
					<div class="data-values mark-<?php echo $entry['status']; ?>">
						<p class="ttl"><?php echo $entry['title'] ?: 'Title Unavailable; '; ?></p>
						<p class="date"><?php echo $entry['date'] ?: 'Date Unavailable'; ?></p>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
<?php endif; ?>