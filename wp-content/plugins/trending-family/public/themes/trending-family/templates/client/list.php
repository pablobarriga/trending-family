<?php
if (! defined('ABSPATH')) exit();

get_header();
?>
<style>
.entry-header, .edit-link {
	display: none;
}

.feed-item {
	padding: 0 5px 0 5px;
}

.owl-dot {
	margin-right: 50px !important;
}
</style>
<section class="section bg-green pad-md text-dark text-center-xs">
    <div class="container">
        <div class="row row-lg">
            <div class="flex-center">
                <div class="col-sm-8 col-xs-12 p-title">
                    <h1>
                        Welcome <span class="txt-wlight"><?php echo $data['data']['client']['first_name'] . ' ' . $data['data']['client']['last_name']; ?></span>
                    </h1>
                </div>
                <div class="col-sm-4 col-xs-12 text-right-md">               
                    <?php if(isset($data['data']['client']['img']) && $data['data']['client']['img']): ?>
                    &nbsp;&nbsp;&nbsp;<img src="<?php echo $data['data']['client']['img']; ?>">
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section bg-grey pad-top-xxs-0">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php if($data['data']['client']['campaigns']): ?>
                <div class="s-title">
                    <h3>Your campaigns</h3>
                </div>
                <table class="table results-table table-with-hover">
                    <thead class="hidden-xs">
                        <tr>
                            <th>Campaign Name</th>
                            <th>Launch Date</th>
                            <th>Status</th>
                            <th>Results</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data['data']['client']['campaigns'] as $campaign): ?>
                        <tr onclick="window.location='?cpgn=<?php echo $campaign['id']; ?>'">
                            <td data-title="Campaign Name"><?php echo $campaign['title']; ?></td>
                            <td data-title="Launch Date"><?php echo $campaign['launch_date']; ?></td>
                            <td data-title="Status"><?php echo $data['data']['campaign_status_map'][$campaign['status']]; ?> <span class="status-marker"></span></td>
                            <td data-title="Results">                                
                                <?php if($campaign['status'] == 'complete'): ?>                                
					               <br><a href="/wp-admin/admin-ajax.php?action=tf_api&api_action=export&format=pdf&campaign=<?php echo $campaign['id']; ?>" ><i class="fa fa-file-pdf-o" style="margin-right:10px;"></i> PDF</a>
					               <br><a href="/wp-admin/admin-ajax.php?action=tf_api&api_action=export&format=csv&campaign=<?php echo $campaign['id']; ?>" ><i class="fa fa-file-excel-o" style="margin-right:10px;"></i> CSV/Excel</a>
				                <?php else: ?>
				                    <?php echo $campaign['results_overview'] ? $campaign['results_overview'] : 'TBD'; ?>
				                <?php endif; ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php else: ?>
                <div class="p-title pull-left">
                    <h3>You currently do not have any campaigns.</h3>
                </div>
                <?php endif; ?>
                    <a href="#" class="btn btn-success btn-add-social pull-right" id="start-new-campaign" onclick="jQuery('#start-new-campaign-form').toggle();return false;" >Start new campaign</a>
                    <div class="social-option-box " >
                        <div class="login-popup-box-wrap" id="start-new-campaign-form" style="top:-100px;right:185px;z-index:99;">
                            <div class="login-popup-box">
                                <form class="validateForm" id="connect_blog" method="POST" autocomplete="off">
                                    <input type="hidden" name="api_action" value="start_new_campaign">
                                    <p class="connect-error" style="display: none;"></p>
                                    <div class="row">
                                        <p>Please fill out the information below and submit.  Someone will contact you shortly.</p>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="campaign-name" placeholder="<?php echo esc_attr__('Campaign Name', 'trending-family'); ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <button class="btn btn-default start_campaign_form_api_submit" ><?php echo esc_html__( 'Submit', 'trending-family'); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer();