<?php if($campaign['results'] && $campaign['enable_results_section']): ?>
<section
    id="results"
    class="section bg-green">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="text-center offset-bottom-sm-5 text-white">Results</h2>
            </div>
        </div>
        <div class="row">
            <div class="flex-center">
                <?php foreach($campaign['results'] as $result): ?>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="circle-num-wrap">
                        <div class="cirle-num">
                            <span class="num"><?php echo $result['result']; ?></span> <span class="unit"><?php echo $result['unit']; ?></span>
                        </div>
                    </div>
                    <p class="circle-num-wrap-text tagline-lg"><?php echo $result['desc']; ?></p>
                </div>
                <?php endforeach; ?>
            </div>
            <!-- end of flex-center -->
        </div>
        <!-- end of row -->
    </div>
</section>
<?php endif; ?>