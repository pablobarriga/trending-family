<?php if($influencer['social_media_accounts']):


$consolidated = [];
foreach($influencer['social_media_accounts'] as $account) {
    if($account['social_media_data']) {
        foreach($account['social_media_data'] as $accountData){
            
            if( ! array_key_exists($accountData['field'], $dataMap[strtolower($account['platformName'])])) continue;
            
            $consolidated[$account['platformName']]['account']['url'] = $account['url'];
            
            if( isset($consolidated[$account['platformName']][$dataMap[strtolower($account['platformName'])][$accountData['field']]])) {
                $consolidated[$account['platformName']]['data'][$dataMap[strtolower($account['platformName'])][$accountData['field']]] += ( $accountData['data'] ?: 0 );
            }
            
            else {
                $consolidated[$account['platformName']]['data'][$dataMap[strtolower($account['platformName'])][$accountData['field']]] = ( $accountData['data'] ?: 0 );
            }
            
        }
    }
}

$exclude = ['Youtube' => ['Total Videos']];
if($consolidated) {
    foreach($consolidated as $platform => $fields) {
      
        foreach($fields['data'] as $field => $fieldData) {
            if( array_key_exists($platform, $exclude) && in_array($field, $exclude[$platform])) {
                unset($consolidated[$platform]['data'][$field]);
            }
        }
    }
}

?>

		
<h3 class="offset-top-sm-3">Total Reach: <?php echo tfRoundNumber($influencer['total_reach']); ?></h3>
<div class="folowers-data-grid">
    <div class="flex-box">
		    <?php if($consolidated): ?>
	            <?php foreach($consolidated as $platformName => $platformData): ?>
	               <?php foreach($platformData['data'] as $field => $count): ?>
				    <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
						<div class="folower-item-box" style="cursor:pointer;" onclick="TF.goToLink('<?php echo $platformData['account']['url']; ?>', '_blank');">
							<div class="num">
							<?php echo tfRoundNumber($count); ?></div>
							<hr>
							<div class="desc">
							     <?php 
							     $icon = strtolower($platformName); 
							     if( $icon == 'blog') {
							         $icon = 'commenting';
							     }
							     ?>
							     <i class="fa fa-<?php echo $icon; ?>"></i> 
							     <?php echo $field; ?>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				 <?php endforeach; ?>
			   <?php endif; ?>
	</div>
</div>
<?php else: ?>
<p>No social media accounts available.</p>
<?php endif;?>