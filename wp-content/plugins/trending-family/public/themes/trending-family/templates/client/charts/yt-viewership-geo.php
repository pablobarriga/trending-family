<?php
$countryviewsYTgeo = [];
$enabled = false;

$youtubeChannels = [];
if($influencer['social_media_accounts']) {
    foreach($influencer['social_media_accounts'] as $key => $val) {
        if(strtolower($val['platformName']) == 'youtube') {
            $youtubeChannels[] = $val;
        }
    }
}

if($youtubeChannels) {
    foreach($youtubeChannels as $channel) {
        
        if( ! $channel['social_media_data'] || ! $channel['connected']) continue;
        
        foreach($channel['social_media_data'] as $socialMediaData) {
            
            if(substr($socialMediaData['field'], 0, 4) != 'Loc:') continue;
             
            $loc = trim(str_replace('Loc:', '', $socialMediaData['field']));
            
            if( ! $countryviewsYTgeo[$channel['title']][$loc]) {
                $countryviewsYTgeo[$channel['title']][$loc] = 0;
            }
            
            $countryviewsYTgeo[$channel['title']][$loc] += $socialMediaData['data'];
            $enabled = true;
        }
    }
}
$countries = [];
$viewsYTgeo = [];

if($countryviewsYTgeo) {
    foreach($countryviewsYTgeo as $accountName => $account) {
        
        if($account) {
            foreach($account as $country => $count) {
                
                if( ! $country || ! $count) continue;
                
                $countries[$accountName][] = $country;
                $viewsYTgeo[$accountName][] = $count;
            }
        }        
    }
}
?>

<h3>YouTube Geographic viewsYTgeo by Country</h3>
 
<?php if($enabled): ?>
<canvas id="canvas-yt-geo-<?php echo $influencer['id']; ?>"></canvas>

<script>

var configYtGeo<?php echo $influencer['id']; ?> = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "viewsYTgeo",
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            data: [],
        }]
    },
    options: {
        responsive: true,
        title:{
            display:false,
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Date'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Time Watched/viewsYTgeo'
                }
            }]
        }
    }
};

var countries<?php echo $influencer['id']; ?> = <?php echo json_encode($countries); ?>;
var viewsYTgeo<?php echo $influencer['id']; ?> = <?php echo json_encode($viewsYTgeo); ?>;

function YtGeoChange<?php echo $influencer['id']; ?>(obj) {
	var selected = obj.val();
	jQuery.each(viewsYTgeo<?php echo $influencer['id']; ?>, function(k,v){
		if(k == selected) {
			configYtGeo<?php echo $influencer['id']; ?>.data.datasets[0].data = v;
		}
	});
	jQuery.each(countries<?php echo $influencer['id']; ?>, function(k,v){
		if(k == selected) {
			configYtGeo<?php echo $influencer['id']; ?>.data.labels = v;
		}
	});

	canvasYtGeo<?php echo $influencer['id']; ?>.update();
}

jQuery(document).ready(function() {

	jQuery('#canvas-yt-geo-<?php echo $influencer['id']; ?>').before('<select id="canvas-yt-geo-page-<?php echo $influencer['id']; ?>" class="form-control" onChange="YtGeoChange<?php echo $influencer['id']; ?>(jQuery(this));"><?php foreach($accountNames as $name) echo '<option value="' . $name . '">' . $name . '</option>'; ?></select>');
	
    var canvasYtGeo<?php echo $influencer['id']; ?> = document.getElementById("canvas-yt-geo-<?php echo $influencer['id']; ?>").getContext("2d");
    window.canvasYtGeo<?php echo $influencer['id']; ?> = new Chart(canvasYtGeo<?php echo $influencer['id']; ?>, configYtGeo<?php echo $influencer['id']; ?>);

    YtGeoChange<?php echo $influencer['id']; ?>(jQuery('#canvas-yt-geo-page-<?php echo $influencer['id']; ?>'));
});
    
</script>
<?php else: ?>
<p><?php echo esc_html('Not available'); ?></p>
<?php endif; ?>