
<script>
 window.chartColors = {
         red: 'rgba(101, 200, 181, 0.2)',
         orange: 'rgba(255, 159, 64, 0.2)',
         green: 'rgba(101, 200, 181, 0.2)',
         blue: 'rgba(48, 107, 151, 0.2)',
         grey: 'rgba(201, 203, 207, 0.2)'
 };
 </script>
 
 <style>
 .show {
    display: inherit !important;
 }
 </style>

<ul class="nav nav-tabs">

	<li class="nav-item active"><a href="#nav-fb-viewership-gender-age-group-<?php echo $influencer['id']; ?>"
		class="nav-link " id="nav-fb-viewership-gender-age-group-tab-<?php echo $influencer['id']; ?>"
		data-toggle="tab" role="tab"
		aria-controls="nav-fb-viewership-gender-age-group-<?php echo $influencer['id']; ?>"
		aria-expanded="true"><i class="fa fa-facebook"></i> Gender/Age</a></li>
		
	<li class="nav-item"><a href="#nav-fb-viewership-impressions-engagement-<?php echo $influencer['id']; ?>"
		class="nav-link" id="nav-fb-viewership-impressions-engagement-tab-<?php echo $influencer['id']; ?>"
		data-toggle="tab" role="tab"
		aria-controls="nav-fb-viewership-impressions-engagement-<?php echo $influencer['id']; ?>"
		aria-expanded="true"><i
			class="fa fa-facebook"></i> Engagement</a></li>
			
	<li class="nav-item"><a href="#nav-tw-viewership-<?php echo $influencer['id']; ?>"
		class="nav-link" id="nav-tw-viewership-tab-<?php echo $influencer['id']; ?>"
		data-toggle="tab" role="tab"
		aria-controls="nav-tw-viewership-<?php echo $influencer['id']; ?>"
		aria-expanded="true"><i
			class="fa fa-twitter"></i> Tweets</a></li>
			
	<li class="nav-item"><a href="#nav-yt-viewership-audience-retention-<?php echo $influencer['id']; ?>"
		class="nav-link" id="nav-yt-viewership-audience-retention-tab-<?php echo $influencer['id']; ?>"
		data-toggle="tab" role="tab"
		aria-controls="nav-yt-viewership-audience-retention-<?php echo $influencer['id']; ?>"
		aria-expanded="true"><i
			class="fa fa-youtube"></i> Audience Retention</a></li>
			
	<li class="nav-item"><a href="#nav-yt-viewership-geo-<?php echo $influencer['id']; ?>"
		class="nav-link" id="nav-yt-viewership-geo-tab-<?php echo $influencer['id']; ?>"
		data-toggle="tab" role="tab"
		aria-controls="nav-yt-viewership-geo-<?php echo $influencer['id']; ?>"
		aria-expanded="true"><i
			class="fa fa-youtube"></i> Geo</a></li>
</ul>

<div class="tab-content" id="nav-tabContent-<?php echo $influencer['id']; ?>">

	<div class="tab-pane active"
		id="nav-fb-viewership-gender-age-group-<?php echo $influencer['id']; ?>" role="tabpanel"
		aria-labelledby="nav-fb-viewership-gender-age-group-tab-<?php echo $influencer['id']; ?>">
    	<?php include( 'charts/fb-viewership-gender-age-group.php' );?>
    </div>
    
	<div class="tab-pane fade"
		id="nav-fb-viewership-impressions-engagement-<?php echo $influencer['id']; ?>" role="tabpanel"
		aria-labelledby="nav-fb-viewership-impressions-engagement-ta-<?php echo $influencer['id']; ?>b">
    	<?php include( 'charts/fb-viewership-impressions-engagement.php' );?>
    </div>
    
	<div class="tab-pane fade"
		id="nav-tw-viewership-<?php echo $influencer['id']; ?>" role="tabpanel"
		aria-labelledby="nav-tw-viewership-tab-<?php echo $influencer['id']; ?>">
    	<?php include( 'charts/tw-viewership.php' );?>
    </div>
    
	<div class="tab-pane fade"
		id="nav-yt-viewership-audience-retention-<?php echo $influencer['id']; ?>" role="tabpanel"
		aria-labelledby="nav-yt-viewership-audience-retention-tab-<?php echo $influencer['id']; ?>">
    	<?php include( 'charts/yt-viewership-audience-retention.php' );?>
    </div>
    
	<div class="tab-pane fade"
		id="nav-yt-viewership-geo-<?php echo $influencer['id']; ?>" role="tabpanel"
		aria-labelledby="nav-yt-viewership-geo-tab-<?php echo $influencer['id']; ?>">
    	<?php include( 'charts/yt-viewership-geo.php' );?>
    </div>

    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>

<!-- Hidden duplicate graphs used for exports -->
<div style="position:absolute;top:0;left:-9999px;">
    <?php include(__DIR__ . '/../pdf/charts/fb-viewership-impressions-engagement.php'); ?>
</div>