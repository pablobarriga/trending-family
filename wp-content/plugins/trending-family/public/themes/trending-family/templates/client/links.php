<?php if($campaign['project_links'] && $campaign['enable_project_links_section']): ?>
<section class="section bg-grey collapse-animation ">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 title-box">
				<h2>Project Links </h2>
				<hr>
			</div>
		</div>
		<div class="row data-carousels-box">
		    <?php foreach($campaign['project_links'] as $link):  ?>
			<div class="col-xs-12 offset-bottom-xs-5">
				<a href="<?php echo $link['link']; ?>" target="_blank" ><h4><?php echo $link['title']; ?></h4></a>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
<?php endif; ?>