
<hr><br>
<div class="feed-grid offset-bottom-xs-9" data-packery='{ "itemSelector": ".feed-item", "gutter": 25 }'>
    <?php foreach($influencer['contents'] as $content): ?>
	<div class="feed-item">
	   <?php 
	   $info = Embed\Embed::create($content['content']);
	   echo $info->code; 
	   ?>
	</div>
	<?php endforeach; ?>
</div>