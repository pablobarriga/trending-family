<?php 
$ageGroups = [];
$maleCount = [];
$femaleCount = [];
$unknownCount = [];
$accountNames = [];
$ageGroupData = [];
$enabled = false;

$facebookPages = [];
if($influencer['social_media_accounts']) {
    foreach($influencer['social_media_accounts'] as $key => $val) {
        if(strtolower($val['platformName']) == 'facebook') {
            $facebookPages[] = $val;
        }
    }
}

if($facebookPages) {
    foreach($facebookPages as $page) {
        
        if( ! $page['social_media_data']) continue;
        
        foreach($page['social_media_data'] as $socialMediaData) {
            $fieldParts = explode('_', $socialMediaData['field']);
            
            $gender = array_shift($fieldParts);
            if( ! in_array($gender, ['Male', 'Female', 'Unknown']) || ! $fieldParts) continue;
            
            $ageGroup = array_shift($fieldParts);
            
            $ageGroupData[$page['title']][$ageGroup][$gender] = $socialMediaData['data'];            
        }
        
        ksort($ageGroupData[$page['title']]);
        
        foreach($ageGroupData[$page['title']] as $age_group => $genders) {            
            $ageGroups[$page['title']][] = $age_group;
            $maleCount[$page['title']][] = isset($genders['Male']) ? (int) $genders['Male'] : 0;
            $femaleCount[$page['title']][] = isset($genders['Female']) ? (int) $genders['Female'] : 0;
            $unknownCount[$page['title']][] = isset($genders['Unknown']) ? (int) $genders['Unknown'] : 0;
            
            $enabled = true;
        }
                    
        ksort($ageGroups[$page['title']]);
                
        $accountNames[$page['title']] = $page['title'];

    }
}
?>

<h3>FaceBook Viewership by Gender & Age Group</h3>

<?php if($enabled): ?>
<canvas id="canvas-fb-male-female-<?php echo $influencer['id']; ?>"></canvas> 

<script>

var configFbMaleFemale<?php echo $influencer['id']; ?> = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "Male",
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            data: []
        }, {
            label: "Female",
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            data: [],
        }, {
            label: "Unknown",
            backgroundColor: window.chartColors.orange,
            borderColor: window.chartColors.orange,
            data: [],
        }]
    },
    options: {
        responsive: true,
        title:{
            display:false,
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Age Group'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Views'
                }
            }]
        }
    }
};

var ageGroups<?php echo $influencer['id']; ?> = <?php echo json_encode($ageGroups); ?>;
var maleCount<?php echo $influencer['id']; ?> = <?php echo json_encode($maleCount); ?>;
var femaleCount<?php echo $influencer['id']; ?> = <?php echo json_encode($femaleCount); ?>;
var unknownCount<?php echo $influencer['id']; ?> = <?php echo json_encode($unknownCount); ?>;

function fbMaleFemaleChange<?php echo $influencer['id']; ?>(obj) {
	var selected = obj.val();
	jQuery.each(maleCount<?php echo $influencer['id']; ?>, function(k,v){
		if(k == selected) {
			configFbMaleFemale<?php echo $influencer['id']; ?>.data.datasets[0].data = v;
		}
	});
	jQuery.each(femaleCount<?php echo $influencer['id']; ?>, function(k,v){
		if(k == selected) {
			configFbMaleFemale<?php echo $influencer['id']; ?>.data.datasets[1].data = v;
		}
	});
	jQuery.each(unknownCount<?php echo $influencer['id']; ?>, function(k,v){
		if(k == selected) {
			configFbMaleFemale<?php echo $influencer['id']; ?>.data.datasets[2].data = v;
		}
	});
	jQuery.each(ageGroups<?php echo $influencer['id']; ?>, function(k,v){
		if(k == selected) {
			configFbMaleFemale<?php echo $influencer['id']; ?>.data.labels = v;
		}
	});

	canvasFbMaleFemale<?php echo $influencer['id']; ?>.update();
}

jQuery(document).ready(function() {

	jQuery('#canvas-fb-male-female-<?php echo $influencer['id']; ?>').before('<select id="canvas-fb-male-female-page-<?php echo $influencer['id']; ?>" class="form-control" onChange="fbMaleFemaleChange<?php echo $influencer['id']; ?>(jQuery(this));"><?php foreach($accountNames as $name) echo '<option value="' . addslashes($name) . '">' . addslashes($name) . '</option>'; ?></select>');
	
    var canvasFbMaleFemale<?php echo $influencer['id']; ?> = document.getElementById("canvas-fb-male-female-<?php echo $influencer['id']; ?>").getContext("2d");
    window.canvasFbMaleFemale<?php echo $influencer['id']; ?> = new Chart(canvasFbMaleFemale<?php echo $influencer['id']; ?>, configFbMaleFemale<?php echo $influencer['id']; ?>);

    fbMaleFemaleChange<?php echo $influencer['id']; ?>(jQuery('#canvas-fb-male-female-page-<?php echo $influencer['id']; ?>'));
});
    
</script>
<?php else: ?>
<p><?php echo esc_html('Not available'); ?></p>
<?php endif; ?>