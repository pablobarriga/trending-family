<?php
if (! defined('ABSPATH')) exit();

get_header();
?>
<style>
.entry-header, .edit-link {
	display: none;
}

.feed-item {
	padding: 0 5px 0 5px;
}

.owl-dot {
	margin-right: 50px !important;
}
</style>

<section>
    <div class="green-full-centered flex-center-full">
        <div class="reset-password-box">
            <p class="login-error-alt">
                <?php if($_GET['reset'] == 1): ?>
                An email with reset instructions has been sent.
                <?php elseif($_GET['reset'] == 2): ?>
                We could not find an account with that email.
                <?php endif; ?>
            </p>
            <br>
            <br>
            <h2>Lost Your Password</h2>
            <p class="lost-password-description">
                Please enter your email address. <br> You will receive a link to create a new password via email.
            </p>
            <form
                method="POST">
                <input
                    type="text"
                    placeholder="Email Address"
                    name="email">
                <button
                    type="submit"
                    class="btn btn-default btn-lg">SUBMIT</button>
            </form>
        </div>
    </div>
</section>


<?php get_footer(); ?>