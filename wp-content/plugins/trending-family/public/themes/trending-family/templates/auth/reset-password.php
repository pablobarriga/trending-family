<?php
if (! defined('ABSPATH')) exit();

get_header();
?>
<style>
.entry-header, .edit-link {
	display: none;
}

.feed-item {
	padding: 0 5px 0 5px;
}

.owl-dot {
	margin-right: 50px !important;
}
</style>

<section>
    <div class="green-full-centered flex-center-full">
        <div class="reset-password-box">
            <p class="login-error-alt">
                <?php if($_GET['reset'] == 1): ?>
                Your password has been successfully reset.
                <?php elseif($_GET['reset'] == 2): ?>
                There was a problem resetting your password.  Please ensure that:
                <br>
                1. email is correct<br>
                2. passwords match<br>
                3. password is at least 6 characters
                <?php endif; ?>
            </p>
            <br>
            <br>
            <h2>Reset Password</h2>
            <p class="lost-password-description">
                Please enter your email address and new password.
            </p>
            <form
                method="POST">
                <input type="hidden" name="token" value="<?php echo $_GET['t']; ?>" >
                <input
                    type="text"
                    placeholder="Email Address"
                    name="email">
                <input
                    type="text"
                    placeholder="Type in new password"
                    name="pass">
                <input
                    type="text"
                    placeholder="Type in new password again"
                    name="pass2">
                <button
                    type="submit"
                    class="btn btn-default btn-lg">SUBMIT</button>
            </form>
        </div>
    </div>
</section>

<?php get_footer(); ?>