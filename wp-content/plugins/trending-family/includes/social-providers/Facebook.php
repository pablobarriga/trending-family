<?php 
namespace Includes\SocialProviders;

use GuzzleHttp\Client as GuzzleClient;
use Facebook\Facebook as FacebookClient;
use Facebook\Authentication\AccessToken;
use Exception;

class Facebook extends UserSocialAccount {
    
    /**
     * Api version
     * 
     * @var string
     */
    private $fbApiVersion = 'v2.10';
    
    /**
     * Facebook instance
     * 
     * @var \Facebook\Facebook
     */
    private $service = null;
    
    /**
     * Constructor
     * 
     * @throws Exception
     */
    public function __construct()
    {
        try {
            $this->service = new FacebookClient([
                'app_id' => env('FB_APP_ID'),
                'app_secret' => env('FB_APP_SECRET'),
                'default_graph_version' => $this->fbApiVersion,
            ]);
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }
    
    /**
     * Get authentication url
     *
     * @throws Exception
     */
    public function getAuthUrl()
    {
        try {            
            $helper = $this->service->getRedirectLoginHelper();            
            return $helper->getLoginUrl($this->getAuthCallbackUrl() . '/?service=facebook', ['read_insights','pages_show_list','public_profile','email']);
       }
        
        catch (Exception $e) {
            throw $e;
        }
    } 
    
    /**
     * Create token from provided code; refresh token if needed
     * https://developers.facebook.com/docs/php/howto/example_facebook_login
     *
     * @throws Exception
     */
    public function getAccessData()
    {
        try {            
            $helper = $this->service->getRedirectLoginHelper();            
            $accessToken = $helper->getAccessToken();
          
            if (! isset($accessToken)) {
                
                if ($helper->getError()) {
                    throw new Exception($helper->getErrorReason());
                } 
                
                else {
                    throw new Exception('Bad Facebook request');
                }
            }
           
            if (! $accessToken->isLongLived()) {
                
                $oAuth2Client = $this->service->getOAuth2Client();
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            }
            
            $response = $this->service->get('/me?fields=id', $accessToken->getValue());
            
            return [
                'account_id' => $response->getGraphUser()->getId(), 
                'access_token' => $accessToken->getValue(),               
            ];
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }   
    
    /**
     * Get Facebook pages
     * 
     * @return array
     */
    public function getPageIds($accessToken, $accountId)
    {
        try { 
            $response = $this->service->get($accountId . '/accounts?fields=id,is_published', $accessToken);

            $pages = $response->getGraphEdge();
            $pageIds = [];
            
            if( ! $pages) {
                return $pageIds;
            }
                 
            foreach ($pages as $page) {    
                
                if( ! $page->getField('is_published')) continue;
                            
               $pageResponse = $this->service->get($page->getField('id') . '/?fields=id', $accessToken);
               $fbPage = json_decode($pageResponse->getBody(), true);
                
                $pageIds[] = $fbPage['id'];                
            }
            
            return $pageIds;
        }

        catch (Exception $e) { 
            throw $e;
        }
    }
}