<?php 
namespace Includes\SocialProviders;

use Exception;

class Blog extends \SocialMedia\UserSocialAccount {    
    
    /**
     *  instance
     * 
     * @var 
     */
    private $service = null;
    
    /**
     * Get accounts 
     * 
     * @throws Exception
     * @return array
     */
    public function accounts()
    {
        try {

            return get_user_meta( $this->user->ID, 'influencer_blogs', true);
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }
    
    public static function allCachedData()
    {
        $users = get_users();
        
        if( ! $users) return false;
        
        $data = [];
        foreach($users as $user) {
            
            $bl = new Blog($user);
            $res = $bl->accounts();
            
            if( ! $res) continue;
            
            foreach($res as $blog) {
                
                $data[$user->ID][] = [
                    'owner' => $user->user_email,
                    'Link' => $blog[0],
                    'Unique Monthly Visitors' => $blog[1],
                    'Monthly Pageviews' => $blog[2],
                ];
            }
        }
        
        
        return $data;
    }
}