<?php
namespace Includes\SocialProviders;

class UserSocialAccount {

    /**
     * User instance
     *
     * @var WP_User
     */
    protected $user;

    /**
     * Constructor
     *
     * @param WP_User $user            
     */
    public function __construct(\WP_User $user = null)
    {
        $this->user = $user;
    }
    
    /**
     * Delete user data
     * 
     * @param string $key
     */
    public function deleteCachedData($key)
    {
        delete_user_meta($this->user->ID, $key .'_data');
    }
    
    /**
     * Set user data
     * 
     * @param string $key
     * @param array $data
     */
    public function cacheData($key, $data = [], $noMerge = false)
    {        
        
        $userData = get_user_meta($this->user->ID, $key . '_data');
        
        if(! $noMerge && $userData && isset($userData[0])) {
            $userData= array_merge($userData[0], $data);
        }
        
        else {
            $userData = $data;
        }
       
        update_user_meta($this->user->ID, $key . '_data', $userData);
    }
    
    /**
     * Get user data
     * 
     * @param string $key
     */
    public function cachedData($key = null)
    {
        $userData = get_user_meta($this->user->ID, $key ? $key . '_data' : null);
        
        if( ! $userData) return false;
        
        if( $key && $userData ) {
            $userData = $userData[0];
        }
//         if( $key == 'google_access_token') {
//             $temp = json_decode($userData['jasekz1@gmail.com'], true);
//             $temp['access_token'] .= 'breakit';
//             $userData['jasekz1@gmail.com'] = json_encode($temp);
//         }

//         foreach($userData as $key => $val) {
//             if( ! $val) unset($userData[$key]);
//         }

        return $userData;
    }
    
    public function getAuthCallbackUrl()
    {
        return filter_var(home_url('/oauth-callback'), FILTER_SANITIZE_URL);
    }
    
    public static function getAllInfluencers()
    {
        return get_users(['role' => ['influencer'], 'include' => [4]]);
    }
}