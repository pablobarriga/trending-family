<?php 
namespace Includes\SocialProviders;

use Exception;

class Youtube extends Google {
    
    /**
     * Youtube instance
     * 
     * @var Google_Service_YouTube
     */
    public $client = null;
    
    /**
     * Constructor
     * 
     * @param \WP_User $user
     * @throws Exception
     */
    public function __construct()
    {
        try {            
            parent::__construct();

            $this->client = new \Google_Service_YouTube($this->google);
            $this->client->reporting = new \Google_Service_YouTubeAnalytics( $this->google );
        }
        
        catch (Exception $e) {
            throw $e;
        }    
    }

    
    /**
     * Get Youtube channels for specified account
     * 
     * @param $email - account email
     * @return array
     */
    public function getChannels($accessToken)
    {                
        try {
            
            $this->google->setAccessToken($accessToken);  
            $channels = $this->client->channels->listChannels('snippet,contentDetails,statistics', ['mine' => true]);
            
            if( ! $channels) {
                return false;
            }
            
            $data = [];
            foreach($channels as $channel) {
                $data[] = $channel['id'];
            }
            
            return $data;
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }
}