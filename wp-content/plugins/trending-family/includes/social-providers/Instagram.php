<?php 
namespace Includes\SocialProviders;

use GuzzleHttp\Client as GuzzleClient;
use Facebook\Facebook as FacebookClient;
use Exception;

class Instagram extends UserSocialAccount {   
    
    /**
     *  Instagram client instance
     * 
     * @var 
     */
    private $service = null;
    
    /**
     * Get Instagram pages
     * 
     * @return array
     */
    public function pages()
    {
        try {   
            
            $data = [];
            $tokens = $this->getAccessTokens();
            
            if( ! $tokens) {
                return $data;
            }
            
            $cached = $this->cachedData('instagram_pages');
            
            foreach($tokens as $usernameKey => $token) {
                
                /*
                 * FIELDS:
                 * 
                 */
                    
                $userinfo = @json_decode(file_get_contents('https://api.instagram.com/v1/users/self/?access_token=' . $token));
                
                if( $userinfo) {
                
                    $accountData[$usernameKey] = [
                        'owner' => $userinfo->data->username,
                        'is_valid' => 1,
                        'id' => $userinfo->data->id,
                        'access_token'  => $user->access_token,
                        'username' => $userinfo->data->username,
                        'fullName' => $userinfo->data->full_name,
                        'profilePicUrl' => $userinfo->data->profile_picture,
                        'followedByCount' => $userinfo->data->counts->followed_by,
                        'followsCount'  => $userinfo->data->counts->follows,
                        'mediaCount' => $userinfo->data->counts->media
                    ];
                    
                    $accountData[$usernameKey]['Link'] = 'https://www.instagram.com/' . $userinfo->data->username;
                    $accountData[$usernameKey]['Followers'] = $userinfo->data->counts->followed_by;
                    $accountData[$usernameKey]['Engagements/Post Last Week'] = '';
                    $accountData[$usernameKey]['Impressions/Post Last Week'] = '';
                    $accountData[$usernameKey]['Clicks to Site/Post Last Week'] = '';
                    
                    $data = array_merge($data, $accountData);
                    
                }
                
                else {                    
                    
                    if(isset($cached[$usernameKey])) {
                        
                        $data[$usernameKey] = $cached[$usernameKey];
                        $data[$usernameKey]['is_valid'] = 0;
                    }
                }
            }
           
            return $data;
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }
    
    /**
     * Create token from provided code and save in db; refresh token if needed
     * 
     * @param array $code
     * @throws Exception
     */
    public function getAccessData($code)
    {      
        try {
        
            $apiData = array(            
                'client_id'       => env('INSTA_CLIENT_ID'),            
                'client_secret'   => env('INSTA_CLIENT_SECRET'),            
                'grant_type'      => 'authorization_code',           
                'redirect_uri'    => $this->getAuthCallbackUrl() . '/?service=instagram',            
                'code'            => $code            
            );
            
            $apiHost = 'https://api.instagram.com/oauth/access_token';            
            
            $ch = curl_init();            
            curl_setopt($ch, CURLOPT_URL, $apiHost);            
            curl_setopt($ch, CURLOPT_POST, count($apiData));            
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($apiData));            
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));            
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);            
            $jsonData = curl_exec($ch);            
            curl_close($ch);
            dd($jsonData);
            $data = @json_decode($jsonData);       
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }
    
    /**
     * Get authentication url
     * 
     * @throws Exception
     */
    public function getAuthUrl()
    {
        try {
            return 'https://api.instagram.com/oauth/authorize?client_id=' . env('INSTA_CLIENT_ID') . '&response_type=code&redirect_uri=' . $this->getAuthCallbackUrl() . '/?service=instagram';
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }
}