<?php 
namespace Includes\SocialProviders;

use Abraham\TwitterOAuth\TwitterOAuth;
use Exception;

class Twitter extends UserSocialAccount { 
    
    public function getAccessData($token, $verifier)
    {
        try {         
            $service = new TwitterOAuth(env('TW_KEY'), env('TW_SECRET'));
            $request_token = $service->oauth('oauth/request_token', ['oauth_callback' => $this->getAuthCallbackUrl() . '/?service=twitter']);
            $connection = new TwitterOAuth( env('TW_KEY'), env('TW_SECRET'), $token, $request_token['oauth_token_secret']);
            return $connection->oauth("oauth/access_token", ["oauth_verifier" => $verifier]);
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }
    
    /**
     * Get authentication url
     *
     * @throws Exception
     */
    public function getAuthUrl()
    {
        try {
            $service = new TwitterOAuth(env('TW_KEY'), env('TW_SECRET'));
            $request_token = $service->oauth('oauth/request_token', ['oauth_callback' => $this->getAuthCallbackUrl() . '/?service=twitter']);
            return $service->url('oauth/authorize', ['oauth_token' => $request_token['oauth_token']]);
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }
}