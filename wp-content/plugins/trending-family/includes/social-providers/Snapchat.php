<?php 
namespace Includes\SocialProviders;

use Exception;

class Snapchat extends \SocialMedia\UserSocialAccount {    
    
    /**
     *  instance
     * 
     * @var 
     */
    private $service = null;
    
    /**
     * Get accounts 
     * 
     * @throws Exception
     * @return array
     */
    public function accounts()
    {
        try {

            return get_user_meta( $this->user->ID, 'influencer_snapchats', true);
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }
    
    public static function allCachedData()
    {
        $users = get_users();
        
        if( ! $users) return false;
        
        $data = [];
        foreach($users as $user) {
            
            $sn = new Snapchat($user);
            $res = $sn->accounts();
            
            if( ! $res) continue;
            
            $data[$user->ID] = $res;
        }
        
        return $data;
    }
}