<?php 
namespace Includes\SocialProviders;

use Exception;

class Pinterest extends \SocialMedia\UserSocialAccount {    

    /**
     * App id
     * 
     * @var string
     */
    private $appId = '4920375803813836819';
    
    /**
     * App secret
     * 
     * @var string
     */
    private $appSecret = '6c3bb7c14384dbd05f6ac8338f9785a76647b53d9e771eb72ef547dcf6a24e24';
    
    /**
     *  instance
     * 
     * @var 
     */
    private $service = null;
    
    /**
     * Get accounts 
     * 
     * @throws Exception
     * @return array
     */
    public function accounts()
    {
        try {

            return get_user_meta($this->user->ID , 'pint_accounts', true);
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }
    
    /**
     * Create token from provided code and save in db; refresh token if needed
     * 
     * @param array $code
     * @throws Exception
     */
    public function saveAccessToken($code)
    {      
        try {
        dd($code);
            $apiData = array(            
                'client_id'       => $this->clientId,            
                'client_secret'   => $this->clientSecret,            
                'grant_type'      => 'authorization_code',           
                'redirect_uri'    => $this->getAuthCallbackUrl() . '/?service=instagram',            
                'code'            => $code            
            );
            
            $apiHost = 'https://api.instagram.com/oauth/access_token';            
            
            $ch = curl_init();            
            curl_setopt($ch, CURLOPT_URL, $apiHost);            
            curl_setopt($ch, CURLOPT_POST, count($apiData));            
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($apiData));            
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));            
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);            
            $jsonData = curl_exec($ch);            
            curl_close($ch);
            
            $data = @json_decode($jsonData);       
            $this->cacheData('instagram_access_token', [$data->user->username => $data->access_token]);
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }
    
    /**
     * Get authentication url
     * 
     * @throws Exception
     */
    public function getAuthUrl()
    {
        try {
            return 'https://api.pinterest.com/oauth/?client_id=' . $this->appId . '&response_type=code&scope=read_public,read_relationships&redirect_uri=' . $this->getAuthCallbackUrl() . '/?service=pinterest';
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }
}