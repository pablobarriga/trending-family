<?php 
namespace Includes\SocialProviders;

class Google extends UserSocialAccount { 
         
    /**
     * Google instance
     * 
     * @var Google_Client
     */
    public $google;
    
    /**
     * Constructor 
     * 
     * @param \WP_User $user
     * @throws Exception
     */
    public function __construct()
    {
        try {            
            $this->google = new \Google_Client();
            $this->google->setClientId( env('GOOGLE_OAUTH_ID') );
            $this->google->setClientSecret( env('GOOGLE_OAUTH_SECRET') );
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }
    
    /**
     * Create token from provided code and save in db; refresh token if needed
     * 
     * @param array $code
     * @throws Exception
     */
    public function getAccessData($code)
    {
        try {
            $this->google->setAccessType("offline");
            $this->google->setRedirectUri($this->getAuthCallbackUrl());
            $this->google->setApprovalPrompt('force');
           
            $accessToken = $this->google->fetchAccessTokenWithAuthCode($code);
            $this->google->setAccessToken($accessToken);
            
            // Refresh the token if it's expired.
            if ($this->google->isAccessTokenExpired()) {
                $this->google->fetchAccessTokenWithRefreshToken($this->google->getRefreshToken());
                $accessToken = $this->google->getAccessToken();
            }
            
            $people = new \Google_Service_People($this->google);
            $email = $people->people->get('people/me', ['requestMask.includeField'=>'person.email_addresses'])->emailAddresses[0]->value;

            return [
                'account_email' => $email,
                'access_token' => json_encode($accessToken),
            ];
        }
        
        catch (Exception $e) {
            dd($e);
            throw $e;
        }
    }
    
    /**
     * Get authentication url
     * 
     * @throws Exception
     */
    public function getAuthUrl()
    {
        try {
            $this->google->setScopes(array(
                'profile',
                'email',
                //'https://www.googleapis.com/auth/youtube',
                //'https://www.googleapis.com/auth/yt-analytics-monetary.readonly',
                'https://www.googleapis.com/auth/youtube.readonly',
                'https://www.googleapis.com/auth/yt-analytics.readonly'
            ));
            $this->google->setAccessType("offline");
            $this->google->setRedirectUri($this->getAuthCallbackUrl());
            $this->google->setApprovalPrompt('force');
            
            $state = mt_rand();
//             $this->google->setState($state);
//             $_SESSION['google_auth_state'] = $state;
            return $this->google->createAuthUrl();
        }
        
        catch (Exception $e) {
            throw $e;
        }
    }
}