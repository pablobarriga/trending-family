<?php

/**
 * Fired during plugin activation
 *
 * @link       elegrit.com
 * @since      1.0.0
 *
 * @package    Trending_Family
 * @subpackage Trending_Family/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Trending_Family
 * @subpackage Trending_Family/includes
 * @author     Zbigniew Jasek <zig@elegrit.com>
 */
class Trending_Family_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
