<?php

/*
 * Template for displaying search form
 */
?>
<form action="<?php echo esc_url(home_url( '/' )); ?>" method="get">
	<div class="input-group search">
		<span class="input-group-btn"><i class="fa fa-search"></i></span>
		<input type="text" class="form-control" name="s" placeholder="<?php echo esc_attr__('Search', 'trending-family'); ?>" value="<?php echo get_search_query(); ?>" title="<?php echo esc_attr_x( 'Search for:', 'label', 'trending-family' ) ?>">
	</div>
</form>
