<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package trending-family
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
} ?>

<div class="col-sm-5 col-md-4 col-sm-offset-0 col-md-offset-1 offset-top-md-3 sidebar">
    <?php if( is_active_sidebar( 'top-sidebar' ) ){ ?>
        <div class="sidebar-element bg-blue">
            <?php dynamic_sidebar( 'top-sidebar' ); ?>
        </div>
    <?php } ?>
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</div><!-- #secondary -->
