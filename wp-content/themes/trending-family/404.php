<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package trending-family
 */

get_header(); ?>

<div <?php post_class('not-found-page green-full-centered'); ?>>
    <div class="message">
        <img src="<?php echo get_template_directory_uri() . '/assets/images/heart-404.png'; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
        <h1><?php echo esc_html__( '404', 'trending-family' ); ?></h1>
        <h4><?php echo esc_html__('PAGE NOT FOUND', 'trending-family' ); ?></h4>
        <a href="<?php echo esc_url( home_url( '/') ); ?>" class="btn" title="<?php echo esc_attr__('Goto Home Page', 'trending-family' ); ?>"><?php echo esc_html__( 'Home', 'trending-family'); ?></a>
    </div>
</div>

<?php get_footer();