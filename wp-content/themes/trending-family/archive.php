<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package trending-family
 */

get_header();

if ( have_posts() ) : ?>

    <section class="offset-top-xs-4 offset-top-md-6">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php the_archive_title( '<h1 class="blog-title">', '</h1>' ); ?>

                    <div class="col-sm-7 articles-list offset-top-xs-4 offset-top-md-3">

                        <?php
                        /* Start the Loop */
                        while ( have_posts() ) : the_post();

                            /*
                             * Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part( 'template-parts/content', 'blog' );

                        endwhile;
                        trending_family_pagination(); ?>


                    </div><!-- end of col-sm-7 -->
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </section>
<?php else :

    get_template_part( 'template-parts/content', 'none' );

endif;
get_footer();