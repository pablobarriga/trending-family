<?php if ( ! defined( 'ABSPATH' ) ) exit;
/*
 * Template Name: Client Landing Page
 */
global $current_user;
wp_get_current_user();
if( !in_array( 'client', $current_user->roles )) {
	global $wp_query;
	$wp_query->set_404();
	status_header( 404 );
	get_template_part( 404 ); exit();
}
get_header(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <section class="section bg-green pad-md text-dark text-center-xs">
            <div class="container">
                <div class="row row-lg">
                    <div class="flex-center">
                        <div class="col-sm-8 col-xs-12 p-title">
                            <h1><?php echo esc_html__('Welcome ', 'trending-family'); ?><span class="txt-wlight"><?php echo esc_html( $current_user->display_name ); ?></span></h1>
                        </div>
                        <div class="col-sm-4 col-xs-12 text-right-md">
                            <img src="<?php echo esc_url( get_wp_user_avatar_src( $current_user->ID, 'large' ) ); ?>" alt="<?php echo esc_attr( $current_user->display_name ); ?>">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        $campaign_args = array(
            'post_type'   => 'campaign',
            'numberposts' => -1,
            'post_status' => array( 'publish'),
            'orderby'     => 'date',
            'order'       => 'DESC',
            'meta_query' => array(
                array(
                    'key' => 'trending_family_campaign_client_name',
                    'value' => $current_user->ID,
                    )
                )
        );
        $campaign_query = new WP_Query( $campaign_args );
        if( $campaign_query->have_posts() ){ ?>

            <section class="section bg-grey pad-top-xxs-0">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="s-title">
                                <h3><?php echo esc_html__('Your campaigns', 'trending-family'); ?></h3>
                            </div>

                            <table class="table results-table table-with-hover">
                                <thead class="hidden-xs">
                                    <tr>
                                        <th><?php echo esc_html__( 'Campaign Name', 'trending-family' ); ?></th>
                                        <th><?php echo esc_html__( 'Launch Date', 'trending-family'); ?></th>
                                        <th><?php echo esc_html__( 'Status', 'trending-family'); ?></th>
                                        <th><?php echo esc_html__( 'Results', 'trending-family' ); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php while( $campaign_query->have_posts()  ){
                                        $campaign_query->the_post();
                                        $launch_date = get_field( 'trending_family_campaign_launch_date' );
                                        $status = get_field( 'trending_family_campaign_status' );
                                        $results = get_field( 'trending_family_campaign_results' ); ?>
                                        <tr>
                                            <td data-title="<?php echo esc_attr('Campaign Name'); ?>"><a href="<?php the_permalink(); ?>" title="<?php echo esc_html__('View Details of', 'trending-family'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></td>
                                            <td data-title="<?php echo esc_attr('Launch Date'); ?>"><?php echo esc_html( $launch_date ); ?></td>
                                            <td data-title="<?php echo esc_attr('Status'); ?>"><?php echo esc_html( $status ); ?> <?php if(!empty( $status ) && $status != 'Complete') { ?><span class="status-marker <?php echo (!empty( $status ) && $status == 'Next Steps') ? esc_attr('active') : ''; ?>"></span><?php } ?></td>
                                            <td data-title="<?php echo esc_attr('Results'); ?>"><?php echo esc_html( $results ); ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </section>
	    <?php } else { ?>
		    <h3><?php echo esc_html__('No Campaigns found.', 'trending-family'); ?></h3>
	    <?php } ?>
    </div>
<?php get_footer();