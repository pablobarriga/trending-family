<?php
if (! defined('ABSPATH'))
    exit();
/*
 * Template Name: Admin Landing Page
 */
global $current_user;
wp_get_current_user();
if (! in_array('client', $current_user->roles)) {
    global $wp_query;
    $wp_query->set_404();
    status_header(404);
    get_template_part(404);
    exit();
}
get_header();


require_once get_template_directory(). "/social-media/UserSocialAccount.php";

use SelvinOrtiz\Dot\Dot as ArrayUtils;

$influencers = SocialMedia\UserSocialAccount::getAllInfluencers();

$influencerYoutubeChannels = SocialMedia\SocialAccounts\Youtube::allCachedData();
$influencerFacebookPages = SocialMedia\SocialAccounts\Facebook::allCachedData();
$influencerInstagramPages = SocialMedia\SocialAccounts\Instagram::allCachedData();
$influencerTwitterPages = SocialMedia\SocialAccounts\Twitter::allCachedData();
$influencerBlogPages = SocialMedia\SocialAccounts\Blog::allCachedData();

$consolidatedData = [];

if($influencerYoutubeChannels) {
    foreach($influencerYoutubeChannels as $wpUserId => $channel) {
        $consolidatedData[$wpUserId]['Youtube Link'] = ArrayUtils::get($channel, 'Link');
        $consolidatedData[$wpUserId]['Youtube Date of Last Post'] = ArrayUtils::get($channel, 'Date of Last Post');
        $consolidatedData[$wpUserId]['Youtube Subscribers Total'] = ArrayUtils::get($channel, 'Subscribers Total');
        $consolidatedData[$wpUserId]['Youtube Views Total'] = ArrayUtils::get($channel, 'Views Total');
        $consolidatedData[$wpUserId]['Youtube Subscriber Last 30 Days'] = ArrayUtils::get($channel, 'Subscriber Last 30 Days');
        $consolidatedData[$wpUserId]['Youtube Average Views Last 30 Days'] = ArrayUtils::get($channel, 'Average Views Last 30 Days');
        $consolidatedData[$wpUserId]['Youtube Average Views Last 10 Videos'] = ArrayUtils::get($channel, 'Average Views Last 10 Videos');
    }
}

if($influencerFacebookPages) {
    foreach($influencerFacebookPages as $wpUserId => $accounts) {
        
        if($accounts) {
            foreach($accounts as $managedPages) {
        
                if($managedPages) {
                    foreach($managedPages as $page) {
                        
                        $consolidatedData[$wpUserId]['Facebook Link'] = ArrayUtils::get($page, 'Link');
                        $consolidatedData[$wpUserId]['Facebook Followers'] = ArrayUtils::get($page, 'Followers');
                    }
                }
            }
        }
    }
}


dd($consolidatedData);
// du($influencerYoutubeChannels);
// du($influencerFacebookPages);
// du($influencerInstagramPages);
// du($influencerTwitterPages);
// dd($influencerBlogPages);

?>

<style>
.tab-content {
    padding: 20px 2px;
}
table thead tr {
    background: #49bda8;
 }
</style>
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script
	src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<section class="section  pad-md text-dark text-center-xs">
		<div class="container" >
		
			<h1><?php echo esc_html__( 'Influencer Accounts', 'trending-family' ); ?></h1>
			<div class="row row-lg">
				<div class="col-xs-12">

                    
					<div >
					
						<div style="width:100%;overflow:auto;">
							<table id="datatable-youtube" class="display datatable"
								cellspacing="0"  >
								<thead>
									<tr>
										<th class="data-general"><br>Influencer</th>
										<th class="data-youtube"><i class="fa fa-youtube"></i><br>Link</th>
										<th class="data-youtube"><i class="fa fa-youtube"></i><br>Last&nbsp;Post</th>
										<th class="data-youtube"><i class="fa fa-youtube"></i><br>Subscribers</th>
										<th class="data-youtube"><i class="fa fa-youtube"></i><br>Subscribers&nbsp;Last&nbsp;30&nbsp;Days</th>
										<th class="data-youtube"><i class="fa fa-youtube"></i><br>Total&nbsp;Views</th>
										<th class="data-youtube"><i class="fa fa-youtube"></i><br>Avg&nbsp;View&nbsp;Last&nbsp;30&nbsp;Days</th>
										<th class="data-youtube"><i class="fa fa-youtube"></i><br>Avg&nbsp;Views&nbsp;Last&nbsp;10&nbsp;Videos</th>
										<th class="data-facebook"><i class="fa fa-facebook"></i><br>Page</th>
										<th class="data-facebook"><i class="fa fa-facebook"></i><br>Followers</th>
										<th class="data-instagram"><i class="fa fa-instagram"></i><br>Link</th>
										<th class="data-instagram"><i class="fa fa-instagram"></i><br>Followers</th>
									</tr>
								</thead>

								<tbody>
								<?php 
								if($influencers) {
								    foreach($influencers as $influencer) { ?>								    
								        <tr>
								        
								        <?php 
								        /***************************************
								         *           YOUTUBE                   *
								         ***************************************/
								        ?>
    								    <?php 
    								    $hasYoutubeEntries = false;
    								    if($influencerYoutubeChannels) {
    								        foreach($influencerYoutubeChannels as $wpUserId => $influencerChannels) {
    								    
    								            $user = new WP_User($wpUserId);
    								            
    								            if($influencerChannels) {
    								                foreach($influencerChannels as $channel) { 
    								                    if( ! $channel['is_valid']) continue;
    								                    $hasYoutubeEntries = true; ?>
                    										<td data-title="Channel"><a 
                    											href="<?php echo $channel['Link']; ?>"><?php echo $channel['youtube_title']; ?></a>
                    										</td>
                    										<td data-title="Last Post"><?php echo date('m.d.Y', strtotime($channel['Date of Last Post'])); ?></td>
                    										<td data-title="Subscribers"><?php echo $channel['Subscribers Total']; ?></td>
                    										<td data-title="Subscribers 30 days"><?php echo $channel['Subscriber Last 30 Days']; ?></td>
                    										<td data-title="Total Views"><?php echo $channel['Views Total']; ?></td>
                    										<td data-title='Average Views Last 30 Days'><?php echo $channel['Average Views Last 30 Days']; ?></td>
                    										<td data-title='Average Views Last 10 Videos'><?php echo $channel['Average Views Last 10 Videos']; ?></td>
                    									
    									<?php        }
    								            }
    								        }
    								    }
    								    
    								    if( ! $hasYoutubeEntries): ?>
    									   <td>n/a</td>
    									   <td>n/a</td>
    									   <td>n/a</td>
    									   <td>n/a</td>
    									   <td>n/a</td>
    									   <td>n/a</td>
    									   <td>n/a</td>
    									<?php endif; ?>
								        
								        <?php 
								        /***************************************
								         *           FACEBOOK                  *
								         ***************************************/
								        ?>
    								    <?php 
    								    $hasFacebookEntries = false;
    								    if($influencerFacebookPages) {
    								        foreach($influencerFacebookPages as $wpUserId => $influencerPages) {
    								            
    								            $user = new WP_User($wpUserId);
    								    
    								            if($influencerPages) {
    								                foreach($influencerPages as $managedPages) {
    								                    
    								                    if($managedPages) {
    								                        foreach($managedPages as $page) { 
            								                    if( ! $page['is_valid']) continue; 
            								                    $hasFacebookEntries = true;?>
                            										<td data-title="Page"><a 
                            											href="<?php echo $page['Link']; ?>"><?php echo $page['pagename']; ?></a></td>
                                									<td data-title='Followers'><?php echo $page['Followers'];?></td>
    									<?php                 }
    								                     }
    								                }
    								            }
    								        }
    								    }
    								    
    								    if( ! $hasFacebookEntries): ?>
    									   <td>n/a</td>
    									   <td>n/a</td>
    									<?php endif; ?>
								        
								        <?php 
								        /***************************************
								         *           INSTAGRAM                 *
								         ***************************************/
								        ?>
    								    <?php 
    								    $hasInstagramEntries = false;
    								    if($influencerInstagramPages) {
    								        foreach($influencerInstagramPages as $wpUserId => $instagramPages) {
    								            
    								            $user = new WP_User($wpUserId);
    								    
    								            if($instagramPages) {
    								                foreach($instagramPages as $page) { 
    								                    if( ! $page['is_valid']) continue;
    								                    $hasInstagramEntries = true; ?>
                    										<td data-title="Channel"><a 
                    											href="<?php echo $page['Link']; ?>"><?php echo $page['owner']; ?></a></td>
                    										<td data-title='Followers'><?php echo $page['Followers']; ?></td>
    									<?php        }
    								            }
    								        }
    								    }
    								    
    								    if( ! $hasInstagramEntries): ?>
    									   <td>n/a</td>
    									   <td>n/a</td>
    									<?php endif; ?>
								        
								        <?php 
								        /***************************************
								         *           TWITTER                   *
								         ***************************************/
								        ?>
								        
								        <?php 
								        /***************************************
								         *           PINTEREST                 *
								         ***************************************/
								        ?>
    									
    									</tr>
    							<?php }
								} ?>
								</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
		</div>	
	</section>
</div>


<!-- Modal -->
<div class="modal fade" id="influencerModal" tabindex="-1" role="dialog" aria-labelledby="influencerModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="influencerModalLabel">Influencer Details</h4>
      </div>
      <div class="modal-body">
      	<div class="form-group"
      		<label>Name: </label><input type="text" id="modal-name" class="form-control" readonly />
        </div>
      	<div class="form-group"
      		<label>Address: </label><input type="text" id="modal-address" class="form-control" readonly />
        </div>
      	<div class="form-group"
      		<label>Children: </label><input type="text" id="modal-children" class="form-control" readonly />
        </div>
      	<div class="form-group"
      		<label>Pets: </label><input type="text" id="modal-pets" class="form-control" readonly />
        </div>
      	<div class="form-group"
      		<label>Suggested Rate: </label><input type="text"  id="modal-suggested-rate" class="form-control" readonly />
        </div>
      	<div class="form-group"
      		<label>Other Languages Spoken: </label><input type="text" id="modal-languages"class="form-control" readonly />
        </div>
      	<div class="form-group"
      		<label>Brand Preferences: </label><input type="text" id="modal-brands" class="form-control" readonly />
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
jQuery(document).ready(function() {
    jQuery('.datatable').DataTable();

    jQuery('.user-details-link').click(function(e){

        var userId = 4;

		jQuery.ajax({
		  url: "<?php echo admin_url('admin-ajax.php'); ?>",
		  type: "GET",
		  data: {action: 'tf_get_influencer_data', userId:userId },
		  cache: false,
		  dataType: 'json',
		  success: function(res){
			jQuery('#influencerModal input').val('');
		    jQuery('#modal-name').val(res.name);
		    jQuery('#modal-address').val(res.address);
		    jQuery('#modal-children').val(res.children);
		    jQuery('#modal-pets').val(res.pets);
		    jQuery('#modal-suggested-rate').val(res.suggested_rate);
		    jQuery('#modal-languages').val(res.languages);
		    jQuery('#modal-brands').val(res.brands);
		    jQuery('#influencerModal').modal('toggle');
		  }
		});
    });
} );

</script>
<?php get_footer();