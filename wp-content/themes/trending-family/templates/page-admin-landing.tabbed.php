<?php
if (! defined('ABSPATH')) exit();
/*
 * Template Name: Admin Landing Page
 */
global $current_user;
wp_get_current_user();
if (! in_array('client', $current_user->roles)) {
    global $wp_query;
    $wp_query->set_404();
    status_header(404);
    get_template_part(404);
    exit();
}
get_header();

require_once get_template_directory() . "/social-media/UserSocialAccount.php";

$influencerYoutubeChannels = SocialMedia\SocialAccounts\Youtube::allCachedData();
$influencerFacebookPages = SocialMedia\SocialAccounts\Facebook::allCachedData();
$influencerInstagramPages = SocialMedia\SocialAccounts\Instagram::allCachedData();
$influencerTwitterPages = SocialMedia\SocialAccounts\Twitter::allCachedData();
$influencerBlogPages = SocialMedia\SocialAccounts\Blog::allCachedData();

// du($influencerYoutubeChannels);
// du($influencerFacebookPages);
// du($influencerInstagramPages);
// du($influencerTwitterPages);
// dd($influencerBlogPages);

?>
<style>
.tab-content {
	padding: 20px 2px;
}

table thead tr {
	background: #49bda8;
}

#datatable-youtube_filter {
	display: none;
}
.filters input {
    height:25px !important;
    width:100%;
}
</style>
<link
    rel="stylesheet"
    type="text/css"
    href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<div
    id="post-<?php the_ID(); ?>"
    <?php post_class(); ?>>
    <section class="section  pad-md text-dark text-center-xs">
        <div class="container">

            <h1><?php echo esc_html__( 'Influencer Accounts', 'trending-family' ); ?></h1>
            <div class="row">
                <div class="col-md-3">

                    <div class="well filters">
                        <h4>Filters</h4>
                        <hr>
                            <div class="row">
                                <div class="col-md-8"> <select 
                                    >
                                    <option value="">Select Field</option>
                                    <option value="">Influencer Name</option>
                                    <option value="">YT Link</option>
                                </select></div>
                                <div class="col-md-4"> <select>
                                    <option value="">=</option>
                                    <option value="">&lt;</option>
                                    <option value="">&gt;</option>
                                </select></div>
                                <br>
                                <div class="col-md-12"><input
                                        type="text"></div>
                            </div>
                        </div>
                </div>
                <div class="col-md-9 col-xs-12">

                    <ul class="nav nav-tabs">

                        <li class="nav-item active"><a
                            href="#nav-datatable-youtube"
                            class="nav-link "
                            id="nav-datatable-youtube-tab"
                            data-toggle="tab"
                            role="tab"
                            aria-controls="nav-datatable-youtube"
                            aria-expanded="true"><i class="fa fa-youtube"></i> </a></li>

                        <li class="nav-item"><a
                            href="#nav-datatable-facebook"
                            class="nav-link "
                            id="nav-datatable-facebook-tab"
                            data-toggle="tab"
                            role="tab"
                            aria-controls="nav-datatable-facebook"
                            aria-expanded="true"><i class="fa fa-facebook"></i> </a></li>

                        <li class="nav-item"><a
                            href="#nav-datatable-instagram"
                            class="nav-link "
                            id="nav-datatable-instagram-tab"
                            data-toggle="tab"
                            role="tab"
                            aria-controls="nav-datatable-instagram"
                            aria-expanded="true"><i class="fa fa-instagram"></i> </a></li>

                        <li class="nav-item"><a
                            href="#nav-datatable-twitter"
                            class="nav-link "
                            id="nav-datatable-twitter-tab"
                            data-toggle="tab"
                            role="tab"
                            aria-controls="nav-datatable-twitter"
                            aria-expanded="true"><i class="fa fa-twitter"></i> </a></li>

                        <li class="nav-item"><a
                            href="#nav-datatable-pinterest"
                            class="nav-link "
                            id="nav-datatable-pinterest-tab"
                            data-toggle="tab"
                            role="tab"
                            aria-controls="nav-datatable-pinterest"
                            aria-expanded="true"><i class="fa fa-pinterest"></i> </a></li>

                        <li class="nav-item"><a
                            href="#nav-datatable-blog"
                            class="nav-link "
                            id="nav-datatable-blog-tab"
                            data-toggle="tab"
                            role="tab"
                            aria-controls="nav-datatable-blog"
                            aria-expanded="true"><i class="fa fa-address-card-o"></i> </a></li>
                    </ul>

                    <div
                        class="tab-content"
                        id="nav-tabContent">

                        <div
                            class="tab-pane active"
                            id="nav-datatable-youtube"
                            role="tabpanel"
                            aria-labelledby="nav-datatable-youtube-tab"
                            style="width: 100%; overflow: auto;">
                            <table
                                id="datatable-youtube"
                                class="display datatable"
                                cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Influencer</th>
                                        <th>Link</th>
                                        <th>Last Post</th>
                                        <th>Subscribers</th>
                                        <th>Subscribers Last 30 Days</th>
                                        <th>Total Views</th>
                                        <th>Avg View Last 30 Days</th>
                                        <th>Avg Views Last 10 Videos</th>
                                        <!-- 
										<th>Viewer Age</th>
										<th>Viewer Gender</th>
										<th>Viewer Geography</th>
										-->
                                    </tr>
                                </thead>

                                <tbody>
								    <?php
            if ($influencerYoutubeChannels) {
                foreach ($influencerYoutubeChannels as $wpUserId => $influencerChannels) {
                    
                    $user = new WP_User($wpUserId);
                    
                    if ($influencerChannels) {
                        foreach ($influencerChannels as $channel) {
                            if (! $channel['is_valid']) continue;
                            ?>
                									<tr data-user-id="<?php echo $wpUserId; ?>">
                                        <td data-title="WP User Name"><a
                                            href="#"
                                            class="user-details-link"><?php echo $user->display_name; ?></a></td>
                                        <td data-title="Channel"><a href="<?php echo $channel['Link']; ?>"><?php echo $channel['youtube_title']; ?></a></td>
                                        <td data-title="Last Post"><?php echo date('m.d.Y', strtotime($channel['Date of Last Post'])); ?></td>
                                        <td data-title="Subscribers"><?php echo $channel['Subscribers Total']; ?></td>
                                        <td data-title="Subscribers 30 days"><?php echo $channel['Subscriber Last 30 Days']; ?></td>
                                        <td data-title="Total Views"><?php echo $channel['Views Total']; ?></td>
                                        <td data-title='Average Views Last 30 Days'><?php echo $channel['Average Views Last 30 Days']; ?></td>
                                        <td data-title='Average Views Last 10 Videos'><?php echo $channel['Average Views Last 10 Videos']; ?></td>
                                        <!-- 
                										<td data-title='Viewer Age'><?php echo $channel['Viewer Age']; ?></td>
                										<td data-title='Viewer Gender'><?php echo $channel['Viewer Gender']; ?></td>
                										<td data-title='Viewer Geography'><?php echo $channel['Viewer Geography']; ?></td>
                										-->
                                    </tr>
									<?php
                        }
                    }
                }
            }
            ?>
								</tbody>
                            </table>
                        </div>

                        <div
                            class="tab-pane"
                            id="nav-datatable-facebook"
                            role="tabpanel"
                            aria-labelledby="nav-datatable-facebook-tab">
                            <table
                                id="datatable-facebook"
                                class="display datatable"
                                cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Influencer</th>
                                        <th>Page</th>
                                        <th>Followers</th>
                                        <!-- 
										<th>Follower Ages</th>
										<th>Follower Gender</th>
										<th>Follower Region</th>
										-->
                                    </tr>
                                </thead>

                                <tbody>
								    <?php
            if ($influencerFacebookPages) {
                foreach ($influencerFacebookPages as $wpUserId => $influencerPages) {
                    
                    $user = new WP_User($wpUserId);
                    
                    if ($influencerPages) {
                        foreach ($influencerPages as $managedPages) {
                            
                            if ($managedPages) {
                                foreach ($managedPages as $page) {
                                    if (! $page['is_valid']) continue;
                                    ?>
                            								<tr data-user-id="<?php echo $wpUserId; ?>">
                                        <td data-title="WP User Name"><a
                                            href="#"
                                            class="user-details-link"><?php echo $user->display_name; ?></a></td>

                                        <td data-title="Page"><a href="<?php echo $page['Link']; ?>"><?php echo $page['pagename']; ?></a></td>
                                        <td data-title='Followers'><?php echo $page['Followers'];?></td>
                                        <!-- 
                            										<td data-title='Follower Ages'><?php echo $page['Follower Ages']; ?></td>
                            										<td data-title='Follower Gender'><?php echo $page['Follower Gender']; ?></td>
                            										<td data-title='Follower Region'><?php echo $page['Follower Region']; ?></td>
                            										-->
                                    </tr>
									<?php
                                }
                            }
                        }
                    }
                }
            }
            ?>
								</tbody>
                            </table>
                        </div>

                        <div
                            class="tab-pane"
                            id="nav-datatable-instagram"
                            role="tabpanel"
                            aria-labelledby="nav-datatable-instagram-tab">
                            <table
                                id="datatable-instagram"
                                class="display datatable"
                                cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Influencer</th>
                                        <th>Link</th>
                                        <th>Followers</th>
                                        <!-- 
										<th>Engagements/Post Last Week</th>
										<th>Impressions/Post Last Week</th>
										<th>Clicks to SIte/Post Last Week</th>
										-->
                                    </tr>
                                </thead>

                                <tbody>
								    <?php
            if ($influencerInstagramPages) {
                foreach ($influencerInstagramPages as $wpUserId => $instagramPages) {
                    
                    $user = new WP_User($wpUserId);
                    
                    if ($instagramPages) {
                        foreach ($instagramPages as $page) {
                            if (! $page['is_valid']) continue;
                            ?>
                									<tr data-user-id="<?php echo $wpUserId; ?>">
                                        <td data-title="WP User Name"><a
                                            href="#"
                                            class="user-details-link"><?php echo $user->display_name; ?></a></td>

                                        <td data-title="Channel"><a href="<?php echo $page['Link']; ?>"><?php echo $page['owner']; ?></a></td>
                                        <td data-title='Followers'><?php echo $page['Followers']; ?></td>
                                        <!-- 
                										<td data-title='Engagements/Post Last Week'><?php echo $page['Engagements/Post Last Week']; ?></td>
                										<td data-title='Impressions/Post Last Week'><?php echo $page['Impressions/Post Last Week']; ?></td>
                										<td data-title='Clicks to Site/Post Last Week'><?php echo $page['Clicks to Site/Post Last Week']; ?></td>
                									    -->
                                    </tr>
									<?php
                        }
                    }
                }
            }
            ?>
								</tbody>
                            </table>
                        </div>

                        <div
                            class="tab-pane"
                            id="nav-datatable-twitter"
                            role="tabpanel"
                            aria-labelledby="nav-datatable-twitter-tab">
                            <table
                                id="datatable-twitter"
                                class="display datatable"
                                cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Influencer</th>
                                        <th>Link</th>
                                        <th>Followers</th>
                                        <!-- 
										<th>28-Day Avg Impressions/Day+</th>
										<th>28-Day Avg Engagement Rate+</th>
										-->
                                    </tr>
                                </thead>

                                <tbody>
								    <?php
            if ($influencerTwitterPages) {
                foreach ($influencerTwitterPages as $wpUserId => $twitterPages) {
                    
                    $user = new WP_User($wpUserId);
                    
                    if ($twitterPages) {
                        foreach ($twitterPages as $page) {
                            if (! $page['is_valid']) continue;
                            ?>
                									<tr data-user-id="<?php echo $wpUserId; ?>">
                                        <td data-title="WP User Name"><a
                                            href="#"
                                            class="user-details-link"><?php echo $user->display_name; ?></a></td>

                                        <td data-title="Channel"><a href="<?php echo $page['Link']; ?>"><?php echo $page['owner']; ?></a></td>
                                        <td data-title='Followers'><?php echo $page['Followers']; ?></td>
                                        <!-- 
                										<td data-title='28-Day Avg Impressions/Day+'><?php echo $page['28-Day Avg Impressions/Day+']; ?></td>
                										<td data-title='28-Day Avg Engagement Rate+'><?php echo $page['28-Day Avg Engagement Rate+']; ?></td>
                										-->
                                    </tr>
									<?php
                        }
                    }
                }
            }
            ?>
								</tbody>
                            </table>
                        </div>

                        <div
                            class="tab-pane"
                            id="nav-datatable-pinterest"
                            role="tabpanel"
                            aria-labelledby="nav-datatable-pinterest-tab">
                            <table
                                id="datatable-pinterest"
                                class="display datatable"
                                cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Influencer</th>
                                        <th>Link</th>
                                        <th>Followers</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td data-title="Channel"><a
                                            class="popup-box-link"
                                            href="http://websatil.com/project-test/trending-family/html/admin-panel.html#popup-profile">Lorem Ipsum</a></td>
                                        <td data-title="URL">Lorem Ipsum dolor</td>
                                        <td data-title="Last Post">MM / DD / YY</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                        <div
                            class="tab-pane"
                            id="nav-datatable-blog"
                            role="tabpanel"
                            aria-labelledby="nav-datatable-blog-tab">
                            <table
                                id="datatable-blog"
                                class="display datatable"
                                cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Link</th>
                                        <th>Unique Monthly Visitors</th>
                                        <th>Monthly Pageviews</th>
                                    </tr>
                                </thead>

                                <tbody>
								    <?php
            if ($influencerBlogPages) {
                foreach ($influencerBlogPages as $wpUserId => $pages) {
                    if (! $page['is_valid']) continue;
                    $user = new WP_User($wpUserId);
                    ?>
        									<tr data-user-id="<?php echo $wpUserId; ?>">
                                        <td data-title="WP User Name"><a
                                            href="#"
                                            class="user-details-link"><?php echo $user->display_name; ?></a></td>
                                        <td data-title='Unique Monthly Visitors'><?php echo $page['Unique Monthly Visitors']; ?></td>
                                        <td data-title='Monthly Pageviews'><?php echo $page['Monthly Pageviews']; ?></td>
                                    </tr>
									<?php
                }
            }
            ?>
								</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<!-- Modal -->
<div
    class="modal fade"
    id="influencerModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="influencerModalLabel">
    <div
        class="modal-dialog"
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4
                    class="modal-title"
                    id="influencerModalLabel">Influencer Details</h4>
            </div>
            <div class="modal-body">
                <div class="form-group"
                    
                    <label>Name: </label>
                    <input
                        type="text"
                        id="modal-name"
                        class="form-control"
                        readonly />
                </div>
                <div class="form-group"
                    
                    <label>Address: </label>
                    <input
                        type="text"
                        id="modal-address"
                        class="form-control"
                        readonly />
                </div>
                <div class="form-group"
                    
                    <label>Children: </label>
                    <input
                        type="text"
                        id="modal-children"
                        class="form-control"
                        readonly />
                </div>
                <div class="form-group"
                    
                    <label>Pets: </label>
                    <input
                        type="text"
                        id="modal-pets"
                        class="form-control"
                        readonly />
                </div>
                <div class="form-group"
                    
                    <label>Other Languages Spoken: </label>
                    <input
                        type="text"
                        id="modal-languages"
                        class="form-control"
                        readonly />
                </div>
                <div class="form-group"
                    
                    <label>Brand Preferences: </label>
                    <input
                        type="text"
                        id="modal-brands"
                        class="form-control"
                        readonly />
                </div>
            </div>
            <div class="modal-footer">
                <button
                    type="button"
                    class="btn btn-default"
                    data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
jQuery(document).ready(function() {
    jQuery('.datatable').DataTable();

    jQuery('.user-details-link').click(function(e){

        var userId = 4;

		jQuery.ajax({
		  url: "<?php echo admin_url('admin-ajax.php'); ?>",
		  type: "GET",
		  data: {action: 'tf_get_influencer_data', userId:userId },
		  cache: false,
		  dataType: 'json',
		  success: function(res){
			jQuery('#influencerModal input').val('');
		    jQuery('#modal-name').val(res.name);
		    jQuery('#modal-address').val(res.address);
		    jQuery('#modal-children').val(res.children);
		    jQuery('#modal-pets').val(res.pets);
		    jQuery('#modal-languages').val(res.languages);
		    jQuery('#modal-brands').val(res.brands);
		    jQuery('#influencerModal').modal('toggle');
		  }
		});
    });
} );

</script>
<?php get_footer();