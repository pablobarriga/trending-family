<?php
if (! defined('ABSPATH'))
    exit();
/*
 * Template Name: Admin Landing Page
 */
global $current_user;
wp_get_current_user();
if (! in_array('client', $current_user->roles)) {
    global $wp_query;
    $wp_query->set_404();
    status_header(404);
    get_template_part(404);
    exit();
}
get_header();


use SelvinOrtiz\Dot\Dot as ArrayUtils;

$influencers = SocialMedia\UserSocialAccount::getAllInfluencers();
?>


<style>
.tab-content {
    padding: 20px 2px;
}
table thead tr {
    background: #49bda8;
 }
 
table.datatable tr td table tr {
    background: none;
}

table tr td {
    text-align: top;
}
#datatable-youtube_filter {
    display:none;
}
</style>
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script
	src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<section class="section  pad-md text-dark text-center-xs">
		<div class="container" >
		
			<h1><?php echo esc_html__( 'Influencer Accounts', 'trending-family' ); ?></h1>
			<div class="row row-lg">
				<div class="col-xs-12">

                    
					<div >
					
						<div style="width:100%;overflow:auto;">
							<table id="datatable-youtube" class="display datatable"
								cellspacing="0"  >
								<thead>
									<tr>
										<th class="data-general"><br>Influencer</th>
										<th class="data-youtube"><i class="fa fa-youtube"></i><br>Link</th>
										<th class="data-youtube"><i class="fa fa-youtube"></i><br>Last&nbsp;Post</th>
										<th class="data-youtube"><i class="fa fa-youtube"></i><br>Subscribers</th>
										<th class="data-youtube"><i class="fa fa-youtube"></i><br>Subscribers&nbsp;Last&nbsp;30&nbsp;Days</th>
										<th class="data-youtube"><i class="fa fa-youtube"></i><br>Total&nbsp;Views</th>
										<th class="data-youtube"><i class="fa fa-youtube"></i><br>Avg&nbsp;View&nbsp;Last&nbsp;30&nbsp;Days</th>
										<th class="data-youtube"><i class="fa fa-youtube"></i><br>Avg&nbsp;Views&nbsp;Last&nbsp;10&nbsp;Videos</th>
										<th class="data-facebook"><i class="fa fa-facebook"></i><br>Page</th>
										<th class="data-facebook"><i class="fa fa-facebook"></i><br>Followers</th>
										<th class="data-instagram"><i class="fa fa-instagram"></i><br>Link</th>
										<th class="data-instagram"><i class="fa fa-instagram"></i><br>Followers</th>
										<th class="data-twitter"><i class="fa fa-twitter"></i><br>Link</th>
										<th class="data-twitter"><i class="fa fa-twitter"></i><br>Followers</th>
                                        <th>Blog<br>Link</th>
                                        <th>Blog<br>Unique&nbsp;Monthly&nbsp;Visitors</th>
                                        <th>Blog<br>Monthly&nbsp;Pageviews</th>
									</tr>
								</thead>

								<tbody>
								<?php 
								if($influencers) {
								    foreach($influencers as $influencer) { 
								        
								        echo '<tr>'; // begin influencer row								        
								        echo '<td>' . $influencer->display_name . '</td>'; // influencer name

								        /***************************************
								         *           YOUTUBE                   *
								         ***************************************/
								        $youtubeService = new SocialMedia\SocialAccounts\Youtube($influencer);
								        $youtubeChannels = $youtubeService->cachedData('youtube_channels'); 	
								        			    
								        $youtubeTableRows = [];    								                    
					                    if($youtubeChannels) {
					                        foreach($youtubeChannels as $channel) { 
					                            
							                    if( ! $channel['is_valid']) continue; 

							                    $youtubeTableRows['Link'][] = '<tr><td data-title="Channel"><a href="' . $channel['Link'] . '">' . str_replace(' ', '&nbsp;', $channel['youtube_title']) . '</a></td></tr>';
							                    $youtubeTableRows['Last Post'][] = '<tr><td data-title="Last Post">' . date('m.d.Y', strtotime($channel['Date of Last Post'])) . '</td></tr>';
							                    $youtubeTableRows['Subscribers'][] = '<tr><td data-title="Subscribers">' . $channel['Subscribers Total'] . '</td></tr>';
							                    $youtubeTableRows['Subscribers 30 days'][] = '<tr><td data-title="Subscribers 30 days">' . $channel['Subscriber Last 30 Days'] . '</td></tr>';
							                    $youtubeTableRows['Total Views'][] = '<tr><td data-title="Total Views">' . $channel['Views Total'] . '</td></tr>';
							                    $youtubeTableRows['Average Views Last 30 Days'][] = '<tr><td data-title="Average Views Last 30 Days">' . $channel['Average Views Last 30 Days'] . '</td></tr>';
							                    $youtubeTableRows['Average Views Last 10 Videos'][] = '<tr><td data-title="Average Views Last 10 Videos">' . $channel['Average Views Last 10 Videos'] . '</td></tr>';
								             }
							             }
    								    
    								    if(ArrayUtils::get($youtubeTableRows, 'Link')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($youtubeTableRows, 'Link')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }
    								    
    								    if(ArrayUtils::get($youtubeTableRows, 'Last Post')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($youtubeTableRows, 'Last Post')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }
    								    
    								    if(ArrayUtils::get($youtubeTableRows, 'Subscribers')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($youtubeTableRows, 'Subscribers')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }
    								    
    								    if(ArrayUtils::get($youtubeTableRows, 'Subscribers 30 days')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($youtubeTableRows, 'Subscribers 30 days')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }
    								    
    								    if(ArrayUtils::get($youtubeTableRows, 'Total Views')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($youtubeTableRows, 'Total Views')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }
    								    
    								    if(ArrayUtils::get($youtubeTableRows, 'Average Views Last 30 Days')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($youtubeTableRows, 'Average Views Last 30 Days')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }
    								    
    								    if(ArrayUtils::get($youtubeTableRows, 'Average Views Last 10 Videos')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($youtubeTableRows, 'Average Views Last 10 Videos')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }

								        /***************************************
								         *           FACEBOOK                  *
								         ***************************************/
								        $facebookService = new SocialMedia\SocialAccounts\Facebook($influencer);
								        $facebookAccounts = $facebookService->cachedData('facebook_pages'); 	
								        			    
								        $facebookTableRows = [];
    								    if($facebookAccounts) {
    								        foreach($facebookAccounts as $managedPages) {
    								                    
							                    if($managedPages) {
							                        foreach($managedPages as $page) { 
							                            
    								                    if( ! $page['is_valid']) continue; 
    								                    
                    									$facebookTableRows['Link'][] = '<tr><td data-title="Page"><a href="' . $page['Link'] . '">' . str_replace(' ', '&nbsp;', $page['pagename']) . '</a></td></tr>';
                        								$facebookTableRows['Followers'][] = '<tr><td data-title="Followers">' . $page['Followers'] . '</td></tr>';
    									             }
    								             }
    								        }
    								    }
    								    
    								    if(ArrayUtils::get($facebookTableRows, 'Link')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($facebookTableRows, 'Link')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }
    								    
    								    if(ArrayUtils::get($facebookTableRows, 'Followers')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($facebookTableRows, 'Followers')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }

								        /***************************************
								         *           INSTAGRAM                 *
								         ***************************************/
								        $instagramService = new SocialMedia\SocialAccounts\Instagram($influencer);
								        $instagramPages = $facebookService->cachedData('instagram_pages'); 	
								        			    
								        $instagramTableRows = [];
    								                    
					                    if($instagramPages) {
					                        foreach($instagramPages as $page) { 
					                            
							                    if( ! $page['is_valid']) continue; 
							                    
            									$instagramTableRows['Link'][] = '<tr><td data-title="Page"><a href="' . $page['Link'] . '">' . str_replace(' ', '&nbsp;', $page['owner']) . '</a></td></tr>';
                								$instagramTableRows['Followers'][] = '<tr><td data-title="Followers">' . $page['Followers'] . '</td></tr>';
								             }
							             }
    								    
    								    if(ArrayUtils::get($instagramTableRows, 'Link')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($instagramTableRows, 'Link')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }
    								    
    								    if(ArrayUtils::get($instagramTableRows, 'Followers')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($instagramTableRows, 'Followers')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }

								        /***************************************
								         *           TWITTER                   *
								         ***************************************/
								        $twitterService = new SocialMedia\SocialAccounts\Twitter($influencer);
								        $twitterAccounts= $twitterService->cachedData('twitter_accounts'); 	
								        			    
								        $twitterTableRows = [];
    								                    
					                    if($twitterAccounts) {
					                        foreach($twitterAccounts as $account) { 
							                    if( ! $account['is_valid']) continue; 
							                    
            									$twitterTableRows['Link'][] = '<tr><td data-title="Page"><a href="' . $page['Link'] . '">' . str_replace(' ', '&nbsp;', $page['owner']) . '</a></td></tr>';
                								$twitterTableRows['Followers'][] = '<tr><td data-title="Followers">' . $page['Followers'] . '</td></tr>';
								             }
							             }
    								    
    								    if(ArrayUtils::get($instagramTableRows, 'Link')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($instagramTableRows, 'Link')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }
    								    
    								    if(ArrayUtils::get($instagramTableRows, 'Followers')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($instagramTableRows, 'Followers')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }

								        /***************************************
								         *           PINTEREST                 *
								         ***************************************/
// 								        $pinterestService = new SocialMedia\SocialAccounts\Pinterest($influencer);
// 								        $pinterestAccounts= $pinterestService->cachedData('pinterest_accounts'); 	
								        			    
// 								        $pinterestTableRows = [];
    								                    
// 					                    if($pinterestAccounts) {
// 					                        foreach($pinterestAccounts as $account) { 
					                            
// 							                    if( ! $account['is_valid']) continue; 
							                    
//             									$pinterestTableRows['Link'][] = '<tr><td data-title="Page"><a href="' . $page['Link'] . '">' . str_replace(' ', '&nbsp;', $page['owner']) . '</a></td></tr>';
//                 								$pinterestTableRows['Followers'][] = '<tr><td data-title="Followers">' . $page['Followers'] . '</td></tr>';
// 								             }
// 							             }
    								    
//     								    if(ArrayUtils::get($instagramTableRows, 'Link')) {
//     								        echo '<td><table>' . implode('', ArrayUtils::get($instagramTableRows, 'Link')) . '</table></td>';
//     								    } else {
//     								        echo '<td></td>';
//     								    }
    								    
//     								    if(ArrayUtils::get($instagramTableRows, 'Followers')) {
//     								        echo '<td><table>' . implode('', ArrayUtils::get($instagramTableRows, 'Followers')) . '</table></td>';
//     								    } else {
//     								        echo '<td></td>';
//     								    }

								        /***************************************
								         *           BLOG                      *
								         ***************************************/
								        $blogAccounts = ( new SocialMedia\SocialAccounts\Blog($influencer) )->accounts();	
								        			    
								        $blogTableRows = [];
    								                    
					                    if($blogAccounts) {
					                        foreach($blogAccounts as $account) { 
							                    
            									$blogTableRows['Link'][] = '<tr><td data-title="Link"><a href="' . $account[0] . '">' . str_replace(' ', '&nbsp;', $account[0]) . '</a></td></tr>';
                								$blogTableRows['Unique Monthly Visitors'][] = '<tr><td data-title="Unique Monthly Visitors">' . $account[1] . '</td></tr>';
                								$blogTableRows['Monthly Page Views'][] = '<tr><td data-title="Monthly Page Views">' . $account[2] . '</td></tr>';
								             }
							             }
    								    
    								    if(ArrayUtils::get($blogTableRows, 'Link')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($blogTableRows, 'Link')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }
    								    
    								    if(ArrayUtils::get($blogTableRows, 'Unique Monthly Visitors')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($blogTableRows, 'Unique Monthly Visitors')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }
    								    
    								    if(ArrayUtils::get($blogTableRows, 'Monthly Page Views')) {
    								        echo '<td><table>' . implode('', ArrayUtils::get($blogTableRows, 'Monthly Page Views')) . '</table></td>';
    								    } else {
    								        echo '<td></td>';
    								    }

    								    
    								    
    								    // end row
    								    echo '</tr>';
    							     }
								} ?>
								</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
		</div>	
	</section>
</div>


<!-- Modal -->
<div class="modal fade" id="influencerModal" tabindex="-1" role="dialog" aria-labelledby="influencerModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="influencerModalLabel">Influencer Details</h4>
      </div>
      <div class="modal-body">
      	<div class="form-group"
      		<label>Name: </label><input type="text" id="modal-name" class="form-control" readonly />
        </div>
      	<div class="form-group"
      		<label>Address: </label><input type="text" id="modal-address" class="form-control" readonly />
        </div>
      	<div class="form-group"
      		<label>Children: </label><input type="text" id="modal-children" class="form-control" readonly />
        </div>
      	<div class="form-group"
      		<label>Pets: </label><input type="text" id="modal-pets" class="form-control" readonly />
        </div>
      	<div class="form-group"
      		<label>Suggested Rate: </label><input type="text"  id="modal-suggested-rate" class="form-control" readonly />
        </div>
      	<div class="form-group"
      		<label>Other Languages Spoken: </label><input type="text" id="modal-languages"class="form-control" readonly />
        </div>
      	<div class="form-group"
      		<label>Brand Preferences: </label><input type="text" id="modal-brands" class="form-control" readonly />
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
jQuery(document).ready(function() {
    var tbl = jQuery('.datatable').dataTable();

    tbl.yadcf([{
        column_number: 0,
        filter_type: "text"
    },{
        column_number: 1,
        filter_type: "text"
    },{
        column_number: 2,
        filter_type: "text"
    },{
        column_number: 3,
        filter_type: "text"
    },{
        column_number: 4,
        filter_type: "text"
    },{
        column_number: 5,
        filter_type: "text"
    },{
        column_number: 6,
        filter_type: "text"
    },{
        column_number: 7,
        filter_type: "text"
    },{
        column_number: 8,
        filter_type: "text"
    },{
        column_number: 9,
        filter_type: "text"
    },{
        column_number: 10,
        filter_type: "text"
    },{
        column_number: 11,
        filter_type: "text"
    },{
        column_number: 12,
        filter_type: "text"
    },{
        column_number: 13,
        filter_type: "text"
    },{
        column_number: 14,
        filter_type: "text"
    },{
        column_number: 15,
        filter_type: "text"
    },{
        column_number: 16,
        filter_type: "text"
    }]);;

    jQuery('.user-details-link').click(function(e){

        var userId = 4;

		jQuery.ajax({
		  url: "<?php echo admin_url('admin-ajax.php'); ?>",
		  type: "GET",
		  data: {action: 'tf_get_influencer_data', userId:userId },
		  cache: false,
		  dataType: 'json',
		  success: function(res){
			jQuery('#influencerModal input').val('');
		    jQuery('#modal-name').val(res.name);
		    jQuery('#modal-address').val(res.address);
		    jQuery('#modal-children').val(res.children);
		    jQuery('#modal-pets').val(res.pets);
		    jQuery('#modal-suggested-rate').val(res.suggested_rate);
		    jQuery('#modal-languages').val(res.languages);
		    jQuery('#modal-brands').val(res.brands);
		    jQuery('#influencerModal').modal('toggle');
		  }
		});
    });
} );

</script>
<?php get_footer();