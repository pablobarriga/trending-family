<?php if ( ! defined( 'ABSPATH' ) ) exit;
/*
 * Template Name: Home Page
 */
get_header(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php
		$case_studies_option = get_field('trending_family_home_page_enable_case_studies_section');
		if( $case_studies_option ) { ?>
            <div id="work" class="main-carousel owl-carousel">

				<?php
				if( have_rows('trending_family_home_page_case_studies') ) {
				    $i = 0;
					while ( have_rows( 'trending_family_home_page_case_studies' ) ) {
						the_row();
						$i++;
						$case_study_hero_image = get_sub_field( 'trending_family_home_page_case_study_hero_image' );
						$case_study_logo = get_sub_field( 'trending_family_home_page_case_study_logo' );
						$case_study_page_link = get_sub_field( 'trending_family_home_page_case_study_page_link' );
						$case_study_tagline = get_sub_field( 'trending_family_home_page_case_study_tagline' ); ?>

                        <a class="slide-item <?php echo $i; ?>" href="<?php echo esc_url( $case_study_page_link ); ?>" title="<?php echo esc_attr__('View Case Study', 'trending-family'); ?>">
							<?php if(!empty($case_study_hero_image)){ ?>
                                <span class="bg-img" style="background-image: url(<?php echo esc_url($case_study_hero_image); ?>);"></span>
							<?php } ?>
                            <div class="slide-cont">
								<?php if(!empty( $case_study_logo )){  ?>
                                    <img class="brend-logo" src="<?php echo esc_url( $case_study_logo ); ?>" alt="<?php echo esc_attr( get_bloginfo('name') ); ?>">
								<?php } ?>
								<?php if(!empty( $case_study_tagline )){ ?>
                                    <p><?php echo $case_study_tagline; ?></p>
								<?php } ?>
                            </div>
                        </a>
					<?php }
				} ?>
            </div><!-- end of work-section -->
		<?php }

		$about_section = get_field( 'trending_family_home_page_enable_about_section' );
		if( $about_section ) {
			$about_video_url = get_field( 'trending_family_home_page_about_section_video_url' );
			$about_video_placeholder_image = get_field( 'trending_family_home_page_about_section_video_placeholder_image' );
			$about_heading = get_field( 'trending_family_home_page_enable_about_section_heading' );
			$about_paragraph = get_field( 'trending_family_home_page_about_section_paragraph' ); ?>

            <section id="about" class="section">
                <div class="container">
                    <div class="row row-lg">
                        <div class="flex-center">
                            <div class="col-sm-6 col-xs-12 offset-bottom-xs-3">
                                <a class="img-heart video-link popup-video" href="<?php echo esc_url( $about_video_url ); ?>">
                                    <img src="<?php echo esc_url( $about_video_placeholder_image ); ?>" alt="<?php echo esc_attr( get_bloginfo('name') ); ?>">
                                </a>
                            </div>
                            <div class="col-sm-6 col-xs-12 text-center-xs">
								<?php if( !empty( $about_heading )){ ?>
                                    <h2><?php echo esc_html( $about_heading ); ?></h2>
								<?php } ?>
								<?php echo $about_paragraph; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
		<?php }
		$services_section = get_field( 'trending_family_home_page_enable_services_section' );
		if( $services_section ) {
			$section_heading = get_field( 'trending_family_home_page_section_heading' ); ?>

            <section id="services" class="section bg-green">
                <div class="container">
                    <div class="row">
						<?php if(!empty( $section_heading )) { ?>
                            <div class="col-xs-12">
                                <h2 class="text-center offset-bottom-sm-5">Our Services</h2>
                            </div>
						<?php }
						if( have_rows('trending_family_home_page_services') ) { ?>
                            <div class="col-xs-12">
                                <div class="feature-controls-dots feature-controls-dots-1 flex-box justify-beetwen"></div>
                                <div class="feature-carousel feature-carousel-1 owl-carousel">
									<?php while( have_rows( 'trending_family_home_page_services' )){
										the_row();
										$icon_image = get_sub_field( 'trending_family_home_page_service_icon' );
										$icon_title = get_sub_field( 'trending_family_home_page_service_icon_title' );
										$main_heading = get_sub_field( 'trending_family_home_page_service_heading' );
										$main_content = get_sub_field( 'trending_family_home_page_service_content' );
										$info_box_heading = get_sub_field( 'trending_family_home_page_service_info_box_heading' );
										$info_box_content = get_sub_field( 'trending_family_home_page_service_info_box_content' );
										$info_box_label = get_sub_field( 'trending_family_home_page_service_info_box_cta_label');
										$info_box_link = get_sub_field( 'trending_family_home_page_service_info_box_cta_link' ); ?>

                                        <div class="feature-slide" data-dot='<i class="icon" style="background-image: url(<?php echo esc_url($icon_image); ?>);">1</i> <?php echo esc_attr( $icon_title ); ?>'>
                                            <div class="top-subheader">
                                                <img src="<?php echo esc_url( $icon_image ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name') ); ?>">
                                                <span><?php echo esc_html( $icon_title ); ?></span>
                                            </div>
                                            <div class="feature-slide-cont">
                                                <div class="row row-lg">
                                                    <div class="flex-center text-center-xs">
                                                        <div class="col-lg-9 col-sm-8 col-xs-12">
															<?php if(!empty( $heading )){ ?>
                                                                <h3><?php echo esc_html( $heading ); ?></h3>
															<?php }
															echo $main_content; ?>
                                                        </div>
                                                        <div class="col-lg-3 col-sm-4 col-xs-12 bord-left-sm">
                                                            <div class="hidden-xs">
                                                                <h4><?php echo esc_html( $info_box_heading ); ?></h4>
                                                                <p class="text-sm"><?php echo esc_html( $info_box_content ); ?></p>
                                                            </div>
															<?php if( !empty( $info_box_label )){ ?>
                                                                <a class="btn btn-default btn-lg smoothScroll" href="<?php echo esc_url( $info_box_link ); ?>"><?php echo esc_html( $info_box_label ); ?></a>
															<?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										<?php
									} ?>
                                </div><!-- end of feature-carousel -->
                            </div><!-- end of col-xs-12 -->
						<?php } ?>
                    </div><!-- end of row -->
                </div><!-- end of container -->
            </section>
		<?php }
		$technology_section = get_field( 'trending_family_home_page_enable_technology_section' );
		if( $technology_section ) {
			$section_heading = get_field( 'trending_family_home_page_technology_section_heading' );
			$section_content = get_field( 'trending_family_home_page_technology_section_content') ;
			$section_images = get_field( 'trending_family_home_page_technology_section_image' ); ?>

            <section class="section bg-grey bg-white-xs">
                <div class="container">
                    <div class="row text-center-xs">
                        <div class="flex-center flex-revers">
                            <div class="<?php echo !empty( $section_images ) ? esc_attr('col-sm-6') : esc_attr('col-sm-12'); ?> col-xs-12 offset-bottom-xs-1">
                                <div class="bracket-outline">
                                    <div>
										<?php if( !empty( $section_heading )) { ?>
                                            <h2><?php echo esc_html( $section_heading ); ?></h2>
										<?php } ?>
										<?php echo $section_content; ?>
                                    </div>
                                </div>
                            </div>
                            
							<?php
                            if(!empty( $section_images )){ ?>
                                <div class="col-sm-6 col-xs-12 text-center bg-grey-xs">
                                    <div class="mega-overlay"></div>
                                    <div class="owl-carousel overlay-slider">
                                        <?php foreach ( $section_images as $section_image ) {
	                                        $slider_image_url = $section_image['url'];
	                                        $slider_image_url = aq_resize( $slider_image_url, 560, 560, true, true, true );
	                                        if ( $slider_image_url ) { ?>
                                                <div class="item">
                                                    <img src="<?php echo esc_url( $slider_image_url ); ?>"
                                                         alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                                                </div>
	                                        <?php }
                                        } ?>
                                    </div>
                                </div>
							<?php } ?>
                        </div>
                    </div>
                </div>
            </section>
		<?php }

		$content_verticals_section = get_field( 'trending_family_home_page_enable_content_verticals_section' );

		if( $content_verticals_section ) {
			$section_heading = get_field( 'trending_family_home_page_content_vertical_section_heading' );
			$section_tagline = get_field( 'trending_family_home_page_content_vertical_section_tagline' ); ?>

            <section class="section bg-white pb0">
                <div class="container">
                    <div class="row">
                        <div class="flex-center">
                            <div class="col-xs-12">
								<?php if( !empty( $section_heading )){ ?>
                                    <h2 class="text-center<?php echo empty( $section_tagline ) ? ' offset-bottom-sm-5': '';?>"><?php echo esc_html( $section_heading ); ?></h2>
                                    <?php if( !empty( $section_tagline ) ){ ?>
                                        <p class="text-center offset-bottom-sm-5"><?php echo $section_tagline; ?></p>
                                    <?php } ?>
								<?php } ?>
                            </div>
							<?php if( have_rows( 'trending_family_home_page_content_vertical_contents' )) { ?>
                                <div class="col-xs-12">
                                    <div class="feature-controls-dots feature-controls-dots-2 flex-box justify-beetwen"></div>
                                    <div class="feature-carousel feature-carousel-2 owl-carousel">
										<?php while( have_rows( 'trending_family_home_page_content_vertical_contents' )) {
											the_row();
											$icon_image       = get_sub_field( 'trending_family_home_page_content_vertical_content_icon' );
											$icon_title       = get_sub_field( 'trending_family_home_page_content_vertical_content_icon_title' );
											$main_heading     = get_sub_field( 'trending_family_home_page_content_vertical_content_heading' );
											$main_content     = get_sub_field( 'trending_family_home_page_content_vertical_content_content' );
											$info_box_heading = get_sub_field( 'trending_family_home_page_content_vertical_content_info_box_heading' );
											$info_box_content = get_sub_field( 'trending_family_home_page_content_vertical_content_info_box_content' );
											$info_box_label   = get_sub_field( 'trending_family_home_page_content_vertical_content_info_box_cta_label' );
											$info_box_link    = get_sub_field( 'trending_family_home_page_content_vertical_content_info_box_cta_link' ); ?>

                                            <div class="feature-slide"
                                                 data-dot='<i class="icon" style="background-image: url(<?php echo esc_url( $icon_image ); ?>); ">1</i> <?php echo esc_attr( $icon_title ); ?>'>
                                                <div class="top-subheader">
                                                    <img src="<?php echo esc_url( $icon_image ); ?>"
                                                         alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                                                    <span><?php echo esc_html( $icon_title ); ?></span>
                                                </div>
                                                <div class="feature-slide-cont">
                                                    <div class="row row-lg">
                                                        <div class="flex-center text-center-xs">
                                                            <div class="col-lg-9 col-sm-8 col-xs-12">
																<?php if ( ! empty( $main_heading ) ) { ?>
                                                                    <h3><?php echo esc_html( $main_heading ); ?></h3>
																<?php }
																echo $main_content; ?>

                                                            </div>
                                                            <div class="col-lg-3 col-sm-4 col-xs-12 bord-left-sm">
                                                                <div class="hidden-xs">
																	<?php if ( ! empty( $info_box_heading ) ) { ?>
                                                                        <h4><?php echo esc_html( $info_box_heading ); ?></h4>
																	<?php }
																	if ( ! empty( $info_box_content ) ) { ?>
                                                                        <p class="text-sm"><?php echo $info_box_content; ?></p>
																	<?php } ?>
                                                                </div>
																<?php if ( ! empty( $info_box_label ) ) { ?>
                                                                    <a class="btn btn-default btn-lg smoothScroll"
                                                                       href="<?php echo esc_url( $info_box_link ); ?>"><?php echo esc_html( $info_box_label ); ?></a>
																<?php } ?>
                                                            </div><!-- end of col-lg-3-->
                                                        </div><!-- end of flex-center -->
                                                    </div><!-- end of row -->
                                                </div><!-- end of feature-slide-cont -->
                                            </div><!-- end of feature-slide -->
										<?php } ?>
                                    </div><!-- end of feature-carousel -->
                                </div><!-- end of col-xs-12 -->
							<?php } ?>
                        </div><!-- end of flex-center -->
                    </div><!-- end of row -->
                </div><!-- end of container -->
            </section>

			<?php
		}
		$sample_influencers_section = get_field( 'trending_family_home_page_enable_sample_influencers_section' );
	    if( $sample_influencers_section ) {
		    $section_heading    = get_field( 'trending_family_home_page_influencers_section_heading' );
		    $sample_influencers = get_field( 'trending_family_home_page_influencers_section_sample_influencers' ); ?>
            <section class="influencers-section">
                <div class="container">
                    <?php if(!empty( $section_heading )){ ?>
                        <h3 class="text-center text-uppercase"><?php echo esc_html( $section_heading ); ?></h3>
                    <?php }
                    if( $sample_influencers ) { ?>
                        <div class="influencers-list">
                            <div class="influencers-carousel owl-carousel">
                                <?php foreach ($sample_influencers as $sample_influencer ){
                                    $influencer_image_url = get_wp_user_avatar_src( $sample_influencer['ID'], 'medium' );
                                    if(empty($influencer_image_url)) {
	                                    $influencer_image_url = get_avatar_url( $sample_influencer['user_email'], array( 'size' => 200 ) );
                                    } ?>
                                    <div class="item">
                                        <div class="influencer-block">
                                            <a href="javascript:void(0);" class="thumb-circle" style="background-image: url(<?php echo esc_url($influencer_image_url); ?>);" title="<?php echo esc_attr(trending_family_influencer_nice_name($sample_influencer)); ?>">&nbsp;</a>
                                        </div>
                                        <h4 class="text-center"><?php echo esc_html(trending_family_influencer_nice_name($sample_influencer)); ?></h4>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </section><!-- end of influencers section -->
		    <?php
	    }
        $pricing_section = get_field( 'trending_family_home_page_enable_pricing_section' );
		if( $pricing_section ) {
			$section_heading = get_field( 'trending_family_home_page_pricing_section_heading' );
			$section_content = get_field( 'trending_family_home_page_pricing_section_content' ) ;
			$section_images  = get_field( 'trending_family_home_page_pricing_section_image' );  ?>
            <section id="pricing" class="section bg-grey">
                <div class="container">
                    <div class="row text-center-xs">
                        <div class="flex-center flex-revers">
	                        <?php if( !empty( $section_images )){ ?>
                                <div class="col-sm-6 col-xs-12 text-center bg-grey-xs">
                                    <div class="mega-overlay"></div>
                                    <div class="owl-carousel overlay-slider">
		                                <?php foreach ( $section_images as $section_image ) {
			                                $slider_image_url = $section_image['url'];
			                                $slider_image_url = aq_resize( $slider_image_url, 560, 560, true, true, true );
			                                if ( $slider_image_url ) { ?>
                                                <div class="item">
                                                    <img src="<?php echo esc_url( $slider_image_url ); ?>"
                                                         alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                                                </div>
			                                <?php }
		                                } ?>
                                    </div>
                                </div>
	                        <?php } ?>
                           
                            <div class="<?php echo !empty( $section_images ) ? esc_attr('col-sm-6') : esc_attr('col-sm-12'); ?> col-xs-12 offset-bottom-xs-1">
                                <div class="bracket-outline bracket-outline-inverted">
                                    <div>
                                        <?php if( !empty( $section_heading )){ ?>
                                            <h2><?php echo esc_html( $section_heading ); ?></h2>
                                        <?php } ?>
                                        <?php echo $section_content; ?>
                                    </div>
                                </div>
                            </div>
							
                        </div>
                    </div>
                </div>
            </section>
            
		<?php } ?>

		<?php
		$blog_option = get_field( 'trending_family_home_page_enable_blog' );
		if( $blog_option ) {
			$blog_section_heading = get_field( 'trending_family_home_page_blog_section_heading' );
			$blog_cat = get_field( 'trending_family_home_page_blog_taxonomy' );
			$no_of_posts = get_field( 'trending_family_home_page_blog_number_of_posts' ) ? get_field( 'trending_family_home_page_blog_number_of_posts' ) : -1;
			$order_by = get_field( 'trending_family_home_page_blog_order_by' ) ? get_field( 'trending_family_home_page_blog_order_by' ) : 'date';
			$sort_order = get_field( 'trending_family_home_page_blog_sort_order' ) ? get_field( 'trending_family_home_page_blog_sort_order' ) : 'DESC';
			?>

            <section id="blog" class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
							<?php if(!empty( $blog_section_heading )){ ?>
                                <h2 class="text-center"><?php echo esc_html( $blog_section_heading ); ?></h2>
							<?php }
							$blog_args = array(
								'cat'            => $blog_cat,
								'posts_per_page' => $no_of_posts,
								'orderby'        => $order_by,
								'order'          => $sort_order
							);
							$blog_query = new WP_Query( $blog_args );

							if( $blog_query->have_posts() ) { ?>
                                <div class="post-carousel owl-carousel">

									<?php
									while( $blog_query->have_posts() ){
										$blog_query->the_post();
										$thumb = get_post_thumbnail_id();
										$featured_image = wp_get_attachment_url( $thumb, 'full' ); ?>
                                        <div id="post-<?php the_ID(); ?>" <?php post_class( 'post-item post-item-v1' ); ?>>
                                            <div class="post-cont">
												<?php if(!empty( $featured_image )){ ?>
                                                    <a href="<?php the_permalink(); ?>" class="thumb-circle" style="background-image: url(<?php echo esc_url( $featured_image ); ?>); ">&nbsp;</a>
												<?php } ?>
                                                <h4><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
                                                <div class="excerpt">
													<?php the_excerpt(); ?>
                                                </div>
                                                <a class="btn btn-default" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo esc_html__('Read More', 'trending-family'); ?></a>
                                            </div>
                                        </div>
										<?php
									} wp_reset_postdata(); ?>
                                </div>
							<?php } ?>
                        </div>
                    </div>
                </div>
            </section>

		<?php } ?>

		<?php
		$contact_form_section = get_field('trending_family_home_page_enable_contact_form_section');

		if( $contact_form_section ){
			$contact_section_heading = get_field( 'trending_family_home_page_contact_section_heading' );
			$contact_section_tagline = get_field( 'trending_family_home_page_contact_section_tagline' );
			$contact_form_shortcode = get_field( 'trending_family_home_page_contact_section_form_shortcode' ); ?>

            <section id="contact" class="section bg-blue">
                <div class="container">
                    <div class="row">
                        <div class="flex-center">
                            <div class="col-lg-4 col-sm-5 col-xs-12 text-center-xs offset-bottom-xs-2 offset-bottom-sm-0">
								<?php if(!empty( $contact_section_heading )){ ?>
                                    <h2><?php echo esc_html( $contact_section_heading ); ?></h2>
								<?php }
								if( !empty( $contact_section_tagline ) ) {
									echo $contact_section_tagline;
								} ?>
                            </div>
							<?php if(!empty( $contact_form_shortcode )) { ?>
                                <div class="col-lg-8 col-sm-7 col-xs-12 form-line-style">
									<?php echo do_shortcode($contact_form_shortcode); ?>
                                </div>
							<?php } ?>
                        </div>
                    </div>
                </div>
            </section>
		<?php }
		$brands_section = get_field( 'trending_family_home_page_enable_brands_&_clients_section') ;
		if( $brands_section ) {
			$brands_section_heading = get_field( 'trending_family_home_page_brand_section_heading' );
			$no_of_brands = get_field( 'trending_family_home_page_brand_section_number_of_posts' ) ? get_field( 'trending_family_home_page_brand_section_number_of_posts' ) : -1;
			$order_by = get_field( 'trending_family_home_page_brand_section_order_by' ) ? get_field( 'trending_family_home_page_brand_section_order_by' ) : 'date';
			$sort_order = get_field( 'trending_family_home_page_brand_section_sort_order' ) ? get_field( 'trending_family_home_page_brand_section_sort_order' ) : 'DESC';
			?>


            <section class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
							<?php if(!empty( $brands_section_heading )){ ?>
                                <h2 class="text-center offset-bottom-xs-3 offset-bottom-sm-5"><?php echo esc_html( $brands_section_heading ); ?></h2>
							<?php }
							$brands_args = array(
								'posts_per_page' => $no_of_brands,
								'orderby'        => $order_by,
								'order'          => $sort_order,
								'meta_key'       => '_thumbnail_id',
								'post_type'      => 'brand'
							);
							$brands_query = new WP_Query( $brands_args );
							if( $brands_query->have_posts() ) { ?>
                                <div class="brand-carousel owl-carousel">
									<?php while( $brands_query->have_posts() ){
										$brands_query->the_post();
										$thumb = get_post_thumbnail_id();
										$featured_image = wp_get_attachment_url( $thumb, 'full' );
										?>
                                        <div id="brands-<?php the_ID(); ?>" <?php post_class('slide-item'); ?>>
                                            <img src="<?php echo esc_url( $featured_image ); ?>" alt="<?php the_title(); ?>">
                                        </div>
									<?php } wp_reset_postdata(); ?>

                                </div><!-- end of owl-carousel -->
							<?php } ?>
                        </div>
                    </div>
                </div>
            </section>
		<?php }
		$testimonial_section = get_field( 'trending_family_home_page_enable_testimonial_section');
		if( $testimonial_section ) {
			$no_of_testimonials = get_field( 'trending_family_home_page_number_of_testimonials' ) ? get_field( 'trending_family_home_page_number_of_testimonials' ) : - 1;
			$order_by           = get_field( 'trending_family_home_page_testimonial_order_by' ) ? get_field( 'trending_family_home_page_testimonial_order_by' ) : 'date';
			$sort_order         = get_field( 'trending_family_home_page_testimonial_sort_order' ) ? get_field( 'trending_family_home_page_testimonial_sort_order' ) : 'DESC';

			$testimonial_args  = array(
				'posts_per_page' => $no_of_testimonials,
				'orderby'        => $order_by,
				'order'          => $sort_order,
				'post_type'      => 'testimonial'
			);
			$testimonial_query = new WP_Query( $testimonial_args );
			if ( $testimonial_query->have_posts() ) { ?>

                <section class="section bg-green pad-md">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="owl-carousel-single owl-carousel">
									<?php
									while ( $testimonial_query->have_posts() ) {
										$testimonial_query->the_post();
										$testimonial_text       = get_field( 'trending_family_testimonial_text' );
										$testimonial_author     = get_field( 'trending_family_testimonial_author' );
										$testimonial_logo_image = get_field( 'trending_family_testimonial_logo' ); ?>
                                        <div id="testimonial-<?php the_ID(); ?>" <?php post_class( 'testimonial-item' ); ?>>
                                            <div class="row">
                                                <div class="flex-center">
													<?php if ( ! empty( $testimonial_logo_image ) ) { ?>
                                                        <div class="col-sm-4 col-xs-12">
                                                            <img src="<?php echo esc_url( $testimonial_logo_image ); ?>"
                                                                 alt="<?php the_title(); ?>">
                                                        </div>
													<?php } ?>
                                                    <div class="col-sm-8 col-xs-12">
                                                        <blockquote>
                                                            <p><?php echo esc_html( $testimonial_text ); ?></p>
                                                        </blockquote>
														<?php if ( ! empty( $testimonial_author ) ) { ?>
                                                            <p class="author">-<?php echo esc_html( $testimonial_author ); ?>
                                                                -</p>
														<?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- end of testimonial-item -->
									<?php }
									wp_reset_postdata(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
			<?php }
		} ?>
    </div>
<?php get_footer();