<?php 


if ( !empty( $_POST['submitted'] ) && !empty( $_POST['action'] ) && $_POST['action'] == 'update_influencer' ) {
    $nonce = $_POST['trending_family_update_influencer_nonce'];
    if (! $nonce || ! wp_verify_nonce($nonce, 'update_influencer')) {
        die('Security Check');
    }
    
    $errors = array();
    
    /* Update user password */
    if (! empty($_POST['pass1']) && ! empty($_POST['pass2'])) {
        
        if ($_POST['pass1'] != $_POST['pass2']) {
            $errors[] = 'The passwords do not match. Please retry.';
        } elseif (strlen($_POST['pass1']) < 4) {
            $errors[] = 'A bit short as a password, don\'t you thing?';
        } elseif (false !== strpos(wp_unslash($_POST['pass1']), "\\")) {
            $errors[] = 'Password may not contain the character "\\" (backslash).';
        } else {
            wp_update_user(array(
                'ID' => $current_user->ID,
                'user_pass' => esc_attr($_POST['pass1'])
            ));
        }
    }
    
    // Update Influencer Channel Name
    if (! empty($_POST['influencer_channel_name'])) {
        
        update_user_meta($current_user->ID, 'influencer_influencer_channel_name', sanitize_text_field($_POST['influencer_channel_name']));
    }
    
    // Update Influencers First Name
    if (! empty($_POST['influencer_first_name']) && is_array($_POST['influencer_first_name'])) {
        
        $i = 1;
        delete_user_meta($current_user->ID, 'influencer_influencer_first_name');
        
        foreach ($_POST['influencer_first_name'] as $influencer_first_name) {
            
            if ($i == 1) {
                update_user_meta($current_user->ID, 'first_name', sanitize_text_field($influencer_first_name));
            }
            
            add_user_meta($current_user->ID, 'influencer_influencer_first_name', sanitize_text_field($influencer_first_name));
            $i ++;
        }
    }
    
    // Update Influencers Last Name
    if (! empty($_POST['influencer_last_name']) && is_array($_POST['influencer_last_name'])) {
        
        $i = 1;
        delete_user_meta($current_user->ID, 'influencer_influencer_last_name');
        
        foreach ($_POST['influencer_last_name'] as $influencer_last_name) {
            
            if ($i == 1) {
                update_user_meta($current_user->ID, 'last_name', sanitize_text_field($influencer_last_name));
            }
            
            add_user_meta($current_user->ID, 'influencer_influencer_last_name', sanitize_text_field($influencer_last_name));
            $i ++;
        }
    }
    
    // Update Kids Name
    if (! empty($_POST['kids_name']) && is_array($_POST['kids_name'])) {
        
        delete_user_meta($current_user->ID, 'influencer_influencer_kids_name');
        
        foreach ($_POST['kids_name'] as $influencer_kids_name) {
            add_user_meta($current_user->ID, 'influencer_influencer_kids_name', sanitize_text_field($influencer_kids_name));
        }
    }
    
    // Update Kids dob
    if (! empty($_POST['kids_dob']) && is_array($_POST['kids_dob'])) {
        
        delete_user_meta($current_user->ID, 'influencer_influencer_kids_dob');
        
        foreach ($_POST['kids_dob'] as $influencer_kids_dob) {
            add_user_meta($current_user->ID, 'influencer_influencer_kids_dob', sanitize_text_field($influencer_kids_dob));
        }
    }
    
    // Update Pets Name
    if (! empty($_POST['pets_name']) && is_array($_POST['pets_name'])) {
        
        delete_user_meta($current_user->ID, 'influencer_influencer_pets_name');
        
        foreach ($_POST['pets_name'] as $influencer_pets_name) {
            add_user_meta($current_user->ID, 'influencer_influencer_pets_name', sanitize_text_field($influencer_pets_name));
        }
    }
    
    // Update Pets Description
    if (! empty($_POST['pets_description']) && is_array($_POST['pets_description'])) {
        
        delete_user_meta($current_user->ID, 'influencer_influencer_pets_description');
        
        foreach ($_POST['pets_description'] as $influencer_pets_description) {
            add_user_meta($current_user->ID, 'influencer_influencer_pets_description', sanitize_text_field($influencer_pets_description));
        }
    }
    
    // Update Languages
    if (! empty($_POST['languages']) && is_array($_POST['languages'])) {
        
        delete_user_meta($current_user->ID, 'influencer_influencer_languages');
        
        foreach ($_POST['languages'] as $language) {
            add_user_meta($current_user->ID, 'influencer_influencer_languages', sanitize_text_field($language));
        }
    }
    
    // Update Brand Preferences
    if (! empty($_POST['brands']) && is_array($_POST['brands'])) {
        
        delete_user_meta($current_user->ID, 'influencer_influencer_brands');
        
        foreach ($_POST['brands'] as $brands) {
            add_user_meta($current_user->ID, 'influencer_influencer_brands', sanitize_text_field($brands));
        }
    }
    
    if (! empty($_POST['email'])) {
        
        if (! is_email(sanitize_email($_POST['email']))) 

        $errors[] = __('The Email you entered is not valid.  please try again.', 'trending-family');
        
        elseif (email_exists(sanitize_email($_POST['email'])) != $current_user->ID) 

        $errors[] = __('This email is already used by another user.  try a different one.', 'trending-family');
        
        else {
            wp_update_user(array(
                'ID' => $current_user->ID,
                'user_email' => sanitize_email($_POST['email'])
            ));
        }
    }
    
    // Update Phone Number
    if (! empty($_POST['phone-number'])) {
        
        update_user_meta($current_user->ID, 'influencer_influencer_phone', sanitize_text_field($_POST['phone-number']));
    }
    
    // Update DOB
    if (! empty($_POST['dob'])) {
        
        update_user_meta($current_user->ID, 'influencer_influencer_dob', sanitize_text_field($_POST['dob']));
    }
    
    // Update Address
    
    if (! empty($_POST['address'])) {
        
        update_user_meta($current_user->ID, 'influencer_influencer_address', sanitize_text_field($_POST['address']));
    }
    
    // Update City
    if (! empty($_POST['city'])) {
        
        update_user_meta($current_user->ID, 'influencer_influencer_city', sanitize_text_field($_POST['city']));
    }
    
    // Update State
    if (! empty($_POST['state'])) {
        
        update_user_meta($current_user->ID, 'influencer_influencer_state', sanitize_text_field($_POST['state']));
    }
    
    // Update ZIP
    
    if (! empty($_POST['zip'])) {
        
        update_user_meta($current_user->ID, 'influencer_influencer_zip', sanitize_text_field($_POST['zip']));
    }
    
    // Update Country
    if (! empty($_POST['country'])) {
        
        update_user_meta($current_user->ID, 'influencer_influencer_country', sanitize_text_field($_POST['country']));
    }
    
    if (empty($errors)) {
        
        do_action('edit_user_profile_update', $current_user->ID);
        
        update_user_meta($current_user->ID, 'admin_clone_update', 1);
        
        set_transient($success, 'Profile updated successfully!', 60 * 60 * 12);
        
        wp_redirect(site_url('/influencers/'));
        
        exit();
    }
}