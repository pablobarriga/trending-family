<?php if ( ! defined( 'ABSPATH' ) ) exit;
/*
 * Template Name: Influencer Landing Page
 */
session_start();
global $current_user;
wp_get_current_user();
 
/*
 * Please don't comment this as this page is only accessible to users with Influencer Role.
 * Please ping me on Slack if you have any confusion.
 * @author Prajwal Shrestha
 */
if(!in_array('influencer', $current_user->roles)) {
	global $wp_query;
	$wp_query->set_404();
	status_header( 404 );
	get_template_part( 404 );
	exit();
}
//include_once get_template_directory(). "/includes/config.php";

/**
 * Handle POST requests
 */
if( $_POST) {
    require __DIR__ . '/page-influencer-landing-post-handler.php'; // update user data
    require __DIR__ . '/page-influencer-landing-post-social-handler.php'; // update social media links
}




/**
 * Handle social medial callbacks
 */
require_once __DIR__ . '/../social-media/oauth-callback.php'; // update user data

// Social media data
$youtubeService = new SocialMedia\SocialAccounts\Youtube($current_user);
$youtubeChannels = $youtubeService->cachedData('youtube_channels');

$facebookService = new SocialMedia\SocialAccounts\Facebook($current_user);
$facebookPages = $facebookService->cachedData('facebook_pages');
$facebookService->pages();

$instagramService = new SocialMedia\SocialAccounts\Instagram($current_user);
$instagramPages = $instagramService->cachedData('instagram_pages');

$twitterService = new SocialMedia\SocialAccounts\Twitter($current_user);
$twitterAccounts = $twitterService->cachedData('twitter_accounts'); 

$pinterestService = new SocialMedia\SocialAccounts\Pinterest($current_user);
$pinterestAccounts =  $pinterestService->accounts();

$snapchatAccounts = ( new SocialMedia\SocialAccounts\Snapchat($current_user) )->accounts();
$blogAccounts = ( new SocialMedia\SocialAccounts\Blog($current_user) )->accounts();

get_header(); ?>

<style>
tr.invalid-account {
    background: #ffa1a1;
}
</style>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <section class="section bg-green pad-md text-dark text-center-xs">
        <div class="container">
            <div class="row row-lg">
                <div class="flex-center">
                    <div class="col-xs-12 p-title">
						<?php
						$influencer_display_first_name = get_the_author_meta( 'first_name', $current_user->ID );
						$influencer_display_last_name = get_the_author_meta( 'last_name', $current_user->ID );
						$influencer_display_name = !empty( $influencer_display_first_name ) ? $influencer_display_first_name . ' ' . $influencer_display_last_name : $current_user->display_name; ?>
                        <h1><?php echo esc_html__('Welcome', 'trending-family'); ?> <span class="txt-wlight"><?php echo esc_html( $influencer_display_name ); ?></span></h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section pad-top-xxs-0 bg-grey">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
					<?php
					if ( !empty( get_transient( $success )) ): ?>
                        <div class="alert alert-success text-center alert-dismissable">
                            <a class="close dismiss-alert" data-dismiss="alert" aria-label="close">&times;</a>
                            <i class="fa fa-thumbs-o-up"></i>
							<?php echo esc_html(get_transient( $success )); ?>
                        </div>
						<?php delete_transient( $success );
					endif;
					if ( !empty( $errors ) ):
						foreach ($errors as $error ): ?>
                            <div class="alert alert-danger text-center alert-dismissable">
                                <a class="close dismiss-alert" data-dismiss="alert" aria-label="close">&times;</a>
                                <i class="fa fa-thumbs-o-up"></i>
								<?php echo $error; ?>
                            </div>
						<?php endforeach;
						$errors = [];
					endif; ?>
                    <div class="s-title">
                        <h3><?php echo esc_html__('Your social media', 'trending-family'); ?><span class="hidden-xs">:</span></h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 mob-social-option-box">
                    <div class="social-option-box offset-bottom-sm-7 offset-bottom-xs-2">
                        <table class="table table-social <?php echo $youtubeChannels ? esc_attr('table-with-hover') : ''; ?>">
                            <thead>
                            <tr>
                                <th class="col-logo without-hp">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/images/blogo-1.png'; ?>" alt="<?php echo esc_attr( $current_user->display_name ); ?>">
                                </th>
                                <th><span class="hidden-xs"><?php echo esc_html__('YouTube Channel(s)', 'trending-family'); ?></span></th>
                                <th class="text-right">
                                    <a class="hidden-xs" href="<?php echo esc_url($youtubeService->getAuthUrl()); ?>"><?php echo esc_html__('Add New', 'trending-family'); ?>
                                        <i class="fa fa-plus-circle"></i>
                                    </a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
							<?php if( $youtubeChannels): ?>
									<?php 
									foreach ($youtubeChannels as $k => $item):  
									
                                        if(isset($item['channel_id'])): ?>
                                            <tr class="<?php echo isset($item['is_valid']) && $item['is_valid'] ? '' : 'invalid-account'; ?>" data-id="<?php echo $item['channel_id'];  ?>" data-user="<?php echo $current_user->ID; ?>">
                                                <td>
                                                    <span class="thumb-circle">
                                                        <img src="<?php echo esc_url($item['thumbnails']); ?>" alt="<?php echo $item['youtube_title']; ?>">
                                                    </span>
                                                </td>
                                                <td>
                                                    <b><?php echo $item['youtube_title']; ?></b> 
                                                    <em>-</em>
                                                    <span class="social-date"><?php echo $item['subscriberCount']; ?> Subscriber</span>
                                                    <em>,</em> 
                                                    <span class="social-date"><?php echo $item['videoCount']; ?> Videos</span>
                                                    <em>,</em>
                                                    <span class="social-date"><?php echo $item['viewCount']; ?> Views</span> 
                                                </td>
                                                <td class="text-right">
                                                    <a class="btn-disconect youtube-disconect">
                                                        <span class="hidden-xs"><?php echo esc_html__('Disconnect', 'trending-family'); ?></span> 
                                                        <i class="fa fa-minus-circle"></i>
                                                    </a>
                                                </td>
                                            </tr>
									    <?php endif; ?>
									    
									<?php endforeach; ?>
							<?php else: ?>
                            <tr>
								<?php if($htmlBody): ?>
                                    <td class="without-hp" colspan="3"><i><?php echo $htmlBody ?></i></td>
								<?php else: ?>
                                    <td class="without-hp" colspan="3"><i><?php echo esc_html__('No account has been connected yet.', 'trending-family'); ?></i></td>
								<?php endif; ?>
                            </tr>
							<?php endif; ?>
                            </tbody>
                        </table>
                        <a class="btn btn-success btn-add btn-block btn-outline visible-xs" href="<?php echo esc_url($authUrl); ?>"><?php echo esc_html__('Add New', 'trending-family'); ?></a>
                    </div>
                    <div class="social-option-box offset-bottom-sm-7 offset-bottom-xs-2">
                        <table class="table table-social <?php echo $facebookPages ? esc_attr('table-with-hover') : ''; ?>">
                            <thead>
                                <tr>
                                    <th class="col-logo without-hp">
                                        <img src="<?php echo get_template_directory_uri() . '/assets/images/blogo-2.png'; ?>" alt="<?php echo esc_attr( $current_user->display_name ); ?>">
                                    </th>
                                    <th>
                                        <span class="hidden-xs"><?php echo esc_html__('Facebook Fan Page(s)', 'trending-family'); ?></span>
                                    </th>
                                    <th class="text-right">
                                        <?php if($facebookPages): ?>
                                        <a href="#"  class="fbconnectpage hidden-xs text-warning" style="padding-right:15px;"><?php echo esc_html__('Connect/Disconnect My Pages', 'trending_family'); ?> <i class="fa fa-plug"></i></a>
                                        <?php endif; ?>
                                        <a href="<?php echo $facebookService->getAuthUrl(); ?>" id="fbaddpage" class="hidden-xs"><?php echo esc_html__('Add New', 'trending_family'); ?> <i class="fa fa-plus-circle"></i></a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody >
							<?php
							if($facebookPages):
								foreach ($facebookPages as $ownerPages) { ?>
								    <?php foreach($ownerPages as $facebookdata) { if( ! $facebookdata['is_connected']) continue;  ?>
                                    <tr class="<?php echo isset($facebookdata['is_valid']) && $facebookdata['is_valid'] ? '' : 'invalid-account'; ?>" id="fblistings" data-page="<?php echo $facebookdata['pageid'] ;?>" data-user="<?php echo $current_user->ID; ?>">
                                        <td><span class="thumb-circle"><img src="<?php echo $facebookdata['fb_pic'] ;?>" alt="<?php echo esc_attr($facebookdata['pagename']); ?>"></span></td>
                                        <td><b><?php echo $facebookdata['pagename']; ?></b> <em>-</em> <span class="social-date"><?php echo $facebookdata['fan_count']; ?> <?php echo esc_html__('Followers', 'trending-family'); ?></span> </td>
                                        <td class="text-right"><a class="btn-disconect face-disconect" id="<?php echo $facebookdata['pageid'] ;?>" href="javascript:void(0);"><span class="hidden-xs">Disconnect</span> <i class="fa fa-minus-circle"></i></a></td>
                                   </tr>
								<?php }}
							   else: ?>
                                <tr>
									<?php if($htmlBody): ?>
                                        <td class="without-hp" colspan="3"><i><?php echo $htmlBody ?></i></td>
									<?php else: ?>
                                        <td class="without-hp" colspan="3"><i><?php echo esc_html__('No account has been connected yet.', 'trending-family'); ?></i></td>
									<?php endif; ?>
                                </tr>
							<?php endif; ?>
                            </tbody>
                        </table>
                        <a class="btn btn-success btn-add btn-block btn-outline visible-xs" href="javascript:void(0);" onclick="fbAuthUser();"><?php echo esc_html__('Add New', 'trending-family'); ?></a>
                        <?php if($facebookPages): ?>
                        <a class="btn btn-success btn-add btn-block btn-outline visible-xs fbconnectpage" href="#"><?php echo esc_html__('Connect/Disconnect My Pages', 'trending-family'); ?></a>
                        <?php endif; ?>
                    </div>
                    <div class="social-option-box offset-bottom-sm-7 offset-bottom-xs-2">
                        <table class="table table-social <?php echo $instagramPages ? esc_attr('table-with-hover') : ''; ?>">
                            <thead>
                            <tr>
                                <th class="col-logo without-hp"><img src="<?php echo get_template_directory_uri() . '/assets/images/blogo-3.png';?>" alt="<?php echo esc_attr( $current_user->display_name ); ?>"></th>
                                <th><span class="hidden-xs"><?php echo esc_html__('Instagram Page(s)', 'trending-family'); ?></span></th>
                                <th class="text-right"><a class="hidden-xs" href="<?php echo esc_url( $instagramService->getAuthUrl() ); ?>"><?php echo esc_html__('Add New', 'trending-family'); ?><i class="fa fa-plus-circle"></i></a></th>
                            </tr>
                            </thead>
                            <tbody>
							<?php if( $instagramPages): ?>
								<?php foreach ($instagramPages as $user): 

								        if(!empty($user)){									  
								            if(isset($user['id'])){
								?>
                                    <tr class="<?php echo isset($user['is_valid']) && $user['is_valid'] ? '' : 'invalid-account'; ?>" data-page="<?php echo $user['id']; ?>" data-user="<?php echo $current_user->ID; ?>">
                                        <td><span class="thumb-circle"><img src="<?php echo esc_url($user['profilePicUrl']); ?>" alt="<?php echo esc_attr( $user['username'] ); ?>"></span></td>
                                        <td><b><?php echo $user['fullName']."(". $user['username'].")" ?></b> <em>-</em> <span class="social-date"><?php echo $user['followedByCount']; ?> <?php echo esc_html__('subscribers', 'trending-family'); ?></span><em>,</em> <span class="social-date"><?php echo $user['followsCount']; ?> <?php echo esc_html__('following', 'trending-family'); ?></span><em>,</em> <span><?php echo $user['mediaCount']; ?> <?php echo esc_html__('posts', 'trending-family'); ?></span></td>
                                        <td class="text-right"><a class="btn-disconect inst-disconect" href="javascript:void(0);" data-key="instagram"><span class="hidden-xs"><?php echo esc_attr__('Disconnect', 'trending-family'); ?></span> <i class="fa fa-minus-circle"></i></a></td>
                                    </tr>
								   <?php }else{ ?>
										 <tr>
											<td class="without-hp" colspan="3"><i><?php echo esc_html__('No account has been connected yet.', 'trending-family'); ?></i></td>
										</tr>
								   <?php } } endforeach; ?>
							<?php else: ?>
                                <tr>
                                    <td class="without-hp" colspan="3"><i><?php echo esc_html__('No account has been connected yet.', 'trending-family'); ?></i></td>
                                </tr>
							<?php endif; ?>
                            </tbody>
                        </table>
                        <a class="btn btn-success btn-add btn-block btn-outline visible-xs" href="<?php echo esc_url( $insta_url ); ?>"><?php echo esc_html__('Add New', 'trending-family'); ?></a>
                    </div>
                    <div class="social-option-box offset-bottom-sm-7 offset-bottom-xs-2">
                        <table class="table table-social <?php echo $twitterAccounts ? esc_attr('table-with-hover') : ''; ?>">
                            <thead>
                            <tr>
                                <th class="col-logo without-hp"><img src="<?php echo get_template_directory_uri() . '/assets/images/blogo-4.png'; ?>" alt="<?php echo esc_attr( $current_user->display_name ); ?>"></th>
                                <th><span class="hidden-xs"><?php echo esc_html__('Twitter Account(s)', 'trending-family'); ?></span></th>
                                <th class="text-right"><a class="hidden-xs" href="<?php echo $twitterService->getAuthUrl(); ?>"><?php echo esc_html__('Connect Account', 'trending-family'); ?> <i class="fa fa-plus-circle"></i></a></th>
                            </tr>
                            </thead>
                            <tbody>
							<?php if ( $twitterAccounts ):
								foreach ( $twitterAccounts as $tw_account ): ?>
                                    <tr class="<?php echo isset($tw_account['is_valid']) && $tw_account['is_valid'] ? '' : 'invalid-account'; ?>" data-page="<?php echo $tw_account['id']; ?>" data-user="<?php echo $current_user->ID; ?>">
                                        <td>
                                            <span class="thumb-circle">
                                                <?php $profile_image_url = $tw_account['profile_image_url']; $profile_image_url = str_replace('_normal', '_bigger', $profile_image_url ); ?>
                                                <img src="<?php echo esc_url($profile_image_url); ?>" alt="<?php echo esc_attr( $tw_account['screen_name'] ); ?>">
                                            </span>
                                        </td>
                                        <td>
                                            <b><?php echo $tw_account['name']."(". $tw_account['screen_name'].")" ?></b> 
                                            <em>-</em> 
                                            <span class="social-date"><?php echo $tw_account['followers_count']; ?> 
                                                <?php echo esc_html__('followers', 'trending-family'); ?>
                                            </span>
                                            <em>,</em> 
                                            <span class="social-date">
                                                <?php echo $tw_account['friends_count']; ?> <?php echo esc_html__('following', 'trending-family'); ?>
                                            </span>
                                            <em>,</em> 
                                            <span>
                                                <?php echo $tw_account['statuses_count']; ?> <?php echo esc_html__('posts', 'trending-family'); ?>
                                            </span>
                                        </td>
                                        <td class="text-right"><a class="btn-disconect twitt-disconect"><span class="hidden-xs"><?php echo esc_html__('Disconnect', 'trending-family'); ?></span> <i class="fa fa-minus-circle"></i></a></td>
                                    </tr>
								<?php endforeach;
							else: ?>
                                <tr>
                                    <td class="without-hp" colspan="3"><i><?php echo esc_html__('No account has been connected yet.', 'trending-family'); ?></i></td>
                                </tr>
							<?php endif; ?>
                            </tbody>
                        </table>
                        <a class="btn btn-success btn-add btn-block btn-outline visible-xs btn-add-social" href="javascript:void(0);"><?php echo esc_html__('Connect Account', 'trending-family'); ?></a>
                        <div class="login-popup-box-wrap right-175">
                            <div class="login-popup-box">
                                <form class="validateForm" method="POST" autocomplete="off">
									<?php wp_nonce_field( 'trending_family_connect_social_nonce', 'security' ); ?>
                                    <p class="connect-error" style="display: none;"></p>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="twitter_account" placeholder="<?php echo esc_attr__('Username *', 'trending-family'); ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <a class="btn btn-default add_page" href="javascript:void(0);"><?php echo esc_html__('Add Account', 'trending-family'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="social-option-box offset-bottom-sm-7 offset-bottom-xs-2">
                        <table class="table table-social <?php echo $pinterestAccounts ? esc_attr('table-with-hover') : ''; ?>">
                            <thead>
                            <tr>
                                <th class="col-logo without-hp"><img src="<?php echo get_template_directory_uri() . '/assets/images/blogo-5.png'; ?>" alt="<?php echo esc_attr( $current_user->display_name ); ?>"></th>
                                <th><span class="hidden-xs"><?php echo esc_html__('Pinterest Account(s)', 'trending-family'); ?></span></th>
                                <th class="text-right"><a class="hidden-xs" href="<?php echo $pinterestService->getAuthUrl(); ?>"><?php echo esc_html__('Connect Account', 'trending-family'); ?>
                                        <i class="fa fa-plus-circle"></i></a></th>
                            </tr>
                            </thead>
                            <tbody>
							<?php if ( $pinterestAccounts ): 
							?>
								<?php foreach ($pinterestAccounts as $pin_account): ?>
                                    <tr data-page="<?php echo $pin_account['id']; ?>" data-user="<?php echo $current_user->ID; ?>">
                                        <td><span class="thumb-circle"><img src="<?php echo esc_url($pin_account['image']['large']['url']); ?>" alt="<?php echo esc_attr( $pin_account['username'] ); ?>"></span></td>
                                        <td><b><?php echo $pin_account['first_name']." ".$pin_account['last_name']." (". $pin_account['username'].")" ?></b> <em>-</em> <span class="social-date"><?php echo $pin_account['counts']['followers']; ?> <?php echo esc_html__('followers', 'trending-family'); ?></span><em>,</em> <span class="social-date"> <?php echo $pin_account['counts']['following']; ?> <?php echo esc_html__('following', 'trending-family'); ?></span><em>,</em> <span> <?php echo $pin_account['counts']['boards']; ?> <?php echo esc_html__('boards', 'trending-family'); ?></span><em>,</em> <span> <?php echo $pin_account['counts']['pins']; ?> <?php echo esc_html__('pins', 'trending-family'); ?></span></td>
                                        <td class="text-right"><a class="btn-disconect pin-disconect"><span class="hidden-xs"><?php echo esc_html__('Disconnect', 'trending-family'); ?></span> <i class="fa fa-minus-circle"></i></a></td>
                                    </tr>
								<?php endforeach; ?>
							<?php else: ?>
                                <tr>
                                    <td class="without-hp" colspan="3"><i><?php echo esc_html__('No account has been connected yet.', 'trending-family'); ?></i></td>
                                </tr>
							<?php endif; ?>
                            </tbody>
                        </table>
                        <a class="btn btn-success btn-add btn-block btn-outline visible-xs btn-add-social" href="javascript:void(0);"><?php echo esc_html__('Connect Account', 'trending-family'); ?></a>
                        <div class="login-popup-box-wrap right-175">
                            <div class="login-popup-box">
                                <form class="validateForm" method="POST" autocomplete="off">
                                    <p class="connect-error" style="display: none;"></p>
									<?php wp_nonce_field( 'trending_family_connect_social_nonce', 'security' ); ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="pinterest_account" placeholder="<?php echo esc_attr__('Username *', 'trending-family'); ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <a class="btn btn-default add_page" href="javascript:void(0);"><?php echo esc_html__('Add Account', 'trending-family'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="social-option-box offset-bottom-sm-7 offset-bottom-xs-2">
                        <table class="table table-social <?php echo !empty( $snapchat_accounts ) ? esc_attr('table-with-hover') : ''; ?>">
                            <thead>
                            <tr>
                                <th class="col-logo without-hp"><img src="<?php echo get_template_directory_uri() .'/assets/images/snapchat_logo.png'; ?>" alt="<?php echo esc_attr( $current_user->display_name ); ?>"></th>
                                <th><span class="hidden-xs"><?php echo esc_html__('Snapchat Account(s)', 'trending-family'); ?></span></th>
                                <th class="text-right btn-add-social"><a class="hidden-xs ss" href="javascript:void(0);"><?php echo esc_html__('Connect Account', 'trending-family'); ?>
                                        <i class="fa fa-plus-circle"></i></a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
							<?php
							if( $snapchatAccounts ){
								$i = 0;
								foreach( $snapchatAccounts as $snap_account ) { ?>
                                    <tr data-index="<?php echo $i; ?>" data-user="<?php echo $current_user->ID; ?>">
                                        <td><b><a href="javascript:void(0);"><?php echo esc_html($snap_account[0]); ?></a></b></td>
                                        <td><span><?php echo esc_html($snap_account[1]); ?> <?php echo esc_html__('estimated Avg Views/Snap', 'trending-family'); ?></span></td>
                                        <td class="text-right"><a  class="btn-disconect snap-disconnect"><span class="hidden-xs"><?php echo esc_html__('Disconnect', 'trending-family'); ?></span> <i class="fa fa-minus-circle"></i></a></td>
                                    </tr>
									<?php
									$i++;
								}
							} else { ?>
                                <tr>
                                    <td class="without-hp" colspan="3"><i><?php echo esc_html__('No account has been connected yet.', 'trending-family'); ?></i></td>
                                </tr>
							<?php } ?>
                            </tbody>
                        </table>
                        <a class="btn btn-success btn-add btn-block btn-outline visible-xs btn-add-social" href="javascript:void(0);"><?php echo esc_html__('Connect Account', 'trending-family'); ?></a>
                        <div class="login-popup-box-wrap right-175">
                            <div class="login-popup-box">
                                <form class="validateForm" id="connect_snapchat" method="POST" autocomplete="off">
									<?php wp_nonce_field( 'trending_family_connect_social_nonce', 'security' ); ?>
                                    <p class="connect-error" style="display: none;"></p>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <input type="text" name="influencer_snapchat_handle" class="form-control" placeholder="<?php echo esc_attr__('Snapchat Handle *', 'trending-family'); ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <input type="text" name="influencer_snapchat_avg_snaps" class="form-control" placeholder="<?php echo esc_attr__('Estimated Avg Views/Snap *', 'trending-family'); ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <a class="btn btn-default add_page" href="javascript:void(0);"><?php echo esc_html__('Submit', 'trending-family'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="social-option-box offset-bottom-sm-7 offset-bottom-xs-2">
                        <table class="table table-social <?php echo !empty( $influencer_blogs ) ? esc_attr('table-with-hover') : ''; ?>">
                            <thead>
                            <tr>
                                <th class="without-hp"><?php echo esc_html__('Blog', 'trending-family'); ?></th>
                                <th><span class="hidden-xs"></th>
                                <th class="text-right btn-add-social"><a class="hidden-xs" href="javascript:void(0);"><?php echo esc_html__('Add Blog', 'trending-family'); ?>
                                        <i class="fa fa-plus-circle"></i></a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
							<?php
							if( $blogAccounts ) {
								$i = 0;
								foreach ( $blogAccounts as $blog ) { ?>
                                    <tr data-index="<?php echo $i; ?>" data-user="<?php echo $current_user->ID; ?>">
                                        <td><b>
                                                <a href="<?php echo esc_url($blog[0]); ?>" title="<?php echo sprintf(__('Goto %s', 'trending-family'), $blog[0]); ?>" target="_blank"><?php echo esc_html($blog[0]); ?></a></b> <em>-</em> <span><?php echo esc_html( $blog[1]); ?> <?php echo esc_html__('unique monthly visitors', 'trending-family'); ?></span><em>,</em>
                                            <span><?php echo esc_html( $blog[2]); ?> <?php echo esc_html__('monthly page views', 'trending-family'); ?></span></td>
                                        <td></td>
                                        <td class="text-right"><a class="btn-disconect blog-disconect"><span class="hidden-xs"><?php echo esc_html__('Disconnect', 'trending-family'); ?></span> <i class="fa fa-minus-circle"></i></a></td>
                                    </tr>
									<?php
									$i++;
								}
							} else { ?>
                                <tr>
                                    <td class="without-hp" colspan="3"><i><?php echo esc_html__('No blog has been connected yet.', 'trending-family'); ?></i></td>
                                </tr>
							<?php } ?>
                            </tbody>
                        </table>
                        <a class="btn btn-success btn-add btn-block btn-outline visible-xs btn-add-social" href="javascript:void(0);"><?php echo esc_html__('Add Blog', 'trending-family'); ?></a>
                        <div class="login-popup-box-wrap">
                            <div class="login-popup-box">
                                <form class="validateForm" id="connect_blog" method="POST" autocomplete="off">
									<?php wp_nonce_field( 'trending_family_connect_social_nonce', 'security' ); ?>
                                    <p class="connect-error" style="display: none;"></p>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="influencer_blog_url" placeholder="<?php echo esc_attr__('Blog URL *', 'trending-family'); ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="influencer_blog_umv" placeholder="<?php echo esc_attr__('Unique Monthly Visitors *', 'trending-family'); ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="influencer_blog_mpv" placeholder="<?php echo esc_attr__('Monthly Page Views *', 'trending-family'); ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <a class="btn btn-default add_page" href="javascript:void(0);"><?php echo esc_html__( 'Submit', 'trending-family'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section pad-top-0 bg-grey bg-white-xs">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="s-title">
                        <h3><?php echo esc_html__('Your profile', 'trending-family'); ?><span class="hidden-xs">:</span></h3>
                    </div>
                </div>
            </div>
            <form id="update_influencer" method="POST" autocomplete="off">
				<?php wp_nonce_field( 'update_influencer', 'trending_family_update_influencer_nonce' ); ?>
                <input type="hidden" name="action" value="update_influencer">
                <div class="row profile-options-box">
                    <div class="col-xs-12">
						<?php
						$influencer_channel_name = get_user_meta($current_user->ID, 'influencer_influencer_channel_name', true); ?>
                        <fieldset>
                            <div class="legend">
                                <span><?php echo esc_html__( 'Channel', 'trending-family' ); ?></span>
                            </div>
                            <div class="row row-sm form-row">
                                <div class="col-sm-12 col-xs-12">
                                    <label class="label_influencer_channel_name"
                                           for="influencer_channel_name"><?php echo esc_html__( 'Name', 'trending-family' ); ?></label>
                                    <div class="form-group">
                                        <input type="text" name="influencer_channel_name"
                                               id="influencer_channel_name"
                                               class="form-control input-lg influencer_channel_name" value="<?php echo esc_attr($influencer_channel_name); ?>">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
						<?php
						$influencer_influencer_first_name = get_user_meta($current_user->ID, 'influencer_influencer_first_name');
						$influencer_influencer_last_name = get_user_meta($current_user->ID, 'influencer_influencer_last_name'); ?>
                        <fieldset>
                            <div class="legend">
                                <span><?php echo esc_html__( 'Influencers', 'trending-family' ); ?></span>
                                <a class="btn-text hidden-xs pull-right addInfluencer"
                                   href="javascript:void(0);">
                                    <span><?php echo esc_html__( 'Add', 'trending-family' ); ?></span><i
                                            class="fa fa-plus-circle"></i>
                                </a>
                            </div>
							<?php
							if( !empty( $influencer_influencer_first_name) && is_array( $influencer_influencer_first_name ) ) {
								for ( $i = 0; $i < count( $influencer_influencer_first_name ); $i++ ) { ?>
                                    <div id="entryInfluencer<?php echo esc_attr( intval( $i + 1 ) ); ?>"
                                         class="clonedInfluencer">
                                        <div class="row row-sm form-row">
                                            <a class="btn-add btnInfluencerDel" href="javascript:void(0);"><i
                                                        class="fa fa-times-circle"></i></a>
                                            <div class="col-sm-6 col-xs-12">
                                                <label class="label_influencer_first_name"
                                                       for="label_influencer_first_name"><?php echo esc_html__( 'First Name', 'trending-family' ); ?></label>
                                                <div class="form-group">
                                                    <input type="text" name="influencer_first_name[]"
                                                           id="influencer_first_name"
                                                           class="form-control input-lg influencer_first_name"
                                                           value="<?php echo esc_attr( $influencer_influencer_first_name[ $i ] ); ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-xs-12">
                                                <label class="label_influencer_last_name"
                                                       for="influencer_last_name"><?php echo esc_html__( 'Last Name', 'trending-family' ); ?></label>
                                                <div class="form-group">
                                                    <input type="text" name="influencer_last_name[]"
                                                           id="influencer_last_name"
                                                           class="form-control input-lg influencer_last_name"
                                                           value="<?php echo esc_attr( $influencer_influencer_last_name[ $i ] ); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								<?php } ?>
                                <div class="row row-sm">
                                    <div class="col-xs-12">
                                        <a class="btn btn-success btn-add btn-block btn-outline offset-top-xs-1 visible-xs addInfluencer"
                                           href="javascript:void(0);">
											<?php echo esc_html__( 'Add', 'trending-family' ); ?>
                                        </a>
                                    </div>
                                </div>
							<?php } else { ?>
                                <div id="entryInfluencer1" class="clonedInfluencer">
                                    <div class="row row-sm form-row">
                                        <a class="btn-add btnInfluencerDel" href="javascript:void(0);"><i class="fa fa-times-circle"></i></a>
                                        <div class="col-sm-6 col-xs-12">
                                            <label class="label_influencer_first_name" for="label_influencer_first_name"><?php echo esc_html__('First Name', 'trending-family'); ?></label>
                                            <div class="form-group">
                                                <input type="text" name="influencer_first_name" id="influencer_first_name" class="form-control input-lg influencer_first_name" value="<?php echo esc_attr( $influencer_display_first_name ); ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <label class="label_influencer_last_name" for="influencer_last_name"><?php echo esc_html__('Last Name', 'trending-family'); ?></label>
                                            <div class="form-group">
                                                <input type="text" name="influencer_last_name" id="influencer_last_name" class="form-control input-lg influencer_last_name" value="<?php echo esc_attr( $influencer_display_last_name ); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row-sm">
                                        <div class="col-xs-12">
                                            <a class="btn btn-success btn-add btn-block btn-outline offset-top-xs-1 visible-xs addInfluencer" href="javascript:void(0);">
												<?php echo esc_html__('Add', 'trending-family'); ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
							<?php } ?>
                        </fieldset>
                        <fieldset>
                            <div class="legend">
                                <span><?php echo esc_html__( 'Contact Info', 'trending-family' ); ?></span>
                            </div>
                            <div class="row row-sm form-row-sm">
                                <div class="col-sm-4 col-xs-12">
                                    <label for="influencer-email"><?php echo esc_html__('E-Mail', 'trending-family'); ?></label>
                                    <div class="form-group">
										<?php
										$influencer_email = get_user_meta($current_user->ID, 'influencer_influencer_email', true); ?>
                                        <input type="text" name="email" class="form-control input-lg" id="influencer-email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>">
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <label for="influencer-phone-number"><?php echo esc_html__('Phone Number', 'trending-family'); ?></label>
                                    <div class="form-group">
										<?php
										$influencer_phone = get_user_meta($current_user->ID, 'influencer_influencer_phone', true); ?>
                                        <input type="text" name="phone-number" class="form-control input-lg phone" id="influencer-phone-number" value="<?php echo !empty( $influencer_phone ) ? esc_attr( $influencer_phone ) : ''; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <label for="influencer-dob"><?php echo esc_html__('Date of Birth', 'trending-family'); ?></label>
                                    <div class="form-group">
										<?php
										$influencer_dob = get_user_meta($current_user->ID, 'influencer_influencer_dob', true); ?>
                                        <input type="text" name="dob" class="form-control input-lg dob datepicker-input" id="influencer-dob" value="<?php echo !empty( $influencer_dob ) ? esc_attr( $influencer_dob ) : ''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm form-row-sm">
                                <div class="col-xs-12">
                                    <label for="influencer-address"><?php echo esc_html__('Address', 'trending-family'); ?></label>
                                    <div class="form-group">
										<?php
										$influencer_address = get_user_meta($current_user->ID, 'influencer_influencer_address', true); ?>
                                        <input type="text" name="address" class="form-control input-lg" id="influencer-address" value="<?php echo !empty( $influencer_address ) ? esc_attr( $influencer_address ) : ''; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm form-row-sm">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <label for="influencer-city"><?php echo esc_html__('City', 'trending-family'); ?></label>
                                    <div class="form-group">
										<?php
										$influencer_city = get_user_meta($current_user->ID, 'influencer_influencer_city', true); ?>
                                        <input type="text" name="city" class="form-control input-lg" id="influencer-city" value="<?php echo !empty( $influencer_city ) ? esc_attr( $influencer_city ) : ''; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <label for="state"><?php echo esc_html__('State', 'trending-family'); ?></label>
                                    <div class="form-group">
										<?php
										$influencer_state = get_user_meta( $current_user->ID, 'influencer_influencer_state', true ); ?>
                                        <select class="form-control input-lg" name="state" id="state"></select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <label for="zip"><?php echo esc_html('Zip', 'trending-family'); ?></label>
                                    <div class="form-group">
										<?php
										$influencer_zip = get_user_meta($current_user->ID, 'influencer_influencer_zip', true); ?>
                                        <input type="text" class="form-control input-lg" name="zip" id="zip" value="<?php echo !empty( $influencer_zip ) ? esc_attr( $influencer_zip ) : ''; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <label for="country"><?php echo esc_html__('Country', 'trending-family'); ?></label>
                                    <div class="form-group">
										<?php
										$influencer_country = get_user_meta( $current_user->ID, 'influencer_influencer_country', true ); ?>
                                        <select class="form-control input-lg" onchange="print_state('state', this.selectedIndex);" id="country" name="country"></select>
										<?php
										if(!empty( $influencer_country ) && !empty( $influencer_state ) ) { ?>
                                            <script language="javascript">print_country("country", "<?php echo esc_js($influencer_country); ?>", "<?php echo esc_js($influencer_state); ?>");</script>
										<?php } elseif(!empty( $influencer_country )){ ?>
                                            <script language="javascript">print_country("country" , "<?php echo esc_js($influencer_country); ?>");</script>
										<?php } else { ?>
                                            <script language="javascript">print_country("country");</script>
										<?php } ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
						<?php
						$influencer_kids_name = get_user_meta($current_user->ID, 'influencer_influencer_kids_name');
						$influencer_kids_dob = get_user_meta($current_user->ID, 'influencer_influencer_kids_dob'); ?>
                        <fieldset>
                            <div class="legend">
								<?php echo esc_html__( 'Kids', 'trending-family' ) ; ?>
                                <em><?php echo esc_html__( '(Optional)', 'trending-family' ); ?></em>
                                <a class="btn-text hidden-xs pull-right addKids"
                                   href="javascript:void(0);"><?php echo esc_html__( 'Add', 'trending-family' ); ?>
                                    <i class="fa fa-plus-circle"></i></a>
                            </div>
							<?php
							if( !empty( $influencer_kids_name) && is_array( $influencer_kids_name ) ) {
								for ( $i = 0; $i < count( $influencer_kids_name ); $i++ ) { ?>
                                    <div id="entryKids<?php echo esc_attr( intval( $i +1 ) ); ?>" class="clonedKids">
                                        <div class="row row-sm form-row">
                                            <a class="btn-add btnKidsDel" href="javascript:void(0);"><i
                                                        class="fa fa-times-circle"></i></a>
                                            <div class="col-sm-8 col-xs-12">
                                                <label for="kids_name"
                                                       class="label_kids_name"><?php echo esc_html__( 'Name', 'trending-family' ); ?></label>
                                                <div class="form-group">
                                                    <input name="kids_name[]" type="text"
                                                           class="form-control input-lg kids_name" id="kids_name" value="<?php echo esc_attr( $influencer_kids_name[$i] ); ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-xs-12">
                                                <label for="kids_dob"
                                                       class="label_kids_dob"><?php echo esc_html__( 'Date of Birth', 'trending-family' ); ?></label>
                                                <div class="form-group">
                                                    <input name="kids_dob[]" type="text"
                                                           class="form-control input-lg input-date dob datepicker-input kids_dob"
                                                           id="kids_dob" value="<?php echo esc_attr( $influencer_kids_dob[$i] ); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								<?php } ?>
                                <div class="row row-sm">
                                    <div class="col-xs-12">
                                        <a class="btn btn-success btn-add btn-block btn-outline offset-top-xs-1 visible-xs addKids"
                                           href="javascript:void(0);">
											<?php echo esc_html__( 'Add', 'trending-family' ); ?>
                                        </a>
                                    </div>
                                </div>
							<?php } else { ?>
                                <div id="entryKids1" class="clonedKids">
                                    <div class="row row-sm form-row">
                                        <a class="btn-add btnKidsDel" href="javascript:void(0);"><i
                                                    class="fa fa-times-circle"></i></a>
                                        <div class="col-sm-8 col-xs-12">
                                            <label for="kids_name"
                                                   class="label_kids_name"><?php echo esc_html__( 'Name', 'trending-family' ); ?></label>
                                            <div class="form-group">
                                                <input name="kids_name[]" type="text"
                                                       class="form-control input-lg kids_name" id="kids_name">
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-12">
                                            <label for="kids_dob"
                                                   class="label_kids_dob"><?php echo esc_html__( 'Date of birth', 'trending-family' ); ?></label>
                                            <div class="form-group">
                                                <input name="kids_dob[]" type="text"
                                                       class="form-control input-lg input-date dob datepicker-input kids_dob"
                                                       id="kids_dob">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row-sm">
                                        <div class="col-xs-12">
                                            <a class="btn btn-success btn-add btn-block btn-outline offset-top-xs-1 visible-xs addKids"
                                               href="javascript:void(0);">
												<?php echo esc_html__( 'Add', 'trending-family' ); ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
								<?php
							} ?>
                        </fieldset>
						<?php
						$influencer_pets_name = get_user_meta($current_user->ID, 'influencer_influencer_pets_name');
						$influencer_pets_description = get_user_meta($current_user->ID, 'influencer_influencer_pets_description'); ?>
                        <fieldset>
                            <div class="legend"><?php echo esc_html__( 'Pets', 'trending-family' ); ?>
                                <em><?php echo esc_html__( '(Optional)', 'trending-family' ); ?></em>
                                <a class="btn-text hidden-xs pull-right addPets" href="javascript:void(0);">
									<?php echo esc_html__( 'Add', 'trending-family' ); ?>
                                    <i class="fa fa-plus-circle"></i></a>
                            </div>
							<?php
							if( !empty( $influencer_pets_name) && is_array( $influencer_pets_name ) ) {
								for ( $i = 0; $i < count( $influencer_pets_name ); $i++ ) { ?>
                                    <div id="entryPets<?php echo esc_attr( intval( $i + 1 ) ); ?>" class="clonedPets">
                                        <div class="row row-sm form-row">
                                            <a class="btn-add btnPetsDel" href="javascript:void(0);"><i
                                                        class="fa fa-times-circle"></i></a>
                                            <div class="col-sm-6 col-xs-12">
                                                <label for="pets_name"
                                                       class="label_pets_name"><?php echo esc_html__( 'Name', 'trending-family' ); ?></label>
                                                <div class="form-group">
                                                    <input type="text" id="pets_name"
                                                           class="form-control input-lg pets_name" name="pets_name[]" value="<?php echo esc_attr( $influencer_pets_name[$i] ); ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-xs-12">
                                                <label for="pets_description"
                                                       class="label_pets_description"><?php echo esc_html__( 'Description', 'trending-family' ); ?></label>
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-lg pets_description"
                                                           name="pets_description[]" id="pets_description" value="<?php echo esc_attr( $influencer_pets_description[$i] ); ?>" placeholder="<?php echo esc_attr__('Animal, Age, etc.', 'trending-family'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								<?php } ?>
                                <div class="row row-sm">
                                    <div class="col-xs-12">
                                        <a class="btn btn-success btn-add btn-block btn-outline offset-top-xs-1 visible-xs addPets"
                                           href="javascript:void(0);"><?php echo esc_html__( 'Add', 'trending-family' ); ?></a>
                                    </div>
                                </div>
							<?php } else { ?>
                                <div id="entryPets1" class="clonedPets">
                                    <div class="row row-sm form-row">
                                        <a class="btn-add btnPetsDel" href="javascript:void(0);"><i
                                                    class="fa fa-times-circle"></i></a>
                                        <div class="col-sm-6 col-xs-12">
                                            <label for="pets_name"
                                                   class="label_pets_name"><?php echo esc_html__( 'Name', 'trending-family' ); ?></label>
                                            <div class="form-group">
                                                <input type="text" id="pets_name[]"
                                                       class="form-control input-lg pets_name" name="pets_name">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <label for="pets_description"
                                                   class="label_pets_description"><?php echo esc_html__( 'Description', 'trending-family' ); ?></label>
                                            <div class="form-group">
                                                <input type="text" class="form-control input-lg pets_description"
                                                       name="pets_description[]" id="pets_description" placeholder="<?php echo esc_attr__('Animal, Age, etc.', 'trending-family'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row-sm">
                                        <div class="col-xs-12">
                                            <a class="btn btn-success btn-add btn-block btn-outline offset-top-xs-1 visible-xs addPets"
                                               href="javascript:void(0);"><?php echo esc_html__( 'Add', 'trending-family' ); ?></a>
                                        </div>
                                    </div>
                                </div>
							<?php } ?>
                        </fieldset>
						<?php
						$influencer_languages = get_user_meta($current_user->ID, 'influencer_influencer_languages'); ?>
                        <fieldset>
                            <div class="legend"><?php echo esc_html__( 'Languages Spoken', 'trending-family' ); ?>
                                <em><?php echo esc_html__( '(Optional)', 'trending-family' ); ?></em>
                                <a class="btn-text hidden-xs pull-right addLanguages"
                                   href="javascript:void(0);">
                                    <span><?php echo esc_html__( 'Add', 'trending-family' ); ?></span><i
                                            class="fa fa-plus-circle"></i>
                                </a>
                            </div>
							<?php
							if( !empty( $influencer_languages) && is_array( $influencer_languages ) ) {
								for ( $i = 0; $i < count( $influencer_languages ); $i++ ) { ?>
                                    <div id="entryLanguages<?php echo esc_attr( intval($i + 1) ); ?>" class="clonedLanguages">
                                        <div class="row row-sm form-row form-row-wl">
                                            <a class="btn-add btnLanguagesDel" href="javascript:void(0);"><i
                                                        class="fa fa-times-circle"></i></a>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-lg languages"
                                                           name="languages[]" id="languages" value="<?php echo esc_attr( $influencer_languages[$i] ); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								<?php } ?>
                                <div class="row row-sm">
                                    <div class="col-xs-12">
                                        <a class="btn btn-success btn-add btn-block btn-outline offset-top-xs-1 visible-xs addLanguages"
                                           href="javascript:void(0);"><?php echo esc_html__( 'Add', 'trending-family' ); ?></a>
                                    </div>
                                </div>
							<?php } else { ?>
                                <div id="entryLanguages1" class="clonedLanguages">
                                    <div class="row row-sm form-row form-row-wl">
                                        <a class="btn-add btnLanguagesDel" href="javascript:void(0);"><i
                                                    class="fa fa-times-circle"></i></a>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-lg languages"
                                                       name="languages[]" id="languages" value="<?php echo esc_attr( 'English' ); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row-sm">
                                        <div class="col-xs-12">
                                            <a class="btn btn-success btn-add btn-block btn-outline offset-top-xs-1 visible-xs addLanguages"
                                               href="javascript:void(0);"><?php echo esc_html__( 'Add', 'trending-family' ); ?></a>
                                        </div>
                                    </div>
                                </div>
							<?php } ?>
                        </fieldset>
						<?php
						$influencer_brands = get_user_meta($current_user->ID, 'influencer_influencer_brands'); ?>
                        <fieldset>
                            <div class="legend"><?php echo  esc_html__( 'Brand Preferences', 'trending-family' ); ?>
                                <em><?php echo esc_html__( '(Optional)', 'trending-family' ); ?></em>
                                <a class="btn-text hidden-xs pull-right addBrands"
                                   href="javascript:void(0);"><?php echo esc_html__( 'Add', 'trending-family' ); ?>
                                    <i class="fa fa-plus-circle"></i>
                                </a>
                            </div>
							<?php
							if( !empty( $influencer_brands ) && is_array( $influencer_brands ) ) {
								for ( $i = 0; $i < count( $influencer_brands ); $i++ ) { ?>
                                    <div id="entryBrands<?php echo esc_attr( intval( $i + 1) ) ; ?>" class="clonedBrands">
                                        <div class="row row-sm form-row form-row-wl">
                                            <a class="btn-add btnBrandsDel" href="javascript:void(0);"><i
                                                        class="fa fa-times-circle"></i></a>
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-lg brands"
                                                           name="brands[]" id="brands" value="<?php echo esc_attr( $influencer_brands[$i] ); ?>" placeholder="<?php echo esc_attr__("WalMart, Macy's, etc.", "trending-family"); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<?php
								} ?>
                                <div class="row row-sm">
                                    <div class="col-xs-12">
                                        <a class="btn btn-success btn-add btn-block btn-outline offset-top-xs-1 visible-xs addBrands"
                                           href="javascript:void(0);"><?php echo esc_html__( 'Add', 'trending-family' ); ?>
                                        </a>
                                    </div>
                                </div>
							<?php } else { ?>
                                <div id="entryBrands1" class="clonedBrands">
                                    <div class="row row-sm form-row form-row-wl">
                                        <a class="btn-add btnBrandsDel" href="javascript:void(0);"><i
                                                    class="fa fa-times-circle"></i></a>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-lg brands"
                                                       name="brands[]" id="brands" placeholder="<?php echo esc_attr__("WalMart, Macy's, etc.", "trending-family"); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row-sm">
                                        <div class="col-xs-12">
                                            <a class="btn btn-success btn-add btn-block btn-outline offset-top-xs-1 visible-xs addBrands"
                                               href="javascript:void(0);"><?php echo esc_html__( 'Add', 'trending-family' ); ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
							<?php } ?>
                        </fieldset>
                        <fieldset>
                            <div class="legend">
								<?php echo esc_html__('Change Password', 'trending-family'); ?><em><?php echo esc_html__(' (Optional)', 'trending-family'); ?></em>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input type="password" class="form-control input-lg" name="pass1" id="pass1" placeholder="<?php echo esc_attr__('New Password', 'trending-family'); ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input type="password" class="form-control input-lg" name="pass2" id="pass2" placeholder="<?php echo esc_attr__('Retype New Password', 'trending-family'); ?>">
                                    </div>
                                </div>
                                <input type="hidden" name="user_login" id="user_login" value="<?php echo esc_attr( $current_user->user_login ); ?>" autocomplete="off" />
                            </div>
                        </fieldset>
                        <div class="text-center offset-top-sm-7">
                            <input type="hidden" name="submitted" id="submitted" value="true" />
                            <input type="submit" class="btn btn-success btn-lg btn-save-influencer" value="Save Changes" id="updateInfluencerBtn">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">FACEBOOK PAGES</h4>
                </div>
                <div class="modal-body" id="pages">
                    <table>
    					<?php
    					if($facebookPages):
    					   foreach ($facebookPages as $ownerPages):
    						  foreach($ownerPages as $key => $facebookdata): ?>
                                    <tr>
                                        <td style="width: 96%;" ><?php echo $facebookdata['pagename']; ?></td>
                                        <td><input id="checkdata" name="facebook_page" type="checkbox" value="<?php echo $facebookdata['pageid']; ?>" <?php echo $facebookdata['is_connected'] ? 'checked="checked"' : ''; ?> ></td>
                                    </tr>
                        <?php endforeach; endforeach; endif;?>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="submitdata" name="submitdata">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
if (strpos($url,'?error=access_denied') !== false) {
	?>
	<script type="text/javascript">
    	jQuery(document).ready( function($){
	    	swal({
		        title: "The account could not be added.",
		        text: "Please try again.",
		        type: "error"
		    });
	    });
	</script>
	<?php
}
if (strpos($url,'?type=pin&state=None') !== false) { ?>
	<script type="text/javascript">
    	jQuery(document).ready( function($){
	    	swal({
		        title: "The account could not be added.",
		        text: "Please try again.",
		        type: "error"
		    });
	    });
	</script>
	<?php
}
?>
<script>
jQuery(document).ready(function(){

    jQuery('.fbconnectpage').click(function(){
        jQuery('#myModal').modal('show');
    });

    jQuery('#submitdata').click(function(){        
        var getAccessToken = jQuery('#data-token').val(),
            data = jQuery(this).closest('form').serializeArray();

        jQuery.ajax({
            method:'POST',
            url: homeUrl.ajaxUrl,
            dataType: 'json',
            data:{action  : 'facebookpage',facebook_data:data},
            success: function(res){
                console.log(res);
                if(res.status){
                	location.reload();
                } else {
                    alert(res.msg);
                }
            }
        });
    });
});
</script>
<?php get_footer();