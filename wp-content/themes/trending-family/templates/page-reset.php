<?php if ( ! defined( 'ABSPATH' ) ) exit;
/*
 * Template Name: Reset Password
 * @package Trending Family
 */
get_header(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <section>
            <div class="green-full-centered flex-center-full">
                <div class="reset-password-box">
					<?php
					if(isset($_REQUEST['error']) ){
						$error_codes = explode( ',', $_REQUEST['error'] );
						foreach ( $error_codes as $code ) { ?>
                            <p class="login-error-alt"><strong><?php echo esc_html__('ERROR: ', 'trending-family'); ?></strong><?php echo trending_family_get_error_message( $code ); ?></p>
							<?php
						}
					}
					if(isset( $_REQUEST['password'] ) && $_REQUEST['password'] == 'changed'){ ?>
                        <p class="login-success"><?php echo esc_html__('Your password has been changed. You can sign in now.', 'trending-family'); ?></p>
						<?php
					}
					$login  = (isset($_GET['login']) ) ? $_GET['login'] : 0;
					if ($login === "failed") { ?>
                        <p class="login-error-alt"><strong><?php echo esc_html__('ERROR: ', 'trending-family'); ?></strong> <?php echo esc_html__('Invalid Username and/or Password.' ,'trending-family'); ?></p><br><br>
					<?php } elseif ($login === "empty") { ?>
                        <p class="login-error-alt"><strong><?php echo esc_html__('ERROR: ', 'trending-family'); ?></strong> <?php echo esc_html__('Username and/or Password is empty.', 'trending-family'); ?></p><br><br>
					<?php } elseif ($login === 'invalidkey') { ?>
                        <p class="login-error-alt"><strong><?php echo esc_html__('ERROR: ', 'trending-family'); ?></strong> <?php echo esc_html__('The key is invalid.', 'trending-family'); ?></p><br><br>
					<?php } elseif ($login === 'expiredkey') { ?>
                        <p class="login-error-alt"><strong><?php echo esc_html__('ERROR: ', 'trending-family'); ?></strong> <?php echo esc_html__('The key is expired.', 'trending-family'); ?></p><br><br>
					<?php } elseif( $login === 'already' ){ ?>
                        <p class="login-error-alt"><strong><?php echo esc_html__('ERROR: ', 'trending-family'); ?></strong> <?php echo esc_html__('You are already logged in.', 'trending-family'); ?></p><br><br>
					<?php } elseif ($login === 'false' ) { ?>
                        <p class="login-success"><?php echo esc_html__('You are log-ged out now.', 'trending-family'); ?></p><br><br>
					<?php }?>

					<?php if(is_user_logged_in()) { ?>
                        <p class="login-error-alt"><strong><?php echo esc_html__('ERROR: ', 'trending-family'); ?></strong> <?php echo esc_html__('You are already logged in.', 'trending-family'); ?></p><br><br>
					<?php } ?>
                    <h2><?php echo esc_html__('Reset Your Password', 'trending-family'); ?></h2>
                    <form id="resetForm" method="POST" action="<?php echo site_url( 'wp-login.php?action=resetpass' ); ?>">
                        <input type="hidden" id="user_login" name="rp_login" value="<?php echo esc_attr( $_REQUEST['login'] ); ?>" autocomplete="off" />
                        <input type="hidden" name="rp_key" value="<?php echo esc_attr( $_REQUEST['key'] ); ?>" />
                        <input type="password" placeholder="<?php echo esc_attr__('New password', 'trending-family'); ?>" name="pass1" autocomplete="off">
                        <input type="password" placeholder="<?php echo esc_attr__('Repeat New password', 'trending-family'); ?>" name="pass2" autocomplete="off">
                        <button type="submit" class="btn btn-default btn-lg"><?php echo esc_html__('RESET PASSWORD', 'trending-family'); ?></button>
                    </form>
                </div>
            </div>
        </section>
    </div>
<?php get_footer();