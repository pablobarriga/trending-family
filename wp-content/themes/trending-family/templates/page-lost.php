<?php if ( ! defined( 'ABSPATH' ) ) exit;
/*
* Template Name: Lost Password
* @package Trending Family
*/
get_header(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <section>
            <div class="green-full-centered flex-center-full">
                <div class="reset-password-box">
					<?php
					if(isset( $_REQUEST['checkemail'] ) && $_REQUEST['checkemail'] == 'confirm') { ?>
                        <p class="login-success"> <?php echo esc_html__('Check your email for a link to reset your password.', 'trending-family'); ?></p><br><br>
					<?php }

					if(isset($_REQUEST['error']) ){
						$error_codes = explode( ',', $_REQUEST['error'] );
						foreach ( $error_codes as $code ) { ?>
                            <p class="login-error-alt"><strong><?php echo esc_html__('ERROR: ', 'trending-family'); ?></strong><?php echo trending_family_get_error_message( $code ); ?></p>
							<?php
						}
					}
					if(is_user_logged_in()) { ?>
                        <p class="login-error-alt"><strong><?php echo esc_html__('ERROR: ', 'trending-family'); ?></strong> <?php echo esc_html__('You are already logged in.', 'trending-family'); ?></p><br><br>
					<?php } ?>
                    <h2><?php echo esc_html__('Lost Your Password', 'trending-family'); ?></h2>
                    <p class="lost-password-description">Please enter your username or email address. <br /> You will receive a link to create a new password via email.</p>
                    <form id="lostForm" method="POST" action="<?php echo wp_lostpassword_url(); ?>">
                        <input type="text" placeholder="<?php echo esc_attr__('Email Address', 'trending-family'); ?>" name="user_login">
                        <button type="submit" class="btn btn-default btn-lg"><?php echo esc_html__('SUBMIT', 'trending-family'); ?></button>
                    </form>
                </div>
            </div>
        </section>
    </div>

<?php get_footer();
