<?php use phpDocumentor\Reflection\DocBlock\Description;
	
	if ( ! defined( 'ABSPATH' ) ) exit;
/*
     * Template Name: Case Study Page
 */
get_header(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php
		$hero_section = get_field( 'trending_family_case_studies_page_enable_hero_section' );
		if( $hero_section ) {
			$hero_logo_image = get_field( 'trending_family_case_studies_page_hero_logo_image' );
			$hero_heading = get_field( 'trending_family_case_studies_page_hero_heading' );
			$hero_content = get_field( 'trending_family_case_studies_page_hero_content' );
			$hero_video_placeholder_image = get_field( 'trending_family_case_studies_page_hero_video_placeholder_image' );
			$hero_video_url = get_field( 'trending_family_case_studies_page_hero_video_url' );
			$hero_cta_label = get_field( 'trending_family_case_studies_page_hero_cta_label' );
			$hero_cta_link = get_field( 'trending_family_case_studies_page_hero_cta_link' ); ?>
            <section class="section bg-green text-dark pad-no">
                <div class="container">
                    <div class="row row-xl">
                        <div class="flex-center flex-revers">
                            <div class="col-sm-6 col-xs-12 hg-hfull col-box">
                                <a class="video-link width-50vw-sm col-box-link popup-video" href="<?php echo esc_url( $hero_video_url ); ?>" style="background-image: url(<?php echo esc_url( $hero_video_placeholder_image ); ?>);">&nbsp;</a>
                            </div>
                            <div class="col-sm-6 col-xs-12 pad-xs text-center">
								<?php if( !empty( $hero_logo_image )){ ?>
                                    <p><img src="<?php echo esc_url( $hero_logo_image ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name')); ?>"></p>
								<?php } ?>
                                <h2 class="offset-bottom-xs-1"><?php echo esc_html( $hero_heading ); ?></h2>
                                <p><?php echo $hero_content; ?></p>
								<?php if( !empty( $hero_cta_label )){ ?>
                                    <p><a class="load-more-ic smoothScroll" href="<?php echo esc_url( $hero_cta_link ); ?>"><i class="fa fa-caret-down wg-circle"></i><?php echo esc_html( $hero_cta_label ); ?></a></p>
								<?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
		<?php }
		$objective_section = get_field( 'trending_family_case_studies_page_enable_objective_section' );
		if( $objective_section ) {
			$objective_section_heading = get_field( 'trending_family_case_studies_page_objective_section_heading');
			$objective_section_content = get_field( 'trending_family_case_studies_page_objective_section_content' ); ?>

            <section id="objectives" class="section bg-grey pad-lg">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 text-center">
							<?php if( !empty( $objective_section_heading )) { ?>
                                <h2><?php echo esc_html( $objective_section_heading ); ?></h2>
							<?php }
							if( !empty( $objective_section_content ) ) { ?>
                                <p class="subtitle"><?php echo $objective_section_content; ?></p>
							<?php } ?>
                        </div>
                    </div>
                </div>
            </section>
		<?php }
		$creative_section = get_field( 'trending_family_case_studies_page_enable_creative_section' );

		if( $creative_section ) {
			$creative_section_heading = get_field( 'trending_family_case_studies_page_creative_section_heading');
			$creative_section_content = get_field( 'trending_family_case_studies_page_creative_section_content' );?>

            <section id="creative" class="section pad-lg">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 text-center">
							<?php if( !empty( $creative_section_heading )){  ?>
                                <h2><?php echo esc_html( $creative_section_heading ); ?></h2>
							<?php }
							if( !empty( $creative_section_content ) ) { ?>
                                <p class="subtitle"><?php echo $creative_section_content; ?></p>
							<?php } ?>
                        </div>
                    </div>
                </div>
            </section>
		<?php  }
		
        $influencers_section = get_field( 'trending_family_case_studies_page_enable_influencers_section' );
        if( $influencers_section ) {
            $influencers_section_heading = get_field( 'trending_family_case_studies_page_influencers_section_heading' ); ?>

            <section class="section bg-blue pad-sm">
                <div class="container">
                    <?php
                    $influencer_count = count( get_field( 'trending_family_case_studies_page_influencers_section_influencers' ) );
                    if( !empty( $influencer_count ) && $influencer_count > (int) 1) {
                        ?>
                        <div class="row">
                            <?php
                            if ( ! empty( $influencers_section_heading ) ) { ?>
                                <div class="col-xs-12 text-center offset-bottom-sm-7">
                                    <h2><?php echo esc_html( $influencers_section_heading ); ?></h2>
                                </div>
                            <?php } ?>

                        </div><!-- end of row -->
		        <?php }


                if( have_rows( 'trending_family_case_studies_page_influencers_section_influencers' )){ ?>
                    <div class="row">
                        <div class="col-xs-12">
	                        <?php if ( !empty( $influencer_count ) && $influencer_count > (int) 1 ) { ?>
                                <div class="feature-controls-dots bordered feature-controls-dots-1 flex-box"></div>
                                <div class="feature-carousel feature-carousel-1 mobile-cont-active owl-carousel">
                            <?php } else { ?>
                                <div class="simple-slider owl-carousel">
                            <?php } ?>
                                    <?php
                                    while( have_rows( 'trending_family_case_studies_page_influencers_section_influencers' )){
                                        the_row();
                                        $influencer = get_sub_field( 'trending_family_case_studies_page_influencers_section_influencer' );
                                        $influencer_name = !empty( $influencer['first_name'] ) ? $influencer['first_name'] . ' ' . $influencer['last_name'] : $influencer['display_name'];
                                        $influencer_title = get_sub_field( 'trending_family_case_studies_page_influencers_section_influencer_title' );
                                        $influencer_no_of_subscribers = get_sub_field( 'trending_family_case_studies_page_influencers_section_influencer_number_of_subscribers' );
                                        $influencer_tagline = get_sub_field( 'trending_family_case_studies_page_influencers_section_tagline' );
                                        $platform_section = get_sub_field( 'trending_family_case_studies_page_influencers_section_influencer_enable_platforms_activated_section' );
                                        if( $platform_section ) {
                                            $platforms_section_title = get_sub_field( 'trending_family_case_studies_page_influencers_section_influencer_platform_section_title' );
                                            $platforms     = get_sub_field( 'trending_family_case_studies_page_influencers_section_influencer_platforms_activated' );
                                        }
                                        if ( !empty( $influencer_count ) && $influencer_count > (int) 1 ) { ?>
    
                                            <div class="feature-slide"
                                                 data-dot='<i class="icon" style="background-image: url(<?php echo esc_url( get_wp_user_avatar_src( $influencer['ID']), 'large' ); ?>);">1</i> <?php echo esc_attr( $influencer_name ); ?>'>
                                                <div class="top-subheader-img visible-xs">
                                                    <img class="img-circle v2" src="<?php echo esc_url( get_wp_user_avatar_src( $influencer['ID']), 'large' ); ?>" alt="<?php echo esc_attr( $influencer_name ); ?>">
                                                </div>
                                                <div class="feature-slide-cont">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <h3><?php echo esc_html( $influencer_title ); ?><?php if( !empty( $influencer_no_of_subscribers ) ) echo sprintf('<span class="txt-sm block-xs"> (%s)</span>', $influencer_no_of_subscribers); ?>
                                                            </h3>
                                                            <?php if(!empty( $influencer_tagline ) ) {
                                                                echo $influencer_tagline; ?><br>
                                                            <?php }
                                                            if( $platform_section ) {
                                                                if ( ! empty( $platforms_section_title ) ) { ?>
                                                                    <h5><?php echo esc_html( $platforms_section_title ); ?></h5>
                                                                <?php }
                                                                if ( ! empty( $platforms ) ) { ?>
                                                                    <div class="row offset-top-xs-3 opacity-md">
                                                                        <?php foreach( $platforms as $platform ){
                                                                            $platform_image = trending_family_get_platform_image( $platform ); ?>
                                                                            <div class="col-md-2 col-sm-3 col-xs-6 offset-bottom-xs-2">
                                                                                <img src="<?php echo esc_url( $platform_image ); ?>" alt="<?php echo esc_attr( $influencer_name ); ?>">
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                <?php }
                                                            } ?>
                                                        </div>
                                                    </div>
                                                </div><!-- end of feature-slide-cont -->
                                            </div><!-- end of feature-slide -->
                                        <?php } else { ?>
                                            <div class="slide">
                                                <div class="row">
                                                    <div class="flex-center">
                                                        <div class="col-sm-6 col-xs-12">
                                                            <img class="img-circle v2" src="<?php echo esc_url( get_wp_user_avatar_src( $influencer['ID']), 'large' ); ?>" alt="<?php echo esc_attr( $influencer_name ); ?>">
                                                        </div>
                                                        <div class="col-sm-6 col-xs-12">
                                                            <div class="slide-cont-xs pad-top-xs-3 pad-bottom-xs-3">
                                                                <div class="text-white">
                                                                    <?php if( !empty( $influencers_section_heading )){ ?>
                                                                        <h2 class="offset-bottom-xs-0"><?php echo esc_html( $influencers_section_heading ); ?></h2>
                                                                    <?php } ?>
                                                                    <p>
                                                                        <span class="big-text"><?php echo esc_html( $influencer_name ); ?></span> <?php if( !empty( $influencer_no_of_subscribers ) ){ ?>&nbsp;
                                                                            <span class="block-xs">(<?php echo esc_html($influencer_no_of_subscribers); ?>)</span>
                                                                        <?php } ?>
                                                                    </p>
                                                                </div>
                                                                <?php if(!empty( $influencer_tagline ) ) { ?>
                                                                    <p class="text-light txt-wlight offset-bottom-xs-4 opacity-md"> <?php echo $influencer_tagline; ?></p>
                                                                <?php }
                                                                if( $platform_section ) {
                                                                    if ( ! empty( $platforms_section_title ) ) { ?>
                                                                        <h5><?php echo esc_html( $platforms_section_title ); ?></h5>
                                                                    <?php }
                                                                    if ( ! empty( $platforms ) ) { ?>
    
                                                                        <div class="row offset-top-xs-2 opacity-md">
                                                                            <?php foreach( $platforms as $platform ){
                                                                                $platform_image = trending_family_get_platform_image( $platform ); ?>
                                                                                <div class="col-sm-3 col-xs-6 offset-top-xs-2">
                                                                                    <img src="<?php echo esc_url( $platform_image ); ?>" alt="<?php echo esc_attr( $influencer_name ); ?>">
                                                                                </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                    <?php }
                                                                } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                <?php } ?>
                            </div><!-- end of owl-carousel -->
                        </div><!-- end of col-xs-12 -->
                    </div><!-- end of row -->
                <?php } ?>
                </div><!-- end of container -->
            </section>
        <?php }

		$slider_section = get_field( 'trending_family_case_studies_page_enable_slider_section' );
		if( $slider_section ){
			$slider_section_heading = get_field( 'trending_family_case_studies_page_slider_section_heading' ); ?>
            <section id="sliders" class="section bg-grey">
                <div class="container">
                    <div class="row">
						<?php if( !empty( $slider_section_heading )){ ?>
                            <div class="col-xs-12 text-center">
                                <h2><?php echo esc_html( $slider_section_heading ); ?></h2>
                            </div>
						<?php } ?>
                    </div>
					<?php if( have_rows( 'trending_family_case_studies_page_slider_section_slider_images' )) { ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="animation-carousel owl-carousel owl-controls-blue">
									<?php while( have_rows( 'trending_family_case_studies_page_slider_section_slider_images' )) {
										the_row();
                                        $slider_link_option = get_sub_field( 'trending_family_case_studies_page_slider_section_slider_enable_link' );
                                        $slider_image       = get_sub_field( 'trending_family_case_studies_page_slider_section_slider_image' ); ?>
                                        <div class="slide-item">
                                            <div class="img-wrap">
                                                <?php if ( ! empty( $slider_image ) ) {
                                                    if ( $slider_link_option ) {
                                                        $slider_link_type = get_sub_field( 'trending_family_case_studies_page_slider_section_slider_link_type' );
                                                        
                                                        $slider_link = get_sub_field( 'trending_family_case_studies_page_slider_section_slider_link' ); ?>
                                                        <a <?php echo ( ! empty( $slider_link_type ) && $slider_link_type == 'video' ) ? esc_attr( 'class=popup-video' ) : ''; ?> href="<?php echo esc_url( $slider_link ); ?>" title="<?php echo esc_attr__( 'View the site', 'trending-family' ); ?>" target="_blank">
                                                    <?php } ?>
                                                    <img src="<?php echo esc_url( $slider_image ); ?>"
                                                         alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                                                    
                                                    <?php if ( $slider_link_option ) { ?>
                                                        </a>
                                                    <?php }
                                                } ?>
                                            </div>
                                        </div>
                                        <?php
									} ?>
                                </div><!-- end of animation-carousel -->
                            </div><!-- end of col-xs-12 -->
                        </div><!-- end of row -->
					<?php } ?>
                </div>
            </section>
		<?php }

		$result_section = get_field( 'trending_family_case_studies_page_enable_results_section' );
		if( $result_section ) {
			$result_section_heading = get_field( 'trending_family_case_studies_page_results_section_heading' );
			$result_section_tagline = get_field( 'trending_family_case_studies_page_results_section_tagline' ); ?>

            <section id="results" class="section bg-green">
                <div class="container">
                    <div class="row">
						<?php if(!empty( $result_section_heading )){ ?>
                            <div class="col-xs-12">
                                <h2 class="text-center offset-bottom-sm-5 text-white"><?php echo esc_html( $result_section_heading ); ?></h2>
                            </div>
						<?php } ?>
                    </div>
					<?php if( have_rows( 'trending_family_case_studies_page_results_section_results' )){
						$result_count = count( get_field( 'trending_family_case_studies_page_results_section_results' ) ); ?>
                        <div class="row">
                            <div class="flex-center">
								<?php
								while ( have_rows('trending_family_case_studies_page_results_section_results') ) {
									the_row();

									$result_number = get_sub_field( 'trending_family_case_studies_page_results_section_result' );
									$result_unit = get_sub_field( 'trending_family_case_studies_page_results_section_result_unit' );
									$result_tagline = get_sub_field( 'trending_family_case_studies_page_results_section_results_result_tagline' ); ?>
                                    <div class="<?php echo esc_attr( trending_family_get_col_value( $result_count )); ?>">
                                        <div class="circle-num-wrap">
											<?php if( !empty( $result_number )) { ?>
                                                <div class="cirle-num">
                                                    <span class="num"><?php echo esc_html( $result_number ); ?></span>
                                                    <?php if(!empty( $result_unit )){ ?>
                                                        <span class="unit"><?php echo esc_html( $result_unit ); ?></span>
                                                    <?php } ?>
                                                </div>
											<?php } ?>
                                        </div>
                                        <?php if( !empty( $result_tagline ) ) { ?>
                                            <p class="circle-num-wrap-text tagline-lg"><?php echo esc_html( $result_tagline ); ?></p>
	                                    <?php } ?>
                                    </div>
								<?php } ?>
                            </div><!-- end of flex-center -->
                        </div><!-- end of row -->
					<?php }
					if( !empty( $result_section_tagline ) ) { ?>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 col-xs-12 text-dark text-center">
                                <hr class="offset-top-xs-5 offset-bottom-xs-6">
                                <p class="tagline-lg"><?php echo $result_section_tagline; ?></p>
                            </div>
                        </div>
					<?php } ?>
                </div>
            </section>
		<?php }
		
        $case_study_page_args = array(
            'post_type'      => 'page',
            'posts_per_page' => -1,
            'orderby'      => 'menu_order',
            'order'        => 'ASC',
            'meta_query'     => array(
	            array(
		            'key'   => '_wp_page_template',
		            'value' => 'templates/page-case-study.php'
	            )
            )
	        
        );
        $case_study_pages = new WP_Query( $case_study_page_args );
        
		if( $case_study_pages->posts ) { ?>
			<section class="section bg-green next-case-study">
                <div class="text-center">
                    <?php
                    foreach( $case_study_pages->posts as $case_study_page_index => $case_study_page) {
                        if ( $case_study_page->ID === get_the_ID() ) {
	                        $case_study_page_index = array_key_exists( $case_study_page_index + 1, $case_study_pages->posts) ? $case_study_page_index + 1 : 0; ?>
                            <a class="load-more-ic text-dark load-more-ic-alt"
                               href="<?php echo esc_url( get_permalink( $case_study_pages->posts[$case_study_page_index]->ID ) ); ?>"
                               title="<?php get_the_title( $case_study_pages->posts[$case_study_page_index]->ID ); ?>"><i
                                        class="fa fa-caret-right wg-circle"></i> <span
                                        class="text-dark"><?php echo esc_html__( 'Next Case Study', 'trending-family' ); ?></span></a>
                       <?php  }
                    } ?>
                </div>
            </section>
            
        <?php
        }
  
		$contact_form_section = get_field('trending_family_case_studies_page_enable_contact_form_section');

		if( $contact_form_section ){
			$contact_section_heading = get_field( 'trending_family_case_studies_page_contact_section_heading' );
			$contact_section_tagline = get_field( 'trending_family_case_studies_page_contact_section_tagline' );
			$contact_form_shortcode = get_field( 'trending_family_case_studies_page_contact_section_form_shortcode' ); ?>
            <section id="contact" class="section bg-blue">
                <div class="container">
                    <div class="row">
                        <div class="flex-center">
                            <div class="col-lg-4 col-sm-5 col-xs-12 text-center-xs offset-bottom-xs-2 offset-bottom-sm-0">
								<?php if( !empty( $contact_section_heading )){ ?>
                                    <h2><?php echo esc_html( $contact_section_heading ); ?></h2>
								<?php }
								echo $contact_section_tagline; ?>

                            </div>
							<?php if( !empty( $contact_form_shortcode ) ){ ?>
                                <div class="col-lg-8 col-sm-7 col-xs-12 form-line-style">
									<?php echo do_shortcode( $contact_form_shortcode ); ?>
                                </div>
							<?php } ?>
                        </div>
                    </div>
                </div>
            </section>
		<?php } ?>
    </div>
<?php get_footer();