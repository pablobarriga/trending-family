<?php 
/*
 * Update Youtube Channels
 * @todo Remove after confirmation from real.it
 */

if (!empty($_POST['youtube-channel-client-id'])){

    $youtube_nonce = $_POST['security'];

    if ( ! $youtube_nonce  || ! wp_verify_nonce( $youtube_nonce, 'trending_family_connect_social_nonce' )  ) {
        die( 'Security Check');
    }

    if (!empty($_POST['youtube-channel-client-id']) && !empty($_POST['youtube-channel-client-id'])){

        $channel = $_POST['youtube-channel-client-id'];

        if(get_user_meta($current_user->ID,'youtube-channels',true)){
            $temp_connect = get_user_meta($current_user->ID,'youtube-channels',true);

            if(in_array($channel , $temp_connect)){
                $errors[] = __('The channel that you are trying to connect is already connected.', 'trending-family');
                
            } else {
                array_push($temp_connect , $channel);
                update_user_meta($current_user->ID, 'youtube-channels', $temp_connect);
            }
            
        } else {
            update_user_meta($current_user->ID, 'youtube-channels', array($channel));
        }
    }
}

/*
* Update Instagram Accounts
* @todo Remove after confirmation from real.it
*/
if(!empty( $_POST['instagram-id'] )){

    $instagram_nonce = $_POST['security'];

    if ( ! $instagram_nonce  || ! wp_verify_nonce( $instagram_nonce, 'trending_family_connect_social_nonce' )  ) {
        die( 'Security Check');
    }

    if ( isset($_POST['instagram-id']) && !empty($_POST['instagram-id'] ) ){
        $inst_id = $_POST['instagram-id'];

        if( get_user_meta($current_user->ID,'instagram', true ) ){            
            $temp_connect = get_user_meta($current_user->ID,'instagram',true);

            if(!in_array($inst_id, $temp_connect)){
                array_push($temp_connect, $inst_id);
                update_user_meta($current_user->ID, 'instagram', $temp_connect);
            } else {
                $errors[] = __('The account that you are trying to connect is already connected.', 'trending-family');
            }

        } else {
            update_user_meta($current_user->ID, 'instagram', array($inst_id));
        }
    }
}

/*
* Update Facebook Pages
*/
if( !empty($_POST['page-id']) ){

    $facebook_nonce = $_POST['security'];

    if ( ! $facebook_nonce  || ! wp_verify_nonce( $facebook_nonce, 'trending_family_connect_social_nonce' )  ) {
        die( 'Security Check');
    }

    if (!empty($_POST['page-id']) && !empty($_POST['page-id'])){
        
        $face = $_POST['page-id'];
        if( get_user_meta($current_user->ID,'facebook_page', true )){
            $temp_connect = get_user_meta($current_user->ID,'facebook_page',true);
            if(in_array($face , $temp_connect)){
                $errors[] = __('The account that you are trying to connect is already connected.', 'trending-family');                
            }else{
                array_push($temp_connect , $face);
                update_user_meta($current_user->ID,'facebook_page' , $temp_connect);
            }

        }else{
            update_user_meta($current_user->ID,'facebook_page' , array($face));
        }
    }
}

/*
* Update Twitter Accounts
*/
if ( !empty( $_POST['twitter_account'] )){

    $twitter_nonce = $_POST['security'];

    if ( ! $twitter_nonce  || ! wp_verify_nonce( $twitter_nonce, 'trending_family_connect_social_nonce' )  ) {
        die( 'Security Check');
    }

    if (!empty($_POST['twitter_account']) && !empty($_POST['twitter_account'])){

        $twitt = $_POST['twitter_account'];
        if(get_user_meta($current_user->ID,'twitter_account',true)){

            $temp_connect = get_user_meta($current_user->ID,'twitter_account',true);
            if(in_array($twitt , $temp_connect)){
                $errors[] = __('The account that you are trying to connect is already connected.', 'trending-family');
            }else{
                array_push($temp_connect , $twitt);
                update_user_meta($current_user->ID,'twitter_account' , $temp_connect);
            }

        }else{
            update_user_meta($current_user->ID,'twitter_account' , array($twitt));

        }
    }
}

/*
* Update Pinterest Accounts
*/
if ( !empty( $_POST['pinterest_account'] ) ){

    $pinterest_nonce = $_POST['security'];

    if ( ! $pinterest_nonce  || ! wp_verify_nonce( $pinterest_nonce, 'trending_family_connect_social_nonce' )  ) {
        die( 'Security Check');
    }

    if (!empty($_POST['pinterest_account']) && !empty($_POST['pinterest_account'])){

        $pin = $_POST['pinterest_account'];

        if(get_user_meta($current_user->ID, 'pint_accounts', true) ){
            $temp_connect = get_user_meta($current_user->ID, 'pint_accounts', true);

            if(in_array($pin , $temp_connect)){
                $errors[] = __('The account that you are trying to connect is already connected.', 'trending-family');
            }else{
                array_push($temp_connect , $pin);
                update_user_meta($current_user->ID, 'pint_accounts', $temp_connect);
            }

        }else{
            update_user_meta($current_user->ID, 'pint_accounts', array( $pin ));
        }
    }
}

// Update Snapchat Handle
if( !empty( $_POST['influencer_snapchat_handle'] ) && !empty( $_POST['influencer_snapchat_avg_snaps'] ) ){

    $snapchat_nonce = $_POST['security'];

    if ( ! $snapchat_nonce  || ! wp_verify_nonce( $snapchat_nonce, 'trending_family_connect_social_nonce' )  ) {
        die( 'Security Check');
    }

    if( !empty($_POST['influencer_snapchat_handle']) && !empty($_POST['influencer_snapchat_avg_snaps']) ){
        $snapchat_handle = $_POST['influencer_snapchat_handle'];
        $snapchat_avg_snaps = $_POST['influencer_snapchat_avg_snaps'];
        $snapchats = array($snapchat_handle, $snapchat_avg_snaps);

        if(get_user_meta($current_user->ID, 'influencer_snapchats', true) ){
            $temp_snapchats = get_user_meta($current_user->ID, 'influencer_snapchats', true);

            if( in_array($snapchats , $temp_snapchats) ){
                $errors[] = __('The handle that you are trying to add is already added.', 'trending-family');
            }else{
                array_push($temp_snapchats , $snapchats);
                update_user_meta($current_user->ID, 'influencer_snapchats' , $temp_snapchats);
            }

        }else{
            update_user_meta($current_user->ID,'influencer_snapchats' , array( $snapchats ) );
        }
    }
}

// Update Blog
if( !empty( $_POST['influencer_blog_url'] ) && !empty($_POST['influencer_blog_umv']) && !empty( $_POST['influencer_blog_mpv']) ){

    $blog_nonce = $_POST['security'];

    if ( ! $blog_nonce  || ! wp_verify_nonce( $blog_nonce, 'trending_family_connect_social_nonce' )  ) {
        die( 'Security Check');
    }

    if( !empty($_POST['influencer_blog_url']) && !empty($_POST['influencer_blog_umv']) && !empty( $_POST['influencer_blog_mpv']) ){
        $blog_url = $_POST['influencer_blog_url'];
        $blog_umv = $_POST['influencer_blog_umv'];
        $blog_mpv = $_POST['influencer_blog_mpv'];
        $blogs = array( $blog_url, $blog_umv, $blog_mpv );

        if(get_user_meta($current_user->ID, 'influencer_blogs', true) ){
            $temp_blogs = get_user_meta($current_user->ID,'influencer_blogs', true);
             
            if( in_array($blogs , $temp_blogs) ){
                $errors[] = __('The blog that you are trying to add is already added.', 'trending-family');
            }else{
                array_push($temp_blogs , $blogs);
                update_user_meta($current_user->ID, 'influencer_blogs', $temp_blogs);
            }

        }else{
            update_user_meta($current_user->ID,'influencer_blogs', array( $blogs ) );
        }
    }
}