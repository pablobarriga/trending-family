<?php if ( ! defined( 'ABSPATH' ) ) exit;
/*
 * Template Name: TF Admin Landing Page
 */

$user = wp_get_current_user();
if(!in_array('tf-admin', $user->roles)) {
	global $wp_query;
	$wp_query->set_404();
	status_header( 404 );
	get_template_part( 404 ); exit();
}
get_header(); ?>
    <script>
        var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 30);
        };

        var ColorRed = "rgba(255, 100, 76, 0.9)";
        var ColorBlue = "rgba(48, 107, 151, 0.9)";
        var ColorGreen = "rgba(101, 200, 181, 0.9)";
    </script>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <section class="section bg-green pad-md text-dark text-center-xs">
            <div class="container">
                <div class="row row-lg">
                    <div class="flex-center">
                        <div class="col-sm-8 col-xs-12 p-title">
                            <h1><?php echo esc_html__('Admin Panel', 'trending-family'); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section bg-grey pad-top-xxs-0">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="s-title">
                            <h3>Family / mom influencers <span class="triangle">&#x25BC;</span></h3>
                        </div>
                        <div class="stats-table scrollbar-inner">
                            <table>
                                <thead>
                                <tr>
                                    <th>Channel</th>
                                    <th>URL</th>
                                    <th>Last Post</th>
                                    <th>Subscribers</th>
                                    <th>Subscribers <br> 30 days</th>
                                    <th>Total Views</th>
                                    <th>Total Views</th>
                                    <th>Total Views</th>
                                    <th>Total Views</th>
                                    <th>Total Views</th>
                                    <th>Total Views</th>
                                    <th>Total Views</th>
                                    <th>Total Views</th>
                                    <th>Total Views</th>
                                    <th>Total Views</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr>
                                    <td data-title="Channel"><a class="popup-box-link" href="#popup-profile">Lorem Ipsum</a></td>
                                    <td data-title="URL">Lorem Ipsum dolor</td>
                                    <td data-title="Last Post">MM / DD / YY</td>
                                    <td data-title="Subscribers">0000000</td>
                                    <td data-title="Subscribers 30 days">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                </tr>
                                <tr>
                                    <td data-title="Channel"><a class="popup-box-link" href="#popup-profile">Lorem Ipsum</a></td>
                                    <td data-title="URL">Lorem Ipsum dolor</td>
                                    <td data-title="Last Post">MM / DD / YY</td>
                                    <td data-title="Subscribers">0000000</td>
                                    <td data-title="Subscribers 30 days">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                </tr>
                                <tr>
                                    <td data-title="Channel"><a class="popup-box-link" href="#popup-profile">Lorem Ipsum</a></td>
                                    <td data-title="URL">Lorem Ipsum dolor</td>
                                    <td data-title="Last Post">MM / DD / YY</td>
                                    <td data-title="Subscribers">0000000</td>
                                    <td data-title="Subscribers 30 days">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                </tr>
                                <tr>
                                    <td data-title="Channel"><a class="popup-box-link" href="#popup-profile">Lorem Ipsum</a></td>
                                    <td data-title="URL">Lorem Ipsum dolor</td>
                                    <td data-title="Last Post">MM / DD / YY</td>
                                    <td data-title="Subscribers">0000000</td>
                                    <td data-title="Subscribers 30 days">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                </tr>
                                <tr>
                                    <td data-title="Channel"><a class="popup-box-link" href="#popup-profile">Lorem Ipsum</a></td>
                                    <td data-title="URL">Lorem Ipsum dolor</td>
                                    <td data-title="Last Post">MM / DD / YY</td>
                                    <td data-title="Subscribers">0000000</td>
                                    <td data-title="Subscribers 30 days">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                </tr>
                                <tr>
                                    <td data-title="Channel"><a class="popup-box-link" href="#popup-profile">Lorem Ipsum</a></td>
                                    <td data-title="URL">Lorem Ipsum dolor</td>
                                    <td data-title="Last Post">MM / DD / YY</td>
                                    <td data-title="Subscribers">0000000</td>
                                    <td data-title="Subscribers 30 days">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                </tr>
                                <tr>
                                    <td data-title="Channel"><a class="popup-box-link" href="#popup-profile">Lorem Ipsum</a></td>
                                    <td data-title="URL">Lorem Ipsum dolor</td>
                                    <td data-title="Last Post">MM / DD / YY</td>
                                    <td data-title="Subscribers">0000000</td>
                                    <td data-title="Subscribers 30 days">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                </tr>
                                <tr>
                                    <td data-title="Channel"><a class="popup-box-link" href="#popup-profile">Lorem Ipsum</a></td>
                                    <td data-title="URL">Lorem Ipsum dolor</td>
                                    <td data-title="Last Post">MM / DD / YY</td>
                                    <td data-title="Subscribers">0000000</td>
                                    <td data-title="Subscribers 30 days">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                </tr>
                                <tr>
                                    <td data-title="Channel"><a class="popup-box-link" href="#popup-profile">Lorem Ipsum</a></td>
                                    <td data-title="URL">Lorem Ipsum dolor</td>
                                    <td data-title="Last Post">MM / DD / YY</td>
                                    <td data-title="Subscribers">0000000</td>
                                    <td data-title="Subscribers 30 days">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                </tr>
                                <tr>
                                    <td data-title="Channel"><a class="popup-box-link" href="#popup-profile">Lorem Ipsum</a></td>
                                    <td data-title="URL">Lorem Ipsum dolor</td>
                                    <td data-title="Last Post">MM / DD / YY</td>
                                    <td data-title="Subscribers">0000000</td>
                                    <td data-title="Subscribers 30 days">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                </tr>
                                <tr>
                                    <td data-title="Channel"><a class="popup-box-link" href="#popup-profile">Lorem Ipsum</a></td>
                                    <td data-title="URL">Lorem Ipsum dolor</td>
                                    <td data-title="Last Post">MM / DD / YY</td>
                                    <td data-title="Subscribers">0000000</td>
                                    <td data-title="Subscribers 30 days">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                </tr>
                                <tr>
                                    <td data-title="Channel"><a class="popup-box-link" href="#popup-profile">Lorem Ipsum</a></td>
                                    <td data-title="URL">Lorem Ipsum dolor</td>
                                    <td data-title="Last Post">MM / DD / YY</td>
                                    <td data-title="Subscribers">0000000</td>
                                    <td data-title="Subscribers 30 days">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                </tr>
                                <tr>
                                    <td data-title="Channel"><a class="popup-box-link" href="#popup-profile">Lorem Ipsum</a></td>
                                    <td data-title="URL">Lorem Ipsum dolor</td>
                                    <td data-title="Last Post">MM / DD / YY</td>
                                    <td data-title="Subscribers">0000000</td>
                                    <td data-title="Subscribers 30 days">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                </tr>
                                <tr>
                                    <td data-title="Channel"><a class="popup-box-link" href="#popup-profile">Lorem Ipsum</a></td>
                                    <td data-title="URL">Lorem Ipsum dolor</td>
                                    <td data-title="Last Post">MM / DD / YY</td>
                                    <td data-title="Subscribers">0000000</td>
                                    <td data-title="Subscribers 30 days">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                    <td data-title="Total Views">0000000</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section collapse-animation">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 title-box">
                        <h2>Lorem Ipsum Dolor <a class="btn-collapse-section" href="#">&nbsp;</a></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="chart-box offset-bottom-xs-7">
                            <h4>Duis Eleifend Orci Imperdiet</h4>
                            <canvas id="chart-11"></canvas>
                        </div>
                        <script>
                            var data11 = {
                                labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"],
                                datasets: [{
                                    type: 'line',
                                    label: 'Dataset 1',
                                    backgroundColor: "rgba(48, 107, 151, 0.2)",
                                    borderColor: "rgba(48, 107, 151, 0.9)",
                                    borderWidth: 2,
                                    //fill: false,
                                    data: [
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor()
                                    ]
                                },  {
                                    type: 'bar',
                                    label: 'Dataset 2',
                                    backgroundColor: "rgba(101, 200, 181, 0.9)",
                                    data: [
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor()
                                    ]
                                }]
                            };
                        </script>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="chart-box offset-bottom-xs-7">
                            <h4>Duis Eleifend Orci Imperdiet</h4>
                            <canvas id="chart-1"></canvas>
                        </div>
                        <script>
                            var data1 = {
                                labels: ["January", "February", "March", "April", "May", "June", "July"],
                                datasets: [{
                                    label: "Unfilled",
                                    //fill: false,
                                    backgroundColor: "rgba(48, 107, 151, 0.2)",
                                    borderColor: "rgba(48, 107, 151, 0.9)",
                                    data: [
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor()
                                    ],
                                }, {
                                    label: "Dashed",
                                    //fill: false,
                                    backgroundColor: "rgba(101, 200, 181, 0.2)",
                                    borderColor: "rgba(101, 200, 181, 0.9)",
                                    //borderDash: [5, 5],
                                    data: [
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor()
                                    ],
                                }],
                            };
                        </script>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="chart-box offset-bottom-xs-7">
                            <h4>Duis Eleifend Orci Imperdiet</h4>
                            <canvas id="chart-13"></canvas>
                        </div>
                        <script>
                            var data13 = {
                                labels: ["1", "2", "3", "4", "5", "6", "7"],
                                datasets: [{
                                    type: 'bar',
                                    label: 'Dataset 1',
                                    backgroundColor: "rgba(101, 200, 181, 1)",
                                    borderWidth: 2,
                                    //fill: false,
                                    data: [
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor()
                                    ]
                                } ]
                            };
                        </script>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="chart-box offset-bottom-xs-7">
                            <h4>Duis Eleifend Orci Imperdiet</h4>
                            <canvas id="chart-14"></canvas>
                        </div>
                        <script>
                            var data14 = {
                                labels: ["January", "February", "March", "April", "May", "June", "July"],
                                datasets: [{
                                    label: "Unfilled",
                                    //fill: false,
                                    backgroundColor: "rgba(48, 107, 151, 0.1)",
                                    borderColor: "rgba(48, 107, 151, 0.9)",
                                    tension: "0.001",
                                    data: [
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor(),
                                        randomScalingFactor()
                                    ],
                                }],
                            };
                        </script>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php get_footer();
