<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package trending-family
 */

get_header(); ?>
<?php
if ( have_posts() ) : ?>

    <section class="offset-top-xs-4 offset-top-md-6">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <h1 class="blog-title"><?php printf( esc_html__( 'Search Results for: %s', 'trending-family' ), '<span>' . get_search_query() . '</span>' ); ?></h1>

                    <div class="col-sm-7 articles-list offset-top-xs-4 offset-top-md-3">
                        <?php
                        /* Start the Loop */
                        while ( have_posts() ) : the_post();

                            /**
                             * Run the loop for the search to output the results.
                             * If you want to overload this in a child theme then include a file
                             * called content-search.php and that will be used instead.
                             */
                            get_template_part( 'template-parts/content', 'search' );

                        endwhile;

                        trending_family_pagination(); ?>
                    </div>
	                <?php get_sidebar(); ?>

                </div>
            </div>

        </div>
    </section>
<?php else :

    get_template_part( 'template-parts/content', 'none' );

endif; ?>

<?php get_footer();
