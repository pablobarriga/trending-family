<?php

/**

 * The template for displaying the footer

 *

 * Contains the closing of the #content div and all content after.

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package trending-family

 */



?>



<footer class="footer">

    <div class="container">

        <div class="row">



            <div class="col-xs-12 text-center">

                <div class="social-links">

                    <?php if( get_theme_mod('trending_family_social_media_youtube_channel')){ ?>

                        <a href="<?php echo esc_url(get_theme_mod('trending_family_social_media_youtube_channel')); ?>" title="<?php echo esc_attr__('View Profile', 'trending-family'); ?>" target="_blank"><i class="fa fa-youtube-square"></i></a>

                    <?php } if( get_theme_mod('trending_family_social_media_twitter_url') ){ ?>

                        <a href="<?php echo esc_url(get_theme_mod('trending_family_social_media_twitter_url')); ?>" title="<?php echo esc_attr__('View Profile', 'trending-family'); ?>" target="_blank"><i class="fa fa-twitter-square"></i></a>

                    <?php } if(get_theme_mod('trending_family_social_media_email_address')) { ?>

                        <a href="mailto:<?php echo get_theme_mod('trending_family_social_media_email_address'); ?>" title="<?php echo esc_attr__('Send Email', 'trending-family'); ?>"><i class="fa fa-envelope-square"></i></a>

                    <?php } if(get_theme_mod('trending_family_social_media_fb_url')){ ?>

                        <a href="<?php echo esc_url(get_theme_mod('trending_family_social_media_fb_url')); ?>" title="<?php echo esc_attr__('View Profile', 'trending-family'); ?>" target="_blank"><i class="fa fa-facebook-square"></i></a>

                    <?php } ?>

                </div>

                <?php if(get_theme_mod('trending_family_contact_details')){ ?>

                    <p><?php echo get_theme_mod('trending_family_contact_details'); ?></p>

                <?php } ?>

                <div class="copyright">

                    <?php if(get_theme_mod('trending_family_copyright_text')){ ?>

                        <?php echo get_theme_mod('trending_family_copyright_text'); ?>

                        <span class="hidden-xs">-</span>

                    <?php }

                    if(get_theme_mod('trending_family_footer_menu_text')){ ?>

                        <span class="block-xs">

                            <?php echo get_theme_mod('trending_family_footer_menu_text'); ?>

                        </span>

                    <?php } ?>

                </div>

            </div>

        </div>

    </div>

</footer>



<div class="mobile-bar">

	<?php

	$custom_logo = get_theme_mod( 'custom_logo' );



	if(!empty($custom_logo)){

		$custom_logo_url = wp_get_attachment_url( $custom_logo, 'full' ); ?>

        <div class="mob-bar-head">

            <a title="<?php echo esc_attr( get_bloginfo('name') ); ?>" class="main-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">

                <img src="<?php echo esc_url( $custom_logo_url ); ?>" alt="<?php echo get_bloginfo( 'name' ); ?>">

            </a>

        </div>

	<?php } ?>

    <div class="mobile-bar-cont">

		<?php



		$footer_defaults = array(

			'theme_location'  => 'menu-2',

			'container'       => false,

			'menu_class'      => 'nav',

			'echo'            => true,

			'fallback_cb'     => 'TrendingFamilyNavWalker::fallback',

			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',

			'depth'           => 0,

			'walker'          => new TrendingFamilyNavWalker()

		); ?>



		<?php

		if ( has_nav_menu( 'menu-2' ) ) {

			wp_nav_menu( $footer_defaults );

		} ?>

    </div>

</div>



<?php wp_footer(); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/yadcf/0.9.1/jquery.dataTables.yadcf.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/yadcf/0.9.1/jquery.dataTables.yadcf.min.js"></script>
</body>

</html>