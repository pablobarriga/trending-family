<?php if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package trending-family
 */

?>

<div <?php post_class('not-found-page'); ?>>
    <img src="<?php echo get_template_directory_uri() . '/assets/images/heart-404.png'; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
    <div class="message">
        <h1><?php echo esc_html__( '404', 'trending-family' ); ?></h1>
        <h4><?php echo esc_html__('NOTHING FOUND', 'trending-family' ); ?></h4>
        <a href="<?php echo esc_url( home_url( '/') ); ?>" class="btn" title="<?php echo esc_attr__('Goto Home Page', 'trending-family' ); ?>"><?php echo esc_html__( 'Home', 'trending-family'); ?></a>
    </div>
</div>