<?php 
$enable_progress_timeline_section = get_field( 'trending_family_campaign_enable_progress_timeline_section' );
if( $enable_progress_timeline_section ) {
	$progress_timeline_collapse = get_field( 'trending_family_progress_timeline_collapse_by_default' );  ?>
	<section class="section bg-blue collapse-animation <?php echo $progress_timeline_collapse ? esc_attr( 'collapse-section' ) : ''; ?>">
		<div class="container">
			<div class="row offset-bottom-xs-7">
				<div class="col-xs-12 title-box">
					<h2><?php echo esc_html__('Project timeline', 'trending-family'); ?><a class="btn-collapse-section" href="javascript:void(0);">&nbsp;</a></h2>
					<hr>
				</div>
				<div class="col-md-4 col-md-offset-8 col-xs-12 data-markers-descript-wrap">
					<div class="row data-markers-descript">
						<div class="flex-center">
							<div class="col-xs-4">
								<div class="circle-mark mark-complete"></div>
								<span><?php echo esc_html__('Complete', 'trending-family'); ?></span>
							</div>
							<div class="col-xs-4">
								<div class="circle-mark mark-progress"></div>
								<span><?php echo esc_html__('In Progress', 'trending-family'); ?></span>
							</div>
							<div class="col-xs-4">
								<div class="circle-mark"></div>
								<span><?php echo esc_html__('Next Steps', 'trending-family'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row data-carousels-box">
				<?php if( have_rows( 'trending_family_campaign_project_timeline_overall_progress' )){ ?>
					<div class="col-xs-12 offset-bottom-xs-5">
						<h4><?php echo esc_html__('Overall Progress', 'trending-family'); ?></h4>
						<div class="data-carousel owl-carousel">
							<?php while( have_rows( 'trending_family_campaign_project_timeline_overall_progress' ) ) {
								the_row();
								$overall_progress_title = get_sub_field( 'trending_family_campaign_project_timeline_overall_progress_title' );
								$overall_progress_date = get_sub_field( 'trending_family_campaign_project_timeline_overall_progress_date' );
								$overall_progress_status = get_sub_field( 'trending_family_campaign_project_timeline_overall_progress_status' ); ?>
								<div class="data-values <?php if( !empty( $overall_progress_status ) && $overall_progress_status != 'Next Steps') { echo ($overall_progress_status == 'Complete' ) ? esc_attr('mark-complete') : esc_attr('mark-progress'); } ?>">
									<p class="ttl"><?php echo esc_html( $overall_progress_title ); ?></p>
									<p class="date"><?php echo esc_html( $overall_progress_date ); ?></p>
								</div>
							<?php } ?>
						</div>
					</div>
				<?php }
					if( have_rows( 'trending_family_campaign_project_timeline_influencers_progress' ) ) {
						while ( have_rows( 'trending_family_campaign_project_timeline_influencers_progress' ) ) {
							the_row();
							$progress_influencer = get_sub_field( 'trending_family_campaign_project_timeline_influencers_influencer'); ?>
							<div class="col-xs-12 offset-bottom-xs-5">
								<h4><?php echo esc_html( $progress_influencer['display_name'] ); ?></h4>
								<?php if( have_rows( 'trending_family_campaign_project_timeline_influencers_influencer_progress' )) { ?>
									<div class="data-carousel owl-carousel">
										<?php while ( have_rows( 'trending_family_campaign_project_timeline_influencers_influencer_progress' ) ) {
											the_row();
											$progress_title = get_sub_field( 'trending_family_campaign_project_timeline_influencers_progress_title' );
											$progress_date = get_sub_field( 'trending_family_campaign_project_timeline_influencers_progress_date' );
											$progress_status = get_sub_field( 'trending_family_campaign_project_timeline_influencers_progress_status' ); ?>
											<div class="data-values <?php if( !empty( $progress_status ) && $progress_status != 'Next Steps') { echo ($progress_status == 'Complete' ) ? esc_attr('mark-complete') : esc_attr('mark-progress'); } ?>">
												<p class="ttl"><?php echo esc_html( $progress_title ); ?></p>
												<p class="date"><?php echo esc_html( $progress_date ); ?></p>
											</div>
										<?php } ?>
									</div>
								<?php } ?>
							</div>
						<?php }
					} ?>
			</div>
		</div>
	</section>
<?php } ?>
