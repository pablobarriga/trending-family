<?php if ( ! defined( 'ABSPATH' ) ) exit;
    // @todo To Remove this file
	/**
	 * Template part for displaying Single Campaign Post when the Campaign is inactive
	 * @link https://codex.wordpress.org/Template_Hierarchy
	 * @package trending-family
	 */
?>

<?php
	$enable_influencers_section = get_field( 'trending_family_campaign_enable_influencers_section' );
	if( $enable_influencers_section ) {
		$collapse_influencers = get_field( 'trending_family_campaign_influencers_collapse_by_default' );
		if ( have_rows( 'trending_family_campaign_influencers' ) ) { ?>

            <section class="section bg-grey bg-white-xs collapse-animation <?php echo $collapse_influencers ? esc_attr( 'collapse-section' ) : ''; ?>">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 offset-bottom-sm-6 offset-bottom-xs-0 title-box">
                            <h2><?php echo esc_html__( 'Influencers', 'trending-family' ); ?> <a
                                        class="btn-collapse-section" href="javascript:void(0);">&nbsp</a>
                            </h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="feature-controls-dots stl-2 bordered feature-controls-dots-1 flex-box justify-beetwen"></div>
                            <div class="feature-carousel feature-carousel-1 mobile-cont-active owl-carousel">
                                <?php while( have_rows('trending_family_campaign_influencers') ) {
                                    the_row();
                                    $associated_influencers = get_sub_field( 'trending_family_campaign_influencers_influencer' );
                                    $platform_activated = get_sub_field( 'trending_family_campaign_influencers_platform' ); ?>
                                    <div class="feature-slide stl-2"
                                         data-dot='<i class="icon" style="background-image: url(<?php echo esc_url( get_wp_user_avatar_src( $associated_influencers['ID'], 'large' ) ); ?>);">1</i><?php echo esc_attr( $associated_influencers['display_name']); ?>'>
                                        <div class="top-subheader-img visible-xs">
                                            <img class="img-circle v2" src="<?php echo esc_url( get_wp_user_avatar_src( $associated_influencers['ID'], 'large' ) ); ?>" alt="<?php echo esc_attr( $associated_influencers['display_name']); ?>">
                                            <div class="subhead-btn"><?php echo esc_attr( $associated_influencers['display_name']); ?></div>
                                        </div>
                                        <div class="feature-slide-cont">
                                            <?php
                                            /*
                                             * Display embedded content first when the campaign is live
                                             * or else don't display these embedded content
                                             */
                                            global $campaign_status;
                                            if( $campaign_status == 'publish' && have_rows( 'trending_family_campaign_contents' ) ) {
                                                /*
                                                 * Campaign Post Title and Featured Image could be removed
                                                 * after the requirement is clear.
                                                 */
                                                // $campaign_post_title          = get_sub_field( 'trending_family_campaign_content_title' );
                                                // $campaign_post_featured_image = get_sub_field( 'trending_family_campaign_content_featured_image' ); ?>
                                            
                                                <div class="feed-wrap">
                                                    <div class="row">
                                                        <div class="col-xs-12 mb20">
                                                            <div class="feed-grid offset-bottom-xs-9">

                                                                <?php
                                                                while ( have_rows( 'trending_family_campaign_contents' ) ) {
	                                                                the_row();
	                                                                if ( have_rows( 'trending_family_campaign_content_posts' ) ) {
		                                                                while ( have_rows( 'trending_family_campaign_content_posts' ) ) {
			                                                                the_row();
			                                                                $media_platform = get_sub_field( 'trending_family_campaign_content_post_media_platform' );
			                                                                $post_embed_url = get_sub_field( 'trending_family_campaign_content_post_post_url' ); ?>
                                                                            <div class="feed-item">
                                                                                <div>
					                                                                <?php echo $post_embed_url; ?>
                                                                                </div>
                                                                            </div>
		                                                                <?php }
	                                                                }
                                                                } ?>
                                                            </div><!-- end of feed-grid -->
                                                        </div>
                                                    </div>
                                                </div><!-- end of feed-wrap -->
                                            <?php
                                            }
                                            /*
                                             * Check if the platforms are activated.
                                             * Display  each influencers' GENERAL channel data for M/F viewership %, ages if the Campaign is not live
                                             * Display data for the specific POST, M/F viewership %, ages, etc. if the Campaign is live
                                             */
                                            if(!empty( $platform_activated )){
                                                $tabs_i = 0;
                                                $tabs_j = 0; ?>
    
                                                <div>
                                                    <!-- Nav tabs -->
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <?php foreach ( $platform_activated as $platform ) {
                                                            $tabs_i++;
                                                            $associated_tab = $associated_influencers['display_name'].$platform; ?>
                                                            
                                                            <li role="presentation" <?php echo ($tabs_i == 1 ) ? 'class=active' : ''; ?>>
                                                                <a href="#<?php echo esc_attr($associated_tab); ?>" aria-controls="<?php echo esc_attr($associated_tab); ?>"
                                                                                                      role="tab" data-toggle="tab"><?php echo esc_attr( $platform ); ?></a></li>
                                                        <?php } ?>
                                                    </ul>
    
                                                    <!-- Tab panes -->
                                                    <div class="tab-content">
                                                        <?php foreach ( $platform_activated as $platform ) {
                                                            $tabs_j++;
                                                            $associated_tab = $associated_influencers['display_name'].$platform; ?>
                                                            <div role="tabpanel" class="tab-pane <?php echo ($tabs_j == 1) ? esc_attr( 'active') : ''; ?>" id="<?php echo esc_attr( $associated_tab ); ?>">
            
                                                                <div class="row">
                                                                    <div class="col-xs-12">
                                                                        <div class="chart-box">
                                                                            <h4>M/F viewership %</h4>
                                                                            <canvas id="chart-t11"></canvas>
                                                                        </div>
                                                                        <script>
                                                                            var data_t11 = {
                                                                                labels: ["January", "February", "March", "April", "May", "June", "July"],
                                                                                datasets: [{
                                                                                    label: "Male",
                                                                                    //fill: false,
                                                                                    backgroundColor: "rgba(48, 107, 151, 0.2)",
                                                                                    borderColor: "rgba(48, 107, 151, 0.9)",
                                                                                    data: [
                                                                                        randomScalingFactor(),
                                                                                        randomScalingFactor(),
                                                                                        randomScalingFactor(),
                                                                                        randomScalingFactor(),
                                                                                        randomScalingFactor(),
                                                                                        randomScalingFactor(),
                                                                                        randomScalingFactor()
                                                                                    ],
                                                                                }, {
                                                                                    label: "Female",
                                                                                    //fill: false,
                                                                                    backgroundColor: "rgba(101, 200, 181, 0.2)",
                                                                                    borderColor: "rgba(101, 200, 181, 0.9)",
                                                                                    //borderDash: [5, 5],
                                                                                    data: [
                                                                                        randomScalingFactor(),
                                                                                        randomScalingFactor(),
                                                                                        randomScalingFactor(),
                                                                                        randomScalingFactor(),
                                                                                        randomScalingFactor(),
                                                                                        randomScalingFactor(),
                                                                                        randomScalingFactor()
                                                                                    ],
                                                                                }],
                                                                            };
                                                                        </script>
                                                                    </div>
                                                                </div>
            
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
    
                                        </div><!-- end of feature-slide-cont -->
                                    </div><!-- end of feature-slide stl-2 -->
                                <?php } ?>
                            </div><!-- end of feature-carousel feature-carousel-1 mobile-cont-active owl-carousel -->
                        </div><!-- end of col-xs-12 -->
                    </div><!-- end of row -->
                </div><!-- end of container -->
            </section>
		<?php }
	}

$enable_progress_timeline_section = get_field( 'trending_family_campaign_enable_progress_timeline_section' );
	
if( $enable_progress_timeline_section ) {
	$progress_timeline_collapse = get_field( 'trending_family_progress_timeline_collapse_by_default' );  ?>

    <section class="section bg-blue collapse-animation <?php echo $progress_timeline_collapse ? esc_attr( 'collapse-section' ) : ''; ?>">
        <div class="container">
            <div class="row offset-bottom-xs-7">
                <div class="col-xs-12 title-box">
                    <h2><?php echo esc_html__('Project timeline', 'trending-family'); ?><a class="btn-collapse-section" href="javascript:void(0);">&nbsp;</a></h2>
                    <hr>
                </div>
                <div class="col-md-4 col-md-offset-8 col-xs-12 data-markers-descript-wrap">
                    <div class="row data-markers-descript">
                        <div class="flex-center">
                            <div class="col-xs-4">
                                <div class="circle-mark mark-complete"></div>
                                <span><?php echo esc_html__('Complete', 'trending-family'); ?></span>
                            </div>
                            <div class="col-xs-4">
                                <div class="circle-mark mark-progress"></div>
                                <span><?php echo esc_html__('In Progress', 'trending-family'); ?></span>
                            </div>
                            <div class="col-xs-4">
                                <div class="circle-mark"></div>
                                <span><?php echo esc_html__('Next Steps', 'trending-family'); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row data-carousels-box">
	            <?php if( have_rows( 'trending_family_campaign_project_timeline_overall_progress' )){ ?>
                    <div class="col-xs-12 offset-bottom-xs-5">
                        <h4><?php echo esc_html__('Overall Progress', 'trending-family'); ?></h4>
                        <div class="data-carousel owl-carousel">
	                        <?php while( have_rows( 'trending_family_campaign_project_timeline_overall_progress' ) ) {
		                        the_row();
		                        $overall_progress_title = get_sub_field( 'trending_family_campaign_project_timeline_overall_progress_title' );
		                        $overall_progress_date = get_sub_field( 'trending_family_campaign_project_timeline_overall_progress_date' );
		                        $overall_progress_status = get_sub_field( 'trending_family_campaign_project_timeline_overall_progress_status' ); ?>
                                <div class="data-values <?php if( !empty( $overall_progress_status ) && $overall_progress_status != 'Next Steps') { echo ($overall_progress_status == 'Complete' ) ? esc_attr('mark-complete') : esc_attr('mark-progress'); } ?>">
                                    <p class="ttl"><?php echo esc_html( $overall_progress_title ); ?></p>
                                    <p class="date"><?php echo esc_html( $overall_progress_date ); ?></p>
                                </div>
	                        <?php } ?>
                        </div>
                    </div>
                <?php }
                if( have_rows( 'trending_family_campaign_project_timeline_influencers_progress' ) ) {
                    while ( have_rows( 'trending_family_campaign_project_timeline_influencers_progress' ) ) {
                        the_row();
	                    $progress_influencer = get_sub_field( 'trending_family_campaign_project_timeline_influencers_influencer'); ?>
                        <div class="col-xs-12 offset-bottom-xs-5">
                            <h4><?php echo esc_html( $progress_influencer['display_name'] ); ?></h4>
	                        <?php if( have_rows( 'trending_family_campaign_project_timeline_influencers_influencer_progress' )) { ?>

                                <div class="data-carousel owl-carousel">
                                    <?php while ( have_rows( 'trending_family_campaign_project_timeline_influencers_influencer_progress' ) ) {
										the_row();
	                                    $progress_title = get_sub_field( 'trending_family_campaign_project_timeline_influencers_progress_title' );
	                                    $progress_date = get_sub_field( 'trending_family_campaign_project_timeline_influencers_progress_date' );
	                                    $progress_status = get_sub_field( 'trending_family_campaign_project_timeline_influencers_progress_status' ); ?>
                                        <div class="data-values <?php if( !empty( $progress_status ) && $progress_status != 'Next Steps') { echo ($progress_status == 'Complete' ) ? esc_attr('mark-complete') : esc_attr('mark-progress'); } ?>">
                                            <p class="ttl"><?php echo esc_html( $progress_title ); ?></p>
                                            <p class="date"><?php echo esc_html( $progress_date ); ?></p>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?php }
                } ?>
            </div>
        </div>
    </section>
<?php } ?>