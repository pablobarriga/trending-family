<?php if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Template part for displaying followers in Campaign Overview page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package trending-family
 */


if( $total_reach ) { ?>
    <h3 class="offset-top-sm-3"><?php echo esc_html__('Total Reach:', 'trending-family'); ?> <?php echo esc_html( $total_reach ); ?></h3>
<?php } ?>
<div class="folowers-data-grid">
    <div class="row">
        <div class="flex-box">
        
			<?php if ( in_array( 'Youtube', $platform_activated ) &&  $youtubeChannels ): // YouTube
			     foreach ($youtubeChannels as $key => $value): if( ! $value['is_valid']) continue; ?>
                    <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
                        <div class="folower-item-box">
                            <div class="num"><?php echo esc_html( $value['subscriberCount'] ); ?></div>
                            <div class="name"><?php echo esc_html( $value['youtube_title'] ); ?></div>
                            <hr>
                            <div class="desc">
                            	<i class="fa fa-youtube"></i> <?php echo esc_html__( 'Subscribers', 'trending-family' ); ?>
                            </div>
                        </div>
                    </div>
				<?php endforeach; ?>
			<?php endif;?>
			
			<?php if (in_array( 'Facebook', $platform_activated ) && $facebookPages ): // FaceBook
			    foreach($facebookPages as $ownerEmail => $ownerPages):
			        if($ownerPages):
        				foreach ($ownerPages as $key => $value): if( ! $value['is_valid']) continue; ?>
                            <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
                                <div class="folower-item-box">
                                    <div class="num"><?php echo esc_html( $value['fan_count'] ); ?></div>
                                    <div class="name"><?php echo esc_html( $value['pagename'] ); ?></div>
                                    <hr>
                                    <div class="desc">
                                    	<i class="fa fa-facebook"></i> <?php echo esc_html__( 'Followers', 'trending-family' ); ?>
                                    </div>
                                </div>
                            </div>
				<?php endforeach; endif; endforeach; ?>
			<?php endif;?>
        
			<?php if (in_array( 'Twitter', $platform_activated ) &&  $twitterAccounts ): // Twitter
			       foreach ($twitterAccounts as $key => $value): if( ! $value['is_valid']) continue; ?>
                    <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
                        <div class="folower-item-box">
                            <div class="num"><?php echo esc_html( $value['followers_count'] ); ?></div>
                            <div class="name"><?php echo esc_html( $value['screen_name'] ); ?></div>
                            <hr>
                            <div class="desc">
                            	<i class="fa fa-twitter"></i> <?php echo esc_html__( 'Followers', 'trending-family' ); ?>
                            </div>
                        </div>
                    </div>
				<?php endforeach; ?>
			<?php endif;?>
        
			<?php if (in_array( 'Instagram', $platform_activated ) &&  $instagramPages ): // Instagram
			         foreach ($instagramPages as $key => $value): if( ! $value['is_valid']) continue; ?>
                    <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
                        <div class="folower-item-box">
                            <div class="num"><?php echo esc_html( $value['followedByCount'] ); ?></div>
                            <div class="name"><?php echo esc_html( $value['fullName'] ); ?></div>
                            <hr>
                            <div class="desc">
                            	<i class="fa fa-instagram"></i> <?php echo esc_html__( 'Followers', 'trending-family' ); ?>
                            </div>
                        </div>
                    </div>
				<?php endforeach; ?>
			<?php endif;?>
        
			<?php if ( $snapchatAccounts ): // Snapchat
			         foreach ($snapchatAccounts as $key => $value): ?>
                    <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
                        <div class="folower-item-box">
                            <div class="num"><?php echo esc_html( $value[1] ); ?></div>
                            <div class="name"><?php echo esc_html( $value[0] ); ?></div>
                            <hr>
                            <div class="desc">
                            	<i class="fa fa-snapchat"></i> <?php echo esc_html__( 'Estimated Avg. Snap', 'trending-family' ); ?>
                            </div>
                        </div>
                    </div>
				<?php endforeach; ?>
			<?php endif;?>
        
			<?php if ( $blogAccounts ): // Snapchat
			         foreach ($blogAccounts as $key => $value): ?>
                    <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
                        <div class="folower-item-box">
                            <div class="num"><?php echo esc_html( $value[1] ); ?></div>
                            <div class="name"><?php echo esc_html( $value[0] ); ?></div>
                            <hr>
                            <div class="desc">
                            	<i class="fa fa-group"></i> <?php echo esc_html__( 'Unique Page Visitors', 'trending-family' ); ?>
                            </div>
                        </div>
                    </div>
				<?php endforeach; ?>
			<?php endif;?>
        
        </div>
    </div>
</div>
<hr>
<br>
	