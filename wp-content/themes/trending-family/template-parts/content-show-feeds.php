<?php 
global $campaign_status; 
if( $campaign_status == 'publish' && have_rows( 'trending_family_campaign_contents' ) ): ?>
	<div class="feed-wrap">
		<div class="row">
			<div class="col-xs-12 mb20">
				<div class="feed-grid offset-bottom-xs-9">
					<?php
						while ( have_rows( 'trending_family_campaign_contents' ) ) {
							the_row();
							if ( have_rows( 'trending_family_campaign_content_posts' ) ) {
								while ( have_rows( 'trending_family_campaign_content_posts' ) ) {
									the_row();
									$media_platform = get_sub_field( 'trending_family_campaign_content_post_media_platform' );
									$post_embed_url = get_sub_field( 'trending_family_campaign_content_post_post_url' ); ?>
									<div class="feed-item col-sm-6 col-xs-12">
										<div>
											<?php echo $post_embed_url; ?>
										</div>
									</div>
								<?php }
							}
						} ?>
				</div><!-- end of feed-grid -->
			</div>
		</div>
	</div><!-- end of feed-wrap -->
<?php endif; ?>									