<?php if ( ! defined( 'ABSPATH' ) ) exit;
	/**
	 * Template part for displaying Single Campaign Post when the Campaign is inactive
	 * @link https://codex.wordpress.org/Template_Hierarchy
	 * @package trending-family
	 */
?>
<?php
require_once get_template_directory(). "/social-media/UserSocialAccount.php";

$enable_influencers_section = get_field( 'trending_family_campaign_enable_influencers_section' );
if( $enable_influencers_section ) {
	$collapse_influencers = get_field( 'trending_family_campaign_influencers_collapse_by_default' );
	if ( have_rows( 'trending_family_campaign_influencers' ) ) { ?>
		<section class="section bg-grey bg-white-xs collapse-animation <?php echo $collapse_influencers ? esc_attr( 'collapse-section' ) : ''; ?>">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 offset-bottom-sm-6 offset-bottom-xs-0 title-box">
						<h2><?php echo esc_html__( 'Influencers', 'trending-family' ); ?> <a
								class="btn-collapse-section" href="javascript:void(0);">&nbsp</a>
						</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="feature-controls-dots stl-2 bordered feature-controls-dots-1 flex-box justify-beetwen"></div>
						<div class="feature-carousel feature-carousel-1 mobile-cont-active owl-carousel">
							<?php while( have_rows('trending_family_campaign_influencers') ) {
								the_row();
								$associated_influencers = get_sub_field( 'trending_family_campaign_influencers_influencer' );
								$platform_activated = get_sub_field( 'trending_family_campaign_influencers_platform' ); 
								?>
								<div class="feature-slide stl-2"
								     data-dot='<i class="icon" style="background-image: url(<?php echo esc_url( get_wp_user_avatar_src( $associated_influencers['ID'], 'large' ) ); ?>);">1</i><?php echo esc_attr( $associated_influencers['display_name']); ?>'>
									<div class="top-subheader-img visible-xs">
										<img class="img-circle v2" src="<?php echo esc_url( get_wp_user_avatar_src( $associated_influencers['ID'], 'large' ) ); ?>" alt="<?php echo esc_attr( $associated_influencers['display_name']); ?>">
										<div class="subhead-btn"><?php echo esc_attr( $associated_influencers['display_name']); ?></div>
									</div>
									<div class="feature-slide-cont">
										<?php 
										// Include Show Followers
			                            if(!empty( $platform_activated )) {
				                            include( 'content-show-followers.php' );
			                            }
											/*
											 * Display embedded content first when the campaign is live
											 * or else don't display these embedded content
											 */
											global $campaign_status;
											if( $campaign_status == 'publish' && have_rows( 'trending_family_campaign_contents' ) ) {
												/*
												 * Campaign Post Title and Featured Image could be removed
												 * after the requirement is clear.
												 */
												// $campaign_post_title          = get_sub_field( 'trending_family_campaign_content_title' );
												// $campaign_post_featured_image = get_sub_field( 'trending_family_campaign_content_featured_image' ); ?>
												<div class="feed-wrap">
													<div class="row">
														<div class="col-xs-12 mb20">
															<div class="feed-grid offset-bottom-xs-9">
																<?php
																	while ( have_rows( 'trending_family_campaign_contents' ) ) {
																		the_row();
																		if ( have_rows( 'trending_family_campaign_content_posts' ) ) {
																			while ( have_rows( 'trending_family_campaign_content_posts' ) ) {
																				the_row();
																				$media_platform = get_sub_field( 'trending_family_campaign_content_post_media_platform' );
																				$post_embed_url = get_sub_field( 'trending_family_campaign_content_post_post_url' ); ?>
																				<div class="feed-item col-sm-6 col-xs-12">
																					<div>
																						<?php echo $post_embed_url; ?>
																					</div>
																				</div>
																			<?php }
																		}
																	} ?>
															</div><!-- end of feed-grid -->
														</div>
													</div>
												</div><!-- end of feed-wrap -->
												<?php
											}
											/*
											 * Check if the platforms are activated.
											 * Display  each influencers' GENERAL channel data for M/F viewership %, ages if the Campaign is not live
											 * Display data for the specific POST, M/F viewership %, ages, etc. if the Campaign is live
											 */
											if(!empty( $platform_activated )) {
											    $facebookService = new SocialMedia\SocialAccounts\Facebook(new WP_User($associated_influencers['ID']));
											    //$facebookService->deleteCachedData('facebook_access_token');die('ok');
											    //$facebookService->cacheData('facebook_pages', $facebookService->pages());
// 											    $facebookPages = $facebookService->cachedData('facebook_pages');
// 											    dd($facebookPages);
												$tabs_f = $tabs_y = $tabs_t = $tabs_ff = $tabs_yy = $tabs_tt = 0;
												// Facebook
												$fbKey = $fbKey_data = rand(100,150);
												$fbKeyIm = $fbKeyIm_data = rand(150,300);
												// Youtube
												$ytKey_mf = $ytKey_mf_data = rand(300,450);
												$ytKey_Audience	= $ytKey_Audience_data = rand(450,600);
												$ytKey_Geo = $ytKey_Geo_data = rand(600,750);
												$ttKey = $ttKey_data = rand(750,900);
												$influencer_medias         = [];
												$influencer_facebook_pages = get_user_meta( $associated_influencers['ID'], 'facebook_data' );
												if ( in_array( 'Facebook', $platform_activated ) && ! empty( $influencer_facebook_pages ) ) {
													foreach ( $influencer_facebook_pages as $influencer_facebook_page ) {
														if ( ! empty( $influencer_facebook_page ) ) {
															$influencer_medias['Facebook'][] = $influencer_facebook_page;
														}
													}
												}
												$influencer_youtube_channels = get_user_meta( $associated_influencers['ID'], 'youtube-channels', TRUE );
												if ( in_array( 'Youtube', $platform_activated ) && ! empty( $influencer_youtube_channels ) ) {
													foreach ( $influencer_youtube_channels as $influencer_youtube_channel ) {
														if ( ! empty( $influencer_youtube_channel ) ) {
															$influencer_medias['Youtube'][] = $influencer_youtube_channel;
														}
													}
												}
												$influencer_twitter_channels = get_user_meta( $associated_influencers['ID'], 'twitter_account', TRUE );
												if ( in_array( 'Twitter', $platform_activated ) && ! empty( $influencer_twitter_channels ) ) {
													foreach ( $influencer_twitter_channels as $influencer_twitter_channel ) {
														if ( ! empty( $influencer_twitter_channel ) ) {
															$influencer_medias['Twitter'][] = $influencer_twitter_channel;
														}
													}
												}
												$influencer_instagram_pages = get_user_meta( $associated_influencers['ID'], 'instagram', TRUE );
												if ( in_array( 'Instagram', $platform_activated ) && ! empty( $influencer_instagram_pages ) ) {
													foreach ( $influencer_instagram_pages as $influencer_instagram_page ) {
														if ( ! empty( $influencer_instagram_page ) ) {
															$influencer_medias['Instagram'][] = $influencer_instagram_page;
														}
													}
												}
												$influencer_pinterest_pages = get_user_meta( $associated_influencers['ID'], 'pint_accounts', TRUE );
												if ( in_array( 'Pinterest', $platform_activated ) && ! empty( $influencer_pinterest_pages ) ) {
													foreach ( $influencer_pinterest_pages as $influencer_pinterest_page ) {
														if ( ! empty( $influencer_pinterest_page ) ) {
															$influencer_medias['Pinterest'][] = $influencer_pinterest_page;
														}
													}
												}
												$fbKeyArr = $fbKeyImArr = array();
												$ytKeymfArr = $ytKeyGeoArr = $ytKeyAudieArr = array();
												// echo "<pre>";
												// print_r($platformName);
												// print_r($influencer_medias['Instagram']);
												// print_r($influencer_medias['Pinterest']);
												if ( ! empty( $influencer_medias ) ) { ?>
														<?php
														$platformName = array();
															foreach ( $influencer_medias as $key => $value ) {
																$dataArray = array();
																if ( is_array( $value ) ) {
																	foreach( $value as $media) {
																		if(!empty( $media )) {
																			// Facebook
																			if ($key == "Facebook") {
																				foreach ($media as $key => $value) {
																					$platformName['Facebook'][] = array('pageid'=>$value['pageid'],'pagename'=>$value['pagename']);
																				}
																			}
																			// Youtube
																			if ($key == "Youtube") {
																				$platformName['Youtube'][] = array('youtube_title'=>$media['youtube_title'],'channel_id'=>$media['channel_id'],'demographics'=>$media['demographics'],'audienceRetention'=>$media['audienceRetention'],'videoGeographicsData'=>$media['videoGeographicsData']);
																				/*echo "<pre>";
																				print_r($media);*/
																			}
																			if ($key == "Twitter") {
																				$platformName['Twitter'][] = array('id' => $media['id'], 'screen_name' => $media['screen_name'], 'user_timeline' => $media['user_timeline']);
																			}
																			// $tabs_i++;
																		}
																	}
																}
															}
														?>
													<div>
														<!-- Nav tabs -->
														<?php 
															// Facebook
															foreach ($platformName as $key => $value) {
																if ($key == "Facebook") {
																	echo '<ul class="nav nav-tabs" role="tablist">';
																	foreach ($value as $key => $valFacebook) {
																		$tabs_f++;
																		?>
																		<li role="presentation" <?php echo ( $tabs_f == 1 ) ? 'class=active' : ''; ?>>
																			<a href="#<?php echo 'Facebook_'.$valFacebook['pageid']; ?>"
																			   aria-controls="<?php echo 'Facebook_'.$valFacebook['pageid']; ?>"
																			   role="tab"
																			   data-toggle="tab"><?php echo 'Facebook['.$valFacebook['pagename'].']'; ?></a>
																		</li>
																		<?php
																	}
																	echo '</ul>';
																}
															}
														foreach ($platformName as $key => $value) {
															if ($key == "Facebook") {
																echo '<div class="tab-content">';
																	foreach ($value as $key => $valFacebook) {
																		$tabs_ff++;
																		?>
																		<div role="tabpanel"
																		     class="tab-pane <?php echo ( $tabs_ff == 1 ) ? esc_attr( 'active' ) : ''; ?>"
																		     id="<?php echo 'Facebook_'.$valFacebook['pageid']; ?>">
																		    <?php
																		    global $wpdb;
																			$tbl_facebook = $wpdb->prefix.'facebook_get_data';
																			$pageIdd = $valFacebook['pageid'];
																			$queryFacebookData = "SELECT * from ".$tbl_facebook." where `page_id` = '$pageIdd' order by date ASC";
																			$resultsFBData = $wpdb->get_results($queryFacebookData);
																			foreach ($resultsFBData as $key => $value) {
																				$pageIdNew = $value->page_id;
																				$pageTokenNew = $value->access_token;
																				$fb = new \Facebook\Facebook([
																				  'app_id' => '296252020832511',
																				  'app_secret' => 'fd0a859ec290e86ab369e4a505ae6567',
																				  'default_graph_version' => 'v2.9',
																				  //'default_access_token' => '{access-token}', // optional
																				]);
																				try {
																				  // Get the \Facebook\GraphNodes\GraphUser object for the current user.
																				  // If you provided a 'default_access_token', the '{access-token}' is optional.
																					// Get Male Female Data
																				  	$response = $fb->get('/'.$pageIdNew.'/insights?metric=page_fans_gender_age', ''.$pageTokenNew.'');
																				  	$data1 = $response->getDecodedBody();
																				  	$fbKeyArr[] = $data1;
																					$today = date('Y-m-d');
																					$pastMonth = date('Y-m-d', strtotime('-1 month'));
																				  	// Get Reach Impressions
																				  	$response1 = $fb->get('/'.$pageIdNew.'/insights?metric=page_impressions,page_engaged_users&period=days_28&since='.$pastMonth.'&until='.$today, ''.$pageTokenNew.'');
																				  	$data2 = $response1->getDecodedBody();
																				  	$fbKeyImArr[] = $data2;
																				$getFacebookContent = $data1['data'][0]['values'][0]['value'];
																				$getFacebookPageImp = $data2['data'];
																				// echo "<pre";
																				// print_r($getFacebookPageImp);
																				if (!empty($getFacebookContent)) {
																					$female = $male = $age = array();
																					foreach ($getFacebookContent as $key2 => $value) {
																						$fbAgeData = explode('.', $key2);
																						if($fbAgeData[0] == 'F')
																						{
																							$female[] = $value;
																							if(!in_array($fbAgeData[1], $age))
																							{
																								$age[] = $fbAgeData[1];
																							}
																						}
																						if($fbAgeData[0] == 'M')
																						{
																							$male[] = $value;
																							if(!in_array($fbAgeData[1], $age))
																							{
																								$age[] = $fbAgeData[1];
																							}
																						}
																					}
																				?>
																				<div class="row">
																					<div class="col-xs-12">
																						<div class="chart-box">
																							<h4>M/F viewership%</h4>
																							<canvas id="chart-t<?php echo $fbKey; ?>"></canvas>
																						</div>
																						<script>
																						var data_t<?php echo $fbKey; ?> = {
																							labels: <?php echo json_encode($age); ?>,
																							datasets: [{
																								label: "Male",
																								//fill: false,
																								backgroundColor: "rgba(48, 107, 151, 0.2)",
																								borderColor: "rgba(48, 107, 151, 0.9)",
																								data: <?php echo json_encode($male); ?>,
																							}, {
																								label: "Female",
																								//fill: false,
																								backgroundColor: "rgba(101, 200, 181, 0.2)",
																								borderColor: "rgba(101, 200, 181, 0.9)",
																								//borderDash: [5, 5],
																								data: <?php echo json_encode($female); ?>,
																							}],
																						};
																					</script>
																					</div>
																				</div>
																				<?php }else{
																					echo "No M/F viewership Found";
																				}
																				if (!empty($getFacebookPageImp)) {
																					$pageImpressions = $pageEngaged = array();
																				  	foreach ($data2['data'] as $key => $value) 
																				  	{
																				  		if($value['name'] == 'page_impressions')
																				  		{
																				  			foreach ($value['values'] as $key => $impres)
																				  			{
																				  				$pageImpressions['value'][] = $impres['value'];
																				  				$pageImpressions['date'][] = date('Y-m-d' , strtotime($impres['end_time']));
																				  			}
																				  		}
																				  		if($value['name'] == 'page_engaged_users')
																				  		{
																				  			foreach ($value['values'] as $key => $impres)
																				  			{
																				  				$pageEngaged['value'][] = $impres['value'];
																				  				$pageEngaged['date'][] = date('Y-m-d' , strtotime($impres['end_time']));
																				  			}
																				  		}
																				  	}
																					?>
																					<div class="row">
																					<div class="col-xs-12">
																						<div class="chart-box">
																							<h4>Page Impressions & Engagement viewership</h4>
																							<canvas id="chart-t<?php echo $fbKeyIm; ?>"></canvas>
																						</div>
																						<script>
																						var data_t<?php echo $fbKeyIm; ?> = {
																							labels: <?php echo json_encode($pageImpressions['date']); ?>,
																							datasets: [{
																								label: "Page Impressions",
																								//fill: false,
																								backgroundColor: "rgba(48, 107, 151, 0.2)",
																								borderColor: "rgba(48, 107, 151, 0.9)",
																								data: <?php echo json_encode($pageImpressions['value']); ?>,
																							}, {
																								label: "Page Engagement",
																								//fill: false,
																								backgroundColor: "rgba(101, 200, 181, 0.2)",
																								borderColor: "rgba(101, 200, 181, 0.9)",
																								//borderDash: [5, 5],
																								data: <?php echo json_encode($pageEngaged['value']); ?>,
																							}],
																						};
																					</script>
																					</div>
																				</div>
																					<?php
																				}else{
																					echo "No Page Impressions Found";
																				}
																				}catch(\Facebook\Exceptions\FacebookResponseException $e) {
																				  // When Graph returns an error
																				  echo 'Graph returned an error: ' . $e->getMessage();
																				  //exit;
																				} catch(\Facebook\Exceptions\FacebookSDKException $e) {
																				  // When validation fails or other local issues
																				  echo 'Facebook SDK returned an error: ' . $e->getMessage();
																				  //exit;
																				}
																				$fbKey++;
																				$fbKeyIm++;
																			}
																			?>
																		 </div>
																		<?php
																}
																echo '</div>';																
															}
														}
														?>
													</div>
													<!-- Youtube -->
													<div>
													<?php
														foreach ($platformName as $key => $value) {
															if ($key == "Youtube") {
																echo '<ul class="nav nav-tabs" role="tablist">';
																$tabs_y++;
																foreach ($value as $key => $valYoutube) {
																	?>
																	<li role="presentation" <?php echo ( $tabs_y == 1 ) ? 'class=active' : ''; ?>>
																		<a href="#<?php echo 'Youtube_'.$valYoutube['channel_id']; ?>"
																		   aria-controls="<?php echo 'Youtube_'.$valYoutube['channel_id']; ?>"
																		   role="tab"
																		   data-toggle="tab"><?php echo 'Youtube['.$valYoutube['youtube_title'].']'; ?></a>
																	</li>
																	<?php
																}
																echo '</ul>';
															}
														}
														foreach ($platformName as $key => $value) {
															if ($key == "Youtube") {
																echo '<div class="tab-content">';
																foreach ($value as $key => $valYoutube) {
																	$serialized_array = $valYoutube['demographics'];
																	$demographics = unserialize($serialized_array);
																	$ytKeymfArr[] = $demographics;
																	// echo "<pre>";
																	// print_r($demographics);
																	/* Geagraphics Chart */
																	$serialized_array_geographics = $valYoutube['videoGeographicsData'];
																	$geographics = unserialize($serialized_array_geographics);
																	$ytKeyGeoArr[] = $geographics;
																	// print_r($geographics);
																	/* Audience Retention */
																	$serialized_array_audience = $valYoutube['audienceRetention'];
																	$audienceRetention = unserialize($serialized_array_audience);
																	$ytKeyAudieArr[] = $audienceRetention;
																	// print_r($audienceRetention);
																	$female = $male = $age = array();
																	foreach ($demographics as $key => $value) {
																		$fbAgeData = explode('age', $value[0]);
																		if($value[1] == 'female')
																		{
																			$female[] = $value[2];
																			if(!in_array($fbAgeData[1], $age))
																			{
																				$age[] = $fbAgeData[1];
																			}
																		}
																		if($value[1] == 'male')
																		{
																			$male[] = $value[2];
																			if(!in_array($fbAgeData[1], $age))
																			{
																				$age[] = $fbAgeData[1];
																			}
																		}
																	}
																	$tabs_yy++;
																	?>
																	<div role="tabpanel"
																	     class="tab-pane <?php echo ( $tabs_yy == 1 ) ? esc_attr( 'active' ) : ''; ?>"
																	     id="<?php echo 'Youtube_'.$valYoutube['channel_id']; ?>">
																	     <div class="row">
																			<div class="col-xs-12">
																				<div class="chart-box">
																					<h4>M/F viewership%</h4>
																					<canvas id="chart-t<?php echo $ytKey_mf; ?>"></canvas>
																				</div>
																				<script>
																					var data_t<?php echo $ytKey_mf; ?> = {
																						labels: <?php echo json_encode($age); ?>,
																						datasets: [{
																							label: "Male",
																							//fill: false,
																							backgroundColor: "rgba(48, 107, 151, 0.2)",
																							borderColor: "rgba(48, 107, 151, 0.9)",
																							data: <?php echo json_encode($male); ?>,
																						}, {
																							label: "Female",
																							//fill: false,
																							backgroundColor: "rgba(101, 200, 181, 0.2)",
																							borderColor: "rgba(101, 200, 181, 0.9)",
																							//borderDash: [5, 5],
																							data: <?php echo json_encode($female); ?>,
																						}],
																					};
																				</script>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-xs-12">
																				<div class="chart-box">
																					<span><h4>Audience Rentention Viewership</h4> 
																						<select name ="audienceRetention" id="audienceRetention" onChange="getAudience(this.value)">
																							<?php 
																							foreach ($audienceRetention as $key => $value) {
																							// foreach ($geographics as $key => $value) {
																								$audienceDate = $watchTime = $audienceViews = array();
																								foreach ($value['audienceRetention'] as $key => $value2) {
																									$audienceDate[] = $value2[0];
																									$watchTime[] = $value2[1];
																									$audienceViews[] = $value2[2];
																								}
																								echo "<option value='".$value['Title']."' views='".json_encode($audienceViews)."' date='".json_encode($audienceDate)."' watchtime='".json_encode($watchTime)."'>".$value['Title']."</option>";
																							}
																							?>
																						</select>
																					</span>
																					<canvas id="chart-t<?php echo $ytKey_Audience; ?>"></canvas>
																				</div>
																				<div id="resultsAudience"></div>
																				<script>
																				function getAudience(getTitle) {
																					jQuery.ajax({
																					  url: "<?php echo admin_url('admin-ajax.php'); ?>",
																					  type: "POST",
  																					  data: {action: 'audienceRetention', formdata2: getTitle, userId2: <?php echo $associated_influencers['ID']; ?>, audienceKey: <?php echo $ytKey_Audience; ?>},
																					  cache: false,
																					  success: function(html){
																					    jQuery("#resultsAudience").html(html);
																					    // console.log(html);
																					  }
																					});
																				}
																				</script>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-xs-12">
																				<div class="chart-box">
																					<span><h4>Geographics Graph Viewership</h4> 
																						<select name ="videoTitle" id="videoTitle" onChange="getGeographics(this.value)">
																							<?php 
																							foreach ($geographics as $key => $value) {
																								$geoViews = $geoCountryCode = array();
																								foreach ($value['geographicsData'] as $key => $value2) {
																									$geoCountryCode[] = $value2[0];
																									$geoViews[] = $value2[1];
																								}
																								echo "<option value='".$value['Title']."' views='".json_encode($geoViews)."' code='".json_encode($geoCountryCode)."'>".$value['Title']."</option>";
																							}
																							?>
																						</select>
																					</span>
																					<canvas id="chart-t<?php echo $ytKey_Geo; ?>"></canvas>
																				</div>
																				<div id="results">
																				</div>
																				<script>
																				function getGeographics(getTitle){
																					jQuery.ajax({
																					  url: "<?php echo admin_url('admin-ajax.php'); ?>",
																					  type: "POST",
  																					  data: {action: 'katalformsbmt', formdata: getTitle, userId: <?php echo $associated_influencers['ID']; ?>, geoKey: <?php echo $ytKey_Geo; ?>},
																					  cache: false,
																					  success: function(html){
																					    jQuery("#results").html(html);
																					    // console.log(html);
																					  }
																					});
																				}
																				</script>
																			</div>
																		</div>
																	</div>
																    <?php
																	$ytKey_Geo++;
																	$ytKey_Audience++;
																	$ytKey_mf++;
																}
															}
														}
													?>
													</div>
													<!-- Twitter Start -->
													<div>
													<?php
														foreach ($platformName as $key => $value) {
															if ($key == "Twitter") {
																echo '<ul class="nav nav-tabs" role="tablist">';
																foreach ($value as $key => $valTwitter) {
																	$tabs_t++;
																	?>
																	<li role="presentation" <?php echo ( $tabs_t == 1 ) ? 'class=active' : ''; ?>>
																		<a href="#<?php echo 'Twitter_'.$valTwitter['id']; ?>"
																		   aria-controls="<?php echo 'Twitter_'.$valTwitter['id']; ?>"
																		   role="tab"
																		   data-toggle="tab"><?php echo 'Twitter['.$valTwitter['screen_name'].']'; ?></a>
																	</li>
																	<?php
																}
																echo '</ul>';
															}
														}
														$ttKeyArr = array();
														foreach ($platformName as $key => $value) {
															if ($key == "Twitter") {
																echo '<div class="tab-content">';
																foreach ($value as $key => $valTwitter) {
																	$tweetDate = $tweetCount = array();
																	$ttKeyArr[] = $valTwitter['user_timeline'];
																	foreach ($valTwitter['user_timeline'] as $key => $value) {
																		$tweetDate[] = $key;
																		$tweetCount[] = $value;
																	}
																	$tabs_tt++;
																	?>
																	<div role="tabpanel"
																	     class="tab-pane <?php echo ( $tabs_tt == 1 ) ? esc_attr( 'active' ) : ''; ?>"
																	     id="<?php echo 'Twitter_'.$valTwitter['id']; ?>">
																	     <div class="row">
																			<div class="col-xs-12">
																				<div class="chart-box">
																					<h4>Tweets viewership</h4>
																					<canvas id="chart-t<?php echo $ttKey; ?>"></canvas>
																				</div>
																				<script>
																					var data_t<?php echo $ttKey; ?> = {
																						labels: <?php echo json_encode($tweetDate); ?>,
																						datasets: [{
																							label: "Tweets Count",
																							//fill: false,
																							backgroundColor: "rgba(48, 107, 151, 0.2)",
																							borderColor: "rgba(48, 107, 151, 0.9)",
																							data: <?php echo json_encode($tweetCount); ?>,
																						}],
																					};
																				</script>
																			</div>
																		</div>
																	</div>
																    <?php
																	$ttKey++;
																}
															}
														}
													?>
													</div>
													<!-- Twitter End -->
												<?php 
												}
												?>
												<script>
												    window.onload = function() {
												    	<?php
														if ( ! empty( $platformName ) ) { 
												    		// foreach ($platformName as $key => $value) {
											    				foreach ($fbKeyArr[0]['data'] as $key => $valFacebook) {
											    					?>
																	var ctx<?php echo $fbKey_data; ?> = document.getElementById('chart-t<?php echo $fbKey_data; ?>');
															        var myChart<?php echo $fbKey_data; ?> = new Chart(ctx<?php echo $fbKey_data; ?>, {
															            type: 'line',
															            data: data_t<?php echo $fbKey_data; ?>,
															        });
																	<?php
																	$fbKey_data++;
											    				}
											    				foreach ($fbKeyImArr[0]['data'] as $key => $valFacebook) {
											    					?>
																	var ctx<?php echo $fbKeyIm_data; ?> = document.getElementById('chart-t<?php echo $fbKeyIm_data; ?>');
															        var myChart<?php echo $fbKeyIm_data; ?> = new Chart(ctx<?php echo $fbKeyIm_data; ?>, {
															            type: 'line',
															            data: data_t<?php echo $fbKeyIm_data; ?>,
															        });
																	<?php
																	$fbKeyIm_data++;
											    				}
											    				foreach ($ytKeymfArr as $key => $value) {
											    					?>
																	var ctx<?php echo $ytKey_mf_data; ?> = document.getElementById('chart-t<?php echo $ytKey_mf_data; ?>');
															        var myChart<?php echo $ytKey_mf_data; ?> = new Chart(ctx<?php echo $ytKey_mf_data; ?>, {
															            type: 'line',
															            data: data_t<?php echo $ytKey_mf_data; ?>,
															        });
																	<?php
																	$ytKey_mf_data++;
											    				}
											    				foreach ($ttKeyArr as $key => $value) {
											    					?>
																	var ctx<?php echo $ttKey_data; ?> = document.getElementById('chart-t<?php echo $ttKey_data; ?>');
															        var myChart<?php echo $ttKey_data; ?> = new Chart(ctx<?php echo $ttKey_data; ?>, {
															            type: 'line',
															            data: data_t<?php echo $ttKey_data; ?>,
															        });
																	<?php
																	$ttKey_data++;
											    				}
											    				foreach ($ytKeyGeoArr as $key => $value) {
											    					?>
																	var ctx<?php echo $ytKey_Geo_data; ?> = document.getElementById('chart-t<?php echo $ytKey_Geo_data; ?>');
															        var myChart<?php echo $ytKey_Geo_data; ?> = new Chart(ctx<?php echo $ytKey_Geo_data; ?>, {
															            type: 'line',
															            data: data_t<?php echo $ytKey_Geo_data; ?>,
															        });
																	<?php
																	$ytKey_Geo_data++;
											    				}
											    				foreach ($ytKeyAudieArr as $key => $value) {
											    					?>
																	var ctx<?php echo $ytKey_Audience_data; ?> = document.getElementById('chart-t<?php echo $ytKey_Audience_data; ?>');
															        var myChart<?php echo $ytKey_Audience_data; ?> = new Chart(ctx<?php echo $ytKey_Audience_data; ?>, {
															            type: 'line',
															            data: data_t<?php echo $ytKey_Audience_data; ?>,
															        });
																	<?php
																	$ytKey_Audience_data++;
											    				}
												    		// }
											    				// ttKey_data
												    	}
												    	?>
												    };
												</script>
												<?php
											}
											?>
									</div><!-- end of feature-slide-cont -->
								</div><!-- end of feature-slide stl-2 -->
							<?php } ?>
						</div><!-- end of feature-carousel feature-carousel-1 mobile-cont-active owl-carousel -->
					</div><!-- end of col-xs-12 -->
				</div><!-- end of row -->
			</div><!-- end of container -->
		</section>
	<?php }
}
$enable_progress_timeline_section = get_field( 'trending_family_campaign_enable_progress_timeline_section' );
if( $enable_progress_timeline_section ) {
	$progress_timeline_collapse = get_field( 'trending_family_progress_timeline_collapse_by_default' );  ?>
	<section class="section bg-blue collapse-animation <?php echo $progress_timeline_collapse ? esc_attr( 'collapse-section' ) : ''; ?>">
		<div class="container">
			<div class="row offset-bottom-xs-7">
				<div class="col-xs-12 title-box">
					<h2><?php echo esc_html__('Project timeline', 'trending-family'); ?><a class="btn-collapse-section" href="javascript:void(0);">&nbsp;</a></h2>
					<hr>
				</div>
				<div class="col-md-4 col-md-offset-8 col-xs-12 data-markers-descript-wrap">
					<div class="row data-markers-descript">
						<div class="flex-center">
							<div class="col-xs-4">
								<div class="circle-mark mark-complete"></div>
								<span><?php echo esc_html__('Complete', 'trending-family'); ?></span>
							</div>
							<div class="col-xs-4">
								<div class="circle-mark mark-progress"></div>
								<span><?php echo esc_html__('In Progress', 'trending-family'); ?></span>
							</div>
							<div class="col-xs-4">
								<div class="circle-mark"></div>
								<span><?php echo esc_html__('Next Steps', 'trending-family'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row data-carousels-box">
				<?php if( have_rows( 'trending_family_campaign_project_timeline_overall_progress' )){ ?>
					<div class="col-xs-12 offset-bottom-xs-5">
						<h4><?php echo esc_html__('Overall Progress', 'trending-family'); ?></h4>
						<div class="data-carousel owl-carousel">
							<?php while( have_rows( 'trending_family_campaign_project_timeline_overall_progress' ) ) {
								the_row();
								$overall_progress_title = get_sub_field( 'trending_family_campaign_project_timeline_overall_progress_title' );
								$overall_progress_date = get_sub_field( 'trending_family_campaign_project_timeline_overall_progress_date' );
								$overall_progress_status = get_sub_field( 'trending_family_campaign_project_timeline_overall_progress_status' ); ?>
								<div class="data-values <?php if( !empty( $overall_progress_status ) && $overall_progress_status != 'Next Steps') { echo ($overall_progress_status == 'Complete' ) ? esc_attr('mark-complete') : esc_attr('mark-progress'); } ?>">
									<p class="ttl"><?php echo esc_html( $overall_progress_title ); ?></p>
									<p class="date"><?php echo esc_html( $overall_progress_date ); ?></p>
								</div>
							<?php } ?>
						</div>
					</div>
				<?php }
					if( have_rows( 'trending_family_campaign_project_timeline_influencers_progress' ) ) {
						while ( have_rows( 'trending_family_campaign_project_timeline_influencers_progress' ) ) {
							the_row();
							$progress_influencer = get_sub_field( 'trending_family_campaign_project_timeline_influencers_influencer'); ?>
							<div class="col-xs-12 offset-bottom-xs-5">
								<h4><?php echo esc_html( $progress_influencer['display_name'] ); ?></h4>
								<?php if( have_rows( 'trending_family_campaign_project_timeline_influencers_influencer_progress' )) { ?>
									<div class="data-carousel owl-carousel">
										<?php while ( have_rows( 'trending_family_campaign_project_timeline_influencers_influencer_progress' ) ) {
											the_row();
											$progress_title = get_sub_field( 'trending_family_campaign_project_timeline_influencers_progress_title' );
											$progress_date = get_sub_field( 'trending_family_campaign_project_timeline_influencers_progress_date' );
											$progress_status = get_sub_field( 'trending_family_campaign_project_timeline_influencers_progress_status' ); ?>
											<div class="data-values <?php if( !empty( $progress_status ) && $progress_status != 'Next Steps') { echo ($progress_status == 'Complete' ) ? esc_attr('mark-complete') : esc_attr('mark-progress'); } ?>">
												<p class="ttl"><?php echo esc_html( $progress_title ); ?></p>
												<p class="date"><?php echo esc_html( $progress_date ); ?></p>
											</div>
										<?php } ?>
									</div>
								<?php } ?>
							</div>
						<?php }
					} ?>
			</div>
		</div>
	</section>
	<script type="text/javascript">
	jQuery(document).ready(function(){
		getAudience(jQuery('#audienceRetention').val());
		getGeographics(jQuery('#videoTitle').val());
	});
	</script>
<?php } ?>