<?php 
$ageGroups = [];
$maleCount = [];
$femaleCount = [];
$unknownCount = [];
$accountNames = [];

if($facebookPages) {
    foreach($facebookPages as $pageOwner) {
        
        if($pageOwner) {
            foreach($pageOwner as $page) {
                
                if($page['gender_age']) {
                    $genderAge = array_pop($page['gender_age']);
                    
                    $ageGroupData = [];
                    foreach($genderAge['value'] as $gender_age => $count) {
                        list($gender, $age_group) = explode('.', $gender_age);
                        $ageGroupData[$page['name']][$age_group][$gender] = $count;
                    }
                    
                    ksort($ageGroupData[$page['name']]);
                    
                    foreach($ageGroupData[$page['name']] as $age_group => $genders) {
                        $ageGroups[$page['name']][] = $age_group;
                        $maleCount[$page['name']][] = isset($genders['M']) ? (int) $genders['M'] : 0;
                        $femaleCount[$page['name']][] = isset($genders['F']) ? (int) $genders['F'] : 0;
                        $unknownCount[$page['name']][] = isset($genders['U']) ? (int) $genders['U'] : 0;
                    }
                    
                    ksort($ageGroups[$page['name']]);
                }
                
                $accountNames[$page['name']] = $page['name'];
            }
        }
    }
}
?>

<h3>FaceBook Viewership by Gender & Age Group</h3>
<canvas id="canvas-fb-male-female"></canvas> 

<script>

var configFbMaleFemale = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "Male",
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            data: []
        }, {
            label: "Female",
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            data: [],
        }, {
            label: "Unknown",
            backgroundColor: window.chartColors.orange,
            borderColor: window.chartColors.orange,
            data: [],
        }]
    },
    options: {
        responsive: true,
        title:{
            display:false,
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Age Group'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Views'
                }
            }]
        }
    }
};

var ageGroups = <?php echo json_encode($ageGroups); ?>;
var maleCount = <?php echo json_encode($maleCount); ?>;
var femaleCount = <?php echo json_encode($femaleCount); ?>;
var unknownCount = <?php echo json_encode($unknownCount); ?>;

function fbMaleFemaleChange(obj) {
	var selected = obj.val();
	jQuery.each(maleCount, function(k,v){
		if(k == selected) {
			configFbMaleFemale.data.datasets[0].data = v;
		}
	});
	jQuery.each(femaleCount, function(k,v){
		if(k == selected) {
			configFbMaleFemale.data.datasets[1].data = v;
		}
	});
	jQuery.each(unknownCount, function(k,v){
		if(k == selected) {
			configFbMaleFemale.data.datasets[2].data = v;
		}
	});
	jQuery.each(ageGroups, function(k,v){
		if(k == selected) {
			configFbMaleFemale.data.labels = v;
		}
	});

	canvasFbMaleFemale.update();
}

jQuery(document).ready(function() {

	jQuery('#canvas-fb-male-female').before('<select id="canvas-fb-male-female-page" class="form-control" onChange="fbMaleFemaleChange(jQuery(this));"><?php foreach($accountNames as $name) echo '<option value="' . $name . '">' . $name . '</option>'; ?></select>');
	
    var canvasFbMaleFemale = document.getElementById("canvas-fb-male-female").getContext("2d");
    window.canvasFbMaleFemale = new Chart(canvasFbMaleFemale, configFbMaleFemale);

    fbMaleFemaleChange(jQuery('#canvas-fb-male-female-page'));
});
    
</script>