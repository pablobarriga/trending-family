<?php
$datesTWtweetCount = [];
$tweets = [];
$accountNames = [];

if($twitterAccounts) {
    foreach($twitterAccounts as $account) {
        
        if($account['user_timeline']) {
            foreach($account['user_timeline'] as $date => $tweetCount) {
                $datesTWtweetCount[$account['screen_name']][] = date('m.d.Y', strtotime($date));
                $tweets[$account['screen_name']][] = $tweetCount;
            }
        }
        
        $accountNames[$account['screen_name']] = $account['screen_name'];
    }
}
?>

<h3>Twitter Tweet Count</h3>
<canvas id="canvas-tw-tweet-count"></canvas> 

<script>

var configTwTweetCount = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "Tweet Count",
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            data: []
        }]
    },
    options: {
        responsive: true,
        title:{
            display:false,
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'datesTWtweetCount'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Tweet Count'
                }
            }]
        }
    }
};

var datesTWtweetCount = <?php echo json_encode($datesTWtweetCount); ?>;
var tweets = <?php echo json_encode($tweets); ?>;

function TwTweetCountChange(obj) {
	var selected = obj.val();
	jQuery.each(tweets, function(k,v){
		if(k == selected) {
			configTwTweetCount.data.datasets[0].data = v;
		}
	});
	jQuery.each(datesTWtweetCount, function(k,v){
		if(k == selected) {
			configTwTweetCount.data.labels = v;
		}
	});

	canvasTwTweetCount.update();
}

jQuery(document).ready(function() {

	jQuery('#canvas-tw-tweet-count').before('<select id="accounts-tw-tweet-count" class="form-control" onChange="TwTweetCountChange(jQuery(this));"><?php foreach($accountNames as $name) echo '<option value="' . $name . '">' . $name . '</option>'; ?></select>');
	
    var canvasTwTweetCount = document.getElementById("canvas-tw-tweet-count").getContext("2d");
    window.canvasTwTweetCount = new Chart(canvasTwTweetCount, configTwTweetCount);

    TwTweetCountChange(jQuery('#accounts-tw-tweet-count'));
});
    
</script>