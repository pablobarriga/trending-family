<?php
$dates = [];
$views = [];
$watchTime = [];
$accountNames = [];

if($youtubeChannels) {
    foreach($youtubeChannels as $channel) {        
                
        if($channel['audienceRetention']) {
            foreach($channel['audienceRetention'] as $channelStats) {
                
                
                if($channelStats['audienceRetention']) {
                    foreach($channelStats['audienceRetention'] as $viewStats) {
                        $dates[$channel['youtube_title']][] = date('m.d.Y', strtotime($viewStats[0]));
                        $watchTime[$channel['youtube_title']][] = $viewStats[1];
                        $views[$channel['youtube_title']][] = $viewStats[2];
                    }
                    
                    $accountNames[$channel['youtube_title']] = $channel['youtube_title'];
                }
            }
        }
    }
}
?>

<h3>YouTube Audience Retention</h3>
<canvas id="canvas-yt-audience-retention"></canvas> 

<script>

var configYtAudienceRetention = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "Time Watched (in seconds)",
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            data: []
        }, {
            label: "Views",
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            data: [],
        }]
    },
    options: {
        responsive: true,
        title:{
            display:false,
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Date'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Time Watched/Views'
                }
            }]
        }
    }
};

var datesYTAudienceRet = <?php echo json_encode($dates); ?>;
var watchTime = <?php echo json_encode($watchTime); ?>;
var views = <?php echo json_encode($views); ?>;

function YtAudienceRetentionChange(obj) {
	var selected = obj.val();
	jQuery.each(watchTime, function(k,v){
		if(k == selected) {
			configYtAudienceRetention.data.datasets[0].data = v;
		}
	});
	jQuery.each(views, function(k,v){
		if(k == selected) {
			configYtAudienceRetention.data.datasets[1].data = v;
		}
	});
	jQuery.each(datesYTAudienceRet, function(k,v){
		if(k == selected) {
			configYtAudienceRetention.data.labels = v;
		}
	});

	canvasYtAudienceRetention.update();
}

jQuery(document).ready(function() {

	jQuery('#canvas-yt-audience-retention').before('<select id="canvas-yt-audience-retention-page" class="form-control" onChange="YtAudienceRetentionChange(jQuery(this));"><?php foreach($accountNames as $name) echo '<option value="' . $name . '">' . $name . '</option>'; ?></select>');
	
    var canvasYtAudienceRetention = document.getElementById("canvas-yt-audience-retention").getContext("2d");
    window.canvasYtAudienceRetention = new Chart(canvasYtAudienceRetention, configYtAudienceRetention);

    YtAudienceRetentionChange(jQuery('#canvas-yt-audience-retention-page'));
});
    
</script>