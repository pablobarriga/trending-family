<?php 
$dates = [];
$impressions = [];
$engagement = [];
$accountNames = [];

if($facebookPages) {
    foreach($facebookPages as $pageOwner) {
        
        if($pageOwner) {
            foreach($pageOwner as $page) {
                
                if($page['impressions']) {                    
                    foreach($page['impressions']['data'][0]['values'] as $impression) {
                        $dates[$page['name']][] = date('m.d.Y', strtotime($impression['end_time']));
                        $impressions[$page['name']][] = $impression['value'];
                    }                    
                    
                    foreach($page['impressions']['data'][1]['values'] as $impression) {
                        $engagement[$page['name']][] = $impression['value'];
                    }
                }
                
                $accountNames[$page['name']] = $page['name'];
            }
        }
    }
}
?>

<h3>FaceBook Viewership by Impressions & Engagement</h3>
<canvas id="canvas-fb-impressions-engagement"></canvas> 

<script>

var configFbImpressionsEngagement = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "Impressions",
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            data: []
        }, {
            label: "Engagement",
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            data: [],
        }]
    },
    options: {
        responsive: true,
        title:{
            display:false,
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Dates'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Views'
                }
            }]
        }
    }
};

var dates = <?php echo json_encode($dates); ?>;
var impressions = <?php echo json_encode($impressions); ?>;
var engagement = <?php echo json_encode($engagement); ?>;

function fbImpressionsEngagementChange(obj) {
	var selected = obj.val();
	jQuery.each(impressions, function(k,v){
		if(k == selected) {
			configFbImpressionsEngagement.data.datasets[0].data = v;
		}
	});
	jQuery.each(engagement, function(k,v){
		if(k == selected) {
			configFbImpressionsEngagement.data.datasets[1].data = v;
		}
	});
	jQuery.each(dates, function(k,v){
		if(k == selected) {
			configFbImpressionsEngagement.data.labels = v;
		}
	});

	canvasFbImpressionsEngagement.update();
}

jQuery(document).ready(function() {

	jQuery('#canvas-fb-impressions-engagement').before('<select id="pages-fb-impressions-engagement" class="form-control" onChange="fbImpressionsEngagementChange(jQuery(this));"><?php foreach($accountNames as $name) echo '<option value="' . $name . '">' . $name . '</option>'; ?></select>');
	
    var canvasFbImpressionsEngagement = document.getElementById("canvas-fb-impressions-engagement").getContext("2d");
    window.canvasFbImpressionsEngagement = new Chart(canvasFbImpressionsEngagement, configFbImpressionsEngagement);

    fbImpressionsEngagementChange(jQuery('#pages-fb-impressions-engagement'));
});
    
</script>