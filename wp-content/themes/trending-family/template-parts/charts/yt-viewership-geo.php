<?php
$countryviewsYTgeo = [];

if($youtubeChannels) {
    foreach($youtubeChannels as $channel) {
        
        if($channel['videoGeographicsData']) {
            foreach($channel['videoGeographicsData'] as $channelStats) {
                
                
                if($channelStats['geographicsData']) {
                    foreach($channelStats['geographicsData'] as $viewsYTgeotats) {
                        if( isset($countryviewsYTgeo[$channel['youtube_title']][$viewsYTgeotats[0]])) {
                            $countryviewsYTgeo[$channel['youtube_title']][$viewsYTgeotats[0]] += $viewsYTgeotats[1];
                        }
                        
                        else {
                            $countryviewsYTgeo[$channel['youtube_title']][$viewsYTgeotats[0]] = $viewsYTgeotats[1];
                        }
                        
                        $accountNames[$channel['youtube_title']] = $channel['youtube_title'];
                    }
                }
            }
        }
    }
}
$countries = [];
$viewsYTgeo = [];

if($countryviewsYTgeo) {
    foreach($countryviewsYTgeo as $accountName => $account) {
        
        if($account) {
            foreach($account as $country => $count) {
                $countries[$accountName][] = $country;
                $viewsYTgeo[$accountName][] = $count;
            }
        }        
    }
}
?>

<h3>YouTube Geographic viewsYTgeo by Country</h3>
<canvas id="canvas-yt-geo"></canvas> 

<script>

var configYtGeo = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "viewsYTgeo",
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            data: [],
        }]
    },
    options: {
        responsive: true,
        title:{
            display:false,
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Date'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Time Watched/viewsYTgeo'
                }
            }]
        }
    }
};

var countries = <?php echo json_encode($countries); ?>;
var viewsYTgeo = <?php echo json_encode($viewsYTgeo); ?>;

function YtGeoChange(obj) {
	var selected = obj.val();
	jQuery.each(viewsYTgeo, function(k,v){
		if(k == selected) {
			configYtGeo.data.datasets[0].data = v;
		}
	});
	jQuery.each(countries, function(k,v){
		if(k == selected) {
			configYtGeo.data.labels = v;
		}
	});

	canvasYtGeo.update();
}

jQuery(document).ready(function() {

	jQuery('#canvas-yt-geo').before('<select id="canvas-yt-geo-page" class="form-control" onChange="YtGeoChange(jQuery(this));"><?php foreach($accountNames as $name) echo '<option value="' . $name . '">' . $name . '</option>'; ?></select>');
	
    var canvasYtGeo = document.getElementById("canvas-yt-geo").getContext("2d");
    window.canvasYtGeo = new Chart(canvasYtGeo, configYtGeo);

    YtGeoChange(jQuery('#canvas-yt-geo-page'));
});
    
</script>