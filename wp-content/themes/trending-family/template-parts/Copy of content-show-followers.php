<?php if ( ! defined( 'ABSPATH' ) ) exit;
	/**
	 * Template part for displaying followers in Campaign Overview page
	 *
	 * @link https://codex.wordpress.org/Template_Hierarchy
	 *
	 * @package trending-family
	 */
	$influencer_medias         = [];
	$total_reach = 0;
	$influencer_facebook_pages = get_user_meta( $associated_influencers['ID'], 'facebook_data' );
	if ( in_array( 'Facebook', $platform_activated ) && ! empty( $influencer_facebook_pages ) ) {
		foreach ( $influencer_facebook_pages as $influencer_facebook_page ) {
			if ( ! empty( $influencer_facebook_page ) ) {
				$influencer_medias['Facebook'][] = $influencer_facebook_page;
				if ( ! empty( $influencer_medias['Facebook'][0] ) ) {
					foreach ( $influencer_medias['Facebook'][0] as $key => $value ) {
						if ( $value['fan_count'] && $value['pagename'] ) {
							$total_reach += intval( $value['fan_count'] );
						}
					}
				}
			}
		}
	}
	$influencer_youtube_channels = get_user_meta( $associated_influencers['ID'], 'youtube-channels', TRUE );
	if ( in_array( 'Youtube', $platform_activated ) && ! empty( $influencer_youtube_channels ) ) {
		foreach ( $influencer_youtube_channels as $influencer_youtube_channel ) {
			if ( ! empty( $influencer_youtube_channel ) ) {
				$influencer_medias['Youtube'][] = $influencer_youtube_channel;
				if (!empty($influencer_medias['Youtube'])) {
					foreach ( $influencer_medias['Youtube'] as $key => $value ) {
						if ( $value['subscriberCount'] && $value['youtube_title'] ) {
							$total_reach += intval( $value['subscriberCount'] );
						}
					}
				}
			}
		}
	}
	$influencer_twitter_channels = get_user_meta( $associated_influencers['ID'], 'twitter_account', TRUE );
	if ( in_array( 'Twitter', $platform_activated ) && ! empty( $influencer_twitter_channels ) ) {
		foreach ( $influencer_twitter_channels as $influencer_twitter_channel ) {
			if ( ! empty( $influencer_twitter_channel ) ) {
				$influencer_medias['Twitter'][] = $influencer_twitter_channel;
				if (!empty($influencer_medias['Twitter'])) {
					foreach ( $influencer_medias['Twitter'] as $key => $value ) {
						if ( $value['followers'] && $value['screen_name'] ) {
							$total_reach += intval( $value['followers'] );
						}
					}
				}
			}
		}
	}
	$influencer_instagram_pages = get_user_meta( $associated_influencers['ID'], 'instagram', TRUE );
	if ( in_array( 'Instagram', $platform_activated ) && ! empty( $influencer_instagram_pages ) ) {
		foreach ( $influencer_instagram_pages as $influencer_instagram_page ) {
			if ( ! empty( $influencer_instagram_page ) ) {
				$influencer_medias['Instagram'][] = $influencer_instagram_page;
				if (!empty($influencer_medias['Instagram'])) {
					foreach ( $influencer_medias['Instagram'] as $key => $value ) {
						if ( $value['followedByCount'] && $value['fullName'] ) {
							$total_reach += intval( $value['followedByCount'] );
						}
					}
				}
			}
		}
	}
	$influencer_pinterest_pages = get_user_meta( $associated_influencers['ID'], 'pint_accounts', TRUE );
	if ( in_array( 'Pinterest', $platform_activated ) && ! empty( $influencer_pinterest_pages ) ) {
		foreach ( $influencer_pinterest_pages as $influencer_pinterest_page ) {
			if ( ! empty( $influencer_pinterest_page ) ) {
				$influencer_medias['Pinterest'][] = $influencer_pinterest_page;
				if (!empty($influencer_medias['Pinterest'])) {
					foreach ( $influencer_medias['Pinterest'] as $key => $value ) {
						if ( $value['counts']['followers'] && $value['username'] ) {
							$total_reach += intval( $value['counts']['followers'] );
						}
					}
				}
			}
		}
	}
	$infuencer_snapchat_accounts = get_user_meta($associated_influencers['ID'], 'influencer_snapchats', true);
	if (!empty($infuencer_snapchat_accounts)) {
		foreach ( $infuencer_snapchat_accounts as $infuencer_snapchat_account ) {
			if ( $infuencer_snapchat_account[0] && $infuencer_snapchat_account[1] ) {
				$total_reach += intval( $infuencer_snapchat_account[1] );
			}
		}
	}
	$influencer_blogs = get_user_meta($associated_influencers['ID'],'influencer_blogs', true);
	if( !empty( $influencer_blogs ) ) {
		foreach ( $influencer_blogs as $influencer_blog ) {
			if ( $influencer_blog[0] && $influencer_blog[2] ) {
				$total_reach += intval( $influencer_blog[2] );
			}
		}
	}
	if( $total_reach ) { ?>
        <h3 class="offset-top-sm-3"><?php echo esc_html__('Total Reach:', 'trending-family'); ?> <?php echo esc_html( $total_reach ); ?></h3>
    <?php } ?>
    <div class="folowers-data-grid">
        <div class="row">
            <div class="flex-box">
				<?php
					/* Start Show Followers */
					/* Youtube */
					if (!empty($influencer_medias['Youtube'])) {
						foreach ($influencer_medias['Youtube'] as $key => $value) {
							if( $value['subscriberCount'] && $value['youtube_title'] ) {
								$youtube_reach = intval( $value['subscriberCount'] ); ?>
                                <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
                                    <div class="folower-item-box">
                                        <div class="num"><?php echo esc_html( $value['subscriberCount'] ); ?></div>
                                        <div class="name"><?php echo esc_html( $value['youtube_title'] ); ?></div>
                                        <hr>
                                        <div class="desc"><i
                                                    class="fa fa-youtube"></i> <?php echo esc_html__( 'Subscribers', 'trending-family' ); ?>
                                        </div>
                                    </div>
                                </div>
								<?php
							}
						}
					}
					//<!--						echo "<li class='customBox'>";-->
					//<!--						echo "<p>".$value['subscriberCount']." Subscribers</p>";-->
					//<!--						echo "<p>".$value['youtube_title']."</p>";-->
					//<!--						echo "</li>";-->
					//<!--					}-->
					//<!--					echo "</ul>";-->
					//<!--				}-->
					/* Facebook */
					if (!empty($influencer_medias['Facebook'][0])) {
						foreach ($influencer_medias['Facebook'][0] as $key => $value) {
							if( $value['fan_count'] && $value['pagename']) { ?>
                                <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
                                    <div class="folower-item-box">
                                        <div class="num"><?php echo esc_html( $value['fan_count'] ); ?></div>
                                        <div class="name"><?php echo esc_html( $value['pagename'] ); ?></div>
                                        <hr>
                                        <div class="desc"><i
                                                    class="fa fa-facebook"></i> <?php echo esc_html__( 'Followers', 'trending-family' ); ?>
                                        </div>
                                    </div>
                                </div>
								<?php
							}
						}
					}
					//<!--					echo "<h3>Facebook</h3>";-->
					//<!--					echo "<ul class='ulFacebook'>";-->
					//<!--					foreach ($influencer_medias['Facebook'][0] as $key => $value) {-->
					//<!--						echo "<li class='customBox'>";-->
					//<!--						echo "<p>".$value['fan_count']." Followers</p>";-->
					//<!--						echo "<p>".$value['pagename']."</p>";-->
					//<!--						echo "</li>";-->
					//<!--					}-->
					//<!--					echo "</ul>";-->
					//<!--				}-->
					/* Twitter */
					if (!empty($influencer_medias['Twitter'])) {
						foreach ($influencer_medias['Twitter'] as $key => $value) {
							if ( $value['followers'] && $value['screen_name'] ) { ?>
                                <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
                                    <div class="folower-item-box">
                                        <div class="num"><?php echo esc_html( $value['followers'] ); ?></div>
                                        <div class="name"><?php echo esc_html( $value['screen_name'] ); ?></div>
                                        <hr>
                                        <div class="desc"><i
                                                    class="fa fa-twitter"></i> <?php echo esc_html__( 'Followers', 'trending-family' ); ?>
                                        </div>
                                    </div>
                                </div>
                                <!--						echo "<li class='customBox'>";-->
                                <!--						echo "<p>".$value['followers']." Followers</p>";-->
                                <!--						echo "<p>".$value['screen_name']."</p>";-->
                                <!--						echo "</li>";-->
                                <!--					}-->
                                <!--					echo "</ul>";-->
                                <!--				}-->
							<?php }
						}
					}
					/* Twitter */
					if (!empty($influencer_medias['Instagram'])) {
						foreach ($influencer_medias['Instagram'] as $key => $value) {
							if ( $value['followedByCount'] && $value['fullName'] ) { ?>
                                <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
                                    <div class="folower-item-box">
                                        <div class="num"><?php echo esc_html( $value['followedByCount'] ); ?></div>
                                        <div class="name"><?php echo esc_html( $value['fullName'] ); ?></div>
                                        <hr>
                                        <div class="desc"><i
                                                    class="fa fa-instagram"></i> <?php echo esc_html__( 'Followers', 'trending-family' ); ?>
                                        </div>
                                    </div>
                                </div>
                                <!--						echo "<li class='customBox'>";-->
                                <!--						echo "<p>".$value['followedByCount']." Followers</p>";-->
                                <!--						echo "<p>".$value['fullName']."</p>";-->
                                <!--						echo "</li>";-->
                                <!--					}-->
                                <!--					echo "</ul>";-->
                                <!--				}-->
							<?php }
						}
					}
					/* Pinterest */
					if (!empty($influencer_medias['Pinterest'])) {
						foreach ($influencer_medias['Pinterest'] as $key => $value) {
							if ( $value['counts']['followers'] && $value['username'] ) { ?>
                                <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
                                    <div class="folower-item-box">
                                        <div class="num"><?php echo esc_html( $value['counts']['followers'] ); ?></div>
                                        <div class="name"><?php echo esc_html( $value['username'] ); ?></div>
                                        <hr>
                                        <div class="desc"><i
                                                    class="fa fa-pinterest"></i> <?php echo esc_html__( 'Followers', 'trending-family' ); ?>
                                        </div>
                                    </div>
                                </div>
                                <!--						echo "<li class='customBox'>";-->
                                <!--						echo "<p>".$value['counts']['followers']." Followers</p>";-->
                                <!--						echo "<p>".$value['username']."</p>";-->
                                <!--						echo "</li>";-->
                                <!--					}-->
                                <!--					echo "</ul>";-->
                                <!--				}-->
                                <!--				/* End Show Followers */-->
                                <!--			}-->
							<?php }
						}
					}
					if (!empty($infuencer_snapchat_accounts)) {
						foreach ( $infuencer_snapchat_accounts as $infuencer_snapchat_account ) {
							if( $infuencer_snapchat_account[0] && $infuencer_snapchat_account[1] ) { ?>
                                <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
                                    <div class="folower-item-box">
                                        <div class="num"><?php echo esc_html( $infuencer_snapchat_account[1] ); ?></div>
                                        <div class="name"><?php echo esc_html( $infuencer_snapchat_account[0] ); ?></div>
                                        <hr>
                                        <div class="desc"><i
                                                    class="fa fa-snapchat"></i> <?php echo esc_html__( 'Estimated Avg. Snap', 'trending-family' ); ?>
                                        </div>
                                    </div>
                                </div>
								<?php
							}
						}
					}
					if( !empty( $influencer_blogs ) ) {
						foreach ( $influencer_blogs as $influencer_blog ) {
							if( $influencer_blog[0] && $influencer_blog[2] ) { ?>
                                <div class="col-md-3 col-sm-4 col-ms-6 col-xs-12">
                                    <div class="folower-item-box">
                                        <div class="num"><?php echo esc_html( $influencer_blog[2] ); ?></div>
                                        <div class="name"><?php echo esc_html( $influencer_blog[0] ); ?></div>
                                        <hr>
                                        <div class="desc"><i
                                                    class="fa fa-snapchat"></i> <?php echo esc_html__( 'Unique Page Visitors', 'trending-family' ); ?>
                                        </div>
                                    </div>
                                </div>
								<?php
							}
						}
					} ?>
            </div>
        </div>
    </div>
    <hr>
    <br>
	