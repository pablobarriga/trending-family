<?php if ( ! defined( 'ABSPATH' ) ) exit;
	/**
	 * Template part for displaying Single Campaign Post when the Campaign is inactive
	 * @link https://codex.wordpress.org/Template_Hierarchy
	 * @package trending-family
	 */
?>
<?php

$enable_influencers_section = get_field( 'trending_family_campaign_enable_influencers_section' );

if( $enable_influencers_section ) {
    
	$collapse_influencers = get_field( 'trending_family_campaign_influencers_collapse_by_default' );
	
	if ( have_rows( 'trending_family_campaign_influencers' ) ) { ?>
		<section class="section bg-grey bg-white-xs collapse-animation <?php echo $collapse_influencers ? esc_attr( 'collapse-section' ) : ''; ?>">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 offset-bottom-sm-6 offset-bottom-xs-0 title-box">
						<h2><?php echo esc_html__( 'Influencers', 'trending-family' ); ?> <a
								class="btn-collapse-section" href="javascript:void(0);">&nbsp</a>
						</h2>
					</div>
				</div>
				<div class="row">
						<div class="feature-controls-dots stl-2 bordered feature-controls-dots-1 flex-box justify-beetwen"></div>
						<div class="feature-carousel feature-carousel-1 mobile-cont-active owl-carousel">
							<?php while( have_rows('trending_family_campaign_influencers') ) {
								the_row();
								$associated_influencers = get_sub_field( 'trending_family_campaign_influencers_influencer' );
								$platform_activated = get_sub_field( 'trending_family_campaign_influencers_platform' );								
								
								$campaign_user = new WP_User($associated_influencers['ID']);
								$total_reach = 0;
								
								// YouTube
								$youtubeService = new SocialMedia\SocialAccounts\Youtube($campaign_user);
								$youtubeService->cacheData('youtube_channels', $youtubeService->channels());
								$youtubeChannels = $youtubeService->cachedData('youtube_channels');
								if( $youtubeChannels) foreach($youtubeChanels as $youtubeChannel) $total_reach += $youtubeChannel['subscriberCount'];
								
								// Twitter
								$twitterService = new SocialMedia\SocialAccounts\Twitter($campaign_user);
								//$twitterService->cacheData('twitter_accounts', $twitterService->accounts());
								$twitterAccounts = $twitterService->cachedData('twitter_accounts');
								if($twitterAccounts) foreach($twitterAccounts as $twitterAccount) $total_reach += $twitterAccount['followers_count'];
								
								// Instagram
								$instagramService = new SocialMedia\SocialAccounts\Instagram($campaign_user);
								//$instagramService->cacheData('instagram_pages', $instagramService->pages());
								$instagramPages = $instagramService->cachedData('instagram_pages');
								if($instagramPages) foreach($instagramPages as $instagramPage) $total_reach += $instagramPage['followers_count'];
								
								// Pinterest
								$pinterestService = new SocialMedia\SocialAccounts\Pinterest($campaign_user);
								$pinterestAccounts =  $pinterestService->accounts();
								
								// Snapchat
								$snapchatAccounts = ( new SocialMedia\SocialAccounts\Snapchat($campaign_user) )->accounts();
								if($snapchatAccounts) foreach($snapchatAccounts as $snapchatAccount) $total_reach += $snapchatAccount[1];
								
								// Blog
								$blogAccounts = ( new SocialMedia\SocialAccounts\Blog($campaign_user) )->accounts();
								if($blogAccounts) foreach($blogAccounts as $blogAccount) $total_reach += $blogAccount[1];
								
								
								// FaceBook
								$facebookService = new SocialMedia\SocialAccounts\Facebook($campaign_user);
								//$facebookService->cacheData('facebook_pages', $facebookService->pages());
								$facebookPages = $facebookService->cachedData('facebook_pages');
								if( $facebookPages) {
								    foreach($facebookPages as $ownerPages) {
								        
								        if($ownerPages) {
								            foreach($ownerPages as $facebookPage) {
								                $total_reach += $facebookPage['fan_count'];
								            }
								        }
								    }
								}
								?>
								<div class="feature-slide stl-2"
								     data-dot='<i class="icon" style="background-image: url(<?php echo esc_url( get_wp_user_avatar_src( $associated_influencers['ID'], 'large' ) ); ?>);">1</i><?php echo esc_attr( $associated_influencers['display_name']); ?>'>
									<div class="top-subheader-img visible-xs">
										<img class="img-circle v2" src="<?php echo esc_url( get_wp_user_avatar_src( $associated_influencers['ID'], 'large' ) ); ?>" alt="<?php echo esc_attr( $associated_influencers['display_name']); ?>">
										<div class="subhead-btn"><?php echo esc_attr( $associated_influencers['display_name']); ?></div>
									</div>
									
									<div class="feature-slide-cont">
										<?php 
										// Include Show Followers
			                            if(!empty( $platform_activated )) {
			                                include( 'content-show-followers.php' );
			                                include( 'content-show-feeds.php' );
			                                include( 'content-show-graphs.php' );			                                
			                            }
			                            ?>
			                         </div>
								</div><!-- end of feature-slide stl-2 -->
							<?php } ?>
						</div><!-- end of feature-carousel feature-carousel-1 mobile-cont-active owl-carousel -->
					
				</div><!-- end of row -->
			</div><!-- end of container -->
		</section>
		<?php include( 'content-show-project-timeline.php' ); ?> 
	<?php }
} ?>