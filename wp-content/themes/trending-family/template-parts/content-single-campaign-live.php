<?php if ( ! defined( 'ABSPATH' ) ) exit;
// @todo To Remove this file
/**
* Template part for displaying Single Campaign Post when the Campaign is live
* @link https://codex.wordpress.org/Template_Hierarchy
* @package trending-family
*/
?>

<?php
$enable_influencers_section = get_field( 'trending_family_campaign_enable_influencers_section' );
if( $enable_influencers_section ){
	$collapse_influencers = get_field( 'trending_family_campaign_influencers_collapse_by_default' ); ?>
	<section class="section bg-grey bg-white-xs collapse-animation <?php echo $collapse_influencers ? esc_attr( 'collapse-section' ) : ''; ?>">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 offset-bottom-sm-6 offset-bottom-xs-0 title-box">
					<h2><?php echo esc_html__('Influencers', 'trending-family'); ?><a class="btn-collapse-section" href="javascript:void(0);">&nbsp</a></h2>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="feature-controls-dots stl-2 bordered feature-controls-dots-1 flex-box justify-beetwen"></div>
					<div class="feature-carousel feature-carousel-1 mobile-cont-active owl-carousel">
						<div class="feature-slide stl-2" data-dot='<i class="icon" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/images/img-450x450.jpg'; ?>);">1</i> Lorem Ipsum'>
							<div class="top-subheader-img visible-xs">
								<img class="img-circle v2" src="<?php echo get_template_directory_uri() . '/assets/images/img-450x450.jpg'; ?>" alt="">
								<div class="subhead-btn">Lorem Ipsum</div>
							</div>
							<div class="feature-slide-cont">
								<div class="row">
									<div class="col-xs-12">
										<h3>Lorem Ipsum Dolor Atem</h3>
										<div class="chart-box">
											<canvas id="chart-1"></canvas>
										</div>
										<script>
                                            var data1 = {
                                                labels: ["January", "February", "March", "April", "May", "June", "July"],
                                                datasets: [{
                                                    label: "Unfilled",
                                                    //fill: false,
                                                    backgroundColor: "rgba(48, 107, 151, 0.2)",
                                                    borderColor: "rgba(48, 107, 151, 0.9)",
                                                    data: [
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor()
                                                    ],
                                                }, {
                                                    label: "Dashed",
                                                    //fill: false,
                                                    backgroundColor: "rgba(101, 200, 181, 0.2)",
                                                    borderColor: "rgba(101, 200, 181, 0.9)",
                                                    //borderDash: [5, 5],
                                                    data: [
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor()
                                                    ],
                                                }],
                                            };
										</script>
									</div>
								</div>
							</div>
						</div>
						<div class="feature-slide stl-2" data-dot='<i class="icon" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/images/img-450x450.jpg'; ?>">1</i> Lorem Ipsum'>
							<div class="top-subheader-img visible-xs">
								<img class="img-circle v2" src="<?php echo get_template_directory_uri() . '/assets/images/img-450x450.jpg'; ?>" alt="">
								<div class="subhead-btn">Lorem Ipsum</div>
							</div>
							<div class="feature-slide-cont">
								<div class="row">
									<div class="col-xs-12">
										<h3>Lorem Ipsum Dolor Atem</h3>
										<div class="chart-box">
											<canvas id="chart-2"></canvas>
										</div>
										<script>
                                            var data2 = {
                                                labels: ["January", "February", "March", "April", "May", "June", "July"],
                                                datasets: [{
                                                    label: "Unfilled",
                                                    //fill: false,
                                                    backgroundColor: "rgba(48, 107, 151, 0.2)",
                                                    borderColor: "rgba(48, 107, 151, 0.9)",
                                                    data: [
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor()
                                                    ],
                                                }, {
                                                    label: "Dashed",
                                                    //fill: false,
                                                    backgroundColor: "rgba(101, 200, 181, 0.2)",
                                                    borderColor: "rgba(101, 200, 181, 0.9)",
                                                    //borderDash: [5, 5],
                                                    data: [
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor()
                                                    ],
                                                }],
                                            };
										</script>
									</div>
								</div>
							</div>
						</div>
						<div class="feature-slide stl-2" data-dot='<i class="icon" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/images/img-450x450.jpg'; ?>);">1</i> Lorem Ipsum'>
							<div class="top-subheader-img visible-xs">
								<img class="img-circle v2" src="<?php echo get_template_directory_uri() . '/assets/images/img-450x450.jpg'; ?>" alt="">
								<div class="subhead-btn">Lorem Ipsum</div>
							</div>
							<div class="feature-slide-cont">
								<div class="row">
									<div class="col-xs-12">
										<h3>Lorem Ipsum Dolor Atem</h3>
										<div class="chart-box">
											<canvas id="chart-3"></canvas>
										</div>
										<script>
                                            var data3 = {
                                                labels: ["January", "February", "March", "April", "May", "June", "July"],
                                                datasets: [{
                                                    label: "Unfilled",
                                                    //fill: false,
                                                    backgroundColor: "rgba(48, 107, 151, 0.2)",
                                                    borderColor: "rgba(48, 107, 151, 0.9)",
                                                    data: [
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor()
                                                    ],
                                                }, {
                                                    label: "Dashed",
                                                    //fill: false,
                                                    backgroundColor: "rgba(101, 200, 181, 0.2)",
                                                    borderColor: "rgba(101, 200, 181, 0.9)",
                                                    //borderDash: [5, 5],
                                                    data: [
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor()
                                                    ],
                                                }],
                                            };
										</script>
									</div>
								</div>
							</div>
						</div>
						<div class="feature-slide stl-2" data-dot='<i class="icon" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/images/img-450x450.jpg'; ?>);">1</i> Lorem Ipsum'>
							<div class="top-subheader-img visible-xs">
								<img class="img-circle v2" src="<?php echo get_template_directory_uri() . '/assets/images/img-450x450.jpg'; ?>" alt="">
								<div class="subhead-btn">Lorem Ipsum</div>
							</div>
							<div class="feature-slide-cont">
								<div class="row">
									<div class="col-xs-12">
										<h3>Lorem Ipsum Dolor Atem</h3>
										<div class="chart-box">
											<canvas id="chart-4"></canvas>
										</div>
										<script>
                                            var data4 = {
                                                labels: ["January", "February", "March", "April", "May", "June", "July"],
                                                datasets: [{
                                                    label: "Unfilled",
                                                    //fill: false,
                                                    backgroundColor: "rgba(48, 107, 151, 0.2)",
                                                    borderColor: "rgba(48, 107, 151, 0.9)",
                                                    data: [
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor()
                                                    ],
                                                }, {
                                                    label: "Dashed",
                                                    //fill: false,
                                                    backgroundColor: "rgba(101, 200, 181, 0.2)",
                                                    borderColor: "rgba(101, 200, 181, 0.9)",
                                                    //borderDash: [5, 5],
                                                    data: [
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor()
                                                    ],
                                                }],
                                            };
										</script>
									</div>
								</div>
							</div>
						</div>
						<div class="feature-slide stl-2" data-dot='<i class="icon" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/images/img-450x450.jpg'; ?>);">1</i> Lorem Ipsum'>
							<div class="top-subheader-img visible-xs">
								<img class="img-circle v2" src="<?php echo get_template_directory_uri() . '/assets/images/img-450x450.jpg'; ?>" alt="">
								<div class="subhead-btn">Lorem Ipsum</div>
							</div>
							<div class="feature-slide-cont">
								<div class="row">
									<div class="col-xs-12">
										<h3>Lorem Ipsum Dolor Atem</h3>
										<div class="chart-box">
											<canvas id="chart-5"></canvas>
										</div>
										<script>
                                            var data5 = {
                                                labels: ["January", "February", "March", "April", "May", "June", "July"],
                                                datasets: [{
                                                    label: "Unfilled",
                                                    //fill: false,
                                                    backgroundColor: "rgba(48, 107, 151, 0.2)",
                                                    borderColor: "rgba(48, 107, 151, 0.9)",
                                                    data: [
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor()
                                                    ],
                                                }, {
                                                    label: "Dashed",
                                                    //fill: false,
                                                    backgroundColor: "rgba(101, 200, 181, 0.2)",
                                                    borderColor: "rgba(101, 200, 181, 0.9)",
                                                    //borderDash: [5, 5],
                                                    data: [
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor(),
                                                        randomScalingFactor()
                                                    ],
                                                }],
                                            };
										</script>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</section>
<?php }

$enable_progress_timeline_section = get_field( 'trending_family_campaign_enable_progress_timeline_section' );
if( $enable_progress_timeline_section ) {
	$progress_timeline_collapse = get_field( 'trending_family_progress_timeline_collapse_by_default' );
    
    ?>
    <section class="section bg-blue collapse-animation <?php echo $progress_timeline_collapse ? esc_attr( 'collapse-section' ) : ''; ?>">
        <div class="container">
            <div class="row offset-bottom-xs-7">
                <div class="col-xs-12 title-box">
                    <h2><?php echo esc_html__('Project timeline', 'trending-family'); ?><a class="btn-collapse-section" title="<?php echo esc_attr__('Collapse', 'trending-family'); ?>" href="javascript:void(0);">&nbsp;</a></h2>
                    <hr>
                </div>
                <div class="col-md-4 col-md-offset-8 col-xs-12 data-markers-descript-wrap">
                    <div class="row data-markers-descript">
                        <div class="flex-center">
                            <div class="col-xs-4">
                                <div class="circle-mark mark-complete"></div>
                                <span><?php echo esc_html__('Complete', 'trending-family'); ?></span>
                            </div>
                            <div class="col-xs-4">
                                <div class="circle-mark mark-progress"></div>
                                <span><?php echo esc_html__('In Progress', 'trending-family'); ?></span>
                            </div>
                            <div class="col-xs-4">
                                <div class="circle-mark"></div>
                                <span><?php echo esc_html__('Next Steps', 'trending-family'); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="row data-carousels-box">
                    <?php if( have_rows( 'trending_family_campaign_project_timeline_overall_progress' )){ ?>
                        <div class="col-xs-12 offset-bottom-xs-5">
                            <h4><?php echo esc_html__('Overall Progress', 'trending-family'); ?></h4>
                            <div class="data-carousel owl-carousel">
                                <?php while( have_rows( 'trending_family_campaign_project_timeline_overall_progress' ) ) {
                                    the_row();
                                    $overall_progress_title = get_sub_field( 'trending_family_campaign_project_timeline_overall_progress_title' );
                                    $overall_progress_date = get_sub_field( 'trending_family_campaign_project_timeline_overall_progress_date' );
                                    $overall_progress_status = get_sub_field( 'trending_family_campaign_project_timeline_overall_progress_status' ); ?>
                                    <div class="data-values <?php if( !empty( $overall_progress_status ) && $overall_progress_status != 'Next Steps') { echo ($overall_progress_status == 'Complete' ) ? esc_attr('mark-complete') : esc_attr('mark-progress'); } ?>">
                                        <p class="ttl"><?php echo esc_html( $overall_progress_title ); ?></p>
                                        <p class="date"><?php echo esc_html( $overall_progress_date ); ?></p>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
				<?php }
				if( have_rows( 'trending_family_campaign_project_timeline_influencers_progress' ) ) {
					while ( have_rows( 'trending_family_campaign_project_timeline_influencers_progress' ) ) {
						the_row();
						$progress_influencer = get_sub_field( 'trending_family_campaign_project_timeline_influencers_influencer'); ?>
						<div class="col-xs-12 offset-bottom-xs-5">
							<h4><?php echo esc_html( $progress_influencer['display_name'] ); ?></h4>
							<?php if( have_rows( 'trending_family_campaign_project_timeline_influencers_influencer_progress' )) { ?>
								<div class="data-carousel owl-carousel">
									<?php while ( have_rows( 'trending_family_campaign_project_timeline_influencers_influencer_progress' ) ) {
										the_row();
										$progress_title = get_sub_field( 'trending_family_campaign_project_timeline_influencers_progress_title' );
										$progress_date = get_sub_field( 'trending_family_campaign_project_timeline_influencers_progress_date' );
										$progress_status = get_sub_field( 'trending_family_campaign_project_timeline_influencers_progress_status' ); ?>

										<div class="data-values <?php if( !empty( $progress_status ) && $progress_status != 'Next Steps') { echo ($progress_status == 'Complete' ) ? esc_attr('mark-complete') : esc_attr('mark-progress'); } ?>">
											<p class="ttl"><?php echo esc_html( $progress_title ); ?></p>
											<p class="date"><?php echo esc_html( $progress_date ); ?></p>
										</div>

									<?php } ?>
								</div>
							<?php } ?>
						</div>
					<?php }
				} ?>
			</div>
		</div>
	</section>
<?php }
$enable_content_section = get_field( 'trending_family_campaign_enable_content_section' );
if( $enable_content_section ) {
	$collapse_contents = get_field( 'trending_family_campaign_contents_collapse_by_default' );
	if ( have_rows( 'trending_family_campaign_contents' ) ) { ?>

		<section
			class="section bg-grey collapse-animation <?php echo $collapse_contents ? esc_attr( 'collapse-section' ) : ''; ?>">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 title-box">
						<h2><?php echo esc_html__( 'Your content:', 'trending-family' ); ?> <a
								class="btn-collapse-section"
								title="<?php echo esc_attr__( 'Collapse', 'trending-family' ); ?>"
								href="javascript:void(0);">&nbsp;</a></h2>
					</div>
				</div>
				<?php while ( have_rows( 'trending_family_campaign_contents' ) ) {
					the_row();
					$campaign_post_title          = get_sub_field( 'trending_family_campaign_content_title' );
					$campaign_post_featured_image = get_sub_field( 'trending_family_campaign_content_featured_image' ); ?>

					<div class="feed-wrap">
						<div class="row">
							<div class="flex-center title-with-circle">
								<?php if ( ! empty( $campaign_post_featured_image ) ) { ?>
									<div class="col-sm-2 col-xs-12">
										<img class="img-circle v3"
										     src="<?php echo esc_url( $campaign_post_featured_image ); ?>"
										     alt="<?php the_title(); ?>">
									</div>
									<?php
								}
								if ( ! empty( $campaign_post_title ) ) { ?>
									<div class="col-sm-10 col-xs-12">
										<h4><?php echo esc_html( $campaign_post_title ); ?></h4>
									</div>
								<?php } ?>
							</div>
						</div>
						<?php if ( have_rows( 'trending_family_campaign_content_posts' ) ) { ?>
							<div class="row">
								<div class="col-xs-12">
									<div class="feed-grid offset-bottom-xs-9">
										<?php while ( have_rows( 'trending_family_campaign_content_posts' ) ) {
											the_row();
											$media_platform = get_sub_field( 'trending_family_campaign_content_post_media_platform' );
											$post_embed_url = get_sub_field( 'trending_family_campaign_content_post_post_url' ); ?>
											<div class="feed-item">
												<div>
													<?php echo $post_embed_url; ?>
												</div>
											</div>
										<?php } ?>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</section>
	<?php }
}

$enable_results_section = get_field( 'trending_family_campaign_enable_results_section' );?>
<?php if( $enable_results_section ){
	$results_collapse = get_field( 'trending_family_campaign_results_collapse_by_default' ); ?>
	<section class="section collapse-animation <?php echo $results_collapse ? esc_attr( 'collapse-section') : ''; ?>">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 title-box">
					<h2><?php echo esc_html__('Results', 'trending-family'); ?><a class="btn-collapse-section" title="<?php echo esc_attr__('Collapse', 'trending-family'); ?>" href="javascript:void(0);">&nbsp;</a></h2>
				</div>
			</div>
			<?php
			while( have_rows( 'trending_family_campaign_contents' )) {
				the_row();
				if ( have_rows( 'trending_family_campaign_content_posts' ) ) {
					$campaign_count = 1;

					while ( have_rows( 'trending_family_campaign_content_posts' ) ) {
						the_row();
						$media_platform = get_sub_field( 'trending_family_campaign_content_post_media_platform');
						if( $media_platform == 'Facebook' || $media_platform == 'Youtube' || $media_platform == 'Instagram'){
							$post_embed_url = get_sub_field( 'trending_family_campaign_content_post_post_url' ); ?>
							<div class="row">
								<div class="col-xs-12">
									<div class="chart-box offset-bottom-xs-7">

										<h4><?php echo esc_html( 'Test', 'trending-family' ); ?></h4>

										<canvas id="chart-<?php echo sanitize_html_class( $campaign_count ); ?>"></canvas>
									</div>
									<script>
                                        var data11 = {
                                            labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"],
                                            datasets: [{
                                                type: 'line',
                                                label: 'Dataset 1',
                                                backgroundColor: "rgba(48, 107, 151, 0.2)",
                                                borderColor: "rgba(48, 107, 151, 0.9)",
                                                borderWidth: 2,
                                                //fill: false,
                                                data: [
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor()
                                                ]
                                            }, {
                                                type: 'bar',
                                                label: 'Dataset 2',
                                                backgroundColor: "rgba(101, 200, 181, 0.9)",
                                                data: [
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor(),
                                                    randomScalingFactor()
                                                ]
                                            }]
                                        };
									</script>
								</div><!-- end of col-xs-12 -->
							</div><!-- end of row -->
						<?php }
						$campaign_count ++;
					}
				}
			} ?>
		</div>
	</section>
<?php } ?>

<script>
    window.onload = function() {
        // Chart 1
        var ctx1 = document.getElementById('chart-1');
        var myChart1 = new Chart(ctx1, {
            type: 'line',
            data: data1,
        });
        // chart 2
        var ctx2 = document.getElementById('chart-2');
        var myChart2 = new Chart(ctx2, {
            type: 'line',
            data: data2,
        });
        // chart 3
        var ctx3 = document.getElementById('chart-3');
        var myChart3 = new Chart(ctx3, {
            type: 'line',
            data: data3,
        });
        // chart 4
        var ctx4 = document.getElementById('chart-4');
        var myChart4 = new Chart(ctx4, {
            type: 'line',
            data: data4,
        });
        // chart 5
        var ctx5 = document.getElementById('chart-5');
        var myChart5 = new Chart(ctx5, {
            type: 'line',
            data: data5,
        });
        // chart 11
        var ctx11 = document.getElementById('chart-11');
        var myChart11 = new Chart(ctx11, {
            type: 'bar',
            data: data11,
        });
        // chart 12
        var ctx12 = document.getElementById('chart-12');
        var myChart12 = new Chart(ctx12, {
            type: 'bar',
            data: data12,
        });
        // chart 13
        var ctx13 = document.getElementById('chart-13');
        var myChart13 = new Chart(ctx13, {
            type: 'bar',
            data: data13,
        });
        // chart 14
        var ctx14 = document.getElementById('chart-14');
        var myChart14 = new Chart(ctx14, {
            type: 'line',
            data: data14,
        });
        // chart 15
        var ctx15 = document.getElementById('chart-15');
        var myChart15 = new Chart(ctx15, {
            type: 'pie',
            data: data15,
        });
        // chart 16
        var ctx16 = document.getElementById('chart-16');
        var myChart16 = new Chart(ctx16, {
            type: 'doughnut',
            data: data16,
        });
    };
</script>