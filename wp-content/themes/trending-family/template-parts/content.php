<?php if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package trending-family
 */

$thumb = get_post_thumbnail_id();
$featured_image = wp_get_attachment_url( $thumb, 'full' );

if( !empty($featured_image) ){
    $featured_image = aq_resize( $featured_image, '1280', '450', true, true, true); ?>
    <div class="hero-bg hero-overlay flex-center-full" style="background-image: url(<?php echo esc_url( $featured_image ); ?>); ">
        <div class="hero-text"><?php the_title('<h1>', '</h1>'); ?></div>
    </div>
<?php } ?>

<section id="post-<?php the_ID(); ?>" <?php post_class('section post-article'); ?>>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="post-title-box offset-bottom-sm-8 offset-bottom-xs-4">
                    <?php the_title('<h1>', '</h1>'); ?>
                    <div class="meta-date"><?php echo get_the_date(); ?> <?php echo get_the_time(); ?></div>
                </div>

                <?php the_content(); ?>
            </div>
            <?php if( trending_family_post_tags() ){ ?>
                <div class="col-xs-12">
                    <div class="meta-tags">
                        <?php echo trending_family_post_tags(); ?>
                    </div>
                </div>
            <?php }
            wp_link_pages( array(
                'before' => '<div class="col-xs-12"><div class="page-links">' . esc_html__( 'Pages:', 'trending-family' ),
                'after'  => '</div></div>',
            ) ); ?>
        </div>
    </div>
</section>
<?php
$next_post = get_next_post();
$prev_post = get_previous_post();
if($next_post || $prev_post ) { ?>
	<div class="to-posts-btns hidden-xs">
        <?php if( $prev_post ) {
	        $prev_title = strip_tags(str_replace('"', '', $prev_post->post_title));?>
            <a class="to-prew" href="<?php echo get_permalink($prev_post->ID); ?>" title="<?php echo sprintf(__('Goto %s', 'trending-family'), $prev_title); ?>"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
        <?php } ?>
        <?php if( $next_post ) {
	        $next_title = strip_tags(str_replace('"', '', $next_post->post_title)); ?>
            <a class="to-next" href="<?php echo get_permalink($next_post->ID); ?>" title="<?php echo sprintf(__('Goto %s', 'trending-family'), $next_title); ?>"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
        <?php } ?>
    </div>
<?php } ?>

<?php
$args = array(
	'post_status' => 'publish',
	'exclude' => array( $post->ID )
);

$recent_posts = wp_get_recent_posts( $args, OBJECT );
if( $recent_posts ) { ?>

    <section class="section bg-grey">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="text-center"><?php echo esc_html__( 'Recent Posts', 'trending-family' ); ?></h2>
                    <div class="post-carousel owl-carousel">
						<?php foreach ( $recent_posts as $recent_post ) {
							$post = $recent_post;
							setup_postdata( $post );
							$thumb          = get_post_thumbnail_id();
							$featured_image = wp_get_attachment_url( $thumb, 'full' ); ?>

                            <div id="post-<?php the_ID(); ?>" <?php post_class( 'post-item post-item-v1' ); ?>>
                                <div class="post-cont">
									<?php if ( ! empty( $featured_image ) ) { ?>
                                        <a href="<?php the_permalink(); ?>" class="thumb-circle"
                                           style="background-image: url(<?php echo esc_url( $featured_image ); ?>);">&nbsp</a>
									<?php } ?>
                                    <h4><a href="<?php the_permalink(); ?>"
                                           title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
                                    <div class="excerpt">
										<?php echo the_excerpt(); ?>
                                    </div>
                                    <a class="btn btn-default offset-top-md-3" href="<?php the_permalink(); ?>"
                                       title="<?php the_title_attribute(); ?>"><?php echo esc_html__( 'Read More', 'trending-family' ); ?></a>
                                </div>
                            </div>
						<?php } wp_reset_query(); ?>

                    </div>
                </div>
            </div>
        </div><!-- end of container -->
    </section>
<?php }

$contact_form_section = get_field('tf_blog_post_enable_contact_section');

if( $contact_form_section ){
    $contact_section_heading = get_field( 'tf_blog_post_contact_section_heading' );
    $contact_section_tagline = get_field( 'tf_blog_post_contact_section_tagline' );
    $contact_form_shortcode = get_field( 'tf_blog_post_contact_section_form_shortcode' ); ?>

    <section id="contact" class="section bg-blue">
        <div class="container">
            <div class="row">
                <div class="flex-center">
                    <div class="col-lg-4 col-sm-5 col-xs-12 text-center-xs offset-bottom-xs-2 offset-bottom-sm-0">
                        <?php if(!empty( $contact_section_heading )){ ?>
                            <h2><?php echo esc_html( $contact_section_heading ); ?></h2>
                        <?php }
                            if( !empty( $contact_section_tagline ) ) {
                                echo $contact_section_tagline;
                            } ?>
                    </div>
                    <?php if(!empty( $contact_form_shortcode )) { ?>
                        <div class="col-lg-8 col-sm-7 col-xs-12 form-line-style">
                            <?php echo do_shortcode($contact_form_shortcode); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<?php }