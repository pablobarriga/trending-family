
<script>
 window.chartColors = {
         red: 'rgba(101, 200, 181, 0.2)',
         orange: 'rgba(255, 159, 64, 0.2)',
         green: 'rgba(101, 200, 181, 0.2)',
         blue: 'rgba(48, 107, 151, 0.2)',
         grey: 'rgba(201, 203, 207, 0.2)'
 };
 </script>
 
 <style>
 .show {
    display: inherit !important;
 }
 </style>

<ul class="nav nav-tabs">

	<li class="nav-item active"><a href="#nav-fb-viewership-gender-age-group"
		class="nav-link " id="nav-fb-viewership-gender-age-group-tab"
		data-toggle="tab" role="tab"
		aria-controls="nav-fb-viewership-gender-age-group"
		aria-expanded="true"><i class="fa fa-facebook"></i> Gender/Age</a></li>
		
	<li class="nav-item"><a href="#nav-fb-viewership-impressions-engagement"
		class="nav-link" id="nav-fb-viewership-impressions-engagement-tab"
		data-toggle="tab" role="tab"
		aria-controls="nav-fb-viewership-impressions-engagement"
		aria-expanded="true"><i
			class="fa fa-facebook"></i> Engagement</a></li>
			
	<li class="nav-item"><a href="#nav-tw-viewership"
		class="nav-link" id="nav-tw-viewership-tab"
		data-toggle="tab" role="tab"
		aria-controls="nav-tw-viewership"
		aria-expanded="true"><i
			class="fa fa-twitter"></i> Tweets</a></li>
			
	<li class="nav-item"><a href="#nav-yt-viewership-audience-retention"
		class="nav-link" id="nav-yt-viewership-audience-retention-tab"
		data-toggle="tab" role="tab"
		aria-controls="nav-yt-viewership-audience-retention"
		aria-expanded="true"><i
			class="fa fa-youtube"></i> Audience Retention</a></li>
			
	<li class="nav-item"><a href="#nav-yt-viewership-geo"
		class="nav-link" id="nav-yt-viewership-geo-tab"
		data-toggle="tab" role="tab"
		aria-controls="nav-yt-viewership-geo"
		aria-expanded="true"><i
			class="fa fa-youtube"></i> Geo</a></li>
</ul>

<div class="tab-content" id="nav-tabContent">

	<div class="tab-pane active"
		id="nav-fb-viewership-gender-age-group" role="tabpanel"
		aria-labelledby="nav-fb-viewership-gender-age-group-tab">
    	<?php include( 'charts/fb-viewership-gender-age-group.php' );?>
    </div>
    
	<div class="tab-pane fade"
		id="nav-fb-viewership-impressions-engagement" role="tabpanel"
		aria-labelledby="nav-fb-viewership-impressions-engagement-tab">
    	<?php include( 'charts/fb-viewership-impressions-engagement.php' );?>
    </div>
    
	<div class="tab-pane fade"
		id="nav-tw-viewership" role="tabpanel"
		aria-labelledby="nav-tw-viewership-tab">
    	<?php include( 'charts/tw-viewership.php' );?>
    </div>
    
	<div class="tab-pane fade"
		id="nav-yt-viewership-audience-retention" role="tabpanel"
		aria-labelledby="nav-yt-viewership-audience-retention-tab">
    	<?php include( 'charts/yt-viewership-audience-retention.php' );?>
    </div>
    
	<div class="tab-pane fade"
		id="nav-yt-viewership-geo" role="tabpanel"
		aria-labelledby="nav-yt-viewership-geo-tab">
    	<?php include( 'charts/yt-viewership-geo.php' );?>
    </div>

    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>