<?php if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package trending-family
 */
?>
<?php
$thumb = get_post_thumbnail_id();
$featured_image = wp_get_attachment_url( $thumb, 'full' ); ?>
<div id="<?php the_ID(); ?>" <?php post_class('blog-post'); ?>>
	<?php if( !empty( $featured_image )){ ?>
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo esc_url( $featured_image ); ?>" alt="<?php the_title(); ?>"></a>
	<?php } ?>
	<h4><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
    <div class="blog-post-content">
        <?php
        $post_excerpt = get_the_excerpt();
        if( !empty( $post_excerpt )){ ?>
            <p> <?php echo $post_excerpt; ?></p>
        <?php } else {
	        if ( strpos( $post->post_content, '<!--more-->' ) ) {

		        /* translators: %s: Name of current post */
		        the_content( '..' );

	        } else {

		        the_excerpt( '' );

	        }
        } ?>

        <a href="<?php the_permalink(); ?>" class="btn btn-success offset-top-md-3" title="<?php the_title_attribute(); ?>"><?php echo esc_html__('READ MORE', 'trending-family'); ?></a>
    </div>
</div>