<?php
/*
 * Helpers Function For Trending Family
 * @package Trending Family
 */
if ( ! defined( 'ABSPATH' ) ) exit;

if(!function_exists('pre')){
	/**
	 *
	 */
	function pre()
	{
		array_map(function($data){
			echo '<pre>';
			print_r($data);
			echo '</pre>';
		}, func_get_args() );
	}
}

if(!function_exists('trending_family_post_tags')){

	/**
	 * @return bool|string|WP_Error
	 */
	function trending_family_post_tags()
	{
		global $post;
		$posttags = get_the_tags($post->ID);
		$output = '';
		if ($posttags) {
			foreach ($posttags as $tag) {
				$link = get_term_link( $tag, 'post_tag' );
				if ( is_wp_error( $link ) )
					return $link;
				$output .= '<a href="'.$link.'" title="'. sprintf(__('View all posts on %s', 'trending-family'), $tag->name) .'">' . $tag->name . '</a>';

			}
			return $output;
		}
		return false;
	}
}

if(!function_exists('trending_family_get_col_value')) {

	/**
	 * @param $i
	 *
	 * @return int|string
	 *
	 * Get the column value
	 */
	function trending_family_get_col_value( $i ) {

		$cols = 0;

		switch ( $i ) {
			case 1:
				$cols = 'col-md-12 col-sm-12 col-xs-12';
				break;
			case 2:
				$cols = 'col-md-6 col-sm-6 col-xs-12';
				break;
			case 3:
				$cols = 'col-md-4 col-sm-4 col-xs-12';
				break;
			case 4:
				$cols = 'col-md-3 col-sm-6 col-xs-12';
				break;
			case 6:
				$cols = 'col-md-2 col-sm-6 col-xs-12';
				break;
			default:
				$cols = 'col-md-3 col-sm-6 col-xs-12';
		}

		return $cols;
	}
}


if(!function_exists('trending_family_truncate')) {

	function trending_family_truncate($string, $length = 200)
	{
		$string = trim($string);

		if (strlen($string) > $length) {
			$string = wordwrap($string, $length);
			$string = explode("\n", $string, 2);
			$string = $string[0];
		}

		return $string;
	}
}

if(!function_exists('trending_family_show_posts_nav')){
	/**
	 * If more than one page exists, return TRUE.
	 */
	function trending_family_show_posts_nav() {
		global $wp_query;
		return ($wp_query->max_num_pages > 1);
	}
}


if(!function_exists('trending_family_pagination')) {
	/**
	 * @param string $pages
	 * @param int $range
	 */
	function trending_family_pagination($pages = '', $range = 4)
	{
		$showitems = ($range * 2) + 1;
		global $paged;
		if (empty($paged)) {
			$paged = 1;
		}
		if ($pages == '') {
			global $wp_query;
			$pages = $wp_query->max_num_pages;
			if (!$pages) {
				$pages = 1;
			}
		}

		if (1 != $pages) {
			echo "<nav class='text-right'><ul class='pagination'>";
			if ($paged > 1 && $showitems < $pages) echo "<li><a href='" . get_pagenum_link($paged - 1) . "' aria-label='Previous'><span aria-hidden='true'><i class='fa fa-angle-left'></i></span></a></li>";

			for ($i = 1; $i <= $pages; $i++) {
				if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
					echo ($paged == $i) ? "<li class='active'><a>" . $i . "</a></li>" : "<li><a href='" . get_pagenum_link($i) . "'>" . esc_html($i) . "</a></li>";
				}
			}

			if ($paged < $pages && $showitems < $pages) echo "<li><a href='" . get_pagenum_link($paged + 1) . "' aria-label='Next'><span aria-hidden='true'><i class='fa fa-angle-right'></i></span></a></li>";
			echo "</nav></ul>";
		}
	}
}

// @todo Get the API KEY and change the API KEY
if(!function_exists('trending_family_youtube_channel_subscriber_count') ){
	function trending_family_youtube_channel_subscriber_count( $request = 'subscriberCount', $channel = 0 ) {
		$params  = array( 'sslverify' => false, 'timeout' => 60 );
		$yt_data = wp_remote_get( 'https://www.googleapis.com/youtube/v3/channels?part=statistics&id='.$channel. '&key=YOUR_API_KEY', $params );
		if ( is_wp_error( $yt_data ) || '400' <= $yt_data['response']['code'] ) {
			echo 'Something went wrong';
		} else {
			$response = json_decode( $yt_data['body'], true );

			echo $count = intval( $response['items'][0]['statistics'][$request] );

		}
	}
}

if(!function_exists( 'trending_family_get_error_message' )) {
	/**
	 * Function to get the error message
	 *
	 * @param $code
	 *
	 * @return string|void
	 */
	function trending_family_get_error_message( $code ) {
		switch ( $code ) {
			case 'expiredkey':
			case 'invalidkey':
				return __( 'The password reset link you used is not valid anymore.', 'trending-family' );

			case 'password_reset_mismatch':
				return __( "The two passwords you entered don't match.", 'trending-family' );

			case 'password_reset_empty':
				return __( "Sorry, we don't accept empty passwords.", 'trending-family' );

			case 'empty_username':
				return __( 'You need to enter your email address to continue.', 'trending-family' );

			case 'invalid_email':
			case 'invalidcombo':
				return __( 'There are no users registered with this email address.', 'trending-family' );

		}
	}
}

if(!function_exists( 'trending_family_get_facebook_follower_count' )) {
	/*
	 * Get the Follower Count from Facebook Page
	 */
	function trending_family_get_facebook_follower_count()
	{
		$json                   = file_get_contents( 'https://graph.facebook.com/__ID__/insights/page_fans/lifetime?access_token=__TOKEN__' );
		$obj                    = json_decode( $json );
		$new_facebook_followers = $obj->data[0]->values[0]->value;

		return $new_facebook_followers;
	}
}
// @todo Testing
if(!function_exists('trending_family_get_twitter_followers')) {
	/*
	 * Get number of Twitter Followers
	 */
	function trending_family_get_twitter_followers( $screenName = 'wpbeginner' ) {
		// some variables
		$consumerKey    = 'YIQPxqfqQto5yaskourlA';
		$consumerSecret = 'OH3xiYM4oN3mjGK3as9m37zkKeyiHgKhBIgiNhoM';
		$token          = get_option( 'cfTwitterToken' );

		// get follower count from cache
		$numberOfFollowers = get_transient( 'cfTwitterFollowers' );

		// cache version does not exist or expired
		if ( false === $numberOfFollowers ) {
			// getting new auth bearer only if we don't have one
			if ( ! $token ) {
				// preparing credentials
				$credentials = $consumerKey . ':' . $consumerSecret;
				$toSend      = base64_encode( $credentials );

				// http post arguments
				$args = array(
					'method'      => 'POST',
					'httpversion' => '1.1',
					'blocking'    => true,
					'headers'     => array(
						'Authorization' => 'Basic ' . $toSend,
						'Content-Type'  => 'application/x-www-form-urlencoded;charset=UTF-8'
					),
					'body'        => array( 'grant_type' => 'client_credentials' )
				);

				add_filter( 'https_ssl_verify', '__return_false' );
				$response = wp_remote_post( 'https://api.twitter.com/oauth2/token', $args );

				$keys = json_decode( wp_remote_retrieve_body( $response ) );

				if ( $keys ) {
					// saving token to wp_options table
					update_option( 'cfTwitterToken', $keys->access_token );
					$token = $keys->access_token;
				}
			}
			// we have bearer token wether we obtained it from API or from options
			$args = array(
				'httpversion' => '1.1',
				'blocking'    => true,
				'headers'     => array(
					'Authorization' => "Bearer $token"
				)
			);

			add_filter( 'https_ssl_verify', '__return_false' );
			$api_url  = "https://api.twitter.com/1.1/users/show.json?screen_name=$screenName";
			$response = wp_remote_get( $api_url, $args );

			if ( ! is_wp_error( $response ) ) {
				$followers         = json_decode( wp_remote_retrieve_body( $response ) );
				$numberOfFollowers = $followers->followers_count;
			} else {
				// get old value and break
				$numberOfFollowers = get_option( 'cfNumberOfFollowers' );
				// uncomment below to debug
				//die($response->get_error_message());
			}

			// cache for an hour
			set_transient( 'tf_twitterfollowers', $numberOfFollowers, 1 * 60 * 60 );
//			update_option( 'cfNumberOfFollowers', $numberOfFollowers );
		}

		return $numberOfFollowers;
	}
}
// @todo To do Real World Testing and Set transients
if(!function_exists( 'trending_family_get_pinterest_follower_count')){
	/*
	 * Get the number of Pinterest Followers
	 */
	function trending_family_get_pinterest_follower_count()
	{
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, 'http://pinterestapi.co.uk/"""yourname"""/likes');
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 0);
		$json = curl_exec($ch);
		curl_close($ch);
		$count = json_decode($json, true);
		$count = $count['meta']['count'];
		return $count;
	}
}

if (!function_exists('curl_reset'))
{
	function curl_reset(&$ch)
	{
		$ch = curl_init();
	}
}

/*
 * Return the appropriate image for Platforms in Case Study Page
 */
if( !function_exists( 'trending_family_get_platform_image')){
	function trending_family_get_platform_image( $platform )
	{
		$image = '';
		switch( $platform ){
			case 'Youtube':
				$image = 'mlogo-1.png';
				break;
			case 'Facebook':
				$image = 'mlogo-2.png';
				break;
			case 'Instagram':
				$image = 'mlogo-3.png';
				break;
			case 'Pinterest':
				$image = 'mlogo-4.png';
				break;
			case 'Twitter':
				$image = 'mlogo-5.png';
				break;
			case 'Snapchat':
				$image = 'mlogo-6.png';
				break;
			default:
				$image = '';
				break;
		}
		return get_template_directory_uri() . '/assets/images/' . $image;

	}
}
/*
 * Return the label in nav bar for single camapaign page
 */
if(!function_exists('trending_family_get_nav_campaign_label')){
	
	
	/**
	 * @param $key
	 * @param $media
	 * @param string $return
	 *
	 * @return string
	 */
	function trending_family_get_nav_campaign_label( $key, $media, $return = 'associated_tab'  ){
		$associated_tab = '';
		$platform_name = '';
		switch ($key){
			case 'Facebook':
				$platform_name = 'Facebook['.$media['pagename'].']';
				$associated_tab = 'Facebook_'.$media['pageid'];
				break;
			case 'Youtube':
				$platform_name = 'Youtube['.$media['youtube_title'].']';
				$associated_tab = 'Youtube_'.$media['channel_id'];
				break;
			case 'Twitter':
				$platform_name = 'Twitter['.$media['screen_name'].']';
				$associated_tab = 'Twitter_'.$media['id'];
				break;
			case 'Instagram':
				$platform_name = 'Instagram['.$media['username'].']';
				$associated_tab = 'Instagram_'.$media['id'];
			
			case 'Pinterest':
				$platform_name = 'Pinterest['.$media['username'].']';
				$associated_tab = 'Pinterest_'.$media['id'];
				break;
			
		}
		if( $return == 'platform'){
			return $platform_name;
		}
		return $associated_tab;
	}
	
}

if(!function_exists('trending_family_has_next') ) {
	/*
	 * Check if the array has next or not
	 * @param $array
	 * @return bool
	 */
	function trending_family_has_next( $array ) {
		if ( is_array( $array ) ) {
			if ( next( $array ) === FALSE ) {
				return FALSE;
			} else {
				return TRUE;
			}
		} else {
			return FALSE;
		}
	}
}


if(!function_exists('trending_family_influencer_nice_name')){
	/**
	 *
	 * Return Nice Name for WP User
	 * @param $influencer
	 *
	 * @return string
	 */
	function trending_family_influencer_nice_name($influencer)
	{
		$first_name = get_user_meta($influencer['ID'], 'influencer_influencer_first_name');
		$last_name = get_user_meta($influencer['ID'], 'influencer_influencer_last_name');
		
		if( !empty($first_name)){
			$nicename = $first_name . ' ' . $last_name;
		} else {
			$nicename = ! empty( $influencer['nickname'] ) ? $influencer['nickname'] : $influencer['user_firstname'] . ' ' . $influencer['user_lastname'];
		}
		return $nicename;
	}
}