<?php
	if ( ! defined( 'ABSPATH' ) ) exit;
	
	if( !function_exists( 'trending_family_hide_editor')) {
		/**
		 * Hide editor on specific pages.
		 * @return void
		 */
		function trending_family_hide_editor()
		{
			$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];
			if ( ! isset( $post_id ) ) {
				return;
			}
			$pageTemplate = get_the_title( $post_id );
			if ( $pageTemplate == 'Home Page' ) {
				remove_post_type_support( 'page', 'editor' );
			}
			if( $pageTemplate == 'Client Landing Page') {
				remove_post_type_support( 'page', 'editor' );
			}
			if( $pageTemplate == 'Influencer Landing Page') {
				remove_post_type_support( 'page', 'editor' );
			}
			if( $pageTemplate == 'Lost Password') {
				remove_post_type_support( 'page', 'editor' );
			}
			if( $pageTemplate == 'Reset Password') {
				remove_post_type_support( 'page', 'editor' );
			}
			$template_file = get_post_meta( $post_id, '_wp_page_template', true );
			if ( $template_file == 'templates/page-home.php' ) {
				remove_post_type_support( 'page', 'editor' );
			}
			if ( $template_file == 'templates/page-case-study.php'  ) {
				remove_post_type_support( 'page', 'editor' );
			}
			if ( $template_file == 'templates/page-influencer-landing.php'  ) {
				remove_post_type_support( 'page', 'editor' );
			}
			if ( $template_file == 'templates/page-lost.php'  ) {
				remove_post_type_support( 'page', 'editor' );
			}
			if ( $template_file == 'templates/page-reset.php'  ) {
				remove_post_type_support( 'page', 'editor' );
			}
		}
		add_action( 'admin_init', 'trending_family_hide_editor' );
	}
// Add Theme Options
	if( function_exists('acf_add_options_page') ) {
		acf_add_options_page(array(
			'page_title' 	=> 'Trending Family Settings',
			'menu_title'	=> 'Trending Family Settings',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}
	if(!function_exists('trending_family_ajax_login_init')){
		/**
		 * @param none
		 * @return void
		 */
		function trending_family_ajax_login_init()
		{
			if( is_user_logged_in()){
				return;
			}
			add_action( 'wp_ajax_nopriv_ajaxlogin', 'trending_family_ajax_login' );
		}
		add_action('init', 'trending_family_ajax_login_init');
	}
	if(!function_exists( 'trending_family_ajax_login' )) {
		/**
		 * Form Action for Login Form
		 */
		function trending_family_ajax_login() {
			if ( empty( $_POST['username'] ) ) {
				echo json_encode( array(
					'loggedin' => false,
					'message'  => __( 'The email/username doesn\'t exist.', 'trending-family' )
				) );
			} else {
				check_ajax_referer( 'trending_family_login_nonce', 'security' );
				$info                  = array();
				$info['user_login']    = $_POST['username'];
				$info['user_password'] = $_POST['password'];
				$info['remember']      = true;
				$user_signon = wp_signon( $info, false );
				if ( is_wp_error( $user_signon ) ) {
					echo json_encode( array( 'loggedin' => false, 'message' => $user_signon->get_error_message() ) );
				} else {
					$redirectUrl = trending_family_get_redirect_url( $user_signon );
					if ( ! $redirectUrl ) {
						$redirectUrl = home_url( '/' );
					}
					echo json_encode( array(
						'loggedin'    => true,
						'message'     => __( 'Login successful', 'trending-family' ),
						'redirectUrl' => $redirectUrl
					) );
				}
			}
			die();
		}
	}
	if(!function_exists( 'trending_family_get_redirect_url' )) {
		/**
		 * Return the redirect url based on the user role
		 *
		 * @param $user
		 *
		 * @return string|void
		 */
		function trending_family_get_redirect_url( $user ) {
			if ( in_array( 'influencer', $user->roles ) ) {
				$redirectUrl = home_url( '/influencers/' );
			}
			if ( in_array( 'client', $user->roles ) ) {
				$redirectUrl = home_url( '/clients/' );
			}
			if ( in_array( 'tf-admin', $user->roles ) ) {
				$redirectUrl = home_url( '/admins/' );
			}
			if ( in_array( 'administrator', $user->roles ) ) {
				$redirectUrl = admin_url();
			}
			return $redirectUrl;
		}
	}
	if( !function_exists( 'trending_family_reset_pass_callback' )) {
		/*
		 *	@desc	Process reset password
		 */
		function trending_family_reset_pass_callback() {
			$errors = new WP_Error();
			$nonce  = $_POST['nonce'];
			if ( ! wp_verify_nonce( $nonce, 'rs_user_reset_password_action' ) ) {
				die ( 'Security checked!' );
			}
			$pass1 = $_POST['pass1'];
			$pass2 = $_POST['pass2'];
			$key   = $_POST['user_key'];
			$login = $_POST['user_login'];
			$user = check_password_reset_key( $key, $login );
			// check to see if user added some string
			if ( empty( $pass1 ) || empty( $pass2 ) ) {
				$errors->add( 'password_required', __( 'Password is required field', 'trending-family' ) );
			}
			// is pass1 and pass2 match?
			if ( isset( $pass1 ) && $pass1 != $pass2 ) {
				$errors->add( 'password_reset_mismatch', __( 'The passwords do not match.', 'trending-family' ) );
			}
			/**
			 * Fires before the password reset procedure is validated.
			 *
			 * @since 3.5.0
			 *
			 * @param object $errors WP Error object.
			 * @param WP_User|WP_Error $user WP_User object if the login and reset key match. WP_Error object otherwise.
			 */
			do_action( 'validate_password_reset', $errors, $user );
			if ( ( ! $errors->get_error_code() ) && isset( $pass1 ) && ! empty( $pass1 ) ) {
				reset_password( $user, $pass1 );
				$errors->add( 'password_reset', __( 'Your password has been reset.', 'trending-family' ) );
			}
			// display error message
			if ( $errors->get_error_code() )
				echo '<p class="error">' . $errors->get_error_message( $errors->get_error_code() ) .'</p>';
			// return proper result
			die();
		}
		add_action( 'wp_ajax_nopriv_reset_pass', 'trending_family_reset_pass_callback' );
		add_action( 'wp_ajax_reset_pass', 'trending_family_reset_pass_callback' );
	}
	if( !function_exists('trending_family_redirect_to_lost_password' ) ) {
		/*
		 *
		 * @param  array   $attributes  Shortcode attributes.
		 * @param  string  $content     The text content for shortcode. Not used.
		 *
		 * @return string  The shortcode output
		 */
		function trending_family_redirect_to_lost_password( $attributes, $content = null )
		{
			if (is_user_logged_in()) {
				wp_redirect(home_url( '/lost-password?login=already' ));
			} else {
				wp_redirect(home_url( '/lost-password' ));
			}
		}
		add_action('lost_password', 'trending_family_redirect_to_lost_password');
	}
	if(!function_exists('trending_family_redirect_to_custom_reset')) {
		/*
		* Redirect to custom page for resetting password
		*/
		function trending_family_redirect_to_custom_reset()
		{
			if ('GET' == $_SERVER['REQUEST_METHOD']) {
				// Verify key / login combo
				$user = check_password_reset_key($_REQUEST['key'], $_REQUEST['login']);
				if (!$user || is_wp_error($user)) {
					if ($user && $user->get_error_code() === 'expired_key') {
						wp_redirect(home_url('reset-password?login=expiredkey'));
					} else {
						wp_redirect(home_url('reset-password?login=invalidkey'));
					}
					exit;
				}
				$redirect_url = home_url('/reset-password');
				$redirect_url = add_query_arg('login', esc_attr($_REQUEST['login']), $redirect_url);
				$redirect_url = add_query_arg('key', esc_attr($_REQUEST['key']), $redirect_url);
				wp_redirect($redirect_url);
				exit;
			}
		}
		add_action( 'login_form_rp', 'trending_family_redirect_to_custom_reset' );
		add_action( 'login_form_resetpass', 'trending_family_redirect_to_custom_reset' );
	}
	if(!function_exists('trending_family_replace_retrieved_password_message')){
		/*
		 * Replace the content for Password Reset Notice
		 * @param $message
		 * @param $key
		 * @param $user_login
		 * @param $user_data
		 * @return mixed $msg
		 */
		function trending_family_replace_retrieved_password_message( $message, $key, $user_login, $user_data )
		{
			$msg = '<html><head>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                            <meta name="viewport" content="width=device-width">
                            <title>Password Reset Notice</title>
	                        <style>
                                body{
                                    margin: 0;
                                    padding: 0;
                                }
                                h1 { border-bottom: 3px solid #000000; margin-bottom: 0; padding-bottom: 0; }
                                h2 { padding-top: 0; margin-top: 0; }
                                td { padding: 5px; }
                                .logo img {
                                    max-width: 200px;
                                    width: 100%;
                                    display: block;
                                    margin: auto;
                                }
                                table {
                                    width: 100%;
                                    display:block;
                                    padding: 15px 15px 30px 15px;
                                    max-width: 480px;
                                    margin:auto; display: block;
                                    background-color: #4ABEA8;
                                }
                                thead {
                                    display: inline-block;
                                    width: 100%;
                                    height: 20px;
                                }
	                            tbody {
                                    display: inline-block;
                                    width: 100%;
                                    overflow: auto;
                                }
	                            td {
                                    padding: 10px 0;
                                    width: 50%;
                                    color: #fff;
                                }
                                tr.alt {
                                    width: 100%;
                                    display: table;
                                    border-bottom: 1px dotted #dddddd;
                                }
                                white {
                                    color: white;
                                }
	                        </style>
                        </head>
                        <body>
                            <div class="logo" style="margin-bottom: 60px;">
                                <img src="http://websatil.com/project-test/trending-family/wp-content/uploads/2017/03/logo.png">
                            </div>
                            <div class="email-template-form" style="max-width: 768px;margin: auto;display: block; text-align:center">
                                <h3>Password Reset Link: </h3>
                                <p>You asked us to reset your password for your account with user login <b>'. $user_login.'.</b></p>
                                <p>If this was a mistake, or you didn\'t ask for a password reset, just ignore this email and nothing will happen.</p>
                                <p>To reset your password, <a class="white" href="'. site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login )).'">Click Here.</a></p>
                            </div>
                        </body>
                        </html>';
			return $msg;
		}
		add_filter( 'retrieve_password_message', 'trending_family_replace_retrieved_password_message', 10, 4 );
	}
	if(!function_exists('trending_family_replace_retrieved_password_title')){
		/**
		 * Replace the subject for Passoword Reset Email
		 * @param $old_subject
		 * @return mixed $subject
		 */
		function trending_family_replace_retrieved_password_title( $old_subject )
		{
			$subject = esc_html__('Trending Family: Password Reset', 'trending-family');
			return $subject;
		}
		add_filter('retrieve_password_title', 'trending_family_replace_retrieved_password_title');
	}
	if(!function_exists('trending_family_do_password_lost')) {
		/**
		 * Redirect to lost-password page.
		 */
		function trending_family_do_password_lost()
		{
			if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
				$errors = retrieve_password();
				$redirect_url = home_url( '/lost-password' );
				if ( is_wp_error( $errors ) ) {
					// Errors found
					$redirect_url = add_query_arg( 'error', join( ',', $errors->get_error_codes() ), $redirect_url );
				} else {
					$redirect_url = add_query_arg( 'checkemail', 'confirm', $redirect_url );
				}
				wp_redirect( $redirect_url );
				exit;
			}
		}
		add_action('login_form_lostpassword', 'trending_family_do_password_lost');
	}
	if(!function_exists('trending_family_do_password_reset')) {
		/**
		 * Resets the user's password if the password reset form was submitted.
		 */
		function trending_family_do_password_reset()
		{
			if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
				$rp_key   = $_REQUEST['rp_key'];
				$rp_login = $_REQUEST['rp_login'];
				$user = check_password_reset_key( $rp_key, $rp_login );
				if ( ! $user || is_wp_error( $user ) ) {
					if ( $user && $user->get_error_code() === 'expired_key' ) {
						wp_redirect( home_url( '/reset-password?login=expiredkey' ) );
					} else {
						wp_redirect( home_url( '/reset-password?login=invalidkey' ) );
					}
					exit;
				}
				if ( isset( $_POST['pass1'] ) ) {
					if ( $_POST['pass1'] != $_POST['pass2'] ) {
						// Passwords don't match
						$redirect_url = home_url( 'reset-password' );
						$redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
						$redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
						$redirect_url = add_query_arg( 'error', 'password_reset_mismatch', $redirect_url );
						wp_redirect( $redirect_url );
						exit;
					}
					if ( empty( $_POST['pass1'] ) ) {
						// Password is empty
						$redirect_url = home_url( 'reset-password' );
						$redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
						$redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
						$redirect_url = add_query_arg( 'error', 'password_reset_empty', $redirect_url );
						wp_redirect( $redirect_url );
						exit;
					}
					// Parameter checks OK, reset password
					reset_password( $user, $_POST['pass1'] );
					wp_redirect( home_url( '/reset-password?password=changed' ) );
				} else {
					echo "Invalid request.";
				}
				exit;
			}
		}
		add_action( 'login_form_rp', 'trending_family_do_password_reset' );
		add_action( 'login_form_resetpass', 'trending_family_do_password_reset' );
	}
	if(!function_exists('trending_family_header_custom_snippet')) {
		/*
		 * Inserts user snippets above </head> tag
		 */
		function trending_family_header_custom_snippet()
		{
			$header_custom_snippet = get_field( 'trending_family_header_snippets', 'option' );
			if( empty( $header_custom_snippet )) return;
			echo $header_custom_snippet;
		}
		add_action('wp_head', 'trending_family_header_custom_snippet');
	}
	if(!function_exists('trending_family_footer_custom_snippet')) {
		/*
		 * Inserts user snippets above </body> tag
		 */
		function trending_family_footer_custom_snippet()
		{
			$footer_custom_snippet = get_field( 'trending_family_footer_snippets', 'option' );
			if( empty( $footer_custom_snippet )) return;
			echo $footer_custom_snippet;
		}
		add_action('wp_footer', 'trending_family_footer_custom_snippet');
	}
	if(!function_exists('delete_face_page')) {
		/*
		 * @author Mesrop
		 * Deletes Social Media Accounts in Influencer Panel
		 */
		function delete_face_page()
		{
			if ( $_POST['action'] && $_POST["action"] == "delete_face_page" ) {
				$nonce  = $_POST['nonce'];
				if ( ! $nonce  || ! wp_verify_nonce( $nonce, 'trending_family_nonce' )  ) {
					die( 'Security Check');
				}
				
				
				$metas = get_user_meta( $_POST['user'], $_POST['key'] )[0];
				foreach ( $metas as $key => $meta ) {
					if ( trim( $_POST['id'] ) == trim( $meta ) ) {
						unset( $metas[ $key ] );
						if ( $metas ) {
							update_user_meta( $_POST['user'], $_POST['key'], $metas );
						} else {
							delete_user_meta( $_POST['user'], $_POST['key'] );
						}
					}
				}
				exit;
			}
		}
		add_action( 'wp_ajax_delete_face_page', 'delete_face_page' );
	}
	if( !function_exists('trending_family_delete_blog')){
		function trending_family_delete_blog()
		{
			if ( $_POST['action'] && $_POST["action"] == "delete_influencers_blog" ) {
				$nonce = $_POST['nonce'];
				if ( ! $nonce || ! wp_verify_nonce( $nonce, 'trending_family_nonce' ) ) {
					echo json_encode( 'Security Check' );
					exit;
				}
				$metas = get_user_meta( $_POST['user'], $_POST['key'] , true);
				unset( $metas[ $_POST['id'] ] );
				if ( $metas ) {
					update_user_meta( $_POST['user'], $_POST['key'], $metas );
				} else {
					delete_user_meta( $_POST['user'], $_POST['key'] );
				}
				exit;
			}
		}
		add_action( 'wp_ajax_delete_influencers_blog', 'trending_family_delete_blog' );
	}
	if(!function_exists('trending_family_facebookpage')) {
		/*
		 * Add Facebook Page for Influencer.
		 */
		function trending_family_facebookpage()
		{
			try {
				$facebookService = new SocialMedia\SocialAccounts\Facebook(wp_get_current_user());
				
				// re-map input
				$pageIds = [];
				if( isset($_POST['facebook_data']) && $_POST['facebook_data']) {
					foreach($_POST['facebook_data'] as $item) {
						$pageIds[] = $item['value'];
					}
				}
				
				$facebookService->connectPages($pageIds);
				$result = [
					'status' => 1,
					'msg' => 'Page(s) successfully connected.',
				];
			}
			
			catch (Exception $e) {
				$result = [
					'status' => 0,
					'msg' => 'There was an error connecting your pages.',
				];
			}
			
			echo json_encode($result);
			exit(0);
			
			// Old code
// 		global $wpdb;
// 		$tbl_facebook = $wpdb->prefix.'facebook_data';
// 		$tbl_facebook_data = $wpdb->prefix.'facebook_get_data';
// 		$current_user     = wp_get_current_user();
// 		$fb_user_id       = $_POST['fb_user_id'];
// 		$data_token = $_POST['data_token'];
// 		$fb_user_pick_url = $_POST['fb_pick'];
// 		$mydata           = $_POST['page_id'];
// 		$pagename         = $_POST['fbpagename'];
// 		$fan_count        = $_POST['fan_count'];
// 		$data_gender_male = $_POST['date-gender-male'];
// 		$data_gender_female = $_POST['date-gender-female'];
// 		$alldata = $_POST['facebook_data'];
// 		foreach ( $alldata as $key => $value ) {
// 			foreach ( $value as $key1 => $value1 ) {
// 				//$value1['dateModify'] = date('Y-m-d');
// 				$arrayName[] = $value1;
// 				//print_r($value1);
// 				$fbPageId = $value1['pageid'];
// 				$queryFacebookData = "SELECT * from ".$tbl_facebook_data." where `page_id` = '".$fbPageId."' && `access_token` = '".$data_token."'";
// 				//die();
// 				$resultsFBData = $wpdb->get_results($queryFacebookData);
// 				if (!empty($resultsFBData)) {
// 					//update
// 					$queryUpdate = "UPDATE ".$tbl_facebook_data." SET `access_token` = '". $data_token ."' where `page_id` = ".$fbPageId."";
// 					$wpdb->query($queryUpdate);
// 				}else{
// 					//insert
// 					$queryInsert = "INSERT into ".$tbl_facebook_data." (`user_id`,`page_id`,`access_token`,`date`) VALUES ('".$current_user->ID."','".$fbPageId."','".$data_token."','".date('Y-m-d H:i:s')."')";
// 					$wpdb->query($queryInsert);
// 				}
// 				/*$fbPageMale = $value1['data-gender-male'];
// 				$fbPageFemale = $value1['data-gender-female'];
// 				$dateModify = date('Y-m');
// 				$queryFacebookData = "SELECT * from ".$tbl_facebook." where date_format(`date`, '%Y-%m') = '".$dateModify."' && `page_id` = '".$fbPageId."'";
// 				//die();
// 				$resultsFBData = $wpdb->get_results($queryFacebookData);
// 				if (!empty($resultsFBData)) {
// 					//update
// 					$queryUpdate = "UPDATE ".$tbl_facebook." SET `male` = ".$fbPageMale.", `female` = ".$fbPageFemale.", `date` = '".date('Y-m-d H:i:s')."' where `page_id` = ".$fbPageId."";
// 					$wpdb->query($queryUpdate);
// 				}else{
// 					//insert
// 					$queryInsert = "INSERT into ".$tbl_facebook." (`user_id`,`page_id`,`male`,`female`,`date`) VALUES ('".$current_user->ID."','".$fbPageId."','".$fbPageMale."','".$fbPageFemale."','".date('Y-m-d H:i:s')."')";
// 					$wpdb->query($queryInsert);
// 				}*/
// 			}
// 		}
// 		update_user_meta( $current_user->ID, 'facebook_data', $arrayName );
// 		echo "Ok";
		}
		add_action( 'wp_ajax_facebookpage', 'trending_family_facebookpage' );
	}
	if(!function_exists('trending_family_del_facebook')) {
		/*
		 * Remove Facebook Account for Influencer
		 */
		function trending_family_del_facebook()
		{
			global $wpdb;
			
			try {
				$service = new SocialMedia\SocialAccounts\Facebook(wp_get_current_user());
				$service->disconnectPage($_POST['fb_disconnect_id']);
				die('ok');
			}
			
			catch (Exception $e) {
				die();
			}
			
			$fb_disconnect_id = $_POST['fb_disconnect_id'];
			$slected_fb_page = get_user_meta( $current_user->ID, 'facebook_data', true );
			foreach ( $slected_fb_page as $allvalue ) {
				if ( $fb_disconnect_id == $allvalue['pageid'] ) {
				} else {
					# code...
					$arrayName[] = $allvalue;
				}
			}
			update_user_meta( $current_user->ID, 'facebook_data', $arrayName );
			echo "Ok";
			die();
		}
		add_action( 'wp_ajax_del_facebook', 'trending_family_del_facebook' );
	}
	if(!function_exists('trending_family_delete_insta_page')) {
		/*
		 * Disconnect Instagram
		 */
		function trending_family_delete_insta_page()
		{
			$insta_id     = $_POST['id'];
			$current_user = wp_get_current_user();
			$get_pre_data = get_user_meta( $current_user->ID, 'instagram', true );
			if ( $get_pre_data != '' ) {
				foreach ( $get_pre_data as $key => $val ) {
					if ( in_array( $insta_id, $val ) ) {
						unset( $get_pre_data[ $key ] );
					}
				}
				$myarray = array_values( $get_pre_data );
				update_user_meta( $current_user->ID, 'instagram', $myarray );
				echo 'ok';
			}
			die();
		}
		add_action( 'wp_ajax_delete_insta_page', 'trending_family_delete_insta_page' );
	}
	if(!function_exists('trending_family_delete_youtube_channel')) {
		/*	 * Disconnect Youtube channel	 */
		function trending_family_delete_youtube_channel()	{
			$channel_id     = $_POST['id'];
			$current_user = wp_get_current_user();
			
			
			try {
				$service = new SocialMedia\SocialAccounts\Youtube(wp_get_current_user());
				$service->deleteChannel($channel_id);
				die('ok');
			}
			
			catch (Exception $e) {
				die();
			}
			
			$get_pre_data = get_user_meta( $current_user->ID, 'youtube-channels', true );
			if ( $get_pre_data != '' ) {
				foreach ( $get_pre_data as $key => $val ) {
					if ( in_array( $channel_id, $val ) ) {
						unset( $get_pre_data[ $key ] );
					}
				}
				$myarray = array_values( $get_pre_data );
				update_user_meta( $current_user->ID, 'youtube-channels', $myarray );
				echo 'ok';
			}
			die();
		}
		add_action( 'wp_ajax_delete_youtube_channel', 'trending_family_delete_youtube_channel' );
	}
	if(!function_exists('trending_family_delete_twitter_page')) {
		/*	 * Disconnect twitter */
		function trending_family_delete_twitter_page()	{
			$channel_id     = $_POST['id'];
			$current_user = wp_get_current_user();
			$get_pre_data = get_user_meta( $current_user->ID, 'twitter_account', true );
			if ( $get_pre_data != '' ) {
				foreach ( $get_pre_data as $key => $val ) {
					if ( in_array( $channel_id, $val ) ) {
						unset( $get_pre_data[ $key ] );
					}
				}
				$myarray = array_values( $get_pre_data );
				update_user_meta( $current_user->ID, 'twitter_account', $myarray );
				echo 'ok';
			}
			die();
		}
		add_action( 'wp_ajax_delete_twitter_page', 'trending_family_delete_twitter_page' );
	}
	if(!function_exists('trending_family_delete_pin_page')) {
		/*
		* Disconnect pinterest
		*/
		function trending_family_delete_pin_page()	{
			$channel_id     = $_POST['id'];
			$current_user = wp_get_current_user();
			$get_pre_data = get_user_meta( $current_user->ID, 'pint_accounts', true );
			if ( $get_pre_data != '' ) {
				foreach ( $get_pre_data as $key => $val ) {
					if ( in_array( $channel_id, $val ) ) {
						unset( $get_pre_data[ $key ] );
					}
				}
				$myarray = array_values( $get_pre_data );
				update_user_meta( $current_user->ID, 'pint_accounts', $myarray );
				echo 'ok';
			}
			die();
		}
		add_action( 'wp_ajax_delete_pin_page', 'trending_family_delete_pin_page' );
	}