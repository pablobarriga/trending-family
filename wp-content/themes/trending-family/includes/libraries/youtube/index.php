<?php
/**
 * Created by PhpStorm.
 * User: Aro
 * Date: 21.04.2017
 * Time: 15:57
 */

require_once 'vendor/autoload.php';
//echo '<pre>';
$client = new Google_Client();
$client->setApplicationName("My Project");
$client->setDeveloperKey("AIzaSyC1x9R0Wp40vEnNeuIYzdiIpefiZdABE78");
$service = new Google_Service_Books($client);
$youtube = new Google_Service_YouTube($client);

if(isset($_GET['channel']) && $_GET['channel'] != ''){
    $channelId = trim($_GET['channel']);
    $channels = $youtube->channels->listChannels("statistics,snippet", array(
        'id' => $channelId
    ));
    $html = '<ul>';
    foreach ($channels as $item) {

        if(!empty($item) && !empty($item->statistics) && !empty($item->snippet)){
            $html.='<li><span>Title --</span><strong>'.$item->snippet->title.'</strong></li>';
            $html.='<li><span>Comment Count --</span><strong>'.$item->statistics->commentCount.'</strong></li>';
            $html.='<li><span>Hidden Subscriber Count --</span><strong>'.$item->statistics->hiddenSubscriberCount.'</strong></li>';
            $html.='<li><span>Subscriber Count --</span><strong>'.$item->statistics->subscriberCount.'</strong></li>';
            $html.='<li><span>Video Count --</span><strong>'.$item->statistics->videoCount.'</strong></li>';
            $html.='<li><span>View Count --</span><strong>'.$item->statistics->viewCount.'</strong></li>';
        }
    }
    $html.= '</ul>';
     echo $html;
}
?>
<form action="" method="get">
    <input type="text" placeholder="Channel ID" name="channel" />
    <input type="submit" value="Search"  />
</form>
