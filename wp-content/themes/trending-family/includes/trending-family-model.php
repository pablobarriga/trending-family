<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Class TrendingFamilyModel
 * @since v1.0.0
 * @author Prajwal Shrestha
 * @package Trending Family
 */
abstract class TrendingFamilyModel {

	/**
	 * @var string
	 */
	static $primary_key = 'id';

	/**
	 * @return string
	 */
	private static function _table() {
		global $wpdb;
		$tablename = 'influencer_usermeta';
		return $wpdb->prefix . $tablename;
	}

	/**
	 * @param $value
	 * @return string|void
	 */
	private static function _fetch_sql($value ) {
		global $wpdb;
		$sql = sprintf( 'SELECT * FROM %s WHERE %s = %%s', self::_table(), static::$primary_key );
		return $wpdb->prepare( $sql, $value );
	}


	/**
	 * @param $value
	 * @return array|null|object|void
	 */
	static function get($value ) {
		global $wpdb;
		return $wpdb->get_row( self::_fetch_sql( $value ) );
	}

	/**
	 * @param $data
	 */
	static function insert($data ) {
		global $wpdb;
		$wpdb->insert( self::_table(), $data );
	}

	/**
	 * @param $data
	 * @param $where
	 */
	static function update($data, $where ) {
		global $wpdb;
		$wpdb->update( self::_table(), $data, $where );
	}

	/**
	 * @param $value
	 * @return false|int
	 */
	static function delete($value ) {
		global $wpdb;
		$sql = sprintf( 'DELETE FROM %s WHERE %s = %%s', self::_table(), static::$primary_key );
		return $wpdb->query( $wpdb->prepare( $sql, $value ) );
	}

	/**
	 * @return int
	 */
	static function insert_id() {
		global $wpdb;
		return $wpdb->insert_id;
	}

	/**
	 * @param $time
	 * @return false|string
	 */
	static function time_to_date($time ) {
		return gmdate( 'Y-m-d H:i:s', $time );
	}

	/**
	 * @return false|string
	 */
	static function now() {
		return self::time_to_date( time() );
	}

	/**
	 * @param $date
	 * @return false|int
	 */
	static function date_to_time($date ) {
		return strtotime( $date . ' GMT' );
	}

}