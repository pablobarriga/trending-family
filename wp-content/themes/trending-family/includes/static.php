<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/*
 * Files to load all the static files
 * @package Trending Family
 */

if(!function_exists('trending_family_scripts')) {
    /**
     * Enqueue scripts.
     *
     */
    function trending_family_scripts() {
	    wp_enqueue_script( 'mobile-custom', get_template_directory_uri() . '/assets/js/jquery.mobile.custom.min.js', array( 'jquery' ), '1.4.5', true );
	    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), '3.3.7', true );
	    wp_enqueue_script( 'bootsnav', get_template_directory_uri() . '/assets/js/bootsnav.js', array( 'jquery' ), '1.2', true );
	    wp_enqueue_script( 'imagesloaded', '//unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js', array( 'jquery' ), false, true );
	    wp_enqueue_script( 'bootstrap-datepicker', get_template_directory_uri() . '/assets/js/bootstrap-datepicker.min.js', array( 'jquery' ), '1.6.4', true );
	    wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/assets/js/magnific-popup.min.js', array( 'jquery' ), '1.1.0', true );
	    wp_enqueue_script( 'trending-family-owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), '2.2.1', true );
	    wp_enqueue_script( 'trending-family-jquery-waypoint', get_template_directory_uri() . '/assets/js/jquery.waypoints.js', array( 'jquery' ), '4.0.1', true );
	    wp_enqueue_script( 'trending-family-packery', get_template_directory_uri() . '/assets/js/packery.pkgd.min.js', array( 'jquery' ), false, true );
	    wp_enqueue_script( 'trending-family-chart', get_template_directory_uri() . '/assets/js/chart.min.js', array( 'jquery' ), false, true );
	    wp_enqueue_script( 'trending-family-scrollbar', get_template_directory_uri() . '/assets/js/jquery.scrollbar.min.js', array( 'jquery' ), '0.2.11', true );
	    wp_enqueue_script( 'trending-family-countries', get_template_directory_uri() . '/assets/js/countries3.js', array(), '3.0', false );
	    wp_enqueue_script( 'trending-family-form-clone', get_template_directory_uri() . '/assets/js/clone-form-td.js', array( 'jquery' ), '1.0', true );
	    wp_enqueue_script( 'trending-family-mobile-custom', get_template_directory_uri() . '/assets/js/jquery.mobile.custom.min.js', array( 'jquery' ), '1.4.5', true );
	    wp_enqueue_script( 'trending-family-engine', get_template_directory_uri() . '/assets/js/engine.js', array( 'jquery' ), false, true );
	    wp_enqueue_script( 'trending-family-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), '20151215', true );
	    wp_enqueue_script( 'trending-family-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );
	    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		    wp_enqueue_script( 'comment-reply' );
	    }
	    wp_enqueue_script( 'trending-family-inputmask', get_template_directory_uri() . '/assets/js/jquery.inputmask.bundle.js', array( 'jquery' ), '3.3.6-0', true );
	    wp_enqueue_script( 'jquery-validation', get_template_directory_uri() . '/assets/js/jquery-validation.js', array( 'jquery' ), '1.16.0', true );
	    wp_enqueue_script( 'sweet-alert', get_template_directory_uri() . '/assets/js/sweetalert2.js', array( 'jquery' ), '6.6.2', true );
	    wp_register_script( 'trending-family-main', get_template_directory_uri() . '/assets/js/dev.js', array( 'jquery' ), false, true );
	    wp_localize_script( 'trending-family-main', 'homeUrl', array(
		    'homeUrl'     => home_url( '/' ),
		    'ajaxUrl'     => admin_url( 'admin-ajax.php' ),
		    'redirectUrl' => home_url(),
		    'ajaxNonce'   => wp_create_nonce('trending_family_nonce'),
	    ) );
	    wp_enqueue_script( 'trending-family-main' );
	    
    }

    add_action('wp_enqueue_scripts', 'trending_family_scripts');
}

if(!function_exists('trending_family_fonts')){
	/**
	 * @return string
	 */
	function trending_family_fonts()
    {
        $fonts_url = '';

        $overpass = _x( 'on', 'Overpass fonts: on or off', 'trending-family' );

        if ( 'off' !== $overpass ) {
            $font_families = array();

            if ( 'off' !== $overpass ) {
                $font_families[] = 'Overpass:100,100i,200,300,300i,400,400i,600,600i,700,700i,800';
            }

            $query_args = array(
                'family' =>  urlencode(implode( '|', $font_families ))
            );

            $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
        }
        return esc_url_raw( $fonts_url );

    }

}

if(!function_exists('trending_family_styles')){
	/**
	 *
	 */
	function trending_family_styles()
    {
        wp_enqueue_style( 'trending-family-fonts', trending_family_fonts(), array(), null );
	    wp_enqueue_style('jquery-ui', get_template_directory_uri() . '/assets/css/jquery-ui.min.css' );
	    wp_enqueue_style( 'sweet-alert', get_template_directory_uri() . '/assets/css/sweetalert2.css' );
        wp_enqueue_style('trending-family-main', get_template_directory_uri(). '/assets/css/main.css' );
        wp_enqueue_style('trending-family-dev', get_template_directory_uri(). '/assets/css/dev.css' );
		if ( is_admin_bar_showing() ) {
			wp_enqueue_style( 'trending-family-admin-bar-style', get_template_directory_uri(). '/assets/css/header.css');
		}

    }
    add_action('wp_enqueue_scripts', 'trending_family_styles');
}