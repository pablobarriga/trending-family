<?php

/*

 * Configuration for the Social Media APIS

 * @package Trending Family

 */




use Facebook\Facebook;



use Abraham\TwitterOAuth\TwitterOAuth;



use DirkGroenen\Pinterest\Pinterest;



use InstagramScraper\Instagram;



$path = get_template_directory() . '/includes/libraries/';



const AUTOLOAD = '/vendor/autoload.php';



include_once $path . 'youtube'. AUTOLOAD;



include_once $path . 'facbook_sdk'. AUTOLOAD;



include_once $path . 'twitter' . AUTOLOAD;



include_once $path . 'pinterest' . AUTOLOAD;



include_once $path . 'instagram' . AUTOLOAD;



session_start();

$current_user = wp_get_current_user();

// $OAUTH2_CLIENT_ID = '737934223539-j2dr51670ugo84ousn4gctn32mpl3b1n.apps.googleusercontent.com';

$OAUTH2_CLIENT_ID = '435460508606-7k6tkajfcqhbt3837ffl6ulk9i066b00.apps.googleusercontent.com';


// $OAUTH2_CLIENT_SECRET = 'HguM6invfqzrPF1d_ttu-pPi';
$OAUTH2_CLIENT_SECRET = 'prAje7WciT9SSp_NHTKTE8TB';



define('CONSUMER_KEY', 'fiEvbqEm3EwTTbJfhJf0xCdC6');

define('CONSUMER_SECRET', '4xqv6HkxmWsnCEl2PmsATEhSBVC2QUjdgchnj0iQ5GZhjfj4xo');



$client = new Google_Client();



$client->setClientId($OAUTH2_CLIENT_ID);



$client->setClientSecret($OAUTH2_CLIENT_SECRET);



/* $client->setScopes(array('https://www.googleapis.com/auth/youtube', 'https://www.googleapis.com/auth/youtube.force-ssl', 'https://www.googleapis.com/auth/youtubepartner-channel-audit', 'https://www.googleapis.com/auth/youtube', 'https://www.googleapis.com/auth/youtube.readonly', 'https://www.googleapis.com/auth/yt-analytics.readonly', 'https://www.googleapis.com/auth/yt-analytics-monetary.readonly','https://www.googleapis.com/auth/youtubepartner')); */

$client->setScopes(array( 
	'profile',
	'email',
	'https://www.googleapis.com/auth/youtube',
	'https://www.googleapis.com/auth/yt-analytics-monetary.readonly',
	'https://www.googleapis.com/auth/youtube.readonly', 
	'https://www.googleapis.com/auth/yt-analytics.readonly'
));

// /$client->setScopes(array('https://www.googleapis.com/auth/youtube.readonly', 'https://www.googleapis.com/auth/yt-analytics.readonly'));


$redirect = filter_var(home_url('/influencers'), FILTER_SANITIZE_URL);



$client->setRedirectUri($redirect);



$client->setApprovalPrompt('force');



// Define an object that will be used to make all API requests.



$youtube = new Google_Service_YouTube($client);



$youtubeReporting = new Google_Service_YouTubeAnalytics( $client );


$tokenSessionKey = 'token-' . $client->prepareScopes();



if (isset($_GET['code']) && !isset($_GET['type'])) {

	

	if (strval($_SESSION['state']) !== strval($_GET['state'])) {

		

		die('The session state did not match.');

		

	}

	

	$client->authenticate($_GET['code']);

	

	$_SESSION[$tokenSessionKey] = $client->getAccessToken();

	//print_r($_SESSION[$tokenSessionKey]);

	// header('Location: ' . $redirect);

	

}



if (isset($_SESSION[$tokenSessionKey])) {

	

	//$client->setAccessToken($_SESSION[$tokenSessionKey]);

	

}



// Check to ensure that the access token was successfully acquired.



if ($client->getAccessToken() != '') {

	$parttt = 'snippet,contentDetails,statistics';
	$paramstt = array('mine' => true);

	$responsett = $youtube->channels->listChannels(

		$parttt,

		$paramstt

	);

	foreach ($responsett as $youdatatt) {

	    $id = "channel==".$youdatatt['id']."";

	    $metrics = 'viewerPercentage';

	    $optparams = array(
	        'dimensions' => 'ageGroup,gender'
	    );

	    $start_date = date('Y-m-d',strtotime('-1 month'));

	    $end_date = date('Y-m-d');

	    $api_response = $metrics;
	    $api = $youtubeReporting->reports->query($id, $start_date, $end_date, $metrics, $optparams);


	    $url = 'https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId='.$youdatatt['id'].'&key=AIzaSyCNSmeQpgFP_rXktEV0tNBz4OxK7FuVQ0Y&maxResults=10';
		$video_list = json_decode(file_get_contents($url));
		// file_put_contents('file.txt', print_r($video_list,true));
		$videosID = array();
		// $videoTitle = array();
		foreach($video_list->items as $item){
			// $item_1 = (array)$item_;
			$videosID[] = array('videoID' => $item->id->videoId,'Title' => $item->snippet->title);
			// file_put_contents('file.txt', print_r($item,true));
		}

		$audienceRetentionArray = array();
		$geographicsDataArray= array();
		
		foreach ($videosID as $key => $value) {
			

			if (!empty($value['videoID']) && !empty(!empty($value['Title']))) {
				$newVideoId = $value['videoID'];
			    $start_date = date('Y-m-d',strtotime('-5 year'));
			    $start_date2 = date('Y-m-d',strtotime('-1 month'));
			    $end_date = date('Y-m-d');
				$newVideoTitle = $value['Title'];
				$id = "channel==".$youdatatt['id']."";

				/* Geographics Data */

				$metrics1 = 'views';

			    $optparams1 = array(
			        'dimensions' => 'country',
			        'filters'	 => 'video=='.$newVideoId.'',
			        'max-results'	=> 5,
			        'sort'			=> '-views'
			    );

			    // $api_response = $metrics1;
			    $geographicsData = $youtubeReporting->reports->query($id, $start_date, $end_date, $metrics1, $optparams1);
			    $geographicsDataArray[] = array('Title' => $newVideoTitle,'geographicsData' => $geographicsData->rows);

				/* Audience Retention Data */

			    $metrics2 = 'averageViewDuration,views';

			    $optparams2 = array(
			        'dimensions' => 'day',
			        'filters'	 => 'video=='.$newVideoId.''
			    );

			    // $api_response = $metrics1;
			    $audienceRetention = $youtubeReporting->reports->query($id, $start_date2, $end_date, $metrics2, $optparams2);
			    $audienceRetentionArray[] = array('Title' => $newVideoTitle,'audienceRetention' => $audienceRetention->rows);
			}
		}

		// echo "<pre>";
		// print_r($geographicsDataArray);

		$audienceRetentionData = serialize($audienceRetentionArray);
		$videoGeographicsData = serialize($geographicsDataArray);

		// file_put_contents('file.txt', print_r($videosID,true));

	    if (isset($api['rows'])) {

			$genderData = $api['rows'];

			$serialized_array = serialize($genderData);

	    }
	}

	$_SESSION[$tokenSessionKey] = $client->getAccessToken();

	function channelsListMine($service, $part, $params,$serialized_array,$audienceRetentionData,$videoGeographicsData) {

		$params = array_filter($params);

		$response = $service->channels->listChannels(

			$part,

			$params

		);

		global $wpdb;

		$current_user = wp_get_current_user();

		foreach ($response as $youdata) {

			// file_put_contents('file.txt', print_r($youdata, true));

			$channel = $youdata['id'] ;

			$custChannelId[] = $youdata['id'];

			$you_title = $youdata['snippet']['title'] ;

			$thumbnails = $youdata['snippet']['thumbnails']['default']['url'];

			$viewCount  = $youdata['statistics']['viewCount'];

			$subscriberCount  = $youdata['statistics']['subscriberCount'];

			$videoCount  = $youdata['statistics']['videoCount'];

			
			// print_r($serialized_array);
			

			$arrayName = array( 'channel_id' => $channel,

			                    'youtube_title'=>$you_title,

			                    'thumbnails' => $thumbnails,

			                    'viewCount' => $viewCount,

			                    'subscriberCount' => $subscriberCount,

			                    'videoCount' => $videoCount,

			                    'demographics' => $serialized_array,

			                    'audienceRetention'	=>	$audienceRetentionData,

			                    'videoGeographicsData' => $videoGeographicsData );

			$get_pre_data = get_user_meta($current_user->ID, 'youtube-channels', true);

			if($get_pre_data != ''){

				foreach( $get_pre_data as $key => $val ){

					if(in_array($channel, $val)){

						unset($get_pre_data[$key]);

					}

				}

				array_push($get_pre_data,$arrayName);

				$myarray = array_values($get_pre_data);

				update_user_meta($current_user->ID, 'youtube-channels', $myarray);

			}else{

				$ar[] = $arrayName;

				update_user_meta($current_user->ID, 'youtube-channels', $ar);

			}

		}

	}

	

	channelsListMine($youtube, 'snippet,contentDetails,statistics', array('mine' => true),$serialized_array,$audienceRetentionData,$videoGeographicsData);

} elseif ($OAUTH2_CLIENT_ID == 'REPLACE_ME') {

	

	$htmlBody = <<<END



<h3>Client Credentials Required</h3>



<p>



You need to set <code>\$OAUTH2_CLIENT_ID</code> and



<code>\$OAUTH2_CLIENT_ID</code> before proceeding.



<p>



END;



} else {

	

	// If the user hasn't authorized the app, initiate the OAuth flow

	

	$state = mt_rand();

	

	$client->setState($state);

	

	$_SESSION['state'] = $state;

	

	$authUrl = $client->createAuthUrl();

	

	$htmlBody = <<<END



<p>No account has been connected yet</p>



END;



}



$fb = new \Facebook\Facebook([

	

	'app_id' => '1880319085566870',

	

	'app_secret' => '6a3e385b313b4f345968eea4984e3082',

	

	'default_graph_version' => 'v2.9',

	

	'default_access_token' => '1880319085566870|-QlHEFXX0KvN7Hx_zvJP5vMCR3s',



]);



$instagram = new Instagram();



$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);



//temp access token to validate app

$request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => $redirect));



//twitter authorize url

$twitter_auth_url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));



if (isset($_REQUEST['oauth_token']) && isset($_REQUEST['oauth_verifier'])) {

	

	//create a instance with temp. request token to get long lived access token

	$connections = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_REQUEST['oauth_token'], $request_token['oauth_token_secret']);

	

	$access_token = $connections->oauth("oauth/access_token", ["oauth_verifier" => $_REQUEST['oauth_verifier']]);

	

	

	$_SESSION['tw_access_token'] = $access_token;

	

	$access_tokens = $_SESSION['tw_access_token'];

	

	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_tokens['oauth_token'], $access_tokens['oauth_token_secret']);

	$user = $connection->get("account/verify_credentials");

	// echo '<pre>';print_r($user);

	$statuses = $connection->get("statuses/user_timeline", ["count" => $user->statuses_count, "trim_user" => true, "exclude_replies" => true, "include_rts" => false, "user_id" => $user->id, "screen_name" => $user->screen_name]);

	// https://api.twitter.com/1.1/statuses/user_timeline.json?trim_user=true&exclude_replies=true&include_rts=false&user_id=3193798780&screen_name=LalitSuffescom

	
	$mArray = array();
	foreach ($statuses as $key => $value) {
		$created_at = $value->created_at;
		if (date("Y-m") == date("Y-m",strtotime($created_at))) {
			$tweetDate = date("Y-m-d",strtotime($created_at));
			if (array_key_exists($tweetDate, $mArray)) {
				$mArray[$tweetDate] = $mArray[$tweetDate]+1;
			}else{
				$mArray[$tweetDate] = 1;
			}
		}
		// echo date("Y-m",strtotime($created_at))."<br>";
	}
	/*echo "<pre>";
	print_r($mArray);
	die();*/
	$user_meta = array(

		'id' => $user->id,

		'screen_name' => $user->screen_name,

		'followers'  => $user->followers_count,

		'user_timeline'	=>	$mArray

	);

	

	$get_pre_data = get_user_meta($current_user->ID, 'twitter_account', true);

	if($get_pre_data != ''){

		foreach( $get_pre_data as $key => $val ){

			if(in_array($user->id, $val)){

				

				unset($get_pre_data[$key]);

			}

		}

		

		array_push($get_pre_data,$user_meta);

		$myarray = array_values($get_pre_data);

		update_user_meta($current_user->ID, 'twitter_account', $myarray);

	}else{

		$ar[] = $user_meta;

		update_user_meta($current_user->ID, 'twitter_account', $ar);

	}

}



define('PINTEREST_APPLICATION_ID', '4906384145044751277');



define('PINTEREST_APPLICATION_SECRET', 'fb0cab49499ca57209b8cfd8ed5d5bfa0880b2c7c2a414640e806765bb63eb16');



// define('PINTEREST_REDIRECT_URI', 'https://websatil.com/project-test/trending-family/influencers/?type=pin');


define('PINTEREST_REDIRECT_URI', 'https://demo.suffescom.com/trending-family/influencers/?type=pin');



$pin_url = 'https://api.pinterest.com/oauth/?client_id='.PINTEREST_APPLICATION_ID.'&redirect_uri='.PINTEREST_REDIRECT_URI.'&response_type=code&scope=read_public,read_relationships';



if(is_user_logged_in()) {

	

	// Pinterest passes a parameter 'code' in the Redirect Url

	if(isset($_GET['code']) && $_GET['type'] == 'pin') {

		try {

			

			$code = $_GET['code'];

			

			$url = 'https://api.pinterest.com/v1/oauth/token';

			

			$curlPost = 'client_id='. PINTEREST_APPLICATION_ID . '&redirect_uri=' . PINTEREST_REDIRECT_URI . '&client_secret=' . PINTEREST_APPLICATION_SECRET . '&code='. $code . '&grant_type=authorization_code';

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $url);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			curl_setopt($ch, CURLOPT_POST, 1);

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

			curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);

			$data = json_decode(curl_exec($ch), true);

			$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			curl_close($ch);

			$access_token=$data['access_token'];

			

			$pinterest = new Pinterest(PINTEREST_APPLICATION_ID, PINTEREST_APPLICATION_SECRET);

			

			$pinterest->auth->setOAuthToken($access_token);

			$_SESSION['pin_access_token'] = $access_token;

			$access_tokens = $_SESSION['pin_access_token'];

			if(isset($_SESSION['pin_access_token']) && $_SESSION['pin_access_token'] != ""){

				$user_meta = file_get_contents('https://api.pinterest.com/v1/me/?access_token=' . $access_token . '&fields=id,username,first_name,last_name,image');

				$userinfo = @json_decode($user_meta);

				

				$dataaa =$pinterest->users->find( $userinfo->data->username, array(

					'fields' => 'username,first_name,last_name,image[small,large],counts'

				) );

				

				$user_meta =array(

					'id' => $dataaa->id,

					'username' => $dataaa->username,

					'first_name' => $dataaa->first_name,

					'last_name' => $dataaa->last_name,

					'image' => $dataaa->image,

					'counts' => $dataaa->counts

				);

				

				$get_pre_data = get_user_meta($current_user->ID, 'pint_accounts', true);

				if($get_pre_data != ''){

					foreach( $get_pre_data as $key => $val ){

						if(in_array($userinfo->data->id, $val)){

							

							unset($get_pre_data[$key]);

						}

					}

					

					array_push($get_pre_data,$user_meta);

					$myarray = array_values($get_pre_data);

					update_user_meta($current_user->ID, 'pint_accounts', $myarray);

				}else{

					$ar[] = $user_meta;

					update_user_meta($current_user->ID, 'pint_accounts', $ar);

				}

			}

			

		}

		catch(Exception $e) {

			echo $e->getMessage();

			exit;

		}

	}}

if(is_user_logged_in()) {

	

	$access_tokens = $_SESSION['pin_access_token'];

	

	$pinterest = new Pinterest(PINTEREST_APPLICATION_ID, PINTEREST_APPLICATION_SECRET);

	

	$pinterest->auth->setOAuthToken($access_tokens);

	

}



/***********instagram*********/

$insta_client_id = '6f4570aa8e9a4cecb9ba924e0c743a5c';

$insta_client_secret = 'c700ffc90e38493d9aa32c8df95f7e90';

// $insta_redirect_uri = 'https://websatil.com/project-test/trending-family/influencers/?type=insta';

$insta_redirect_uri = 'http://demo.suffescom.com/trending-family/influencers/?type=insta';



$insta_url = "https://api.instagram.com/oauth/authorize?client_id=$insta_client_id&redirect_uri=$insta_redirect_uri&response_type=code";



if(isset($_GET['code']) && $_GET['type'] == 'insta')

{

	$code = $_GET['code'];

	

	$apiData = array(

		'client_id'       => $insta_client_id,

		'client_secret'   => $insta_client_secret,

		'grant_type'      => 'authorization_code',

		'redirect_uri'    => $insta_redirect_uri,

		'code'            => $code

	);

	

	$apiHost = 'https://api.instagram.com/oauth/access_token';

	

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $apiHost);

	curl_setopt($ch, CURLOPT_POST, count($apiData));

	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($apiData));

	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$jsonData = curl_exec($ch);

	curl_close($ch);

	

	$user = @json_decode($jsonData);

	

	$data = file_get_contents('https://api.instagram.com/v1/users/self/?access_token='.$user->access_token.'');

	$userinfo = @json_decode($data);

	

	$user_meta = array(

		'id' => $userinfo->data->id,

		'access_token'  => $user->access_token,

		'username' => $userinfo->data->username,

		'fullName' => $userinfo->data->full_name,

		'profilePicUrl' => $userinfo->data->profile_picture,

		'followedByCount' => $userinfo->data->counts->followed_by,

		'followsCount'    => $userinfo->data->counts->follows,

		'mediaCount'    => $userinfo->data->counts->media

	);

	$current_user = wp_get_current_user();

	$get_pre_data = get_user_meta($current_user->ID, 'instagram', true);

	if($get_pre_data != ''){

		foreach( $get_pre_data as $key => $val ){

			if(in_array($userinfo->data->id, $val)){

				

				unset($get_pre_data[$key]);

			}

		}

		

		array_push($get_pre_data,$user_meta);

		$myarray = array_values($get_pre_data);

		update_user_meta($current_user->ID, 'instagram', $myarray);

	}else{

		$ar[] = $user_meta;

		update_user_meta($current_user->ID, 'instagram', $ar);

	}

}