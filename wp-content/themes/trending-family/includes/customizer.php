<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/*
 * Trending Family Customizer.
 *
 * Contains methods for customizing the theme customization screen.
 *
 * @link http://codex.wordpress.org/Theme_Customization_API
 * @since Trending Family 1.0
 *
 * @package libertyspring
 */
class TrendingFamily_Customize {

	public static function register ( $wp_customize ) {

		$wp_customize->add_section( 'trending_family_social_media',
			array(
				'title'       => esc_html__( 'Social Media', 'trending-family' ),
				'capability'  => 'edit_theme_options',
				'description' => esc_html__('Allows you to add the URL of the social media profiles.', 'trending-family'),
			)
		);

		$wp_customize->add_setting( 'trending_family_social_media_youtube_channel',
			array(
				'default'    => '',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'  => 'refresh',
				'sanitize_callback' => 'esc_url_raw'
			)
		);

		$wp_customize->add_setting( 'trending_family_social_media_twitter_url',
			array(
				'default'    => '',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'  => 'refresh',
				'sanitize_callback' => 'esc_url_raw'
			)
		);


		$wp_customize->add_setting( 'trending_family_social_media_email_address',
			array(
				'default'    => '',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'  => 'refresh',
				'sanitize_callback' => 'trending_family_sanitize_email'
			)
		);

		$wp_customize->add_setting( 'trending_family_social_media_fb_url',
			array(
				'default'    => '',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'  => 'refresh',
				'sanitize_callback' => 'esc_url_raw'
			)
		);


		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'trending_family_social_media_youtube_channel',
			array(
				'label'    => esc_html__( 'Youtube Channel', 'trending-family' ),
				'section'  => 'trending_family_social_media',
				'type'     => 'text',
			)
		) );

		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'trending_family_social_media_twitter_url',
			array(
				'label'    => esc_html__( 'Twitter Profile URL', 'trending-family' ),
				'section'  => 'trending_family_social_media',
				'type'     => 'text',
			)
		) );

		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'trending_family_social_media_email_address',
			array(
				'label'    => esc_html__( 'Email Address', 'trending-family' ),
				'section'  => 'trending_family_social_media',
				'type'     => 'text',
			)
		) );


		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'trending_family_social_media_fb_url',
			array(
				'label'    => esc_html__( 'Facebook Profile URL', 'trending-family' ),
				'section'  => 'trending_family_social_media',
				'type'     => 'text',
			)
		) );


		$wp_customize->add_section( 'trending_family_footer',
			array(
				'title'       => esc_html__( 'Footer', 'trending-family' ),
				'capability'  => 'edit_theme_options',
				'description' => esc_html__('Allows you to customize content for the footer.', 'trending-family'),
			)
		);

		$wp_customize->add_setting( 'trending_family_contact_details',
			array(
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'  => 'refresh',
			)
		);

		$wp_customize->add_setting( 'trending_family_copyright_text',
			array(
				'default'    => '&copy; <script>document.write(new Date().getFullYear());</script> ALL RIGHTS RESERVED',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'  => 'refresh',
			)
		);

		$wp_customize->add_setting( 'trending_family_footer_menu_text',
			array(
				'default'    => '',
				'type'       => 'theme_mod',
				'capability' => 'edit_theme_options',
				'transport'  => 'refresh',
			)
		);

		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'trending_family_contact_details',
			array(
				'label'    => esc_html__( 'Contact Details', 'trending-family' ),
				'section'  => 'trending_family_footer',
				'type'     => 'textarea',
			)
		) );


		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'trending_family_copyright_text',
			array(
				'label'    => esc_html__( 'Copyright Content', 'trending-family' ),
				'section'  => 'trending_family_footer',
				'type'     => 'textarea',
			)
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'trending_family_footer_menu_text',
			array(
				'label'    => esc_html__( 'Menu Text', 'trending-family' ),
				'section'  => 'trending_family_footer',
				'type'     => 'textarea',
			)
		) );


		$wp_customize->get_setting( 'blogname' )->transport          = 'postMessage';
		$wp_customize->get_setting( 'blogdescription' )->transport   = 'postMessage';
		$wp_customize->get_setting( 'header_textcolor' )->transport  = 'postMessage';
		$wp_customize->get_setting( 'background_color' )->transport  = 'postMessage';
		$wp_customize->get_setting( 'trending_family_social_media_youtube_channel' )->transport = 'postMessage';
		$wp_customize->get_setting( 'trending_family_social_media_twitter_url' )->transport = 'postMessage';
		$wp_customize->get_setting( 'trending_family_social_media_email_address' )->transport = 'postMessage';
		$wp_customize->get_setting( 'trending_family_social_media_fb_url' )->transport = 'postMessage';
		$wp_customize->get_setting( 'trending_family_contact_details' )->transport = 'postMessage';
		$wp_customize->get_setting( 'trending_family_copyright_text' )->transport = 'postMessage';
		$wp_customize->get_setting( 'trending_family_footer_menu_text' )->transport = 'postMessage';

	}

	/**
	 * This outputs the javascript needed to automate the live settings preview.
	 * Also keep in mind that this function isn't necessary unless your settings
	 * are using 'transport'=>'postMessage' instead of the default 'transport'
	 * => 'refresh'
	 *
	 * Used by hook: 'customize_preview_init'
	 *
	 * @see add_action('customize_preview_init',$func)
	 * @since MyTheme 1.0
	 */
	public static function live_preview() {
		wp_enqueue_script( 'trending-family-themecustomizer', get_template_directory_uri() . '/assets/js/customizer.js', array(  'jquery', 'customize-preview' ), false, true);
	}

	public static function trending_family_sanitize_file_url( $url )
	{

		$output = '';

		$filetype = wp_check_filetype( $url );

		if ( $filetype["ext"] ) {

			$output = esc_url( $url );
		}
		return $output;
	}

	public static function trending_family_sanitize_text( $str )
	{
		return sanitize_text_field( $str );
	}

	public static function trending_family_sanitize_textarea( $text )
	{
		return esc_textarea( $text );
	}

	public static function trending_family_sanitize_number( $int )
	{
		return absint( $int );
	}

	public static function trending_family_sanitize_email( $email )
	{
		if(is_email( $email )){
			return $email;
		}else{
			return '';
		}
	}
}

// Setup the Theme Customizer settings and controls...
add_action( 'customize_register' , array( 'TrendingFamily_Customize' , 'register' ) );

// Enqueue live preview javascript in Theme Customizer admin screen
add_action( 'customize_preview_init' , array( 'TrendingFamily_Customize' , 'live_preview' ) );
