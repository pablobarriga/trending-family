<?php
/*
 * Registers Custom Post Types for the theme
 * @package Trending Family
 */
if ( ! defined( 'ABSPATH' ) ) exit;

if(!function_exists('register_testimonial_posts_init')) {
	/**
	 *
	 */
	function register_testimonial_posts_init()
	{
		// Register Testimonials
		$testimonials_labels = array(
			'name'          => 'Testimonials',
			'singular_name' => 'Testimonial',
			'menu_name'     => 'Testimonials'
		);
		$testimonials_args   = array(
			'labels'          => $testimonials_labels,
			'public'          => true,
			'capability_type' => 'post',
			'has_archive'     => false,
			'supports'        => array( 'title' )
		);
		register_post_type( 'testimonial', $testimonials_args );
	}
	add_action( 'init', 'register_testimonial_posts_init' );
}

if(!function_exists('register_brands_and_labels_posts_init')) {
	/**
	 *
	 */
	function register_brands_and_labels_posts_init()
	{
		// Register Brands & Clients
		$brands_labels = array(
			'name'          => 'Brands & Clients',
			'singular_name' => 'Brand & Client',
			'menu_name'     => 'Brands & Clients'
		);
		$brands_args   = array(
			'labels'          => $brands_labels,
			'public'          => true,
			'capability_type' => 'post',
			'has_archive'     => false,
			'supports'        => array( 'title', 'thumbnail' )
		);
		register_post_type( 'brand', $brands_args );
	}

	add_action( 'init', 'register_brands_and_labels_posts_init' );
}


if(!function_exists('register_campaign_posts_init')) {
	/**
	 *
	 */
	function register_campaign_posts_init()
	{
		// Register Campaign
		$campaigns_labels = array(
			'name'          => 'Campaigns',
			'singular_name' => 'Campaign',
			'menu_name'     => 'Campaigns'
		);
		$campaigns_args   = array(
			'labels'          => $campaigns_labels,
			'public'          => true,
			'capability_type' => 'post',
			'has_archive'     => false,
			'supports'        => array( 'title', 'thumbnail' )
		);
		register_post_type( 'campaign', $campaigns_args );
	}

	add_action( 'init', 'register_campaign_posts_init' );
}