<?php if ( ! defined( 'ABSPATH' ) ) exit;
/**
 *
 * Add ACF Option for blog posts
 *
 * @since v1.0.1
 *
 * @author Bluelit
 *
 * @package Trending Family
 */

if( !function_exists('trending_family_blog_post_single_option') ) {
	
	function trending_family_blog_post_single_option() {
		acf_add_local_field_group( array(
			'key'                   => 'tf_blog_post_option',
			'title'                 => 'Post Single Option',
			'fields'                => array(
				array(
					'key'               => 'tf_blog_post_option_1',
					'label'             => 'Enable Contact Section',
					'name'              => 'tf_blog_post_enable_contact_section',
					'type'              => 'true_false',
					'prefix'            => '',
					'instructions'      => 'Enable Contact Section',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => 'true',
					'placeholder'       => '',
					'prepend'           => '',
					'append'            => '',
					'maxlength'         => '',
					'readonly'          => 0,
					'disabled'          => 0,
				),
				array(
					'key'               => 'tf_blog_post_option_2',
					'label'             => 'Section Heading',
					'name'              => 'tf_blog_post_contact_section_heading',
					'type'              => 'text',
					'prefix'            => '',
					'instructions'      => 'Enter the heading text for Contact Section.',
					'required'          => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'tf_blog_post_option_1',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => 'Contact us',
					'placeholder'       => '',
					'prepend'           => '',
					'append'            => '',
					'maxlength'         => '',
					'readonly'          => 0,
					'disabled'          => 0,
				),
				array(
					'key'               => 'tf_blog_post_option_3',
					'label'             => 'Section Tagline',
					'name'              => 'tf_blog_post_contact_section_tagline',
					'type'              => 'textarea',
					'rows'              => 5,
					'prefix'            => '',
					'instructions'      => 'Enter the paragraph for the Contact Section.',
					'required'          => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'tf_blog_post_option_1',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => 'Are you a brand interested in partnering with top family-friendly influencers?',
					'placeholder'       => '',
					'prepend'           => '',
					'append'            => '',
					'maxlength'         => '',
					'readonly'          => 0,
					'disabled'          => 0,
				),
				array(
					'key'               => 'tf_blog_post_option_4',
					'label'             => 'Form Shortcode',
					'name'              => 'tf_blog_post_contact_section_form_shortcode',
					'type'              => 'text',
					'prefix'            => '',
					'instructions'      => 'Enter the shortcode for the Contact Form. You can get the shortcodes from <a href="' . admin_url( '/admin.php?page=wpcf7' ) . '" target="_blank">here</a>',
					'required'          => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'tf_blog_post_option_1',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => '[contact-form-7 id="467" title="Contact Us Form"]',
					'placeholder'       => '',
					'prepend'           => '',
					'append'            => '',
					'maxlength'         => '',
					'readonly'          => 0,
					'disabled'          => 0,
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'post',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
		) );
	}
	
	add_action( 'acf/init', 'trending_family_blog_post_single_option' );
	
}
