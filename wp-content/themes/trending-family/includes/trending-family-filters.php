<?php
if ( ! defined( 'ABSPATH' ) ) exit;

add_filter('show_admin_bar', '__return_false');

if(!function_exists('trending_family_get_custom_logo')){

    /**
     * @param $html
     * @return mixed
     */
    function trending_family_get_custom_logo($html)
    {

        $to_remove = array('itemprop="url"', 'itemprop="logo"');
        $to_replace = array('class="custom-logo-link"');

        $html = str_replace($to_replace, 'class="navbar-brand"', $html);

        $html = str_replace($to_remove, '', $html);

        return $html;
    }

    add_filter( 'get_custom_logo', 'trending_family_get_custom_logo');
}

if(!function_exists('trending_family_custom_login_logo')) {

	/**
	 *
	 */
	function trending_family_custom_login_logo()
    { ?>
        <style type="text/css">
            #login h1 a, .login h1 a {
                background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png);
                padding-bottom: 30px;
            }
        </style>
    <?php }

    add_action('login_enqueue_scripts', 'trending_family_custom_login_logo');
}

if(!function_exists('trending_family_login_url')) {

	/**
	 * @return string|void
	 */
	function trending_family_login_url()
    {
        return home_url();
    }

    add_filter('login_headerurl', 'trending_family_login_url');
}

if(!function_exists('trending_family_login_title')) {

	/**
	 * @return string
	 */
	function trending_family_login_title()
    {
        return esc_html__('Trending Family', 'trending-family');
    }
    add_filter('login_headertitle', 'trending_family_login_title');
}


if(!function_exists( 'trending_family_search_filter' )) {
    /*
     * Add filters for search results
     */
	function trending_family_search_filter( $query )
    {

		if ( $query->is_search && ! is_admin() ) {
			$query->set( 'post_type', array( 'post', 'page' ) );
		}
		return $query;
	}
	add_filter( 'pre_get_posts', 'trending_family_search_filter' );
}

if( !function_exists('trending_family_set_content_type')) {
    /*
     * Set Content type to text and HTML for email
     */
	function trending_family_set_content_type()
    {
		return "text/html";
	}
	add_filter( 'wp_mail_content_type', 'trending_family_set_content_type' );
}

if( !function_exists('trending_family_async_scripts')) {
    // Async load
	function trending_family_async_scripts( $url ) {
		if ( strpos( $url, '#asyncload' ) === false ) {
			return $url;
		} else if ( is_admin() ) {
			return str_replace( '#asyncload', '', $url );
		} else {
			return str_replace( '#asyncload', '', $url ) . "' async='async";
		}
	}

	add_filter( 'clean_url', 'trending_family_async_scripts', 11, 1 );
}

if(!function_exists('trending_family_yoast_to_bottom')) {
	/**
     * Moves the Yoast SEO meta box to bottom of the edit page
	 * @return string
	 */
	function trending_family_yoast_to_bottom() {
		return 'low';
	}
	add_filter( 'wpseo_metabox_prio', 'trending_family_yoast_to_bottom' );
}