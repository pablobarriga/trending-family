<?php

/**

 * The header for our theme

 *

 * This is the template that displays all of the <head> section and everything up until <div id="content">

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package trending-family

 */



?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <style type="text/css">
    li.customBox {
        list-style: none;
        display: inline-block;
        border: 1px solid black;
        padding: 15px;
    }

    .feature-slide h3{
        margin-top: 20px !important;
    }
    </style>

    <?php wp_head(); ?>

</head>



<body <?php body_class(); ?>>



<header class="header">

    <nav class="navbar navbar-default navbar-fixed navbar-sticky bootsnav">

        <div class="container">

            <div class="navbar-header">

                <button class="navbar-toggle collapsed btn-mobile-bar" type="button">

                    <span class="sr-only"><?php echo esc_html__('Toggle navigation', 'trending-family'); ?></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                </button>

                <?php

                $main_logo = get_theme_mod( 'custom_logo' );



                if(!empty($main_logo)){

                    $main_logo_url = wp_get_attachment_url( $main_logo, 'full' ); ?>



                    <a title="<?php echo esc_attr( get_bloginfo('name') ); ?>" class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">

                        <img src="<?php echo esc_url( $main_logo_url ); ?>" alt="<?php echo get_bloginfo( 'name' ); ?>">

                    </a>



	            <?php } ?>



            </div>


            <div class="btns-box login-widget">

                <?php if( !is_user_logged_in() ) {  ?>

                    <a href="javascript:void(0);" class="btn btn-login-popup"><i class="fa fa-user"></i><span><?php echo esc_html__('LOG IN', 'trending-family' ); ?></span></a>

                <?php } else {

                    $user = wp_get_current_user(); ?>

                    <a href="<?php echo esc_url( trending_family_get_redirect_url($user) ); ?>" class="btn profile-link"><i class="fa fa-tachometer"></i><span><?php echo esc_html($user->display_name); ?></span></a>

                    <a href="<?php echo esc_url( wp_logout_url( home_url('/') ) ); ?>" class="btn"> <i class="fa fa-user"></i><span><?php echo esc_html__('LOG OUT', 'trending-family'); ?></span></a>



                <?php } ?>

            </div>



            <div class="main-menu collapse navbar-collapse">

                <?php

                $defaults = array(

	                'theme_location'  => 'menu-1',

	                'container'       => false,

	                'menu_class'      => 'nav navbar-nav navbar-center',

	                'echo'            => true,

	                'fallback_cb'     => 'TrendingFamilyNavWalker::fallback',

	                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',

	                'depth'           => 0,

	                'walker'          => new TrendingFamilyNavWalker()

                );

                if ( has_nav_menu( 'menu-1' ) ) {

                    wp_nav_menu( $defaults );

                } ?>

            </div>

        </div>

    </nav>

    <div class="login-popup-box-wrap">

        <div class="container">

            <div class="row">

                <div class="col-xs-12">

                    <div class="login-popup-box">

                        <form id="trending_family_api_login_form" class="trending_family_form" method="POST" autocomplete="off">

                            <p class="login-error" style="display: none;"></p>

                            <div class="row">

                                <div class="col-xs-12">

                                    <div class="form-group">

                                        <input type="text" id="username" name="username" class="form-control" placeholder="<?php echo esc_attr__('Username', 'trending-family'); ?>" required>

                                    </div>

                                </div>

                                <div class="col-xs-12">

                                    <div class="form-group">

                                        <input type="password" id="user_pass" name="password" class="form-control" placeholder="<?php echo esc_attr__('Password', 'trending-family'); ?>" required>

                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-6">

                                    <div class="form-group">

                                        <a class="btn btn-link" href="/forgot-password" title="<?php echo esc_attr__('Forgot Password', 'trending-family'); ?>"><?php echo esc_html__('Forgot password', 'trending-family'); ?></a>

                                    </div>

                                </div>

                                <div class="col-xs-6 text-right">

                                    <div class="form-group">

	                                    <?php wp_nonce_field( 'trending_family_login_nonce', 'security' ); ?>

                                        <a id="login_form_api_submit" class="btn btn-default"><?php echo esc_html__('LOG IN', 'trending-family'); ?></a>

                                    </div>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93339411-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93339411-1');
</script>

</header>
