/*
 Author: Prajwal Shrestha
 Ver: 1.0
 */
jQuery(document).ready( function ($) {

    ( function(){
        var num = $('.clonedInfluencer').length;
        if( num <= 1) {
            $('.btnInfluencerDel').css('display', 'none');
        }

        $('.addInfluencer').live('click', function () {
            var num = $('.clonedInfluencer').length,
                newNum = new Number(num + 1),
                newElem = $('#entryInfluencer' + num).clone().attr('id', 'entryInfluencer' + newNum).fadeIn('slow');
            newElem.find('.label_influencer_first_name').attr('for', 'ID' + newNum + '_label_influencer_first_name');
            newElem.find('.influencer_first_name').attr('id', 'ID' + newNum + '_influencer_first_name').attr('name', 'influencer_first_name[]').val('');
            newElem.find('.label_influencer_last_name').attr('for', 'ID' + newNum + '_label_influencer_last_name');
            newElem.find('.influencer_last_name').attr('id', 'ID' + newNum + '_influencer_last_name').attr('name', 'influencer_last_name[]').val('');

            $('#entryInfluencer' + num).after(newElem);
            $('#ID' + newNum + '_label_influencer_first_name').focus();
            $('#entryInfluencer' + newNum + ' .btnInfluencerDel').css('display', 'block');

        });

        $('.btnInfluencerDel').live('click', function ( event ) {
            var $this = $(this);

            var toRemove = $this.closest($('.clonedInfluencer'));
            var num = $('.clonedInfluencer').length;
            $(toRemove).slideUp('slow', function () {
                $(this).remove();
                if (num - 1 === 1) $('.btnInfluencerDel').css('display', 'none');
            });
            return false;
            $(this).css('display', 'block');
        });

    })();

    ( function(){

        $('.addKids').live('click', function () {
            var newElem = $('#kidsTpl').clone().fadeIn('slow');
            newElem.attr('id', null);

			$('#kidsTpl').before(newElem);
            $(".datepicker-input").on( "focusin", function () {
                $(this).datepicker({
                    startDate: new Date(),
                    format: 'dd/mm/yyyy',
                    changeMonth: true,
                    changeYear: true,
                    yearRange: '1930:2030',
                    inline: true,
                    dy: true,
                    orientation: "bottom auto"
                });
            });

        });

        $('.btnKidsDel').live('click', function ( event ) {
            event.preventDefault();
            var $this = $(this);

            var toRemove = $this.closest($('.clonedKids'));
            $(toRemove).slideUp('slow', function () {
                $(this).remove();
            });

            return false;
        });

    })();

    ( function(){

        $('.addPets').live('click', function () {
            var newElem = $('#petsTpl').clone().fadeIn('slow');
            newElem.attr('id', null);

			$('#petsTpl').before(newElem);
        });

        $('.btnPetsDel').live('click', function ( event ) {
            event.preventDefault();
            var $this = $(this);

            var toRemove = $this.closest($('.clonedPets'));
            $(toRemove).slideUp('slow', function () {
                $(this).remove();
            });

            return false;
        });

    })();

    ( function(){

        $('.addLanguages').live('click', function () {
            var newElem = $('#languagesTpl').clone().fadeIn('slow');
            newElem.attr('id', null);

			$('#languagesTpl').before(newElem);

        });

        $('.btnLanguagesDel').live('click', function ( event ) {
            event.preventDefault();
            var $this = $(this);

            var toRemove = $this.closest($('.clonedLanguages'));
            $(toRemove).slideUp('slow', function () {
                $(this).remove();
            });

            return false;
        });

    })();

    ( function(){

        $('.addBrands').live('click', function () {
            var newElem = $('#brandsTpl').clone().fadeIn('slow');
            newElem.attr('id', null);

			$('#brandsTpl').before(newElem);

        });

        $('.btnBrandsDel').live('click', function ( event ) {
            event.preventDefault();
            var $this = $(this);

            var toRemove = $this.closest($('.clonedBrands'));
            $(toRemove).slideUp('slow', function () {
                $(this).remove();
            });

            return false;
        });
    })();
});