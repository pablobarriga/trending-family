(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

jQuery(document).ready( function($){
    "use strict";
    var currentPageUrl = window.location.href;
    var tempHomeUrl = homeUrl.homeUrl + '#';
    if( homeUrl.homeUrl == currentPageUrl ||  currentPageUrl.includes( tempHomeUrl ) ){
        $('#menu-header-menu li').each(function(){
            if( $(this).hasClass( 'current_page_item' )){
                $(this).removeClass( 'current_page_item' );
            }
        });
        $('#menu-header-menu li a').each(function(){
            var href = $(this).attr('href');
            href = href.substring(href.indexOf("#") );
            $(this).attr('href', href );
        });
    }
    // Animate scroll and add active menu class in the nav menu
    $('#menu-header-menu li a[href^="#"]').on( 'click', function(e){
        $(document).off("scroll");
        $('li').each(function () {
            if( $(this).hasClass( 'current_page_item' )) {
                $(this).removeClass('current_page_item');
            }
        })
        $(this).parent('li').addClass('current_page_item');
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('#' + this.hash.slice(1) +'');
            if (target.length) {
                $('html,body').stop(false, true).animate({
                    scrollTop: target.offset().top - 35
                }, 500, function() {
                    // window.location.hash = target;
                    $(document).on("scroll", onScroll);
                });
                event.preventDefault();
            }
        }
    });
    // Animate scroll from the page other than home
    if (window.location.hash) {
        var target = window.location.hash;
        if (target.length) {
            $('html,body').stop(false, true).animate({
                scrollTop: $(target).offset().top + 35
            }, 500, function() {
                $(document).on("scroll", onScroll);
            });
        }
    }
    $(document).on("scroll", onScroll);
    function onScroll(event){
        if( homeUrl.homeUrl == currentPageUrl || currentPageUrl.includes( tempHomeUrl ) ) {
            var scrollPos = $(document).scrollTop();
            $('#menu-header-menu a').each(function () {
                var currLink = $(this);
                if( currLink.attr('href').includes('#')) {
                    var refElement = $(currLink.attr("href"));
                    if (refElement.position().top - 100 <= scrollPos && refElement.position().top + refElement.height() - 100 > scrollPos) {
                        $('#menu-header-menu li').removeClass("current_page_item");
                        currLink.parent('li').addClass("current_page_item");
                    }
                    else {
                        currLink.parent('li').removeClass("current_page_item");
                    }
                }
            });
        }
    }
    $("form#trending_family_login_form input").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            $('a#login_form_submit').click();
            return false;
        } else {
            return true;
        }
    });
    $('a#login_form_submit').on('click', function(e) {
        var form = $(this).closest('form');
        form.submit();
    });
    $('form#trending_family_login_form').validate({
        submitHandler: function( form ){
            $('p.login-error').css('display', 'none').text('');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: homeUrl.ajaxUrl,
                data: {
                    'action': 'ajaxlogin',
                    'username': $('form#trending_family_login_form #username').val(),
                    'password': $('form#trending_family_login_form #user_pass').val(),
                    'security': $('form#trending_family_login_form #security').val()
                },
                success: function (data) {
                    if (data.loggedin == true) {
                        document.location.href = data.redirectUrl;
                    } else {
                        $('p.login-error').css('display', 'block').append(data.message);
                    }
                }
            });
        }
    });
    $('a.smoothScroll').click(function(event) {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('#' + this.hash.slice(1) +'');
            if (target.length) {
                $('html,body').stop(false, true).animate({
                    scrollTop: target.offset().top - 35
                }, 500);
                event.preventDefault();
            }
        }
    });
    ( function() {
        $(".datepicker-input").on( "focusin", function () {
            $(this).datepicker({
                startDate: new Date(),
                format: 'dd/mm/yyyy',
                changeMonth: true,
                changeYear: true,
                yearRange: '1930:2030',
                inline: true,
                dy: true,
            });
        });
    })();

    $('.overlay-slider').owlCarousel({
        items:1, loop: true, center: false, autoplay: true, dots:false
    });

    $(".phone").inputmask( "999-999-9999",  {
        placeholder:"#"
    });
    $(".email").inputmask({ alias: "email"});
    $('.dismiss-reload').on('click', function(e) {
        e.preventDefault();
        document.location.href = window.location.href.split("?")[0];
    });
    $('a.add_page').on('click', function(){
        var form = $(this).closest('form');
        form.submit();
    });
    $('a.youtube-disconect').on('click', function( event ){
        event.preventDefault();
        var $this = $(this);
        swal({
            title: "Are you sure?",
            text: "This account will be permanently deleted.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, disconnect!",
            cancelButtonText: "No, don't!",
        }).then(function() {
            var a =  $this.closest("tr");
            var id = $this.closest("tr").data('id');
            var key  = 'youtube-channels';
            var user = $this.closest("tr").data('user');
            var security = homeUrl.ajaxNonce;
            var data = {
                action:"delete_youtube_channel",
                id:id ,
                user: user ,
                key:key,
                nonce: security
            };
            $.ajax({
                url: homeUrl.ajaxUrl,
                data: data,
                type: 'post',
                success:function (res) {
                    console.log(res);
                    a.fadeOut();
                }
            });
            swal("Disconnected!", "The account has been disconnected.", "success");
        });
    });
    $('a.inst-disconect').on('click' , function( event ) {
        event.preventDefault();
        var $this = $(this);
        swal({
            title: "Are you sure?",
            text: "This account will be permanently deleted.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#f86071",
            confirmButtonText: "Yes, disconnect!",
            cancelButtonText: "No, don't!",
        }).then(function() {
            var a =  $this.closest("tr");
            var id = $this.closest('tr').data('page');
            var user = $this.closest('tr').data('user');
            var key = 'instagram';
            var security = homeUrl.ajaxNonce;
            var data = {
                action:"delete_insta_page",
                id:id,
                user: user,
                key:key,
                nonce: security
            };
            $.ajax({
                url: homeUrl.ajaxUrl,
                data: data,
                type: 'post',
                success:function (res) {
                    console.log(res);
                    a.fadeOut();
                }
            });
            swal("Disconnected!", "The account has been disconnected.", "success");
        });
    });
    $('a.twitt-disconect').on('click' , function( event ) {
        event.preventDefault();
        var $this = $(this);
        swal({
            title: "Are you sure?",
            text: "This account will be permanently deleted.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#f86071",
            confirmButtonText: "Yes, disconnect!",
            cancelButtonText: "No, don't!",
        }).then(function() {
            var a =  $this.closest("tr");
            var id = $this.closest('tr').data('page');
            var user = $this.closest('tr').data('user');
            var key = 'twitter_account';
            var security = homeUrl.ajaxNonce;
            var data = {
                action:"delete_twitter_page",
                id:id,
                user:user,
                key:key,
                nonce: security
            };
            $.ajax({
                url: homeUrl.ajaxUrl,
                data: data,
                type: 'post',
                success:function () {
                    a.fadeOut();
                }
            });
            swal("Disconnected!", "The account has been disconnected.", "success");
        });
    });
    $('a.pin-disconect').on('click' , function( event ) {
        event.preventDefault();
        var $this = $(this);
        swal({
            title: "Are you sure?",
            text: "This account will be permanently deleted.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#f86071",
            confirmButtonText: "Yes, disconnect!",
            cancelButtonText: "No, don't!",
        }).then(function() {
            var a =  $this.closest("tr");
            var id = $this.closest('tr').data('page');
            var user = $this.closest('tr').data('user');
            var key = 'pint_accounts';
            var security = homeUrl.ajaxNonce;
            var data = {
                action:"delete_pin_page",
                id:id,
                user: user,
                key:key,
                nonce: security
            };
            $.ajax({
                url: homeUrl.ajaxUrl,
                data: data,
                type: 'post',
                success:function () {
                    a.fadeOut();
                }
            });
            swal("Disconnected!", "The account has been disconnected.", "success");
        });
    });
    // Remove Snapchat Handle
    $('a.snap-disconnect').on('click', function( event ){
        event.preventDefault();
        var $this = $(this);
        swal({
            title: "Are you sure?",
            text: "This account will be permanently deleted.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, disconnect!",
            cancelButtonText: "No, don't!",
        }).then(function() {
            var a =  $this.closest("tr");
            var id = $this.closest("tr").data('index');
            var user = $this.closest("tr").data('user');
            var key = 'influencer_snapchats';
            var security = homeUrl.ajaxNonce;
            var data = {
                action:"delete_influencers_blog" ,
                id:id ,
                user: user ,
                key:key,
                nonce: security
            };
            $.ajax({
                url: homeUrl.ajaxUrl,
                data: data,
                type: 'post',
                success:function () {
                    a.fadeOut();
                }
            });
            swal("Disconnected!", "The account has been disconnected.", "success");
        });
    });
    // Remove Blog
    $('a.blog-disconect').on('click', function( event ){
        event.preventDefault();
        var $this = $(this);
        swal({
            title: "Are you sure?",
            text: "This account will be permanently deleted.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#f86071",
            confirmButtonText: "Yes, disconnect!",
            cancelButtonText: "No, don't!",
        }).then(function() {
            var a = $this.closest("tr");
            var id = $this.closest("tr").data('index');
            var user = $this.closest("tr").data('user');
            var key = 'influencer_blogs';
            var security = homeUrl.ajaxNonce;
            var data = {
                action: "delete_influencers_blog",
                id: id,
                user: user,
                key: key,
                nonce: security
            };
            $.ajax({
                url: homeUrl.ajaxUrl,
                data: data,
                type: 'post',
                success: function (data) {
                    a.fadeOut();
                }
            });
        });
    });
    // feed-grid on Client Compaign page
    // $(window).on('load', function() {
    //
    //     $('.feed-grid').packery({
    //
    //         itemSelector: '.feed-item',
    //
    //         gutter: 25,
    //
    //     });
    //
    // });
    $(document).mouseup(function (e){
        var container = $("form.validateForm");
        if (!$("a.btn-add-social").is(e.target) && !container.is(e.target) && container.has(e.target).length === 0) {
            $("a.btn-add-social").removeClass("active");
            $('.social-option-box').find(".login-popup-box-wrap").hide();
        }
    });
    $("form.validateForm").each(function(){
        $(this).validate({
            ignore: false
        });
    });

    // Home Page - influencers carousel
    function influencersCarousel() {
        var checkWidth = $(window).width();
        var owlPost = $(".influencers-carousel");
        if (checkWidth >= 768) {
            if(typeof owlPost.data('owl.carousel') != 'undefined'){
                owlPost.data('owl.carousel').destroy();
            }
            owlPost.removeClass('owl-carousel');
        } else if (checkWidth < 768) {
            owlPost.addClass('owl-carousel');
            owlPost.owlCarousel({
                loop:true,
                margin: 10,
                nav: true,
                slideSpeed : 500,
                animateOut: 'fadeOut',
                touchDrag: true,
                mouseDrag: true,
                autoplay: false,
                autoplaySpeed: 8000,
                autoplayTimeout: 8000,
                dots: false,
                // items: 1
                responsive:{
                    0:{
                        items:1
                    },
                    480:{
                        items:2
                    },
                    650:{
                        items:3
                    }
                }
            });
        }
    }
    influencersCarousel();
    $(window).resize(influencersCarousel);

});
jQuery(document).ready(function() {
    jQuery('a.stopjump').click(function(e)
    {
        e.preventDefault();
    });
});