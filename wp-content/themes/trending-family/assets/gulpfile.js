var gulp           = require('gulp'),
    gutil          = require('gulp-util' ),
    sass           = require('gulp-sass'),
    concat         = require('gulp-concat'),
    uglify         = require('gulp-uglify'),
    cleanCSS       = require('gulp-clean-css'),
    rename         = require('gulp-rename'),
    cache          = require('gulp-cache'),
    autoprefixer   = require('gulp-autoprefixer'),
    bourbon        = require('node-bourbon'),
    sourcemaps = require('gulp-sourcemaps'),
    notify         = require("gulp-notify");

gulp.task('sass', function() {
    //return gulp.src('scss/*.scss')
    return gulp.src('scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: bourbon.includePaths
        }).on("error", notify.onError()))
        //.pipe(autoprefixer(['last 10 versions']))
        // .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('css'))
});

gulp.task('watch', ['sass'], function() {
    gulp.watch('scss/**/*.+(sass|scss|css)', ['sass']);
});

//gulp.task('clearcache', function () { return cache.clearAll(); });

gulp.task('default', ['watch']);
