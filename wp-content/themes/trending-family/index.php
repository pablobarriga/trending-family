<?php
	/**
	 * The main template file
	 *
	 * This is the most generic template file in a WordPress theme
	 * and one of the two required files for a theme (the other being style.css).
	 * It is used to display a page when nothing more specific matches a query.
	 * E.g., it puts together the home page when no home.php file exists.
	 *
	 * @link https://codex.wordpress.org/Template_Hierarchy
	 *
	 * @package trending-family
	 */
	
	get_header();
	
	if ( have_posts() ) {
		$hero_image = get_field( 'trending_family_blog_hero_image', 'option' );
		$page_title = get_field( 'trending_family_blogpage_title', 'option' );
		$enable_sidebar = get_field( 'trending_family_blog_enable_sidebar', 'option' );
		if( !empty( $hero_image ) ){ ?>
            <div class="hero-bg hero-overlay flex-center-full" style="background-image: url(<?php echo esc_url( $hero_image ); ?>); ">
				<?php if( !empty( $page_title )){ ?>
                    <div class="hero-text"><h1><?php echo $page_title; ?></h1></div>
				<?php } ?>
            </div>
		<?php } ?>

        <section class="offset-top-xs-4 offset-top-md-6">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
						<?php
							// Commented for now
							/*if( !empty( $page_title )){ ?>
								<h1 class="blog-title"><?php echo esc_html( strip_tags( $page_title ) ); ?></h1>
							<?php } ?>
							*/ ?>
						<?php if( $enable_sidebar ) { ?>
                        <div class="col-sm-7 articles-list offset-top-xs-4 offset-top-md-3">
							<?php }
								
								
								
								/* Start the Loop */
								while ( have_posts() ) {
									the_post();
									
									/*
									 * Include the Post-Format-specific template for the content.
									 * If you want to override this in a child theme, then include a file
									 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
									 */
									get_template_part( 'template-parts/content', 'blog' );
									
								}
								trending_family_pagination();
								
								if( $enable_sidebar ) { ?>
                        </div><!-- end of col-sm-7 -->
					<?php get_sidebar();
						} ?>
                    </div>

                </div>
            </div>
        </section>
	<?php } else {
		get_template_part( 'template-parts/content', 'none' );
	}
	get_footer();