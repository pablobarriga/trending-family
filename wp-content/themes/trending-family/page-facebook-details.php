<?php
/*
 * Template Name: Facebook Details
 */
global $current_user;
wp_get_current_user();
if(!in_array('influencer', $current_user->roles)) {
	global $wp_query;
	$wp_query->set_404();
	status_header( 404 );
	get_template_part( 404 );
	exit();
}
get_header(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <section class="section bg-green pad-md text-dark text-center-xs">
            <div class="container">
                <div class="row row-lg">
                    <div class="flex-center">
                        <div class="col-xs-12 p-title">
							<?php
							$influencer_display_first_name = get_the_author_meta( 'first_name', $current_user->ID );
							$influencer_display_last_name = get_the_author_meta( 'last_name', $current_user->ID );
							$influencer_display_name = !empty( $influencer_display_first_name ) ? $influencer_display_first_name . ' ' . $influencer_display_last_name : $current_user->display_name; ?>
                            <h1><?php echo esc_html__('Welcome', 'trending-family'); ?> <span class="txt-wlight"><?php echo esc_html( $influencer_display_name ); ?></span></h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section pad-top-xxs-0 bg-grey">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">

                        <div class="s-title">
                            <h3><?php echo esc_html__('Facebook Details', 'trending-family'); ?><span class="hidden-xs">:</span></h3>
                        </div>

                        <div class="col-md-12 hide" id="impressionsRecords">

                            <div class="col-md-4">

                                <p id="organic_page_imp"></p>

                                <div class="col-md-6" id="organic_page_imp_old"></div>

                                <div class="col-md-6" id="organic_page_imp_per"></div>

                                <label>organic page impressions</label>

                            </div>

                            <div class="col-md-4">

                                <p id="organic_page_post_imp"></p>

                                <div class="col-md-6" id="organic_page_post_imp_old"></div>

                                <div class="col-md-6" id="organic_page_post_imp_per"></div>

                                <label>organic page posts impressions</label>

                            </div>

                            <div class="col-md-4">

                                <p id="organic_unique_page_post_imp"></p>

                                <div class="col-md-6" id="organic_unique_page_post_imp_old"></div>

                                <div class="col-md-6" id="organic_unique_page_post_imp_per"></div>

                                <label>unique organic page impressions</label>

                            </div>

                            <div class="col-md-4">

                                <p id="page_user_engage"></p>

                                <div class="col-md-6" id="page_user_engage_old"></div>

                                <div class="col-md-6" id="page_user_engage_per"></div>

                                <label>page engaged users</label>

                            </div>

                            <div class="col-md-4">

                                <p id="page_like"></p>

                                <div class="col-md-6" id="page_like_old"></div>

                                <div class="col-md-6" id="page_like_per"></div>

                                <label>likes</label>

                            </div>

                            <div class="col-md-4">

                                <p id="page_checkin_place"></p>

                                <div class="col-md-6" id="page_checkin_place_old"></div>

                                <div class="col-md-6" id="page_checkin_place_per"></div>

                                <label>page check-in places</label>

                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </section>

    </div>

    <script>

        function statusChangeCallback(response)
        {

            if (response.status === 'connected') {
                getPageInsights();
                console.log('ok');

            } else {

                //document.getElementById('status').innerHTML = 'Please log into this app.';

            }

        }

        function checkLoginState() {


            FB.getLoginStatus(function(response) {

                statusChangeCallback(response);

            });

        }

        function fbAuthUser() {
            FB.login(checkLoginState);
        }


        window.fbAsyncInit = function() {

            FB.init({

                appId      : '296252020832511',

                xfbml      : true,

                version    : 'v2.8'

            });

            FB.getLoginStatus(function(response) {

                statusChangeCallback(response);

            });

        };

        (function(d, s, id){

            var js, fjs = d.getElementsByTagName(s)[0];

            if (d.getElementById(id)) {return;}

            js = d.createElement(s); js.id = id;

            js.src = "//connect.facebook.net/en_US/sdk.js";

            fjs.parentNode.insertBefore(js, fjs);

        }(document, 'script', 'facebook-jssdk'));

        function getPageInsights() {

            var pageId = '<?php echo $_GET['facpage_id']?>';

            //var until = jQuery('span.fromDate').text();

            // var since = jQuery('span.toDate').text();

            FB.api(

                pageId+"/insights?metric=page_impressions_organic, page_engaged_users, page_places_checkin_total, page_posts_impressions_organic, page_posts_impressions_organic_unique&fields=id, name, title, values&period=days_28",

                function (response) {

                    if (response && !response.error)

                    {
                        organic_page_imp = organic_page_post_imp = organic_unique_page_post_imp = page_user_engage = page_checkin_place = page_like = 0;

                        var str = '';

                        console.log(response);

                        if(!jQuery.isEmptyObject(response.data))

                        {

                            var page = response.data;

                            for(var i in page)

                            {

                                str += '<div class="clearfix"></div><h2>'+ page[i].title+'</h2>';

                                if(!jQuery.isEmptyObject(page[i].values))
                                {

                                    var values = page[i].values;

                                    for(var j in values)

                                    {

                                        if(page[i].name == 'page_impressions_organic')

                                        {

                                            organic_page_imp = (parseInt(organic_page_imp) + parseInt(values[j].value));

                                        }



                                        if(page[i].name == 'page_engaged_users')
                                        {

                                            page_user_engage = parseInt(page_user_engage + values[j].value);

                                        }

                                        if(page[i].name == 'page_places_checkin_total')
                                        {

                                            page_checkin_place = parseInt(page_checkin_place + values[j].value);

                                        }



                                        if(page[i].name == 'page_posts_impressions_organic')

                                        {

                                            organic_page_post_imp = parseInt(organic_page_post_imp + values[j].value);

                                        }

                                        if(page[i].name == 'page_posts_impressions_organic_unique')
                                        {

                                            organic_unique_page_post_imp = parseInt(organic_unique_page_post_imp + values[j].value);

                                        }

                                    }

                                }

                            }

                        }

                        localStorage.setItem('organicPageImp', organic_page_imp);

                        localStorage.setItem('organicPagePostImp', organic_page_post_imp);

                        localStorage.setItem('organicUniquePagePostImp', organic_unique_page_post_imp);

                        localStorage.setItem('pageUserEngage', page_user_engage);

                        localStorage.setItem('pageCheckinPlace', page_checkin_place);

                        console.log('organic_page_imp', organic_page_imp);

                        console.log('organic_page_post_imp', organic_page_post_imp);

                        console.log('organic_unique_page_post_imp', organic_unique_page_post_imp);

                        console.log('page_user_engage', page_user_engage);

                        console.log('page_checkin_place', page_checkin_place);


                        FB.api(

                            pageId+"/insights?metric=page_fan_adds",

                            function (response) {

                                if (response && !response.error) {

                                    var str = '';

                                    var page_like = 0;

                                    if(!jQuery.isEmptyObject(response.data))
                                    {

                                        var page = response.data;

                                        for(var i in page)
                                        {

                                            str += '<div class="clearfix"></div><h2>'+ page[i].title+'</h2>';

                                            if(!jQuery.isEmptyObject(page[i].values))

                                            {

                                                var values = page[i].values;

                                                for(var j in values)

                                                {

                                                    if(page[i].name == 'page_fan_adds')

                                                    {

                                                        page_like = parseInt(page_like + values[j].value);

                                                    }

                                                }

                                            }

                                        }

                                    }

                                    localStorage.setItem('pageLike', page_like);

                                    console.log('page_like', page_like);

                                    // get old date pages records
                                    FB.api(

                                        pageId+"/insights?metric=page_impressions_organic, page_engaged_users, page_places_checkin_total, page_posts_impressions_organic, page_posts_impressions_organic_unique&fields=id, name, title, values&period=days_28",

                                        function (response) {

                                            if (response && !response.error) {

                                                var str = '';

                                                var organic_page_imp_old = organic_page_post_imp_old = organic_unique_page_post_imp_old = page_user_engage_old = page_checkin_place_old = page_like_old = 0;

                                                if(!jQuery.isEmptyObject(response.data))
                                                {

                                                    var page = response.data;

                                                    for(var i in page)
                                                    {
                                                        str += '<div class="clearfix"></div><h2>'+ page[i].title+'</h2>';

                                                        if(!jQuery.isEmptyObject(page[i].values))
                                                        {
                                                            var values = page[i].values;
                                                            for(var j in values)
                                                            {
                                                                if(page[i].name == 'page_impressions_organic')
                                                                {

                                                                    organic_page_imp_old = parseInt(organic_page_imp_old + values[j].value);

                                                                }



                                                                if(page[i].name == 'page_engaged_users')
                                                                {
                                                                    page_user_engage_old = parseInt(page_user_engage_old + values[j].value);

                                                                }

                                                                if(page[i].name == 'page_places_checkin_total')
                                                                {

                                                                    page_checkin_place_old = parseInt(page_checkin_place_old + values[j].value);

                                                                }


                                                                if(page[i].name == 'page_posts_impressions_organic')
                                                                {

                                                                    organic_page_post_imp_old = parseInt(organic_page_post_imp_old + values[j].value);

                                                                }

                                                                if(page[i].name == 'page_posts_impressions_organic_unique')

                                                                {

                                                                    organic_unique_page_post_imp_old = parseInt(organic_unique_page_post_imp_old + values[j].value);

                                                                }

                                                            }

                                                        }

                                                    }


                                                    jQuery('#organic_page_imp').html(localStorage.getItem('organicPageImp'));

                                                    jQuery('#organic_page_imp_old').html(organic_page_imp_old);

                                                    var opi = ( ( ( parseInt(localStorage.getItem('organicPageImp')) - organic_page_imp_old ) / parseInt(localStorage.getItem('organicPageImp')) ) * 100 ).toFixed(2);

                                                    if(opi != "NaN"){

                                                        if(opi < 0)

                                                        {

                                                            opi = '<i class="fa fa-caret-down" style="color:red"></i> '+ opi +'%';

                                                        }else{

                                                            opi = '<i class="fa fa-caret-up" style="color:green"></i> '+ opi +'%';

                                                        }

                                                    }else{

                                                        opi = '0%';

                                                    }

                                                    jQuery('#organic_page_imp_per').html(opi);

                                                    jQuery('#organic_page_post_imp').html(localStorage.getItem('organicPagePostImp'));

                                                    jQuery('#organic_page_post_imp_old').html(organic_page_post_imp_old);

                                                    var oppi = ( ( ( parseInt( localStorage.getItem('organicPagePostImp') ) - organic_page_post_imp_old ) / parseInt( localStorage.getItem('organicPagePostImp') ) ) * 100 ).toFixed(2);

                                                    if(oppi != "NaN"){

                                                        if(oppi < 0)

                                                        {

                                                            oppi = '<i class="fa fa-caret-down" style="color:red"></i> '+ oppi +'%';

                                                        }else{

                                                            oppi = '<i class="fa fa-caret-up" style="color:green"></i> '+ oppi +'%';

                                                        }

                                                    }else{

                                                        oppi = '0%';

                                                    }

                                                    jQuery('#organic_page_post_imp_per').html( oppi );

                                                    jQuery('#organic_unique_page_post_imp').html(localStorage.getItem('organicUniquePagePostImp'));

                                                    jQuery('#organic_unique_page_post_imp_old').html(organic_unique_page_post_imp_old);

                                                    var ouppi = ( ( ( parseInt( localStorage.getItem('organicUniquePagePostImp') ) - organic_unique_page_post_imp_old ) / parseInt( localStorage.getItem('organicUniquePagePostImp') ) ) * 100 ).toFixed(2);



                                                    if(ouppi != "NaN"){

                                                        if(ouppi < 0)

                                                        {

                                                            ouppi = '<i class="fa fa-caret-down" style="color:red"></i> '+ ouppi +'%';

                                                        }else{

                                                            ouppi = '<i class="fa fa-caret-up" style="color:green"></i> '+ ouppi +'%';

                                                        }

                                                    }else{

                                                        ouppi = '0%';

                                                    }

                                                    jQuery('#organic_unique_page_post_imp_per').html(ouppi);

                                                    jQuery('#page_user_engage').html(localStorage.getItem('pageUserEngage'));

                                                    jQuery('#page_user_engage_old').html(page_user_engage_old);

                                                    var pue = ( ( ( parseInt( localStorage.getItem('pageUserEngage') ) - page_user_engage_old ) / parseInt( localStorage.getItem('pageUserEngage') ) ) * 100 ).toFixed(2);

                                                    if(pue != "NaN"){

                                                        if(pue < 0)

                                                        {

                                                            pue = '<i class="fa fa-caret-down" style="color:red"></i> '+ pue +'%';

                                                        }else{

                                                            pue = '<i class="fa fa-caret-up" style="color:green"></i> '+ pue +'%';

                                                        }

                                                    }else{

                                                        pue = '0%';

                                                    }

                                                    jQuery('#page_user_engage_per').html(pue);


                                                    jQuery('#page_checkin_place').html(localStorage.getItem('pageCheckinPlace'));

                                                    jQuery('#page_checkin_place_old').html(page_checkin_place_old);

                                                    var pcp = ( ( ( parseInt( localStorage.getItem('pageCheckinPlace') ) - page_checkin_place_old ) / parseInt( localStorage.getItem('pageCheckinPlace') ) ) * 100 ).toFixed(2);

                                                    if(pcp != "NaN"){

                                                        if(pcp < 0)

                                                        {

                                                            pcp = '<i class="fa fa-caret-down" style="color:red"></i> '+ pcp +'%';

                                                        }else{

                                                            pcp = '<i class="fa fa-caret-up" style="color:green"></i> '+ pcp +'%';

                                                        }

                                                    }else{

                                                        pcp = '0%';

                                                    }

                                                    jQuery('#page_checkin_place_per').html( pcp );

                                                    // get old date pages like

                                                    FB.api(

                                                        pageId+"/insights?metric=page_fan_adds",

                                                        function (response) {

                                                            if (response && !response.error) {

                                                                var str = '';
                                                                var page_like_old = 0;
                                                                if(!jQuery.isEmptyObject(response.data))
                                                                {

                                                                    var page = response.data;

                                                                    for(var i in page)
                                                                    {

                                                                        str += '<div class="clearfix"></div><h2>'+ page[i].title+'</h2>';

                                                                        if(!jQuery.isEmptyObject(page[i].values))
                                                                        {

                                                                            var values = page[i].values;

                                                                            for(var j in values)
                                                                            {

                                                                                if(page[i].name == 'page_fan_adds')
                                                                                {

                                                                                    page_like_old = parseInt(page_like_old + values[j].value);

                                                                                }
                                                                            }

                                                                        }

                                                                    }

                                                                    jQuery('#page_like').html(localStorage.getItem('pageLike'));

                                                                    jQuery('#page_like_old').html(page_like_old);

                                                                    var pl = ( ( ( parseInt( localStorage.getItem('pageLike') ) - page_like_old ) / parseInt( localStorage.getItem('pageLike') ) ) * 100 ).toFixed(2);

                                                                    if(pl != "NaN"){

                                                                        if(pl < 0)

                                                                        {

                                                                            pl = '<i class="fa fa-caret-down" style="color:red"></i> '+ pl +'%';

                                                                        }else{

                                                                            pl = '<i class="fa fa-caret-up" style="color:green"></i> '+ pl +'%';

                                                                        }

                                                                    }else{

                                                                        pl = '0%';

                                                                    }

                                                                    jQuery('#page_like_per').html(pl);

                                                                    jQuery('div#impressionsRecords').removeClass('hide');

                                                                }

                                                            }

                                                            else{

                                                                console.log(response.error);

                                                            }

                                                            jQuery('img#loader').addClass('hide');

                                                        });

                                                }

                                            }else{

                                                console.log(response.error);

                                            }

                                        });

                                }

                                else{

                                    console.log(response.error);

                                }

                            });
                    }else{
                        console.log(response.error);
                    }

                });
        }

    </script

<?php get_footer();