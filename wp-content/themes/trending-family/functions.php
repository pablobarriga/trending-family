<?php
	/**
	 * trending-family functions and definitions
	 *
	 * @link https://developer.wordpress.org/themes/basics/theme-functions/
	 *
	 * @package trending-family
	 */
	if ( ! function_exists( 'trending_family_setup' ) ) :
		/**
		 * Sets up theme defaults and registers support for various WordPress features.
		 *
		 * Note that this function is hooked into the after_setup_theme hook, which
		 * runs before the init hook. The init hook is too late for some features, such
		 * as indicating support for post thumbnails.
		 */
		function trending_family_setup()
		{
			/*
			 * Make theme available for translation.
			 * Translations can be filed in the /languages/ directory.
			 * If you're building a theme based on trending-family, use a find and replace
			 * to change 'trending-family' to the name of your theme in all the template files.
			 */
			load_theme_textdomain( 'trending-family', get_template_directory() . '/languages' );
			// Add default posts and comments RSS feed links to head.
			add_theme_support( 'automatic-feed-links' );
			/*
			 * Let WordPress manage the document title.
			 * By adding theme support, we declare that this theme does not use a
			 * hard-coded <title> tag in the document head, and expect WordPress to
			 * provide it for us.
			 */
			add_theme_support( 'title-tag' );
			/*
			 * Enable support for custom logo.
			 *
			 */
			add_theme_support( 'custom-logo');
			/*
			 * Enable support for Post Thumbnails on posts and pages.
			 *
			 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
			 */
			add_theme_support( 'post-thumbnails' );
			// This theme uses wp_nav_menu() in one location.
			register_nav_menus( array(
				'menu-1' => esc_html__( 'Primary', 'trending-family' ),
				'menu-2' => esc_html__( 'Footer', 'trending-family' ),
			) );
			/*
			 * Switch default core markup for search form, comment form, and comments
			 * to output valid HTML5.
			 */
			add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			) );
			/*
			 * Set up the WordPress core custom background feature.
			 */
			add_theme_support( 'custom-background', apply_filters( 'trending_family_custom_background_args', array(
				'default-color' => 'ffffff',
				'default-image' => '',
			) ) );
			// Add theme support for selective refresh for widgets.
			add_theme_support( 'customize-selective-refresh-widgets' );
			// Remove unnecessary costumizer
			if(!function_exists('trending_family_remove_customizer_settings')) {
				function trending_family_remove_customizer_settings($wp_customize)
				{
					$wp_customize->remove_section('colors');
					$wp_customize->remove_section('background_image');
					$wp_customize->remove_section('header_image');
				}
				add_action('customize_register', 'trending_family_remove_customizer_settings', 20);
			}
		}
	endif;
	add_action( 'after_setup_theme', 'trending_family_setup' );
	/**
	 * Set the content width in pixels, based on the theme's design and stylesheet.
	 *
	 * Priority 0 to make it available to lower priority callbacks.
	 *
	 * @global int $content_width
	 */
	function trending_family_content_width() {
		$GLOBALS['content_width'] = apply_filters( 'trending_family_content_width', 640 );
	}
	add_action( 'after_setup_theme', 'trending_family_content_width', 0 );
	/**
	 * Register widget area.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
	 */
	function trending_family_widgets_init() {
		register_sidebar( array(
			'name'          => esc_html__( 'Top Sidebar', 'trending-family' ),
			'id'            => 'top-sidebar',
			'description'   => esc_html__( 'Add widgets for top section of sidebar here.', 'trending-family' ),
			'before_widget' => '<div id="%1$s" class="%2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4>',
			'after_title'   => '</h4>',
		) );
		register_sidebar( array(
			'name'          => esc_html__( 'Sidebar', 'trending-family' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'trending-family' ),
			'before_widget' => '<div id="%1$s" class="sidebar-element bg-blue widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4>',
			'after_title'   => '</h4>',
		) );
	}
	add_action( 'widgets_init', 'trending_family_widgets_init' );
	/**
	 * Implement the Custom Header feature.
	 */
	require get_template_directory() . '/includes/custom-header.php';
	/**
	 * Custom template tags for this theme.
	 */
	require get_template_directory() . '/includes/template-tags.php';
	/**
	 * Custom functions that act independently of the theme templates.
	 */
	require get_template_directory() . '/includes/extras.php';
	/**
	 * Customizer additions.
	 */
	require get_template_directory() . '/includes/customizer.php';
	/**
	 * Load Jetpack compatibility file.
	 */
	require get_template_directory() . '/includes/jetpack.php';
	/**
	 * Load files to hook into actions.
	 */
	require get_template_directory() . '/includes/trending-family-actions.php';
	/**
	 * Load files to enqueue static files
	 */
	require get_template_directory() . '/includes/static.php';
	/**
	 * Load files to enqueue helpers files
	 */
	require get_template_directory() . '/includes/helpers.php';
	/**
	 * Load files to hook into filters.
	 */
	require get_template_directory() . '/includes/trending-family-filters.php';
	/**
	 * Load files to hook into filters.
	 */
	require get_template_directory() . '/includes/trending-family-nav-walker.php';
	/**
	 * Load files to hook into filters.
	 */
	require get_template_directory() . '/includes/trending-family-post-types.php';
	/**
	 * Load Aqua Resizer.
	 */
	require get_template_directory() . '/includes/aq_resizer.php';
	/*
	 * Load PHPExcel Library
	 */
	require get_template_directory() . '/includes/libraries/PHPExcel/Classes/PHPExcel.php';
	
	/*
	 * Load Blog Post Option
	 */
	require get_template_directory() . '/includes/trending-family-blog-post-option.php';
	/*
	 * All the action hooks that were here are moved to /includes/trending-family-actions.php
	 * All the filter hooks that were here are moved to /includes/trending-family-filters.php
	 * Please ping me on Slack if you have any confusions.
	 * @author Prajwal Shrestha
	 */
	//katalformsbmt
	add_action('wp_ajax_tf_get_influencer_data', 'tf_get_influencer_data');
	add_action('wp_ajax_nopriv_tf_get_influencer_data', 'tf_get_influencer_data');
	function tf_get_influencer_data(){
		
		$userData = (array) get_user_meta($_REQUEST['userId']);
		
		if( $userData) {
			
			foreach($userData as $key => $val) {
				if(substr($key, 0, 10) != 'influencer') {
					unset($userData[$key]);
				}
			}
		}
		
		$data['name'] = $userData['influencer_influencer_first_name'][0] . ' ' . $userData['influencer_influencer_last_name'][0];
		$data['address'] = $userData['influencer_influencer_address'][0] . ' ' . $userData['influencer_influencer_city'][0] . ', ' . $userData['influencer_influencer_state'][0] . ' ' . $userData['influencer_influencer_zip'][0];
		
		if($userData['influencer_influencer_kids_name']) {
			$children = [];
			foreach($userData['influencer_influencer_kids_name'] as $key => $child) {
				$age = 'n/a';
				if($userData['influencer_influencer_kids_name'] && isset($userData['influencer_influencer_kids_dob'][$key])) {
					$age = (int) (( time() - strtotime($userData['influencer_influencer_kids_dob'][$key]) )/(60*60*24*365));
				}
				$children[] = $age;
			}
		}
		$data['children'] = $children ? implode(', ', $children) : 'n/a';
		
		if($userData['influencer_influencer_pets_name']) {
			$pets = [];
			$pet_descriptions = [];
			foreach($userData['influencer_influencer_pets_name'] as $key => $pet) {
				$description = '';
				if($userData['influencer_influencer_pets_name'] && isset($userData['influencer_influencer_pets_description'][$key])) {
					$description = $userData['influencer_influencer_pets_description'][$key];
				}
				$pets[] = $pet;
				
				if($description) {
					$pet_descriptions[] = $description;
				}
			}
		}
		$data['pets'] = $pets ? count($pets) . ' ' . implode(', ', $pet_descriptions) : 'n/a';
		
		//$data['suggested_rate'] = $userData['influencer_influencer_first_name'][0];
		$data['languages'] = isset($userData['influencer_influencer_languages']) ? implode(', ', $userData['influencer_influencer_languages']) : '';
		$data['brands'] = isset($userData['influencer_influencer_brands']) ? implode(', ', $userData['influencer_influencer_brands']) : '';
		
		die(json_encode($data));
	}
	
	//katalformsbmt
	add_action('wp_ajax_katalformsbmt', 'custom_katalformsbmt');
	add_action('wp_ajax_nopriv_katalformsbmt', 'custom_katalformsbmt');
	function custom_katalformsbmt(){
		if (isset($_POST['formdata'])) {
			$videoTitle = $_POST['formdata'];
			$userId = $_POST['userId'];
			$geoKey = $_POST['geoKey'];
			$videoData = get_user_meta( $userId, 'youtube-channels', TRUE );
			foreach ($videoData as $key => $value) {
				$videoGeographicsData = $value['videoGeographicsData'];
				$geographics = unserialize($videoGeographicsData);
				$countryCode = $geoViews = array();
				foreach ($geographics as $key => $value2) {
					if (trim(strtolower($videoTitle)) == trim(strtolower($value2['Title']))) {
						foreach ($value2['geographicsData'] as $key => $value3) {
							$countryCode[] = $value3[0];
							$geoViews[] = $value3[1];
						}
					}else{
					}
				}
				?>
                <script>
                    var data_t<?php echo $geoKey; ?> = {
                        labels: <?php echo json_encode($countryCode); ?>,
                        datasets: [{
                            label: "Views",
                            //fill: false,
                            backgroundColor: "rgba(101, 200, 181, 0.2)",
                            borderColor: "rgba(101, 200, 181, 0.9)",
                            //borderDash: [5, 5],
                            data: <?php echo json_encode($geoViews); ?>,
                        }],
                    };
                </script>
                <script type="text/javascript">
                    var ctx<?php echo $geoKey; ?> = document.getElementById('chart-t<?php echo $geoKey; ?>');
                    var myChart<?php echo $geoKey; ?> = new Chart(ctx<?php echo $geoKey; ?>, {
                        type: 'line',
                        data: data_t<?php echo $geoKey; ?>,
                    });
                </script>
				<?php
			}
		}
		die();
	}
//audienceRetention
	add_action('wp_ajax_audienceRetention', 'custom_audienceRetention');
	add_action('wp_ajax_nopriv_audienceRetention', 'custom_audienceRetention');
	function custom_audienceRetention(){
		if (isset($_POST['formdata2'])) {
			$videoTitle = $_POST['formdata2'];
			$userId = $_POST['userId2'];
			$audienceKey = $_POST['audienceKey'];
			$videoData = get_user_meta( $userId, 'youtube-channels', TRUE );
			foreach ($videoData as $key => $value) {
				$audienceRetentionData = $value['audienceRetention'];
				$audienceRetention = unserialize($audienceRetentionData);
				$audienceDate = $watchTime = $audienceViews = array();
				foreach ($audienceRetention as $key => $value2) {
					if (trim(strtolower($videoTitle)) == trim(strtolower($value2['Title']))) {
						foreach ($value2['audienceRetention'] as $key => $value3) {
							$audienceDate[] = $value3[0];
							$watchTime[] = $value3[1];
							$audienceViews[] = $value3[2];
						}
					}else{
					}
				}
				?>
                <script>
                    var data_t<?php echo $audienceKey; ?> = {
                        labels: <?php echo json_encode($audienceDate); ?>,
                        datasets: [{
                            label: "Watch Time in Seconds",
                            //fill: false,
                            backgroundColor: "rgba(48, 107, 151, 0.2)",
                            borderColor: "rgba(48, 107, 151, 0.9)",
                            data: <?php echo json_encode($watchTime); ?>,
                        }, {
                            label: "Views",
                            //fill: false,
                            backgroundColor: "rgba(101, 200, 181, 0.2)",
                            borderColor: "rgba(101, 200, 181, 0.9)",
                            //borderDash: [5, 5],
                            data: <?php echo json_encode($audienceViews); ?>,
                        }],
                    };
                </script>
                <script type="text/javascript">
                    var ctx<?php echo $audienceKey; ?> = document.getElementById('chart-t<?php echo $audienceKey; ?>');
                    var myChart<?php echo $audienceKey; ?> = new Chart(ctx<?php echo $audienceKey; ?>, {
                        type: 'line',
                        data: data_t<?php echo $audienceKey; ?>,
                    });
                </script>
				<?php
			}
		}
		die();
	}
	function secToHR($seconds) { 
		$hours = floor($seconds / 3600);
		$minutes = floor(($seconds / 60) % 60);
		$seconds = $seconds % 60;
		return (int)$minutes.' Minutes '.$seconds.' Second';
	}
	
