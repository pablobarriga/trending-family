<?php
	/*
	 * Single Campaign Page
	 */
	global $current_user;
	/*
	 * Don't comment the snippet below
	 * As this page is only accessible to users with role Client
	 * @author Prajwal Shrestha
	 */
	wp_get_current_user();
	if( !in_array( 'client', $current_user->roles )) {
		global $wp_query;
		$wp_query->set_404();
		status_header( 404 );
		get_template_part( 404 );
		exit();
	}
	get_header();
	include_once get_template_directory(). "/includes/config.php"; ?>
    <script async>
        var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 30);
        };
        var ColorRed = "rgba(255, 100, 76, 0.9)";
        var ColorBlue = "rgba(48, 107, 151, 0.9)";
        var ColorGreen = "rgba(101, 200, 181, 0.9)";
    </script>
    <div id="fb-root"></div>
    <script async>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=643720905835870";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <section class="section bg-green pad-md text-dark text-center-xs">
        <div class="container">
            <div class="row row-lg">
                <div class="flex-center">
                    <div class="col-xs-12 p-title">
                        <h1><?php the_title(); ?>
                            <img src="<?php echo esc_url( get_wp_user_avatar_src( $current_user->ID, 'large' ) ); ?>" alt="<?php echo esc_attr( $current_user->display_name ); ?>">
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
	/*
	* If the Campaign is live Load content-single-campaign-live.php
	*/
	$campaign_status = get_post_status( $post->ID );
	get_template_part( 'template-parts/content', 'single-campaign' );
?>
    <section class="section bg-green text-center">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="btn-group">
                        <a href="javascript:void(0);" class="btn btn-default btn-lg st2" id="campaign-download-pdf"><?php echo esc_html__('Download PDF', 'trending-family'); ?></a>
                        <a href="javascript:void(0);" class="btn btn-default btn-lg st2" id="campaign-export-excel"><?php echo esc_html__('Export to Excel', 'trending-family'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer();