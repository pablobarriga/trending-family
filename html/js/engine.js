(function($){

    // Mobile Service Carousel
    $('body').on('click', '.top-subheader', function(){
        $(this).closest('.feature-carousel').toggleClass('mobile-cont-active');
    });

    // Mobile Ber Button
    $('.btn-mobile-bar').click(function(event) {
        if ($("body").hasClass('mobile-bar-active')) {
            $("body").removeClass('mobile-bar-active');
        } else {
            $("body").addClass('mobile-bar-active');
        }
    });
    $(".mobile-bar").on("swipeleft", function(){
      $("body").removeClass('mobile-bar-active');
    });

    // Mobile Service Carousel
    $('body').on('click', '.btn-login-popup', function(event){
        event.preventDefault();
        if ($("body").hasClass('login-popup-active')) {
            $("body").removeClass('login-popup-active');
        } else {
            $("body").addClass('login-popup-active');
        }
    });

     // Button collapse section
    $('.section').on('click', '.btn-collapse-section', function(event){
        event.preventDefault();
        $(this).closest('.section').toggleClass('collapse-section');
    });

    // Open login for influencer pannels
    $(".social-option-box").on("click", ".btn-add-social", function(event) {
        event.preventDefault();
        $(this).toggleClass("active");
        $(this).closest('.social-option-box').find(".login-popup-box-wrap").toggle();
    })

    // Custom Select
    //$('body').on('click', '.cst-select-lg li', function(){
        //$(this).closest('.cst-select-lg').find('.btn').text('dfsdf');
    //});

    // Counter animation for counter box  ----------------------------------------------
    if ($(".cirle-num .num").length > 0) {
        var fadeInRankImg = new Waypoint({
            element: $('.cirle-num .num'),
            offset: '100%',
            handler: function() {
                console.log('1');
                $.each($('.cirle-num .num'), function(i,e) {
                    $(e).counterUp({
                        delay: 10,
                        time: 1500
                    });
                });
                this.destroy();
                return false;
            }
        });
    }

    // Home Page Main carousel
    $('.main-carousel').owlCarousel({
        items:3,
        loop:true,
        center: false,
        margin:0,
        nav: false,
        dots: true,
        smartSpeed: 550,
        autoplay:true,
        autoplayTimeout:90000,
        autoplayHoverPause:false,
        responsive:{
            0:{
                items:1,
                nav: false,
                dots: true,
            },
            768:{
                items:2,
                nav: false,
                dots: true,
            },
            998:{
                items:3,
                center: true,
                nav: true,
                dots: false,
            }
        }
    });

    // Home Page Blog posts carousel
    $('.post-carousel').owlCarousel({
        items:3,
        loop:true,
        center: false,
        margin:30,
        nav: false,
        dots: true,
        smartSpeed: 350,
        autoplay:true,
        autoplayTimeout:9000,
        autoplayHoverPause:false,
        responsive:{
            0:{
                items:1,
            },
            520:{
                items:2,
            },
            780:{
                items:3,
            },
            1020:{
                nav: true,
            }
        }
    });

    // Home Page - Brands carousel
    $('.brand-carousel').owlCarousel({
        items:7,
        loop:true,
        center: false,
        margin:20,
        nav: false,
        dots: true,
        autoplay:true,
        autoplayTimeout:9000,
        autoplayHoverPause:false,
        responsive:{
            0:{
                items:3,
            },
            520:{
                items:4,
            },
            780:{
                items:4,
            },
            980:{
                items:6,
            },
            1020:{
                nav: true,
            },
            1100:{
                items:7,
                nav: true,
            },
        }
    });

    // Data Carousel
    var $dataCarousel = $('.data-carousel');
    if($dataCarousel.length) {
        $dataCarousel.each(function() {
            var $thisCarousel = $(this);
            var progressItemIndex = $thisCarousel.find('.mark-progress').index();
            $thisCarousel.owlCarousel({
                items:5,
                loop:false,
                center: false,
                margin:2,
                nav: false,
                dots: false,
                autoplay:false,
                autoplayTimeout:9000,
                autoplayHoverPause:false,
                responsive:{
                    0:{
                        items:1,
                        startPosition : progressItemIndex,
                        onResized: function(event) {
                            var elementNumber = $(event.target).find('.mark-progress').closest('.owl-item').index();
                            $(event.target).trigger('to.owl.carousel', elementNumber);
                        },
                    },
                    520:{
                        items:2,
                        startPosition : progressItemIndex,
                        onResized: function(event) {
                            var elementNumber = $(event.target).find('.mark-progress').closest('.owl-item').index();
                            $(event.target).trigger('to.owl.carousel', elementNumber);
                        },
                    },
                    768:{
                        items:3,
                        nav: true,
                        startPosition : 0,
                        onResized: function(event) {
                            $(event.target).trigger('to.owl.carousel', 0);
                        },
                    },
                    1000:{
                        items:4,
                        nav: true,
                        startPosition : 0,
                        onResized: function(event) {
                            $(event.target).trigger('to.owl.carousel', 0);
                        },
                    },
                    1200:{
                        items:5,
                        nav: true,
                        startPosition : 0,
                        onResized: function(event) {
                            $(event.target).trigger('to.owl.carousel', 0);
                        },
                    },
                }
            });
        });
    }

    // Home Page - Testimonials carousel
    $('.owl-carousel-single').owlCarousel({
        items:1,
        loop:true,
        center: false,
        margin:20,
        nav: false,
        dots: true,
        smartSpeed: 350,
        autoplay:true,
        autoplayTimeout:9000,
        autoplayHoverPause:false,
    });

    // Home Page - influencers carousel
    jQuery(function() {
        function influencersCarousel() {
          var checkWidth = jQuery(window).width();
          var owlPost = jQuery(".influencers-carousel");
          if (checkWidth >= 768) {
            if(typeof owlPost.data('owl.carousel') != 'undefined'){
              owlPost.data('owl.carousel').destroy(); 
            }
            owlPost.removeClass('owl-carousel');
          } else if (checkWidth < 768) {
            owlPost.addClass('owl-carousel');
            owlPost.owlCarousel({
              loop:true,
              margin: 10,
              nav: true,
              slideSpeed : 500,
              animateOut: 'fadeOut',
              touchDrag: true,
              mouseDrag: true,
              autoplay: false,
              autoplaySpeed: 8000,
              autoplayTimeout: 8000,
              dots: false,
              // items: 1
              responsive:{
                0:{
                    items:1
                },
                480:{
                    items:2
                },
                650:{
                    items:3
                }
              }
            });
          }
        }

        influencersCarousel();
        jQuery(window).resize(influencersCarousel); 
    });

    // Popup Link with youtube iframe
    $('.popup-video').magnificPopup({
        //disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });

    $('.popup-box-link').magnificPopup({
        type: 'inline',
        preloader: false,
    });

    // Service Carousel 1
    $('.feature-carousel-1').owlCarousel({
        items:1,
        loop:false,
        center: false,
        margin:20,
        nav: false,
        dots: true,
        autoplay:false,
        dotsData: true,
        mouseDrag: false,
        touchDrag: true,
        autoplayTimeout:9000,
        autoplayHoverPause:false,
        dotsContainer: '.feature-controls-dots-1',
        responsive:{
            0:{
                dots: false,
                nav: true,
            },
            768:{
                dots: true,
            },
        }
    });

    // Service Carousel 2
    $('.feature-carousel-2').owlCarousel({
        items:1,
        loop:false,
        center: false,
        margin:20,
        nav: false,
        dots: true,
        autoplay:false,
        dotsData: true,
        autoplayTimeout:9000,
        mouseDrag: false,
        touchDrag: true,
        autoplayHoverPause:false,
        dotsContainer: '.feature-controls-dots-2',
        responsive:{
            0:{
                dots: false,
                nav: true,
            },
            768:{
                dots: true,
            },
        }
    });

    // Carousel with Image inamation
    $('.animation-carousel').owlCarousel({
        items: 3,
        margin: 5,
        dots: true,
        nav: false,
        loop: true,
        paginationSpeed : 600,
        navSpeed: 600,
        slideSpeed: 600,
        center:true,
        mouseDrag: true,
        touchDrag: true,
        // autoHeight: true,
        autoplay:true,
        autoplayTimeout: 6000,
        responsive:{
            0:{
                items:1,
                nav: false,
            },
            768:{
                items:3,
                nav: false,
            },
            1120:{
                items:3,
                nav: true,
            },
        },
    });


    // Simple Carousel
    $('.simple-slider').owlCarousel({
        items:1,
        loop:true,
        center: false,
        margin:20,
        nav: false,
        dots: false,
        autoplay:true,
        dotsData: false,
        autoplayTimeout:9000,
        autoplayHoverPause:false,
    });


    // Commented by Prajwal as different script is implemented for the page scroll on nav menu click
    // Scroll to anchor ID by click
    // $('li.anchor-link > a').click(function(event) {
    //     if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
    //         var target = $(this.hash);
    //         target = target.length ? target : $('#' + this.hash.slice(1) +'');
    //         if (target.length) {
    //             $('html,body').stop(false, true).animate({
    //                 scrollTop: target.offset().top - 35
    //             }, 500);
    //             event.preventDefault();
    //         }
    //     }
    // });


// feed-grid on Client Compaign page
    // $('.feed-grid').packery({
    //   // options
    //   itemSelector: '.feed-item',
    //   gutter: 25
    // });

// Popup Edit profile
    $('.disabled-form').find('.form-control').attr('disabled', true);
    $('body').on('click', ".disable-form-wrapper .btn-disable-form", function(event) {
        event.preventDefault();
        var parenFormWrap = $(this).closest('.disable-form-wrapper');
        if(parenFormWrap.hasClass('disabled-form')) {
            // if form is unavailable for edit
            parenFormWrap.removeClass('disabled-form').find('.form-control').prop("disabled", false);
            $(this).text('Save Changes');
        } else {
            // if form is available for edit
            parenFormWrap.addClass('disabled-form').find('.form-control').prop("disabled", true);
            $(this).text('Edit');
        }
    });

    // Datapicker
    $('.datepicker-input').datepicker({
        orientation: "bottom auto"
    });


})(jQuery);





/*!
* jquery.counterup.js 1.0
*
* Copyright 2013, Benjamin Intal http://gambit.ph @bfintal
* Released under the GPL v2 License
*
* Date: Nov 26, 2013
*/
(function( $ ){
  "use strict";
  $.fn.counterUp = function( options ) {

    // Defaults
    var settings = $.extend({
        'time': 400,
        'delay': 10
    }, options);

    return this.each(function(){

        // Store the object
        var $this = $(this);
        var $settings = settings;

        var counterUpper = function() {
            var nums = [];
            var divisions = $settings.time / $settings.delay;
            var num = $this.text();
            var isComma = /[0-9]+,[0-9]+/.test(num);
            num = num.replace(/,/g, '');
            var isInt = /^[0-9]+$/.test(num);
            var isFloat = /^[0-9]+\.[0-9]+$/.test(num);
            var decimalPlaces = isFloat ? (num.split('.')[1] || []).length : 0;

            // Generate list of incremental numbers to display
            for (var i = divisions; i >= 1; i--) {

                // Preserve as int if input was int
                var newNum = parseInt(num / divisions * i);

                // Preserve float if input was float
                if (isFloat) {
                    newNum = parseFloat(num / divisions * i).toFixed(decimalPlaces);
                }

                // Preserve commas if input had commas
                if (isComma) {
                    while (/(\d+)(\d{3})/.test(newNum.toString())) {
                        newNum = newNum.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                    }
                }

                nums.unshift(newNum);
            }

            $this.data('counterup-nums', nums);
            $this.text('0');

            // Updates the number until we're done
            var f = function() {
                $this.text($this.data('counterup-nums').shift());
                if ($this.data('counterup-nums').length) {
                    setTimeout($this.data('counterup-func'), $settings.delay);
                } else {
                    delete $this.data('counterup-nums');
                    $this.data('counterup-nums', null);
                    $this.data('counterup-func', null);
                }
            };
            $this.data('counterup-func', f);

            // Start the count up
            setTimeout($this.data('counterup-func'), $settings.delay);
        };

        // Perform counts when the element gets into view
        //$this.waypoint(counterUpper, { offset: '100%', triggerOnce: true });
        counterUpper();

    });
  };

})( jQuery );





